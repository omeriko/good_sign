import jQuery from "jquery";
import SignaturePositioningTool from "./SignaturePositioningTool";
import Tooltips from "./Tooltips";
import GoodSignBase from "./GoodSignBase";
import StepOne from "./steps/StepOne";
import StepTwo from "./steps/StepTwo";
import StepFilesUpload from "./steps/StepFilesUpload";
import StepThree from "./steps/StepThree";
import StepFour from "./steps/StepFour";
import StepFive from "./steps/StepFive";
import StepNetworkUnavailableError from "./steps/StepNetworkUnavailableError";
import PayloadForServer from "./PayloadForServer";

class App extends GoodSignBase {
    constructor() {   
        super();     
        this.current_stage = 0;        
        this.DEBUG_EMULATE_SERVER = false;
        this.DEBUG_JUMP_DIRECTLY_TO_STAGE = 1;        
        this.DEBUG_CLEAR_CONSOLE_ON_START= false;
        this.DEBUG_AUTOFILL_TEXTS_AND_CBXS = false; 
        
        this.isMobile = this.is_mobile();
        this.ENV = "prod"; 
        this.business_name = "";
        this.business_email_address = "";
        this.business_ip_address = "UNKNOWN";
        this.multiple_endusers = false;
        this.enduser_fullname = "";
        this.end_user_contact_value = "";
        this.additional_message = "";
        this.is_new_import = true;
        this.business_self_sign = false;
        this.is_demo = false;
        this.review_doc_course = false;
        
        this.end_user_secret_code = { enabled: false, value: "", type: "", source: "" };
        this.reusable_doc = { value: false, require_enduser_details: false };
        this.server_token = ""; 
        this.journey_id="";
        this.doc_name = "";
        this.embedded_images = { exists: false, title: "", items:[] }; 
        
        this.data_images = null;
        this.mobile_scenario_all_placeholders = null;
       
        this.IS_ONLINE = false;         
        this.language = this.get_language();
        
        this.hide_legal_terms = false;        
        this.set_data_from_hidden_fields();
        this.set_data_from_querystring();
        this.tooltip_util = Tooltips.instance;  
        
        this.url_to_server_generated_pdf = "";
        this.override_debug_behavior();
        this.prepare_head();
        this.append_additional_markup_to_body();        
        this.remove_paragraph_margin_bottom();        
        this.data_images = this.set_data_images_class_names_and_sizes();        
        this.wrap_data_images_with_containers();       
        this.increase_height_to_last_page();

        this.left_btn_action = document.body.querySelector("#btn-action");
        this.approve_agreement_container = document.querySelector("#approve-terms-container");        
        this.txt_i_accept = this.approve_agreement_container.querySelector("#txt-i-accept");        

        this.step_one = new StepOne(
            this,
            this.ENV,
            this.is_demo, 
            this.isMobile, 
            this.language,
            this.business_self_sign,
            this.review_doc_course,
            this.enduser_fullname, 
            this.business_name, 
            this.hide_legal_terms, 
            this.end_user_secret_code, 
            this.reusable_doc);
        
        
        if (navigator.onLine === true) {            
            this.IS_ONLINE = true;
        } else {
            this.IS_ONLINE = false;
            this.handle_go_to_stage_one(); 
            this.handle_go_to_stage_network_unavailable_err(); 
            return;          
        } 
        
        this.step_two = new StepTwo(
            this,
            this.ENV,
            this.is_demo, 
            this.isMobile, 
            this.language,
            this.multiple_endusers,            
            this.end_user_contact_value,
            this.tooltip_util,
            this.journey_id,
            this.server_token,
            this.business_name,
            this.business_email_address,
            this.business_ip_address,
            this.doc_name,
            this.enduser_fullname, 
            this.business_self_sign,
            this.review_doc_course,
            this.embedded_images.exists,
            this.DEBUG_EMULATE_SERVER);

        this.step_two.pre_render(this.DEBUG_AUTOFILL_TEXTS_AND_CBXS);    
        
        this.step_three = new StepThree(
            this.ENV, 
            this,
            this.is_demo, 
            this.isMobile, 
            this.language,
            this.multiple_endusers,
            this.business_self_sign, 
            this.business_name,
            this.embedded_images.exists,
            this.DEBUG_EMULATE_SERVER); 
            
        this.step_files_upload = new StepFilesUpload(
            this,
            this.ENV, 
            this.is_demo, 
            this.isMobile, 
            this.language,
            this.embedded_images,
            this.journey_id,
            this.enduser_fullname, 
            this.business_name,
            this.reusable_doc,
            this.DEBUG_EMULATE_SERVER); 
            
        this.step_four = new StepFour(
            this.ENV,            
            this.is_demo, 
            this.isMobile, 
            this.language,
            this.business_name,
            this.additional_message);

        this.step_five = new StepFive(
            this.ENV,            
            this.is_demo, 
            this.isMobile, 
            this.language,
            this.business_name,
            this.business_self_sign,
            this.review_doc_course);
        
        this.bind_events.call(this);
    }

    initialize () {

        switch(this.DEBUG_JUMP_DIRECTLY_TO_STAGE) {
            case 1: 
                this.handle_go_to_stage_one(); 
                break;
            case 2: 
                this.left_btn_action.classList.remove("partial-disabled");
                this.handle_go_to_stage_one(); 
                this.handle_go_to_stage_two({review_doc: false}); 
                break;            
            case 2.5: 
                this.left_btn_action.classList.remove("partial-disabled");    
                this.handle_go_to_stage_one();   
                this.handle_go_to_stage_embedded_images_upload();
                break;
            case 3:                 
                this.handle_go_to_stage_one();                
                this.handle_go_to_stage_three(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;           
            case 4: 
                this.handle_go_to_stage_one();                
                this.handle_go_to_stage_four(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;    
            case 5: 
                this.handle_go_to_stage_one();                
                this.handle_go_to_stage_five(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;            
            case 6:  //disconnected step 
                this.handle_go_to_stage_one();    
                this.handle_go_to_stage_network_unavailable_err();                         
                break;                 
            default: 
                this.handle_go_to_stage_one();
        }
        this.step_one.remove_style_shel_omer3();
        this.DEBUG_CLEAR_CONSOLE_ON_START && setTimeout( () => console.clear(), 1000);                                
    } 

    notify_enduser_details_changed(step_zero_payload) {
        this.step_two.set_enduser_details(step_zero_payload);
    }

    //Hello and Welcome stage
    handle_go_to_stage_one () {               
        this.current_stage = 1; 
        this.step_one.render();        
    }    
   
    //Signing and filling the document palceholders
    handle_go_to_stage_two(options) {
        this.current_stage = 2;
        this.step_two.render(options.review_doc);        
    }

    /* User has 2 options:
        1. click "Send" for Remote Session or "Generate File" for Self Sign session.
        2. click "Back to doc"*/
    handle_go_to_stage_three() {        
        this.current_stage = 3;
        this.step_three.render();        
    }

    handle_go_to_stage_embedded_images_upload() {        
        this.current_stage = 2.5;
        this.step_files_upload.render();
    }

    /* Great message, Ajax request has sent all PDF 
       assets to the server successfully and received a positive response */
    handle_go_to_stage_four (url) {
        this.url_to_server_generated_pdf = url;       
        if(this.business_self_sign == false) {            
            this.current_stage = 4;
            this.step_four.render();                        
        } else {
            this.current_stage = 5;
            this.step_five.render();
        }
    }

    /*Thankyou and goodbye + open document button*/
    handle_go_to_stage_five(url) {        
        if(this.review_doc_course === true) {
            this.url_to_server_generated_pdf = url;    
        }
        this.current_stage = 5;
        this.step_five.render();
    }       

    /*UI when Network Unavailable Error*/
    handle_go_to_stage_network_unavailable_err() {        
        this.current_stage = 6;
        new StepNetworkUnavailableError(this.ENV, this.is_demo, this.isMobile, this.language).render();
    }       

    open_pdf_in_new_tab() {       
       window.open(this.url_to_server_generated_pdf, '_blank');       
    }  
   
    handle_action_click() {
        switch (this.current_stage) {
            case 1:                
                if (this.left_btn_action.classList.contains("partial-disabled") === false) {
                    this.handle_go_to_stage_two({review_doc: false});
                }
                break;
            case 2:
                if(this.review_doc_course === false) { 
                    this.step_two.handle_btn_next_click(false);
                } else {
                    this.step_two.handle_review_doc_approval_click();
                }
                break;
            case 2.5: 
                this.append_embedded_images_to_payload();
                break;
            case 3:      
                if (this.left_btn_action.classList.contains("partial-disabled") === false) {
                    this.step_three.handle_post_data_to_server();
                } else if(this.tooltip_util.has_tooltip(this.left_btn_action) === false){
                    this.tooltip_util.attach_approve_read_doc_to_element(this.left_btn_action);
                }
                break;
            case 4:                                 
                this.handle_go_to_stage_five();            
                break;
            case 5:
                this.open_pdf_in_new_tab();
                break; 
            default: break;
        }

        return false;
    } 
    
    append_embedded_images_to_payload() {
        PayloadForServer.instance.fill_payload_with_embedded_images(this.embedded_images.items);
        this.step_files_upload.unrender();             
        this.handle_go_to_stage_three();
    }
    
    bind_events() {
        this.left_btn_action.addEventListener("click", this.handle_action_click.bind(this), false);

        this.left_btn_action.addEventListener("mouseenter", e => {            
            if (e.target.classList.contains("partial-disabled") && this.tooltip_util.has_tooltip(e.target) === false) {
                if(this.current_stage === 1) {
                    this.tooltip_util.attach_approve_agreement_to_element(e.target);
                } else if(this.current_stage === 3) {
                    this.tooltip_util.attach_approve_read_doc_to_element(e.target);
                }
            }
        }, false);

        this.left_btn_action.addEventListener('mouseleave', e => {
             if(e.target.classList.contains("partial-disabled") && this.tooltip_util.has_tooltip(e.target)) {
                 this.tooltip_util.remove_tooltip(e.target);
             }
        }, false);

        // this.approve_agreement_container.querySelectorAll(".cbx-inner").forEach(item => {
        //     item.addEventListener("click", e => {
        //         this.handle_approve_agreement_click(e.target);                
        //     });
        // });

        this.approve_agreement_container.querySelector(".cbx-inner.fa-square")
        .addEventListener("click", e => {
            console.log("about to call .cbx-inner.fa-square click handler");
            this.handle_approve_agreement_click(e.target);                
         });

        this.approve_agreement_container.querySelector(".cbx-inner.fa-check-square")
        .addEventListener("click", e => {
            console.log("about to call .cbx-inner.fa-check-square click handler");
            this.handle_approve_agreement_click(e.target);                
         });

        this.txt_i_accept.addEventListener('click', e => {
            this.handle_approve_agreement_click(e.target);
        }); 

        window.addEventListener('offline', () => { 
            this.handle_go_to_stage_network_unavailable_err(); 
        });
        
        window.addEventListener('online', () => { 
            this.initialize();
        });
    }

    handle_approve_agreement_click(ele_trigger) {
        if(ele_trigger.parentNode.dataset.isDirty === "false") {

            ele_trigger.parentNode.dataset.isDirty = "true"; 
            ele_trigger.parentNode.querySelector(".unchecked").style.display = "none";
            jQuery(ele_trigger.parentNode.querySelector(".checked")).fadeIn(250);
            this.left_btn_action.classList.remove("partial-disabled");

        } else if (ele_trigger.parentNode.dataset.isDirty === "true"){
            
            ele_trigger.parentNode.dataset.isDirty = "false";                
            jQuery(ele_trigger.parentNode.querySelector(".checked")).fadeOut(100, function() {
                this.parentNode.querySelector(".unchecked").style.display = "block"    
            });
            this.left_btn_action.classList.add("partial-disabled");
        }
    } 

    get_enduser_name() {
        let result = "";
        if(this.multiple_endusers === false) {
            var hidden_customer_name = document.querySelector("#customer-fullname");
            if (hidden_customer_name !== null) {
                result = hidden_customer_name.value.trim();
            }
        } else {
            var cookie = document.cookie.split('; ').find(item => { 
                if(item.startsWith('end_user_name=')) {                
                   return true;
                }
            });        
            
            if(cookie !== undefined){
                result = decodeURIComponent(cookie.split("=")[1]);
            } else {
                result = "ריטה גולדמן";
            }
        }

        return result;
    }

    get_enduser_contact_value() {
        let result = "";
        
        if(this.multiple_endusers === true) {
            var cookie = document.cookie.split('; ').find(item => { 
                if(item.startsWith('end_user_contact_value=')) {                
                   return true;
                }
            }); 
            if(cookie !== undefined){
                result = decodeURIComponent(cookie.split("=")[1]);    
            } else {
                result = "rita.goldmannn@gmail.com";
            }
        } 
        return result;
    }

    get_journey_id() {
        let result = "";
        if(this.multiple_endusers === false) {
            let hidden_journey_id = document.querySelector("#journey-id");
            if (hidden_journey_id !== null) {	
                result = hidden_journey_id.value;	
            }
        } else {
            let hidden_doc_id = document.querySelector("#encrypted-doc-id");
            if (hidden_doc_id !== null) {	
                result = hidden_doc_id.value;	
            }
        }

        return result;
    }
    
    set_data_from_hidden_fields () {        
        
        let hidden_business_address = document.querySelector("#mail-to-address");
        if (hidden_business_address !== null) {
            this.business_email_address = hidden_business_address.value;
        }

        let hidden_business_ip_address = document.querySelector("#customer-ip-address");
        if (hidden_business_ip_address !== null) {
            this.business_ip_address = hidden_business_ip_address.value;
        }

        let hidden_business_name = document.querySelector("#mail-to-name");
        if (hidden_business_name !== null) {
            this.business_name = hidden_business_name.value;            
        }

        let hidden_multiple_enduser = document.querySelector("#doc-process-type");
        if (hidden_multiple_enduser !== null) {	            
            const which = hidden_multiple_enduser.value.trim().toLowerCase();
            this.multiple_endusers = which === "parallel" ? true : false;            
        }

        this.enduser_fullname = this.get_enduser_name();
        this.end_user_contact_value = this.get_enduser_contact_value();

        let hidden_additional_message = document.querySelector("#additional-message");
        if (hidden_additional_message !== null) {
            this.additional_message = hidden_additional_message.value;
        }
        
        let hidden_business_self_sign = document.querySelector("#business-self-sign");
        if (hidden_business_self_sign !== null) {
            this.business_self_sign = hidden_business_self_sign.value === "true";
        }

        let hidden_token = document.querySelector("#token");	
        if (hidden_token !== null) {	
            this.server_token = hidden_token.value;	
        }

        let hidden_doc_name = document.querySelector("#doc_name");
        if (hidden_doc_name !== null) {
            this.doc_name = hidden_doc_name.value;
        }
        
        this.journey_id = this.get_journey_id();        

        let hidden_secret = document.querySelector("#end-user-code");
        if (hidden_secret !== null && hidden_secret.value.trim() !== "") {
            this.end_user_secret_code.enabled = true;
            this.end_user_secret_code.value = hidden_secret.value.trim();
            this.end_user_secret_code.type = hidden_secret.getAttribute("value-type");
            this.end_user_secret_code.source = hidden_secret.getAttribute("value-source");
            hidden_secret.parentNode.removeChild(hidden_secret);
        }
        
        let hidden_reusable_doc = document.querySelector("#reusable-doc");
        if (hidden_reusable_doc !== null) {	            
            this.reusable_doc.value = hidden_reusable_doc.value.trim() === "true";
            this.reusable_doc.require_enduser_details = hidden_reusable_doc.getAttribute("require-enduser-details") === "true";            
        }  
        
        let hidden_is_demo = document.querySelector("#is-demo");
        if (hidden_is_demo !== null) {	            
            this.is_demo = hidden_is_demo.value.toLowerCase() === "true";
        }  

        let hidden_review_doc_course = document.querySelector("#review-doc-course");
        if (hidden_review_doc_course !== null) {	            
            this.review_doc_course = hidden_review_doc_course.value.toLowerCase() === "true";
            if(this.review_doc_course === true) {
                this.additional_message = "";
            }
        }
        
        let hidden_is_new_import = document.querySelector("#is-new-import");
        if (hidden_is_new_import !== null) {	            
            this.is_new_import = true;            
        }
                        
        let hidden_embedded_image1 = document.querySelector("#embedded-image1");
        if (hidden_embedded_image1 !== null) {	            
            
            if(hidden_embedded_image1.getAttribute("item-name") !== "") {
                this.embedded_images.title = hidden_embedded_image1.value;
                this.embedded_images.exists = true;
                this.embedded_images.items.push({ name: hidden_embedded_image1.getAttribute("item-name"), token: "", dt: null, file_name: "", file_size: "", 
                required: hidden_embedded_image1.getAttribute("required") === "true" } );
            }
            
        }
        
        let hidden_embedded_image2 = document.querySelector("#embedded-image2");
        if (hidden_embedded_image2 !== null) {
            if(hidden_embedded_image2.getAttribute("item-name") !== "") {                
                this.embedded_images.items.push({ name: hidden_embedded_image2.getAttribute("item-name"), token: "", dt: null, file_name: "", file_size: "", 
                required: hidden_embedded_image2.getAttribute("required") === "true" } );
            }
            
        }

        let hidden_embedded_image3 = document.querySelector("#embedded-image3");
        if (hidden_embedded_image3 !== null) {
            if(hidden_embedded_image3.getAttribute("item-name") !== "") {                
                this.embedded_images.items.push({ name: hidden_embedded_image3.getAttribute("item-name"), token: "", dt: null, file_name: "", file_size: "", 
                required: hidden_embedded_image3.getAttribute("required") === "true" } );
            }            
        }

        let hidden_embedded_image4 = document.querySelector("#embedded-image4");
        if (hidden_embedded_image4 !== null) {
            if(hidden_embedded_image4.getAttribute("item-name") !== "") {                
                this.embedded_images.items.push({ name: hidden_embedded_image4.getAttribute("item-name"), token: "", dt: null, file_name: "", file_size: "", 
                required: hidden_embedded_image4.getAttribute("required") === "true"} );
            }            
        }

        let hidden_embedded_image5 = document.querySelector("#embedded-image5");
        if (hidden_embedded_image5 !== null) {
            if(hidden_embedded_image5.getAttribute("item-name") !== "") {                
                this.embedded_images.items.push({ name: hidden_embedded_image5.getAttribute("item-name"), token: "", dt: null, file_name: "", file_size: "", 
                required: hidden_embedded_image5.getAttribute("required") === "true"} );
            }            
        }
    }

    set_data_from_querystring() {
        const urlParams = new URLSearchParams(window.location.search);        
        const hide_legal_terms = urlParams.get('hide-legal-terms');
        if(hide_legal_terms !== null && hide_legal_terms !== '') {
            if(hide_legal_terms.toLowerCase().trim() === "true") {
                this.hide_legal_terms = true;
            }
        }
    }

    override_debug_behavior () {
        var debug_env = document.querySelector("#debug-env");
        if(debug_env !== null) {
            this.ENV = debug_env.value.toLowerCase().trim();
            console.log(`working with env: ${this.ENV}`);
        }

        var debug_jump_to_ele = document.querySelector("#debug-jump-directly-to-page");
        if(debug_jump_to_ele !== null) {
            this.DEBUG_JUMP_DIRECTLY_TO_STAGE = Number(debug_jump_to_ele.value.trim());
        }
        
        var debug_emulate_server = document.querySelector('#debug-emulate-server');
        if(debug_emulate_server !== null) {
            this.DEBUG_EMULATE_SERVER = debug_emulate_server.value.toLowerCase().trim() === 'true';
        }
        var debug_autofill_texts_and_cbxs = document.querySelector('#debug-autofill-texts-and-cbxs');
        if(debug_autofill_texts_and_cbxs !== null) {
            this.DEBUG_AUTOFILL_TEXTS_AND_CBXS = debug_autofill_texts_and_cbxs.value.toLowerCase().trim() === 'true';            
        }
    }

    set_data_images_class_names_and_sizes(){        
        var image_collection = document.querySelectorAll("img[src^='data:image']:not([data-tx-name])");
        
        image_collection.forEach(function(item) {
            //add each data image a class name "data-image" for better selector
            item.classList.add("data-image");
            item.dataset.initialWidth = item.clientWidth;
            item.dataset.initialHeight = item.clientHeight;                
        });
        
        return image_collection;            
    }

    wrap_data_images_with_containers() {
            
        var images_parent = this.data_images[0].parentNode;

        remove_br_elements();           
        wrap_each_data_image_with_container.call(this);
        
        function wrap_each_data_image_with_container() {
            var i, pivot_wrapper_div = null, wrappers_collection = null;                    

            for (i = 0; i < this.data_images.length; i++) {
                pivot_wrapper_div = document.createElement('div');
                pivot_wrapper_div.setAttribute('id', `image-wrapper${i + 1}`);
                pivot_wrapper_div.classList.add('image-wrapper');
                if(this.isMobile===true){
                    pivot_wrapper_div.classList.add('mobile-images');
                }
                images_parent.appendChild(pivot_wrapper_div);
            } 
            
            wrappers_collection = document.querySelectorAll('.image-wrapper');
            for (i = 0; i < wrappers_collection.length; i++) {
                wrappers_collection[i].appendChild(this.data_images[i]);
            }                
            set_containers_of_data_image_sizes();
        }
        function set_containers_of_data_image_sizes () {
            var wrappers_coll = document.querySelectorAll('.image-wrapper');
            wrappers_coll.forEach(function(item){
                item.style.width = `${item.offsetWidth}px`;
                item.style.height = `${item.offsetHeight}px`;
                item.dataset.initialWidth = item.offsetWidth;
                item.dataset.initialHeight = item.offsetHeight;
            });    
        }

        function remove_br_elements() {
            var i, brNodeList = null;

            brNodeList = images_parent.querySelectorAll("br");

            for (i = 0; i < brNodeList.length; i++) {
                brNodeList[i].parentNode.removeChild(brNodeList[i]);
            }
        } 
    }

    prepare_head () {
        let head = document.querySelector("head");
        let old_viewport, title, base;
        
        base = head.querySelector("base");
        if (base !== null) {
            base.parentNode.removeChild(base);
        }

        old_viewport = head.querySelector("meta[name='viewport']");
        if (old_viewport !== null) {
            old_viewport.parentNode.removeChild(old_viewport);            
        }        

        title = head.querySelector("title");
        if (title !== null) { 
            title.innerHTML = "good-sign חתימה-בטוחה";
        } else {
            title = document.createElement("title");
            title.innerText = "good-sign חתימה-בטוחה";
            head.appendChild(title);
        }

        var old_theme_color = head.querySelector("meta[name='theme-color']");
        if (old_theme_color !== null) {
            old_theme_color.parentNode.removeChild(old_theme_color);
        }

        var new_theme_color = document.createElement("meta");
        new_theme_color.setAttribute("name", "theme-color");
        new_theme_color.setAttribute("content", "#586AC3");
        head.appendChild(new_theme_color);
    }

    append_additional_markup_to_body() { 
        let html = `<div id="overlay">      
                        <div id="modal-dialog-text-area">
                            <table class="tbl-modal">
                                <tr>
                                    <td class="first-row"> <span class="close-modal"> <i class="far fa-times-circle fa-3x"></i> </span> 
                                    </td>
                                </tr>
                                <tr>
                                    <td class="second-row">
                                        <label class="lbl" for="text-field-area"></label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="third-row">
                                        <div class="sign-plate">
                                            <textarea class="txt-area" id="text-field-area"></textarea>
                                            <div class="sign-plate-bottom">
                                                <div class="chars-counter"> <span id="max-lbl"></span><span id="max-chars-allowed"></span><span id="chars-lbl"></span>(<span id="chars-left-lbl"></span><span id="chars-remaining"></span>)</div>
                                                <div class="required-info"> <sup><i class="fas fa-asterisk"></i></sup>  <span class="required-txt"></span> 
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="last-row-wide">
                                        <a href="javascript:void(0)" id="btn-ok-text-area" class="btn-ok-text"></a>
                                    </td>
                                </tr>
                            </table>
                        </div>                        
                    </div>
                    <div id="white-board" class="white-board">                                            
                        <div class="inner">
                            <div class="title"></div>
                            <div class="sub-title"></div>
                            <div id="files-upload-list"></div>
                            <p id="additional-legal-terms" class="additional-legal-terms"></p>
                            <div id="approve-terms-container" class="approve-terms-container" data-is-dirty="false"> 
                                <i class="far fa-square cbx-inner unchecked"></i>  
                                <i class="fas fa-check-square cbx-inner checked btn-approve" style="display: none"></i>  
                                <span id="txt-i-accept" class="btn-approve"></span> 
                                <a id="btn-open-terms" href="javascript:void(0)"></a>
                            </div>                            
                        </div>
                        <div id="choose-file-container">
                            <div class="choose-header">
                                <span class="txt"></span>
                                <span class="image-info">(תמונה מסוג jpg או png)</span>
                                <div id="choose-close-btn"> 
                                    <i class="far fa-times-circle fa-3x" aria-hidden="true" title="סגור"></i> 
                                </div>
                            </div>
                            <div class="drop-zone drop-zone-border" style="background-color: rgba(88, 106, 195, .2);">
                                <div class="bounce-text disable-button"> גרור קובץ לכאן </div>
                            </div>
                            <div class="or-container">
                                <div class="h-line"></div>
                                <div class="or-txt">או</div>
                                <div class="h-line"></div>
                            </div>
                            <button id="btn-choose-file" class="btn-choose-file"> 
                                <span class="txt">בחר קובץ</span> 
                                <i class="far fa-folder-open"></i>
                            </button> 
                            <div class="upload-validator"></div>                            
                            <input type="file" id="file-upload"/>
                        </div>                        
                        <div id="advertise-good-sign" class="advertise-good-sign"> 
                            <span class="advertise-prefix"></span>  
                            <a target="_blank" rel="noopener" class="link-to-good-sign" href="http://good-sign.co.il?step5-marketing-small">
                                <span class="txt-gs">good-sign</span> 
                                <span class="txt-safe-sign">חתימה-בטוחה</span>
                            </a> 
                        </div>
                    </div>
                    <div id="bottom-bar" class="bottom-bar">
                        <div id="right-side" class="right-side">
                            <button id="back-to-doc" class="back-to-doc"> <i class="fas"></i>  <span class="txt"></span> 
                            </button>
                        </div>
                        <div id="center-side" class="center-side"></div>
                        <div id="left-side" class="left-side">
                            <button id="btn-action" class="btn-action partial-disabled"> <span class="txt"></span>  <i class="fas fa-2x "></i> 
                            </button> <i id="upload-to-server-spinner" class="fas fa-spinner fa-spin"></i>  <span id="signatures-validator" class="signatures-validator"></span> 
                        </div>
                    </div>
                    <div id="zoom-pan-restore" class="zoom-pan-restore">
                        <div class="inner"></div>
                    </div>
                    <div id="logo-container">
                        <img class="img"/>                        
                    </div>`;
        let frag = document.createRange().createContextualFragment(html);
        window.document.body.appendChild(frag);
        if(this.is_new_import === true) {
            const p = window.document.querySelector("body > p[dir='rtl']");
            p.style.fontSize = "12pt";
        }       
    } 

    remove_paragraph_margin_bottom(){
        var p_ele = document.querySelector('body > p');
        if(Number(p_ele.style.marginBottom.replace('pt','')) > 0) {
            p_ele.style.marginBottom = 0;
        }
    }
    
    increase_height_to_last_page() {
        var wrappers_coll = document.querySelectorAll('.image-wrapper');
        var bigger_height = SignaturePositioningTool.instance.get_current_height() * 4/3 * 1.3;// * 4/3: convert from points to px, *1.3: increase height by a factor of 1.3
        wrappers_coll[wrappers_coll.length-1].style.height = `${bigger_height}px`;
        wrappers_coll[wrappers_coll.length-1].dataset.initialHeight = bigger_height;
    }
}

window.addEventListener("DOMContentLoaded", () => {
    var app = new App();
    if( app.IS_ONLINE === true) {
        app.initialize();
        
    }
});

module.exports = App;