import Overlay from './Overlay';
import Mobile from './Mobile';
import Language from "./Language";

class GoodSignBase {
    
    constructor() {
        this.URL_PRODUCTION = "https://good-sign.net";
        this.URL_PRE_PRODUCTION = "https://stage.good-sign.co.il";
        this.URL_DEV = "http://192.168.1.20:8000";
    }
    
    get_url (env, route) {
        let url = "";
        switch(env) {
            case "dev": 
                url = this.URL_DEV;
                break;
            case "pre-prod": 
                url = this.URL_PRE_PRODUCTION;
                break;
            case "prod": 
                url = this.URL_PRODUCTION;
                break;
            default: 
        }
        return url + route;
    }

    disable_body_scrolling () {
        document.body.classList.add('overflow-y-hidden');
        document.body.classList.add('overflow-x-hidden');
    }

    enable_body_scrolling () {
        document.body.classList.remove('overflow-y-hidden');
        document.body.classList.remove('overflow-x-hidden');
    }

    show_overlay () {
        Overlay.instance.show();
    }
    
    hide_overlay () {
        Overlay.instance.hide();
    }

    get_overlay_element() {
        return Overlay.instance.get_element();
    }
    
    is_mobile () {
        return Mobile.instance.is_mobile();
    }

    get_language () {
        return Language.instance.get_language();
    } 

    is_safari () {
        return !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);        
    }

    remove_asterisk_from_placeholder (placeholder){
        let fa_ele = placeholder.querySelector('.fa-asterisk');
        if(fa_ele !== null) {
            placeholder.removeChild(fa_ele);
        }        
    }

    append_asterisk_to_placeholder (placeholder) {
        placeholder.setAttribute('title', this.language === 'heb' ? 'שדה חובה' : 'Required field');
        let fa_ele = document.createElement('i');
        fa_ele.classList.add('fas', 'fa-asterisk');            
        placeholder.appendChild(fa_ele);
    }    
}

module.exports = GoodSignBase;