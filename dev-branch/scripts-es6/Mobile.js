
class Mobile {
    constructor () {
        if(!Mobile.instance) {
            Mobile.instance = this;
            this._is_mobile = [];
        }
        return Mobile.instance;
    }
    
    is_mobile () {
        if(this._is_mobile.length === 0) {
            this.init();
        }
        return this._is_mobile[0];
    }    

    init () {
        var is_mobile = (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
        this._is_mobile.push(is_mobile);
    }
}

const instance = new Mobile();
module.exports.instance = instance;