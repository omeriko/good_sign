import StageZeroModal from "../modalDialogs/StageZeroModal";

class StepZero {
    constructor (stepOne, end_user_secret_code, reusable_doc, business_name) {
        this.stepOne = stepOne;
        this.end_user_secret_code = end_user_secret_code;
        this.reusable_doc = reusable_doc;
        this.business_name = business_name;
    } 
    
    render() {
       new StageZeroModal(
            this,                    
            this.end_user_secret_code, 
            this.reusable_doc, 
            this.business_name).open();
    }

    handle_zero_modal_complete(step_zero_payload) {
        this.stepOne.handle_step_zero_complete(step_zero_payload);
    }   
}

module.exports = StepZero;