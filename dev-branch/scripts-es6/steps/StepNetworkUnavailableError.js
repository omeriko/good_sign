import StepBase from './StepBase';

class StepNetworkUnavailableError extends StepBase {
    constructor (env, is_demo, isMobile, language) {
        super(env, language, isMobile, is_demo);       
        this.advertise_good_sign = document.querySelector("#advertise-good-sign");
    }

    render() {
        this.white_board.style.display = "flex";
        this.white_board_title.style.display = "block";
        this.white_board_title.innerHTML = this.get_title();
        this.white_board_subtitle.style.display = "none";
        this.approve_agreement_container.style.display = "none";
        this.additional_legal_terms.style.display = "none";

        this.bottom_bar.style.display = "block";
        this.bottom_bar_right.style.display = "none";
        this.bottom_bar_left.style.display = "none";
        this.hide_logo();
        this.append_style_tag_to_hide_placeholders();
    }

    get_title() {
        return this.isMobile === true ? 
            get_mobile_offline_message.call(this) : get_desktop_offline_message.call(this);

        function get_mobile_offline_message() {
            var result = this.language === "heb" ? "צר לנו, מכשירך אינו מחובר לרשת" : "Sorry, your device isn't connected to a network";
                result += this.language === "heb" ? "<br><span class='net-error-try-later gray'>נסה שנית כאשר תהיה מחובר </span>" : "<br><span class='net-error-try-later gray'>Try again once you get reconnected</span>";
            return result;
        }

        function get_desktop_offline_message() {
            var result = this.language === "heb" ? "המחשב שלך אינו מחובר לרשת" : "Sorry, your computer is not connected to a network";
                result += this.language === "heb" ? "<br><span class='net-error-try-later gray'> אנא בדוק את החיבור שלך לרשת</span>" : "<br><span class='net-error-try-later gray'>Please check your computer's connection</span>";
            return result;
        }   
    }

    append_style_tag_to_hide_placeholders() {
        const html = 
        `<style id="shel-omer2">   
            [src^="data:image"], 
            [src$="sign_field.jpg"], 
            [src$="sign_field_opaque.jpg"], 
            [src$="sign_text.jpg"], 
            [src$="sign_text_area.jpg"], 
            [src$="date_field.jpg"], 
            [src$="check_box.jpg"] { visibility:hidden }
        </style>`;
        const frag = document.createRange().createContextualFragment(html);
        const head = document.querySelector("head");
        head.appendChild(frag);        
    }
}

module.exports = StepNetworkUnavailableError;