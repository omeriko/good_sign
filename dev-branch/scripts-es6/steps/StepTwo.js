import jQuery from "jquery";
import StepBase from './StepBase';
import PayloadForServer from "../PayloadForServer";
import SignatureModal from "../modalDialogs/SignatureModal";
import TextRowModal from "../modalDialogs/TextRowModal";
import TextAreaModal from "../modalDialogs/TextAreaModal";
import DocumentPlaceholders from "../DocumentPlaceholders";
import SignaturePositioningTool from "../SignaturePositioningTool";

class StepTwo extends StepBase {
    constructor (
        parent,
        env, 
        is_demo, 
        isMobile, 
        language,
        is_multiple_endusers,            
        enduser_contact_value,        
        tooltip_util, 
        journey_id, 
        server_token, 
        business_name, 
        business_email_address,
        business_ip_address,
        doc_name, 
        enduser_fullname, 
        business_self_sign,
        review_doc_course,
        has_embedded_images,
        DEBUG_EMULATE_SERVER) {

        super(env, language, isMobile, is_demo);
        this.documentPlaceholders = null;
        this.container_p = {};
        this.parent = parent; 
        this.is_multiple_endusers = is_multiple_endusers;            
        this.enduser_contact_value = enduser_contact_value;      
        this.tooltip_util = tooltip_util;
        this.journey_id = journey_id; 
        this.server_token = server_token; 
        this.business_name = business_name;
        this.business_email_address = business_email_address;
        this.business_ip_address = business_ip_address; 
        this.doc_name = doc_name; 
        this.enduser_fullname = enduser_fullname;
        this.enduser_email = "";
        this.business_self_sign = business_self_sign;
        this.review_doc_course = review_doc_course;
        this.has_embedded_images = has_embedded_images;
        this.DEBUG_EMULATE_SERVER = DEBUG_EMULATE_SERVER;
        this.modal_dialog_signature = new SignatureModal();            
        this.modal_dialog_text_row = new TextRowModal();
        this.modal_dialog_text_area = new TextAreaModal(window.document.body.querySelector("#modal-dialog-text-area"));
        this.btn_zoom_pan_restore = document.querySelector("#zoom-pan-restore");
    }

    pre_render(DEBUG_AUTOFILL_TEXTS_AND_CBXS) {
        this.documentPlaceholders = new DocumentPlaceholders(
            this.modal_dialog_text_area, 
            this.modal_dialog_text_row, 
            this.modal_dialog_signature, 
            this.enduser_contact_value,  
            this.is_multiple_endusers);
        this.documentPlaceholders.position_all_placeholders();
        
        if(this.isMobile === true) {
            this.init_pinched_elements_data_structure();
        }        
        
        if(DEBUG_AUTOFILL_TEXTS_AND_CBXS === true) {
            this.documentPlaceholders.fill_all_text_placeholders();            
        }

        if(this.isMobile === true) {            
            this.btn_zoom_pan_restore.classList.add(this.language);
            this.btn_zoom_pan_restore.classList.add('mobile-images');
            this.btn_zoom_pan_restore.querySelector(".inner").innerHTML =  this.language === 'heb' ?  
                            "<div class='txt'>חזרה</div><div class='txt'>לגודל</div><div class='txt'>התחלתי</span>" : 
                            "<div class='txt'>Back</div><div class='txt'>to default</div><div class='txt'>view</span>";
        }
        this.bind_events();
    }
    
    render(is_review) {        
        if(this.is_demo === true && this.isMobile === true) {
            this.hide_demo_labels();
        }

        window.scroll({ top: 0, left: 0, behavior: "auto" });          
        this.enable_body_scrolling(); 

        this.white_board.style.display = "none";
        this.bottom_bar.style.display = "block";
        
        this.bottom_bar_left.style.display = "flex";
        this.left_btn_action.style.display = "flex";
        this.left_btn_action.classList.remove("partial-disabled");  
        this.render_left_button();
        this.signatures_validator.innerHTML = this.get_bottom_message();

        this.bottom_bar_right.style.display = "none";

        this.hide_logo();
        const is_customer_with_logo = this.is_customer_with_logo(this.business_name);
        if( is_customer_with_logo === true && 
            this.isMobile === false && 
            SignaturePositioningTool.instance.paper_name !== "a4_landscape") {
            this.show_logo();
        }

        if(this.review_doc_course === false) {
            this.handle_btn_next_click(is_review);
        }
    }

    render_left_button() {
        if(this.review_doc_course === false) {
            this.left_btn_action_txt.innerHTML = this.language === "heb" ? "הבא" : "Next";        
            this.left_btn_action.querySelector(".fas").classList.remove("fa-chevron-circle-left", "fa-chevron-circle-right", "fa-paper-plane", "fa-external-link-alt", "fa-file-pdf", "fa-check-circle");
            this.left_btn_action.querySelector(".fas").classList.add(this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right");
        } else {
            this.left_btn_action_txt.innerHTML = this.language === "heb" ? "אישור" : "Approve";        
            this.left_btn_action.querySelector(".fas").classList.remove("fa-chevron-circle-left", "fa-chevron-circle-right", "fa-paper-plane", "fa-external-link-alt", "fa-file-pdf", "fa-check-circle");
            this.left_btn_action.querySelector(".fas").classList.add("fa-check-circle");
        }
    }

    get_bottom_message() {
        let result = "";
        if(this.review_doc_course === false) {
            result = this.language === "heb" ? "עבור על המסמך" : "Review the document";
        } else {
            if(this.isMobile === true) {
                result = this.language === "heb" ? "אני מאשר שקראתי<br> את המסמך" : "I approve reading this document";
            } else {
                result = this.language === "heb" ? "אני מאשר שקראתי את המסמך" : "I approve reading this document";
            }
        }

        return result;
    }

    bind_events() {
        if(this.isMobile === true) {           
            // Listen for pinch and pan 
            this.bind_pinch_and_pan();

            this.btn_zoom_pan_restore.addEventListener('click', () => {
                this.restore_container_to_initial_scale();
            }, false);           
        }

        // Listen for the key press.
        window.document.body.addEventListener('keyup', e => {
            switch(e.keyCode) {
                case 27: //esc key press
                    if(this.modal_dialog_signature.is_open() === true) {
                        this.modal_dialog_signature.close();
                    } else if(this.modal_dialog_text_row.is_open() === true) {
                        this.modal_dialog_text_row.close();                          
                    } else if(this.modal_dialog_text_area.is_open() === true) {
                        this.modal_dialog_text_area.close();
                    } 
                    break;
                case 13:  //'enter' key press
                    if(this.modal_dialog_text_row.is_open() === true) {
                        this.modal_dialog_text_row.trigger_insert_text();
                    } else if(this.modal_dialog_signature.is_open() === true) {
                        this.modal_dialog_signature.trigger_insert_signature();                        
                    } 
                    break;    
            }          
        }); 
    }

    handle_review_doc_approval_click() {
        this.left_btn_action.classList.add("fully-disabled"); 
        this.upload_to_server_spinner.style.display = "block";        

        if(this.DEBUG_EMULATE_SERVER === false ) {            
            this.prepare_payload_for_server();
            this.upload_payload_to_server();
        } else {            
            setTimeout(() => {
                this.upload_to_server_spinner.style.display = "none";
                this.left_btn_action.classList.remove("fully-disabled");                
                this.parent.handle_go_to_stage_five("");
            },2000);                        
        }
    }

    upload_payload_to_server() {
        const url = this.get_url(this.ENV, "/upload-pdf-assets");        
        
        jQuery.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: PayloadForServer.instance.payload
        }).done( data => {            
            this.upload_to_server_spinner.style.display = "none";            
            this.left_btn_action.classList.remove("fully-disabled");
            this.parent.handle_go_to_stage_five(data.long_url);
        }).fail(()=>{
            this.handle_server_error();
        });
    }

    handle_server_error() {
        this.upload_to_server_spinner.style.display = "none";
        this.left_btn_action.style.display = "none";
        this.white_board_title.innerHTML = this.language === "heb" ? "לצערנו אירעה תקלה במערכת <i class='icon-warning-sign'></i>" : "Oooops, we have a system error <i class='icon-warning-sign'></i>";
        this.white_board_subtitle.innerHTML = this.language === "heb" ? "<span class='gray'>נסה לאשר את קריאת המסמך מאוחר יותר</span>" : "<span class='gray'>Please try to approve reading this document later</span>";
        this.signatures_validator.innerHTML = this.language === "heb" ? "נסה לאשר את קריאת המסמך מאוחר יותר" : "Please try to approve reading this document later";
    }

    handle_btn_next_click(is_review) {
        let top, ele, sorted_reuired_placeholders = this.documentPlaceholders.get_required_placeholders_vertically_sorted();//returns only required textual fields, required text areas and signatures

        if(sorted_reuired_placeholders.length === 0) {            
            if(is_review === false){
                this.prepare_payload_for_server();                
                if(this.has_embedded_images === false) {
                    this.parent.handle_go_to_stage_three();
                } else {
                    this.parent.handle_go_to_stage_embedded_images_upload();
                }               
            }             
        } else {
            ele = sorted_reuired_placeholders[0];
            if(this.isMobile === true) { 
                if(this.tooltip_util.has_tooltip(ele) === false) {
                    this.tooltip_util.attach_tooltip_to_element(ele);
                }
            }
            top = jQuery(ele).offset().top;// ..ele.offsetTop;
            window.scroll({ top: top - 400, left: 0, behavior: 'smooth' }); 
        }
    }

    prepare_payload_for_server() {
                
        this.isMobile && this.restore_container_to_initial_scale(); 
        this.disable_body_scrolling();
        this.documentPlaceholders.remove_all_tooltips();        
        window.scroll({ top: 0, left: 0, behavior: 'auto' });
      
        PayloadForServer.instance.initialize_payload(
            this.journey_id, 
            this.server_token, 
            this.language, 
            this.business_name, 
            this.business_email_address,
            this.business_ip_address, 
            this.doc_name, 
            this.enduser_fullname,
            this.enduser_email,
            this.enduser_contact_value,
            this.business_self_sign,
            this.review_doc_course,
            this.is_demo, 
            this.is_multiple_endusers);

        PayloadForServer.instance.fill_payload_with_pages_and_placeholders();
                 
    }

    init_pinched_elements_data_structure(){
        let container = document.querySelector("body > p:nth-child(1)"), 
        image_wrappers = container.querySelectorAll(".image-wrapper"),                       
        pivot_data_image = null, 
        sign_plh_coll = null,
        text_field_coll = null,
        date_field_coll = null,
        text_area_coll = null,
        checkbox_coll = null,
        in_advance_field_coll = null, 
        in_advance_signature_coll = null, 
        pivot_image_wrapper = null;

        container.style.left = `${Number((window.document.body.offsetWidth / 2) - (container.offsetWidth/2))}px`;
        container.style.top = "0"; 
        
        this.container_p = {
            ref: container,
            initial_width: container.offsetWidth,
            current_width: container.offsetWidth,
            initial_height: container.offsetHeight,
            current_height: container.offsetHeight,
            initial_left: container.offsetLeft,
            current_left: container.offsetLeft,
            initial_top: container.offsetTop,
            current_top: container.offsetTop,
            children: []                    
        };

        image_wrappers.forEach( item => {
            
            pivot_data_image = item.querySelector('.data-image');
            sign_plh_coll = item.querySelectorAll('.sign-here-placeholder');
            text_field_coll = item.querySelectorAll('.text-field-placeholder');
            date_field_coll = item.querySelectorAll('.date-field-placeholder');
            text_area_coll = item.querySelectorAll(".text-area-placeholder");
            checkbox_coll = item.querySelectorAll(".checkbox-placeholder");
            in_advance_field_coll = item.querySelectorAll(".text-in-advance-placeholder");
            in_advance_signature_coll = item.querySelectorAll('[data-tx-name=in-advance-signature]');

            pivot_image_wrapper = {
                ref: item,
                current_width: item.offsetWidth,                       
                current_height: item.offsetHeight,        
                initial_width: Number(item.dataset.initialWidth),                               
                initial_height: Number(item.dataset.initialHeight),
                initial_left: item.offsetLeft,
                current_left: item.offsetLeft,                    
                children: {
                    data_image: { 
                        ref: pivot_data_image,
                        current_width: pivot_data_image.clientWidth,
                        initial_width: Number(pivot_data_image.dataset.initialWidth),
                        current_height: pivot_data_image.clientHeight,                            
                        initial_height: Number(pivot_data_image.dataset.initialHeight)                            
                    },
                    sign_placeholders:[],
                    text_field_placeholders:[],
                    date_field_placeholders:[],
                    text_area_placeholders:[],
                    checkbox_placeholders:[],
                    in_advance_placeholders:[],
                    in_advance_signature_placeholders: []
                }
            };

            sign_plh_coll.forEach(function(sign_plh){
                var new_sign_plh =  {
                    ref: sign_plh,
                    current_width: sign_plh.offsetWidth,
                    initial_width: Number(sign_plh.dataset.initialWidth),
                    current_height: sign_plh.offsetHeight,
                    initial_height: Number(sign_plh.dataset.initialHeight),
                    current_top: Number(sign_plh.dataset.initialTop),
                    initial_top: Number(sign_plh.dataset.initialTop),
                    current_right: Number(sign_plh.dataset.initialRight),                        
                    initial_right: Number(sign_plh.dataset.initialRight)       
                };
                pivot_image_wrapper.children.sign_placeholders.push(new_sign_plh);
            });

            checkbox_coll.forEach(function(cbx_plh){
                var new_cbx_plh =  {
                    ref: cbx_plh,
                    current_font_size: Number(cbx_plh.dataset.initialFontSize),
                    initial_font_size: Number(cbx_plh.dataset.initialFontSize),                    
                    current_top: Number(cbx_plh.dataset.initialTop),
                    initial_top: Number(cbx_plh.dataset.initialTop),
                    current_right: Number(cbx_plh.dataset.initialRight),                        
                    initial_right: Number(cbx_plh.dataset.initialRight)       
                };
                pivot_image_wrapper.children.checkbox_placeholders.push(new_cbx_plh);
            });

            text_field_coll.forEach(function(textf_plh){
                var new_text_field_plh =  {
                    ref: textf_plh,
                    current_width: textf_plh.offsetWidth,                        
                    initial_width: Number(textf_plh.dataset.initialWidth),
                    current_height: textf_plh.offsetHeight,
                    initial_height: Number(textf_plh.dataset.initialHeight),
                    current_top: Number(textf_plh.dataset.initialTop),
                    initial_top: Number(textf_plh.dataset.initialTop),
                    current_right: Number(textf_plh.dataset.initialRight),                        
                    initial_right: Number(textf_plh.dataset.initialRight),
                    initial_font_size: Number(textf_plh.dataset.initialFontSize),
                    current_font_size: Number(textf_plh.dataset.initialFontSize),
                    initial_line_height: Number(textf_plh.dataset.initialLineHeight),
                    current_line_height: Number(textf_plh.dataset.initialLineHeight)
                };
                pivot_image_wrapper.children.text_field_placeholders.push(new_text_field_plh);
            });

            date_field_coll.forEach(function(date_plh) {
                var new_date_field_plh =  {
                    ref: date_plh,
                    current_width: date_plh.offsetWidth,                        
                    initial_width: Number(date_plh.dataset.initialWidth),
                    current_height: date_plh.offsetHeight,
                    initial_height: Number(date_plh.dataset.initialHeight),
                    current_top: Number(date_plh.dataset.initialTop),
                    initial_top: Number(date_plh.dataset.initialTop),
                    current_right: Number(date_plh.dataset.initialRight),                        
                    initial_right: Number(date_plh.dataset.initialRight),
                    initial_font_size: Number(date_plh.dataset.initialFontSize),
                    current_font_size: Number(date_plh.dataset.initialFontSize),
                    initial_line_height: Number(date_plh.dataset.initialLineHeight),
                    current_line_height: Number(date_plh.dataset.initialLineHeight)
                };
                pivot_image_wrapper.children.date_field_placeholders.push(new_date_field_plh);
            });

            text_area_coll.forEach(function(texta_plh){
                var new_textarea_plh =  {
                    ref: texta_plh,
                    current_width: texta_plh.offsetWidth,
                    initial_width: Number(texta_plh.dataset.initialWidth),
                    current_height: texta_plh.offsetHeight,                        
                    initial_height: Number(texta_plh.dataset.initialHeight),
                    current_top: Number(texta_plh.dataset.initialTop),
                    initial_top: Number(texta_plh.dataset.initialTop),
                    current_right: Number(texta_plh.dataset.initialRight),
                    initial_right: Number(texta_plh.dataset.initialRight),
                    initial_font_size: Number(texta_plh.dataset.initialFontSize),
                    current_font_size: Number(texta_plh.dataset.initialFontSize),
                    initial_line_height: Number(texta_plh.dataset.initialLineHeight),
                    current_line_height: Number(texta_plh.dataset.initialLineHeight)
                };
                pivot_image_wrapper.children.text_area_placeholders.push(new_textarea_plh);
            });

            in_advance_field_coll.forEach(function(in_adv_item){
                var new_in_adv_obj = {
                    ref: in_adv_item,
                    current_width: in_adv_item.offsetWidth,
                    initial_width: Number(in_adv_item.dataset.initialWidth),
                    current_height: in_adv_item.offsetHeight,                        
                    initial_height: Number(in_adv_item.dataset.initialHeight),
                    current_top: Number(in_adv_item.dataset.initialTop),
                    initial_top: Number(in_adv_item.dataset.initialTop),
                    current_right: Number(in_adv_item.dataset.initialRight),
                    initial_right: Number(in_adv_item.dataset.initialRight),
                    initial_font_size: Number(in_adv_item.dataset.initialFontSize),
                    current_font_size: Number(in_adv_item.dataset.initialFontSize),
                    initial_line_height: Number(in_adv_item.dataset.initialLineHeight),
                    current_line_height: Number(in_adv_item.dataset.initialLineHeight)
                };
                pivot_image_wrapper.children.in_advance_placeholders.push(new_in_adv_obj);
            });

            in_advance_signature_coll.forEach(function(in_adv_item) {
                var new_in_adv_obj = {
                    ref: in_adv_item,
                    current_width: in_adv_item.offsetWidth,
                    initial_width: Number(in_adv_item.dataset.initialWidth),
                    current_height: in_adv_item.offsetHeight,
                    initial_height: Number(in_adv_item.dataset.initialHeight),
                    current_top: Number(in_adv_item.dataset.initialTop),
                    initial_top: Number(in_adv_item.dataset.initialTop),
                    current_right: Number(in_adv_item.dataset.initialRight),                        
                    initial_right: Number(in_adv_item.dataset.initialRight) 
                };

                pivot_image_wrapper.children.in_advance_signature_placeholders.push(new_in_adv_obj);
            });
            
            this.container_p.children.push(pivot_image_wrapper);               
        });            
    }
    
    bind_pinch_and_pan () {  
            
        let mc = new Hammer.Manager(this.container_p.ref);            
        let pinch = new Hammer.Pinch();
        let pan = new Hammer.Pan({ direction: Hammer.DIRECTION_ALL });
        
        pinch.recognizeWith(pan);            
        mc.add([pinch, pan]);            
        mc.add( new Hammer.Tap({ event: 'doubletap', taps: 2, interval: 500, posThreshold: 100 }) );
        
        mc.on("pinchstart panstart", () => {
            this.documentPlaceholders.remove_all_tooltips();
            
        }).on("pinchmove panmove", ev => {                
            this.container_p.ref.style.left = `${this.container_p.current_left + ev.deltaX}px`;                
            this.container_p.ref.style.top =  `${this.container_p.current_top + ev.deltaY}px`;             

            if(this.container_p.current_width * ev.scale >= this.container_p.initial_width) {
                //set new width and height to p
                this.container_p.ref.style.width = (this.container_p.current_width * ev.scale) + "px";
                this.container_p.ref.style.height = (this.container_p.current_height * ev.scale) + "px";
                
                //calculate new width and height for all children: images_wrapper 
                this.container_p.children.forEach( item => {                        
                    item.ref.style.width = `${item.current_width * ev.scale}px`;
                    item.ref.style.height = `${item.current_height * ev.scale}px`;
                    
                    //calculate new width and height for data_image child 
                    item.children.data_image.ref.style.width = `${item.children.data_image.current_width * ev.scale}px`;
                    item.children.data_image.ref.style.height = `${item.children.data_image.current_height * ev.scale}px`;

                    //calculate new width and height for checkbox children 
                    item.children.checkbox_placeholders.forEach( item => {
                        item.ref.querySelector(".checked").style.fontSize = `${item.current_font_size * ev.scale}px`;
                        item.ref.querySelector(".unchecked").style.fontSize = `${item.current_font_size * ev.scale}px`;                        
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;                            
                    });

                    //calculate new width and height for signature children 
                    item.children.sign_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;                            
                    });

                    //calculate new sizes for date_field children 
                    item.children.date_field_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                        item.ref.style.fontSize = `${item.current_height * 0.5 * ev.scale}px`;                           
                        item.ref.style.lineHeight = `${(item.current_height * ev.scale) -(4*ev.scale)}px`;
                    });

                    //calculate new sizes for text_field children 
                    item.children.text_field_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                        item.ref.style.fontSize = `${item.current_height * 0.5 * ev.scale}px`
                        item.ref.style.lineHeight = `${(item.current_height * ev.scale) -(4*ev.scale)}px`;
                    });

                     //calculate new sizes for text_area children 
                     item.children.text_area_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                        item.ref.style.fontSize = `${item.current_font_size * ev.scale}px`;                           
                        item.ref.style.lineHeight = `${item.current_line_height * ev.scale}px`;
                    });

                    //calculate new sizes for in advance labels children 
                    item.children.in_advance_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                        item.ref.style.fontSize = `${item.current_height * 0.5 * ev.scale}px`;                           
                        item.ref.style.lineHeight = `${item.current_line_height * ev.scale}px`;
                    });
                     //calculate new sizes for in advance signatures children
                    item.children.in_advance_signature_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                    });
                    
                });
            }
        })
        .on("pinchend panend", ev => {
            //console.log(`pinchend panend:dir: ${ev.direction}, scale ${ev.scale}, dX: ${ev.deltaX}, ele: ${ev.target.className} `);
            
            this.container_p.current_width = Number(this.container_p.ref.style.width.replace('px', ''));
            this.container_p.current_height = Number(this.container_p.ref.style.height.replace('px', ''));
            this.container_p.current_left = Number(this.container_p.ref.style.left.replace('px',''));
            this.container_p.current_top = Number(this.container_p.ref.style.top.replace('px',''));
                            
            //calculate and update current_width for all children: images_wrapper 
            this.container_p.children.forEach( item => {
                item.current_width =  Number(item.ref.style.width.replace('px',''));
                item.current_height = Number(item.ref.style.height.replace('px',''));

                item.children.data_image.current_width =  Number(item.children.data_image.ref.style.width.replace('px',''));
                item.children.data_image.current_height = Number(item.children.data_image.ref.style.height.replace('px',''));

                //calculate new width and height for checkbox children 
                item.children.checkbox_placeholders.forEach( item => {
                    item.current_font_size  =  Number(item.ref.querySelector(".checked").style.fontSize.replace('px',''));
                    item.current_top    =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right  =  Number(item.ref.style.right.replace('pt',''));                        
                });

                //calculate new width and height for signature children 
                item.children.sign_placeholders.forEach( item => {
                    item.current_width  =  item.ref.offsetWidth;
                    item.current_height =  item.ref.offsetHeight;
                    item.current_top    =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right  =  Number(item.ref.style.right.replace('pt',''));                        
                });

                //calculate new sizes for date_field children 
                item.children.date_field_placeholders.forEach( item => {
                    item.current_width       =  item.ref.offsetWidth;
                    item.current_height      =  item.ref.offsetHeight;
                    item.current_top         =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right       =  Number(item.ref.style.right.replace('pt',''));
                    item.current_font_size    =  Number(item.ref.style.fontSize.replace('px',''));
                    item.current_line_height =  Number(item.ref.style.lineHeight.replace('px',''));                        
                });

                //calculate new sizes for text_field children 
                item.children.text_field_placeholders.forEach( item => {
                    item.current_width       =  item.ref.offsetWidth;
                    item.current_height      =  item.ref.offsetHeight;
                    item.current_top         =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right       =  Number(item.ref.style.right.replace('pt',''));
                    item.current_font_size    =  Number(item.ref.style.fontSize.replace('px',''));
                    item.current_line_height =  Number(item.ref.style.lineHeight.replace('px',''));                        
                });

                //calculate new sizes for text_area children 
                item.children.text_area_placeholders.forEach( item => {
                    item.current_width    =  item.ref.offsetWidth;
                    item.current_height   =  item.ref.offsetHeight;
                    item.current_top      =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right    =  Number(item.ref.style.right.replace('pt',''));
                    item.current_font_size = Number(item.ref.style.fontSize.replace('px',''));
                    item.current_line_height = Number(item.ref.style.lineHeight.replace('px',''));
                });

                //calculate new sizes for in_advance children 
                item.children.in_advance_placeholders.forEach( item => {
                    item.current_width    =  item.ref.offsetWidth;
                    item.current_height   =  item.ref.offsetHeight;
                    item.current_top      =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right    =  Number(item.ref.style.right.replace('pt',''));
                    item.current_font_size = Number(item.ref.style.fontSize.replace('px',''));
                    item.current_line_height = Number(item.ref.style.lineHeight.replace('px',''));
                });

                item.children.in_advance_signature_placeholders.forEach( item =>{
                    item.current_width  =  item.ref.offsetWidth;
                    item.current_height =  item.ref.offsetHeight;
                    item.current_top    =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right  =  Number(item.ref.style.right.replace('pt',''));
                });
            }); 
            jQuery(this.btn_zoom_pan_restore).show(500);
        })
        .on("doubletap", () => {
           this.restore_container_to_initial_scale();
        });
    }

    restore_container_to_initial_scale () {

        jQuery(this.btn_zoom_pan_restore).fadeOut(200);
        //return the size of it to its initial size            
        this.container_p.current_width    =  this.container_p.initial_width;
        this.container_p.current_height   =  this.container_p.initial_height;
        this.container_p.ref.style.width  = `${this.container_p.current_width}px`;
        this.container_p.ref.style.height = `${this.container_p.current_height}px`;
        this.container_p.current_left     =  this.container_p.initial_left;    
        this.container_p.ref.style.left   = `${this.container_p.current_left}px`;
        this.container_p.current_top      =  this.container_p.initial_top;   
        this.container_p.ref.style.top    = `${this.container_p.current_top}px`;          
        
        this.container_p.children.forEach( item => {
            item.current_width = item.initial_width;
            item.current_height = item.initial_height;
            item.ref.style.width  = `${item.current_width}px`;
            item.ref.style.height = `${item.current_height}px`;
            
            item.children.data_image.current_width = item.children.data_image.initial_width;
            item.children.data_image.current_height = item.children.data_image.initial_height;
            item.children.data_image.ref.style.width = `${item.children.data_image.current_width}px`;
            item.children.data_image.ref.style.height = `${item.children.data_image.current_height}px`;

            //set initial sizes for checkbox children 
            item.children.checkbox_placeholders.forEach( item => {
                item.current_font_size = item.initial_font_size; 
                item.current_top    = item.initial_top; 
                item.current_right  = item.initial_right;                
               
                item.ref.querySelector(".checked").style.fontSize = `${item.current_font_size}px`; 
                item.ref.querySelector(".unchecked").style.fontSize = `${item.current_font_size}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
            });

            //set initial sizes for signature children 
            item.children.sign_placeholders.forEach( item => {
                item.current_width  = item.initial_width; 
                item.current_height = item.initial_height; 
                item.current_top    = item.initial_top; 
                item.current_right  = item.initial_right;
                
                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
            });

            //set initial sizes for date_field children 
            item.children.date_field_placeholders.forEach( item => {                
                item.current_width    = item.initial_width; 
                item.current_height   = item.initial_height; 
                item.current_top      = item.initial_top; 
                item.current_right    = item.initial_right;
                item.current_font_size = `${item.initial_font_size}em`;
                item.current_line_height = item.initial_line_height;

                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
                item.ref.style.fontSize    = `${item.initial_font_size}px`; 
                item.ref.style.lineHeight  = `${item.current_line_height}px`;
            });

            //set initial sizes for text_field children 
            item.children.text_field_placeholders.forEach( item => {                
                item.current_width    = item.ref.dataset.isDirty === "false" ? item.initial_width : "auto"; 
                item.current_height   = item.initial_height; 
                item.current_top      = item.initial_top; 
                item.current_right    = item.initial_right;
                item.current_font_size = item.initial_font_size;
                item.current_line_height = item.initial_line_height;

                item.ref.style.width  = item.ref.dataset.isDirty === "false" ? `${item.current_width}px` : "auto"; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
                item.ref.style.fontSize    = `${item.initial_font_size}px`; 
                item.ref.style.lineHeight  = `${item.current_line_height}px`;
            });

            //set initial sizes for text_area children 
            item.children.text_area_placeholders.forEach( item => {
                item.current_width    = item.initial_width; 
                item.current_height   = item.initial_height; 
                item.current_top      = item.initial_top; 
                item.current_right    = item.initial_right;
                item.current_font_size = item.initial_font_size;
                item.current_line_height = item.initial_line_height;

                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
                item.ref.style.fontSize    = `${item.current_font_size}px`; 
                item.ref.style.lineHeight  = `${item.current_line_height}px`;
            });

            //calculate new sizes for in advance labels children 
            item.children.in_advance_placeholders.forEach( item => {
                item.current_width    = item.initial_width; 
                item.current_height   = item.initial_height; 
                item.current_top      = item.initial_top; 
                item.current_right    = item.initial_right;
                item.current_font_size = item.initial_font_size;
                item.current_line_height = item.initial_line_height;

                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
                item.ref.style.fontSize    = `${item.current_font_size}px`; 
                item.ref.style.lineHeight  = `${item.current_line_height}px`;
            });
            //calculate new sizes for in advance signatures children 
            item.children.in_advance_signature_placeholders.forEach( item => {
                item.current_width  = item.initial_width; 
                item.current_height = item.initial_height; 
                item.current_top    = item.initial_top; 
                item.current_right  = item.initial_right;
                
                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
            });
        });
    }

    set_enduser_details(step_zero_payload) {
        this.enduser_fullname = step_zero_payload.end_user_name_or_company;
        this.enduser_email = step_zero_payload.end_user_email;
    }
}  

module.exports = StepTwo;