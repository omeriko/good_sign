import StepBase from './StepBase';

class StepFour extends StepBase {
    constructor (env, is_demo, isMobile, language, business_name, additional_message) {
        super(env, language, isMobile, is_demo);        
        this.business_name = business_name;
        this.additional_message = additional_message;
    } 

    render() {
        console.log("render called on step 4");        
        this.disable_body_scrolling();
                
        this.white_board.style.display = "flex";
        this.white_board_title.style.display = "block";
        this.white_board_title.innerHTML = this.language === "heb" ? `מעולה,` : `Great,`;

        this.white_board_subtitle.style.display = "block";
        this.white_board_subtitle.innerHTML = this.get_subtitle_html();            
        
        this.approve_agreement_container.style.display = "none";        
        this.additional_legal_terms.style.display = "none";

        this.bottom_bar.style.display = "block";

        this.bottom_bar_left.style.display = "flex";        
        this.left_btn_action.style.display = "flex";
        this.left_btn_action_txt.innerHTML = this.language === "heb" ? "הבא" : "Next";     
        this.left_btn_action.querySelector(".fas").classList.remove("fa-chevron-circle-left", "fa-chevron-circle-right", "fa-paper-plane", "fa-external-link-alt", "fa-file-pdf");
        this.left_btn_action.querySelector(".fas").classList.add(this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right");
        
        this.signatures_validator.style.display = "block";
        this.signatures_validator.innerHTML = this.language === "heb" ? "לחץ על <b>הבא</b> להמשך..." : "Click <b>Next</b> to continue...";

        this.bottom_bar_right.style.display = "none";

        this.hide_logo();
        const is_customer_with_logo = this.is_customer_with_logo(this.business_name);
        if( is_customer_with_logo === true) {
            this.show_logo();
        }
    }

    get_subtitle_html() {
        let result = this.language === "heb" ? 
            `<span class='title-lower'> עותק חתום נשלח בהצלחה אל <b>${this.business_name}</b> <i class='far fa-thumbs-up'></i></span>` : 
            `<span class='title-lower'>A signed copy was succesfully sent to <b>${this.business_name}</b> <i class='far fa-thumbs-up'></i></span>`;

        result += this.additional_message.trim() === "" ? "" : `<div class='additional-message'><i class='fas fa-info-circle'></i><p>${this.additional_message}</p></div>`;

        return result;
    }
}

module.exports = StepFour;