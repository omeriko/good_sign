
import jQuery from "jquery";
import StepBase from './StepBase';
import StepZero from './StepZero';
import AgreementModal from "../modalDialogs/AgreementModal";

class StepOne extends StepBase {
    constructor (
        parent,
        env, 
        is_demo, 
        isMobile, 
        language, 
        business_self_sign,
        review_doc_course,
        enduser_details, 
        business_name, 
        hide_legal_terms, 
        end_user_secret_code, 
        reusable_doc) {
        
        super(env, language, isMobile, is_demo);
        this.parent = parent;
        this.business_self_sign = business_self_sign;
        this.review_doc_course = review_doc_course;
        this.step_zero = null;        
        this.end_user_secret_code = end_user_secret_code;
        this.reusable_doc = reusable_doc;
        this.enduser_details = enduser_details;
        this.business_name = business_name;
        this.hide_legal_terms = hide_legal_terms;                
        
        this.modal_dialog_agreement = null;
    }   
    
    render () {        
        this.disable_body_scrolling();
        this.set_language_styles();
        this.isMobile && this.set_mobile_styles();
                
        this.white_board.style.display = "flex";
        this.white_board_title.style.display = "block";
        this.white_board_title.innerHTML = this.get_title();

        if(this.business_self_sign === false) {
            this.white_board_subtitle.style.display = "block";
            this.white_board_subtitle.innerHTML = this.get_subtitle();
        } else {
            this.white_board_subtitle.style.display = "none";
        }

        this.approve_agreement_container.style.display = "none";

        if(this.is_demo === false && this.hide_legal_terms === false && this.business_self_sign === false) {
            
            this.approve_agreement_container.style.display = "flex";
            this.txt_i_accept.innerText = this.language === 'heb' ? "אני מאשר את" : "I accept the";
            this.button_open_terms.innerText = this.language === 'heb' ? "תנאי השימוש" : "Terms and Conditions";
            this.additional_legal_terms.style.display = "none";

            this.create_agreement_dialog();

        } else if(this.is_demo === true) {            
            this.left_btn_action.classList.remove("partial-disabled");
            this.append_demo_labels_on_four_corners();
            this.show_demo_labels();
        } 

        this.handle_logo_image();
        
        this.bottom_bar.style.display = "block";

        this.bottom_bar_left.style.display = "flex";        
        this.left_btn_action.style.display = "flex";
        this.left_btn_action_txt.innerHTML = this.language === "heb" ? "הבא" : "Next";        
        this.left_btn_action.querySelector(".fas").classList.remove("fa-chevron-circle-left", "fa-chevron-circle-right", "fa-paper-plane", "fa-external-link-alt", "fa-file-pdf");
        this.left_btn_action.querySelector(".fas").classList.add(this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right");
        this.signatures_validator.innerHTML = this.language === "heb" ? "לחץ על  <b>הבא</b> כדי להתחיל" : "Click <b>Next</b> to begin";
        
        this.bottom_bar_right.style.display = "none";

        if(this.hide_legal_terms === true || this.business_self_sign === true) {
            this.left_btn_action.classList.remove("partial-disabled");
        }

        this.remove_style_shel_omer3();        
        this.bind_events();

        const should_open_stage0_popup = this.should_open_stage_zero_popup();
        if(should_open_stage0_popup === true) {
            this.step_zero = new StepZero(this, this.end_user_secret_code, this.reusable_doc, this.business_name);
            this.step_zero.render();
        }
    }

    handle_logo_image() {       
        let logo_image_name = "";
        let logo_extra_class = ""; 
        const is_customer_with_logo = this.is_customer_with_logo(this.business_name);

        if(is_customer_with_logo === true) {
            if(this.business_name.includes("הזנפרץ")) {
                logo_image_name = "shift.jpeg";
                logo_extra_class = "hazenfraz";
            } else if(this.business_name === "Sivan Gvili") {
                logo_image_name = "zim.jpeg";
                logo_extra_class = "zim";            
            } else if(this.business_name.includes("חקימוביץ")) {
                logo_image_name = "vhl.jpeg";
                logo_extra_class = "hazenfraz";            
            } else if(this.business_name === "goodsign") {
                logo_image_name = "example_logo.png";
                logo_extra_class = "example-logo";
            } 

            const logo_image_url = this.get_url(this.ENV, `/public/images/${logo_image_name}`);
            this.logo_image.setAttribute("src", logo_image_url);
            this.logo_image.classList.add(logo_extra_class);
            this.show_logo();
        }
    }

    create_agreement_dialog() {
        let has_additional_legal_terms = false;

        if(this.business_name.includes("הזנפרץ")) {
            has_additional_legal_terms = true;
            this.modal_dialog_agreement = new AgreementModal(has_additional_legal_terms);
            this.additional_legal_terms.innerHTML = this.modal_dialog_agreement.get_additional_terms_html();
            this.additional_legal_terms.style.display = "block";
        } else {
            this.modal_dialog_agreement = new AgreementModal(has_additional_legal_terms);
        }
    }

    bind_events () {
        this.button_open_terms.addEventListener('click', function() {            
            this.modal_dialog_agreement.open();
        }.bind(this), false);        
    }    
    
    handle_step_zero_complete (step_zero_payload) {
        this.enduser_details = step_zero_payload.end_user_name_or_company;
        this.white_board_title.innerHTML = this.get_title();
        this.parent.notify_enduser_details_changed(step_zero_payload);
    }

    get_subtitle() {
        let result = "";

        if(this.review_doc_course === false) {
            result = this.language === "heb" ? `קיבלת מסמך לחתימה מאת <b>${this.business_name}</b>` : `<b>${this.business_name}</b> sent you a document that requires your signature`;
        } else {
            result = this.language === "heb" ? `קיבלת מסמך לעיון מאת <b>${this.business_name}</b>` : `<b>${this.business_name}</b> sent you a document that requires your review`;
        }
        return result;
    }

    get_title () {
        let result = '';       
        if(this.business_self_sign === false) {
            if(this.is_demo === true) {
                if(this.language === "heb") {
                    result = `שלום <span>אורח/ת</span>,`;
                } else {
                    result = "Hello trial-user";
                }
            }
            else {
                if(this.language === "heb") {
                    result = `שלום ${this.enduser_details !== '' ? this.enduser_details : ''  },`;            
                } else if(this.language === "eng") {
                    result = `Hi ${this.enduser_details !== '' ? this.enduser_details : ''  },`;
                }
            }
        } else {
            result = `<b class="gray">${this.business_name}</b>:<br>${this.language === "heb" ? "מסלול לחתימה עצמית" : "Self signature process"} <i class="fas fa-edit"></i>`;
        }

        return result;       
    }

    remove_style_shel_omer3 () {
        var styleEle = document.querySelector("#shel-omer2");
        if(styleEle !== null) {
            styleEle.parentNode.removeChild(styleEle);
        }  
    }

    append_demo_labels_on_four_corners() {
        const text = this.language === "heb" ? "הדגמה" : "demo"; 

        let div_demo = document.createElement("div");
        div_demo.classList.add("demo-item", "body-demo-top-left");
        div_demo.innerText = text;
        window.document.body.appendChild(div_demo);

        div_demo = document.createElement("div");
        div_demo.classList.add("demo-item", "body-demo-top-right");
        div_demo.innerText = text;
        window.document.body.appendChild(div_demo); 
        
        div_demo = document.createElement("div");
        div_demo.classList.add("demo-item", "body-demo-bottom-left");
        div_demo.innerText = text;
        window.document.body.appendChild(div_demo);

        div_demo = document.createElement("div");
        div_demo.classList.add("demo-item", "body-demo-bottom-right");
        div_demo.innerText = text;
        window.document.body.appendChild(div_demo); 

        document.querySelectorAll(".demo-item").forEach(item => {
            if(this.isMobile) {
                item.classList.add("mobile-images");
            }
        });
    }

    should_open_stage_zero_popup() {
        let result = false;
        if(this.end_user_secret_code.enabled === true || (this.reusable_doc.value === true && this.reusable_doc.require_enduser_details ===  true)) {
            result = true;
        }

        return result;
    }
}

module.exports = StepOne;