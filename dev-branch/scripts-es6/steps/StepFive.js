import StepBase from './StepBase';

class StepFive extends StepBase {
    constructor (
        env, 
        is_demo, 
        isMobile, 
        language, 
        business_name, 
        business_self_sign,
        review_doc_course) {
        super(env, language, isMobile, is_demo);
        this.business_name = business_name;
        this.business_self_sign = business_self_sign;
        this.review_doc_course = review_doc_course;        
        this.advertise_good_sign = document.querySelector("#advertise-good-sign");
    }

    get_subtitle_heb() {
        const message = `בעל עסק, מעוניין בשירות חתימות-אלקטרוניות?<br> 
        הצטרף ל <a class='link-to-good-sign' target='_blank' rel='noopener' href='https://www.good-sign.co.il?step5-marketing-banner=true'>good-sign</a> וקבל במתנה <b>חודש חינם</b> <i class='far fa-smile' style='font-size:1em'></i><br>
        <a class='link-to-good-sign' target='_blank' rel='noopener' href='https://www.good-sign.co.il?step5-marketing-banner=true'>ליצירת קשר</a>
`;
               
        const result = `<div class='marketing-banner marketing'><p>${message}</p></div>`;
        return result;
    }

    render() {
        this.disable_body_scrolling();
                
        this.white_board.style.display = "flex";
        this.white_board_title.style.display = "block";
        this.white_board_title.innerHTML = this.get_title();

        this.white_board_subtitle.style.display = "block";
        this.white_board_subtitle.innerHTML = this.get_subtitle_heb();
        
        this.approve_agreement_container.style.display = "none";        
        this.additional_legal_terms.style.display = "none";

        if(this.business_self_sign === false) {
            this.advertise_good_sign.querySelector('.advertise-prefix').innerText = this.language === 'heb' ? "באמצעות:" : "powered by:";       
            this.advertise_good_sign.classList.add(this.language); 
            this.advertise_good_sign.style.display = "block";
            this.isMobile && this.advertise_good_sign.classList.add("mobile-images"); 
            this.is_demo && this.advertise_good_sign.classList.add("is-demo"); 
        }

        this.bottom_bar.style.display = "block";

        this.bottom_bar_left.style.display = "flex";        
        this.left_btn_action.style.display = "flex";
        this.left_btn_action_txt.innerHTML = this.language === "heb" ? "פתח מסמך" : "<span style='font-size: .85em'>View Document</span>";
        this.left_btn_action.querySelector(".fas").classList.remove("fa-chevron-circle-left", "fa-chevron-circle-right", "fa-paper-plane", "fa-external-link-alt", "fa-file-pdf");
        this.left_btn_action.querySelector(".fas").classList.add("fa-external-link-alt");
        
        this.signatures_validator.style.display = "block";
        this.signatures_validator.innerHTML = this.get_bottom_message();

        this.bottom_bar_right.style.display = "none";

        this.hide_logo();
        const is_customer_with_logo = this.is_customer_with_logo(this.business_name);
        if( is_customer_with_logo === true) {
            this.show_logo();
        }
    }

    get_bottom_message() {
        let result = "";
        if(this.review_doc_course === false) {
            result = this.language === 'heb' ? "לצפייה במסמך החתום לחץ <b>פתח מסמך</b>" : "Click here to review the document";
        } else {
            result = this.language === 'heb' ? "לצפייה במסמך לחץ <b>פתח מסמך</b>" : "Click here to review the document";
        }

        return result;
    }

    get_title() {
        let result;
        if(this.business_self_sign === false) {
            if(this.language === "heb") {
                result = `<div>    
                            תודה-רבה על שיתוף הפעולה, סיימנו <i class="fas fa-check"></i>
                            <div class="demo-leave-details" style="display: ${this.is_demo=== true ? 'block' : 'none' }">
                                <a href="https://good-sign.co.il" class="leave-details-link" target="_blank" rel='noopener'>
                                    <span>אהבת? להשארת פרטים לחץ כאן</span>
                                    <i class='fas fa-external-link-alt'></i>
                                </a>
                            </div>
                          <div>`;
            } else {
                result = `<div>    
                            Thanks, we're done here <i class="fas fa-check"></i>
                            <div class="demo-leave-details" style="display: ${this.is_demo=== true ? 'block' : 'none' }">
                                <a href="https://good-sign.co.il" class="leave-details-link" target="_blank" rel="noopener">Click here to leave your details  <i class="far fa-hand-pointer"></i></a>
                            </div>
                          </div>`;
            }
            
        } else {
            result = this.language === "heb" ? 
                "המסמך מוכן <i class='fas fa-check'></i>" : 
                "The file is ready <i class='fas fa-check'></i>";
        }
        return result;
    }
}

module.exports = StepFive;