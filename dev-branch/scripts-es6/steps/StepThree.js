import jQuery from "jquery";
import StepBase from './StepBase';
import PayloadForServer from "../PayloadForServer";

class StepThree extends StepBase {
    constructor (
        env, 
        parent, 
        is_demo, 
        isMobile, 
        language,
        is_multiple_endusers,
        business_self_sign, 
        business_name,
        has_embedded_images,
        DEBUG_EMULATE_SERVER) {
        super(env,language, isMobile, is_demo);
        this.is_multiple_endusers = is_multiple_endusers;
        this.parent = parent;        
        this.business_self_sign = business_self_sign; 
        this.business_name = business_name;
        this.has_embedded_images = has_embedded_images;
        this.DEBUG_EMULATE_SERVER = DEBUG_EMULATE_SERVER;
        this.bind_events();
    } 

    bind_events() {
        this.back_to_doc.addEventListener("click", () => {            
            this.parent.handle_go_to_stage_two({review_doc: true});                
        }, false);
    }
    
    render() {        
        this.disable_body_scrolling();
                
        this.white_board.style.display = "flex";
        this.white_board_title.style.display = "block";
        this.white_board_title.innerHTML = this.get_title();

        this.white_board_subtitle.style.display = "block";
        this.white_board_subtitle.innerHTML = this.get_subtitle();
        
        this.approve_agreement_container.style.display = "none";        
        this.additional_legal_terms.style.display = "none";

        if(this.is_demo === false) {
            this.approve_agreement_container.style.display = "flex";
            this.approve_agreement_container.dataset.isDirty = "false";
            this.approve_agreement_container.querySelector(".checked").style.display = "none";
            this.approve_agreement_container.querySelector(".unchecked").style.display = "block";

            this.txt_i_accept.innerText = this.language === 'heb' ? "אני מאשר שקראתי את המסמך" : "I approve i've read the document";
            this.button_open_terms.style.display = "none";
            this.additional_legal_terms.style.display = "none";
        }


        this.bottom_bar.style.display = "block";

        this.bottom_bar_left.style.display = "flex";        
        this.left_btn_action.style.display = "flex";
        this.left_btn_action_txt.innerHTML = this.get_left_btn_caption(); 
        
        this.is_demo === false && this.left_btn_action.classList.add("partial-disabled");       
        
        this.left_btn_action.querySelector(".fas").classList.remove("fa-chevron-circle-left", "fa-chevron-circle-right", "fa-paper-plane", "fa-external-link-alt", "fa-file-pdf");
        this.left_btn_action.querySelector(".fas").classList.add(this.business_self_sign === false ? "fa-paper-plane" : "fa-file-pdf");
        
        this.signatures_validator.style.display = "none";
        this.signatures_validator.innerHTML = "";

        if(this.has_embedded_images === false) {
            this.bottom_bar_right.style.display = "block";
            this.back_to_doc.querySelector(".txt").innerText = this.language === "heb" ? "חזרה למסמך" : "back to doc'";
            this.back_to_doc.querySelector(".fas").classList.add(this.language === "heb" ? "fa-long-arrow-alt-right" : "fa-long-arrow-alt-left");              
        } else {
            this.bottom_bar_right.style.display = "none";
        }

        if(this.is_demo === true) {
            this.show_demo_labels();
        }

        this.hide_logo();
        const is_customer_with_logo = this.is_customer_with_logo(this.business_name);
        if( is_customer_with_logo === true) {
            this.show_logo();
        }
    }

    /* end user has Clicked "שלח" or "Generate File" button, 
       which means now we send the payload to the server */
    handle_post_data_to_server() {
        this.upload_to_server_spinner.style.display = "block";
        this.back_to_doc.classList.add("none");
        this.left_btn_action.classList.add("fully-disabled");

        if(this.DEBUG_EMULATE_SERVER === false ) {
            this.upload_payload_to_server();
        } else {                        
            setTimeout(() => {
                this.upload_to_server_spinner.style.display = "none";
                this.left_btn_action.classList.remove("fully-disabled");                
                this.parent.handle_go_to_stage_four();  
            },2000);                        
        }
    }

    upload_payload_to_server() {
        const route = this.is_multiple_endusers === true ? "/upload-pdf-assets-parallel" : "/upload-pdf-assets"
        const url = this.get_url(this.ENV, route);
                
        jQuery.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: PayloadForServer.instance.payload
        }).done( data => {            
            this.upload_to_server_spinner.style.display = "none";            
            this.left_btn_action.classList.remove("fully-disabled");
            this.parent.handle_go_to_stage_four(data.long_url);
        }).fail(()=>{
            this.handle_server_error();
        });
    }

    handle_server_error() {
        this.upload_to_server_spinner.style.display = "none";
        this.left_btn_action.style.display = "none";
        this.white_board_title.innerHTML = this.language === "heb" ? "לצערנו אירעה תקלה במערכת <i class='icon-warning-sign'></i>" : "Oooops, we have a system error <i class='icon-warning-sign'></i>";
        this.white_board_subtitle.innerHTML = this.language === "heb" ? "<span class='gray'>נסה לחתום שוב על המסמך מאוחר יותר</span>" : "<span class='gray'>Please try to sign the document later</span>";
        this.signatures_validator.innerHTML = this.language === "heb" ? "נסה לחתום שוב על המסמך מאוחר יותר" : "Please try to sign the document later";
    }

    get_title() {
        let result;
        if(this.business_self_sign === false) {
            result = this.language === "heb" ? `לחץ <b>שלח</b>,` : `Click <b>Send</b>,`;
        } else {
            result = this.language === "heb" ? `לחץ <b>צור מסמך</b>` : `Click <b>Generate File</b>`;
        }
        return result;
    }

    get_subtitle() {
        let result;
        if(this.business_self_sign === false) {
            result = this.language === "heb" ? `כדי להחזיר קובץ חתום אל <b>${this.business_name}</b>` : `In order to return a signed document to <b>${this.business_name}</b>`
         } else {
            result = "";
        }
        return result;
    }

    get_left_btn_caption() {
        let result;
        if(this.business_self_sign === false) {
            result = this.language === "heb" ? "שלח" : "Send";
        } else {
            result = this.language === "heb" ? "צור מסמך" : "Generate File";
        }
        return result;
    }
}

module.exports = StepThree;