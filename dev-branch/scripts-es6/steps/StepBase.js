import GoodSignBase from "../GoodSignBase";

class StepBase extends GoodSignBase {
    constructor(env, language, isMobile, is_demo) {
        super();
        console.log("step base called");
        this.ENV = env;
        this.language = language;
        this.isMobile = isMobile;
        this.is_demo = is_demo; 
        this.white_board = document.querySelector("#white-board");
        this.white_board_title = this.white_board.querySelector(".title");
        this.white_board_subtitle = this.white_board.querySelector(".sub-title");

        this.approve_agreement_container = document.querySelector("#approve-terms-container");        
        this.txt_i_accept = this.approve_agreement_container.querySelector("#txt-i-accept");        
        this.button_open_terms = this.approve_agreement_container.querySelector("#btn-open-terms");
        this.additional_legal_terms = document.querySelector("#additional-legal-terms");
        
        this.bottom_bar = document.querySelector("#bottom-bar");            
        this.bottom_bar_left = this.bottom_bar.querySelector("#left-side");
        this.left_btn_action = this.bottom_bar_left.querySelector("#btn-action");
        this.left_btn_action_txt = this.bottom_bar_left.querySelector("#btn-action > .txt");
        this.upload_to_server_spinner = this.bottom_bar.querySelector("#upload-to-server-spinner");
        this.bottom_bar_right = this.bottom_bar.querySelector("#right-side");
        this.back_to_doc = this.bottom_bar.querySelector("#back-to-doc");
        this.signatures_validator = this.bottom_bar.querySelector("#signatures-validator");

        this.logo_container = document.querySelector("#logo-container");
        this.logo_image = this.logo_container.querySelector(".img");        
    }

    set_language_styles () {        
        this.white_board.classList.add(this.language);
        this.bottom_bar.classList.add(this.language);             
        this.logo_container.classList.add(this.language);
    }

    set_mobile_styles() {        
        window.document.body.style.position = "absolute";
        this.bottom_bar.classList.add("mobile-images");
        this.white_board.classList.add("mobile-images");
        this.logo_container.classList.add("mobile-images");
    }

    show_logo() {
        this.logo_container.style.display = this.logo_image.style.display = "block";
    }
    
    hide_logo() {
        this.logo_container.style.display = this.logo_image.style.display = "none";
    }

    show_demo_labels() {
        window.document.body.querySelectorAll(".demo-item").forEach(item => item.style.display = "block");
    }

    hide_demo_labels() {
        window.document.body.querySelectorAll(".demo-item").forEach(item => item.style.display = "none");    
    }

    is_customer_with_logo(business_name) {
        return (business_name.includes("הזנפרץ")=== true || 
                business_name === "Sivan Gvili" || 
                business_name.includes("חקימוביץ") === true || 
                business_name === "goodsign");
    }

}

module.exports = StepBase;