import jQuery from "jquery";
import StepBase from './StepBase';
import axios from "axios";
import ConfirmModal from "../modalDialogs/ConfirmModal";
import AlertModal from "../modalDialogs/AlertModal";
import imageCompression from 'browser-image-compression';

class StepFilesUpload extends StepBase {
    constructor (
        parent,
        env, 
        is_demo, 
        isMobile, 
        language,
        embedded_images, 
        journey_id,
        enduser_details, //TODO: need to delete
        business_name, 
        reusable_doc,
        DEBUG_EMULATE_SERVER) {
        
        super(env, language, isMobile, is_demo);
        this.parent = parent;
        this.DEBUG_EMULATE_SERVER = DEBUG_EMULATE_SERVER;
        this.embedded_images = embedded_images;
        this.num_required_images = this.count_required_images();
        this.journey_id = journey_id; 
        this.enduser_details = enduser_details;
        this.business_name = business_name;
        this.reusable_doc = reusable_doc; 
        this.files_upload_list = this.white_board.querySelector("#files-upload-list");
        this.choose_file_constainer = this.white_board.querySelector("#choose-file-container");
        this.choose_title = this.choose_file_constainer.querySelector(".choose-header > .txt");
        this.choose_close_btn = this.choose_file_constainer.querySelector("#choose-close-btn");
        this.btn_open_file_dialog = this.choose_file_constainer.querySelector("#btn-choose-file");
       
        this.allowed_file_types = ["image/png","image/jpg","image/jpeg"];
        const allowed_file_extensions = [".png",".jpg",".jpeg"];
        this.finput = document.querySelector("#file-upload");
        this.finput.setAttribute("accept", allowed_file_extensions.join());
        this.drop_zone = document.querySelector('.drop-zone');
        this.upload_validator = document.querySelector('.upload-validator');
        this.abort_upload_ctrl = null;
        this.confirm_modal = new ConfirmModal();
        this.alert_modal = new AlertModal();
        this.rendered = false;
        this.image_compress_options = {
            maxSizeMB: 0.5
        };
    }

    render () {
        console.log("render called on step Files Upload");        
        this.disable_body_scrolling();
                       
        this.white_board.style.display = "flex";
        this.white_board_title.style.display = "block";
        this.white_board_title.innerHTML = this.get_title();
        
        if(this.isMobile === true) {
           this.render_additional_mobile_styles();
        }

        this.white_board_subtitle.style.display = "block";
        this.white_board_subtitle.innerHTML = this.get_subtitle();              
        this.approve_agreement_container.style.display = "none";

        this.files_upload_list.style.display = "inline-flex";
        
        this.bottom_bar.style.display = "block";

        this.bottom_bar_left.style.display = "none";        
        this.left_btn_action.style.display = "none";
        this.left_btn_action_txt.innerHTML = this.language === "heb" ? "הבא" : "Next";        
        this.left_btn_action.querySelector(".fas").classList.remove("fa-chevron-circle-left", "fa-chevron-circle-right", "fa-paper-plane", "fa-external-link-alt", "fa-file-pdf");
        this.left_btn_action.querySelector(".fas").classList.add(this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right");
        this.signatures_validator.innerHTML = this.get_bottom_text();
        
        this.bottom_bar_right.style.display = "block";  
        this.back_to_doc.querySelector(".txt").innerText = this.language === "heb" ? "חזרה למסמך" : "back to doc'";
        this.back_to_doc.querySelector(".fas").classList.add(this.language === "heb" ? "fa-long-arrow-alt-right" : "fa-long-arrow-alt-left");              

        this.hide_logo();
        const is_customer_with_logo = this.is_customer_with_logo(this.business_name);
        if( is_customer_with_logo === true) {
            this.show_logo();
        }

        this.upload_status = "done";
        if(this.rendered === false) {
            this.append_files_items_to_list();
            this.bind_events();
        }
        this.rendered = true;

        const is_complete = this.check_completion();
        console.log(`ending of render function, calling is_complete: ${is_complete === true ? "true" : "false"}`);
        if(is_complete === true) {
            console.log(`is_complete equals true block`);
            this.bottom_bar_left.style.display = "flex";        
            this.left_btn_action.style.display = "flex";
        }
    }

    unrender() {  
        this.files_upload_list.style.display = "none";      
        if(this.isMobile === true) {
            this.remove_additional_mobile_styles();
        }   
    }

    append_files_items_to_list() {        
        this.embedded_images.items.forEach( item => {           
            let new_item = null;
            if(item.required === true) {
                if(this.isMobile === false) {
                    new_item = this.render_file_upload_item_desktop(item);
                } else {
                    new_item = this.render_file_upload_item_mobile(item);
                }
                this.files_upload_list.appendChild(new_item);        
            }    
        }); 
        
        this.embedded_images.items.forEach( item => {           
            let new_item = null;
            if(item.required === false) {
                if(this.isMobile === false) {
                    new_item = this.render_file_upload_item_desktop(item);
                } else {
                    new_item = this.render_file_upload_item_mobile(item);
                }
                this.files_upload_list.appendChild(new_item);     
            }      
        }); 
    }
    
    render_file_upload_item_mobile(data_item) {
        const new_item = document.createElement("div");
        new_item.classList.add("file-upload-item-mobile","before-upload");
       
        new_item.dataset.itemName = data_item.name; 

        const txt = document.createElement("div");
        txt.classList.add("txt");
        txt.innerHTML = `${data_item.required === true ? "<span class='required-embedded-image'>*</span>" : ""} ${data_item.name}`;
        const file_progress_container = document.createElement("div");
        file_progress_container.classList.add("file-progress-container-mobile");
        this.append_progress_item_mobile(file_progress_container); 

        const btn_before = this.render_before_upload_button_mobile();

        new_item.appendChild(txt);
        new_item.appendChild(btn_before);
        new_item.appendChild(file_progress_container);
        return new_item;
    }

    render_file_upload_item_desktop(data_item) {
        const new_item = document.createElement("div");
        new_item.classList.add("file-upload-item");
        new_item.classList.add("before-upload");
       
        new_item.dataset.itemName = data_item.name;
        new_item.title = data_item.name;

        const clickable_container = document.createElement("div");
        clickable_container.classList.add("clickable-container");

        const btn_before = this.render_before_upload_button();
        clickable_container.appendChild(btn_before);
        
        const btn_after = this.render_after_upload_button();
        clickable_container.appendChild(btn_after);

        const txt = document.createElement("div");
        txt.classList.add("txt", "disable-button");
        txt.innerHTML = `${data_item.required === true ? "<span class='required-embedded-image'>*</span>" : ""} ${data_item.name}`;
        clickable_container.appendChild(txt);
        
        const file_progress_container = document.createElement("div");
        file_progress_container.classList.add("file-progress-container-desktop");
        this.append_progress_item_desktop(file_progress_container); 
                
        new_item.appendChild(clickable_container);
        new_item.appendChild(file_progress_container);

        return new_item;
    }

    append_progress_item_desktop(container) {        
        const html =  
            `<div class="progress-item">
                <div class="file-icon">
                    <i class="far fa-image"></i> 
                </div>
                <div class="file-details-and-progress">
                    <div class="first-row">
                        <div class="file-desc file-name"></div>
                        <div class="file-desc file-size"></div>
                    </div>                            
                    <div class="second-row">
                        <progress class="progress" value="0" max="100"></progress>
                    </div>                    
                </div>
                <div class="btn-trash-container">
                    <i class="fas fa-trash"></i>
                    <i class="delete-spinner fas fa-spinner fa-spin"></i>                                        
                </div>
            </div>`;
        
        const frag = document.createRange().createContextualFragment(html);
        container.appendChild(frag);
    }

    append_progress_item_mobile(container) {
        const html =  
            `<div class="progress-item-mobile">                
                <progress class="progress" value="50" max="100"></progress>
                <div class="upload-complete">
                    <span class="file-size"></span>
                    <i class='fas fa-check'></i>
                </div> 
                <div class="btn-trash-container">
                    <i class="fas fa-trash"></i>
                    <i class="delete-spinner fas fa-spinner fa-spin"></i>                                        
                </div>
            </div>`;

        const frag = document.createRange().createContextualFragment(html);
        container.appendChild(frag);
    }

    bind_events () { 
       
        this.files_upload_list.querySelectorAll('.fa-trash').forEach( item => {
            item.addEventListener('click', e => {
                const selected_container = this.isMobile === false ? 
                    e.target.closest(".file-upload-item"):
                    e.target.closest(".file-upload-item-mobile");
                this.abort_or_delete_upload(selected_container)
            },true);
        });
        
        if(this.isMobile === false) {
            this.files_upload_list.querySelectorAll('.clickable-container').forEach(item => {
                item.addEventListener('click', e => {
                    const parent_node = e.target.parentNode;
                    if(parent_node.classList.contains("after-upload") === false) {
                        this.handle_show_choose_container(e);
                    }
                },true);
            });
        
            this.choose_close_btn.addEventListener('click', e => {
                this.hide_validator_error();
                this.handle_hide_choose_container(e);
            },true);

            this.btn_open_file_dialog.addEventListener('click', e => {                
                this.finput.click();               
            });

            this.drop_zone.addEventListener('dragover', e => {
                e.stopPropagation();
                e.preventDefault();
                e.dataTransfer.dropEffect = 'copy';
            });

            this.drop_zone.addEventListener('dragenter', () => {
                this.drop_zone.classList.add("animate-border");
                this.drop_zone.style.backgroundColor = "rgba(88, 106, 195, .5)";
                const bounce_text = this.drop_zone.querySelector(".bounce-text");               
                bounce_text.classList.add("bounce");
                bounce_text.innerText = "שחרר כאן";
                
            }, false);

            this.drop_zone.addEventListener('dragleave', () => {
                this.drop_zone.classList.remove("animate-border");
                this.drop_zone.style.backgroundColor = "rgba(88, 106, 195, .2)";
                const bounce_text = this.drop_zone.querySelector(".bounce-text");
                bounce_text.classList.remove("bounce");
                bounce_text.innerText = "גרור קובץ לכאן";
            }, false);

            this.drop_zone.addEventListener('drop', async (e) => {
                e.stopPropagation();
                e.preventDefault();
                            
                const file = e.dataTransfer.files[0];
                this.hide_validator_error();
                const validate = this.validate_file(file);
                if(validate.ok === true) { 
                    this.handle_hide_choose_container();
                    this.files_upload_list.querySelectorAll(".clickable-container").forEach( item=> {
                        item.classList.add("disable-button");                
                    });      
                    this.abort_upload_ctrl = await this.upload_file(file);
                } else {
                    this.show_validator_error(validate.err_message);                    
                }

                this.drop_zone.classList.remove("animate-border");
                this.drop_zone.style.backgroundColor = "rgba(88, 106, 195, .2)";
                const bounce_text = this.drop_zone.querySelector(".bounce-text");
                bounce_text.innerText = "גרור קובץ לכאן";
            }, false); 
        }

        if(this.isMobile === true) {
            this.files_upload_list.querySelectorAll('.btn-before-mobile').forEach(item => {
                item.addEventListener('click', e => {
                    this.choose_title.innerText = e.target.parentNode.dataset.itemName;
                    this.finput.click();               
                },true);
            });
        }

        this.finput.addEventListener('change', e => { 
            if(this.finput.value !== "") {
                const file = e.target.files[0];            
                this.hide_validator_error();
                const validate = this.validate_file(file);            
                if(validate.ok === true) { 
                    this.handle_hide_choose_container();           
                    this.abort_upload_ctrl = this.upload_file(file);
                } else {
                    this.show_validator_error(validate.err_message);
                    this.finput.value = "";     
                }
            }
        });
        
        this.back_to_doc.addEventListener("click", () => {            
            this.parent.handle_go_to_stage_two({review_doc: true});                
        }, false);
    }
        
    async upload_file (file) {
        const controller = new AbortController();
        let file_to_upload = null;
        
        if(file.size > 524288 /*2097152*/) {
            file_to_upload = await imageCompression(file, this.image_compress_options);
        } else {
            file_to_upload = file;
        }

        this.upload_status = "in-progress";
        let selected_container = this.files_upload_list.querySelector(`[data-item-name="${this.choose_title.innerText}"]`);
        if(this.isMobile === false) {
            //init progress control with file name and file size
            this.init_progress_items_from_file_desktop(selected_container, file_to_upload.name, file_to_upload.size);
            selected_container.querySelector(".second-row").style.display = "block"; 
        } else {
            this.init_progress_items_from_file_mobile(selected_container, file_to_upload.size); 
        } 

        this.files_upload_list.querySelectorAll('.file-upload-item.after-upload, .file-upload-item-mobile.after-upload').forEach(item => {
            item.querySelector(".fa-trash").style.opacity = 0.4;
            item.querySelector(".fa-trash").classList.add("disable-button");
        });

        const progress = selected_container.querySelector(".progress");

        const formData = new FormData();
        formData.append("embedded-image", file_to_upload);
        formData.append("customer_name",this.business_name);
        formData.append("journey_id", this.journey_id);
        const url = this.get_url(this.ENV, "/upload-embedded-image");
        if(this.DEBUG_EMULATE_SERVER === false) {
            axios.post(url, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                onUploadProgress: function (progressEvent) {
                    const value = progressEvent.loaded/progressEvent.total * 100;
                    const fixed_value = Number(value.toFixed(0)); 
                    progress.value = fixed_value;                       
                },
                signal: controller.signal
            }).then( response => {                    
                this.handle_upload_success(response, selected_container, file_to_upload);
            }).catch( e => {
                this.handle_upload_failure(e, selected_container);
            });
        } else {
            setTimeout(() => {
                const fake_response = {
                    data: {
                        aes_token: "xyz123"
                    }
                };
                this.handle_upload_success(fake_response, selected_container, file_to_upload);
            },3000);
        }

        return controller;
    }

    handle_upload_failure(e, selected_container) {
        // Check the exception is was caused request cancellation
        this.upload_status = "done";
        if(e.code !== "ERR_NETWORK") {
            if (e.message === 'canceled') { 
                const progress_container = this.isMobile === false ? 
                    selected_container.querySelector(".file-progress-container-desktop"): 
                    selected_container.querySelector(".file-progress-container-mobile");

                jQuery(progress_container).animate({opacity: 0}, 800, ()=>{
                    this.isMobile === false ? 
                        progress_container.style.visibility = "hidden" : 
                        progress_container.style.display = "none";                  

                    progress_container.style.opacity = 1;

                    if(this.isMobile === false) {                   
                        this.files_upload_list.querySelectorAll(".clickable-container").forEach( item=> {
                            item.classList.remove("disable-button");            
                        });
                    } else {
                        selected_container.querySelector(".btn-before-mobile").style.display = "flex";
                        this.files_upload_list.querySelectorAll(".btn-before-mobile").forEach( item=> {
                            item.classList.remove("disable-button");            
                        }); 
                    }

                    this.files_upload_list.querySelectorAll('.file-upload-item.after-upload, .file-upload-item-mobile.after-upload').forEach(item => {
                        item.querySelector(".fa-trash").style.opacity = 1;
                        item.querySelector(".fa-trash").classList.remove("disable-button");
                    });
                });
            }
        } else {
            this.handle_server_error();
        } 
    }

    handle_upload_success(response, selected_container, file) {
        this.upload_status = "done";            
        this.finput.value = "";
        selected_container.classList.remove("before-upload");
        selected_container.classList.add("after-upload");

        if(this.isMobile === false) {
            this.files_upload_list.querySelectorAll(".clickable-container").forEach( item=> {
                item.classList.remove("disable-button");            
            });

            const second_row = selected_container.querySelector(".second-row");
            jQuery(second_row).animate({opacity: 0}, 500, ()=>{
                second_row.style.display = "none";
                second_row.style.opacity = 1;
            });

            const btn_abort_or_delete = selected_container.querySelector(".fa-trash");
            const title = `מחק ${selected_container.dataset.itemName}`;
            btn_abort_or_delete.setAttribute("title", title);
        } else {
            const progress = selected_container.querySelector(".progress");
            jQuery(progress).animate({opacity: 0}, 400, ()=>{
                progress.style.display = "none";
                progress.style.opacity = 1;
                selected_container.querySelector(".upload-complete").style.display = "flex";
            });

            //enable file upload list items clicks
            this.files_upload_list.querySelectorAll(".btn-before-mobile").forEach( item=> {
                item.classList.remove("disable-button");            
            }); 
        }

        this.files_upload_list.querySelectorAll('.file-upload-item.after-upload, .file-upload-item-mobile.after-upload').forEach(item => {
            item.querySelector(".fa-trash").style.opacity = 1;
            item.querySelector(".fa-trash").classList.remove("disable-button");
        });
        
        this.update_step_state(selected_container, response.data.aes_token, file, Date.now());
        const is_complete = this.check_completion();
        if(is_complete === true) {
            this.bottom_bar_left.style.display = "flex";        
            this.left_btn_action.style.display = "flex";
        }
    }
    
    init_progress_items_from_file_desktop(selected_container, file_name, file_size) {
       
        selected_container.querySelector(".file-name").innerText = this.formatFilename(file_name);
        selected_container.querySelector(".file-name").title = file_name;
        selected_container.querySelector(".file-size").innerText = this.formatBytes(file_size);
        
        const btn_abort_or_delete = selected_container.querySelector(".fa-trash");
        btn_abort_or_delete.setAttribute("title", "בטל הוספה");
         //show the progress control         
         selected_container.querySelector(".file-progress-container-desktop").style.visibility = "visible";
         //disable file upload list items clicks
         this.files_upload_list.querySelectorAll(".clickable-container").forEach( item=> {
             item.classList.add("disable-button");            
         });        
    }

    init_progress_items_from_file_mobile(selected_container, file_size) {
        selected_container.querySelector(".btn-before-mobile").style.display = "none";
        selected_container.querySelector(".file-size").innerText = this.formatBytes(file_size);
        //show the progress control
         selected_container.querySelector(".file-progress-container-mobile").style.display = "flex";
         //disable file upload list items clicks
         this.files_upload_list.querySelectorAll(".btn-before-mobile").forEach( item=> {
             item.classList.add("disable-button");            
         });  
    }
    
    abort_or_delete_upload (selected_container) {
        if(this.upload_status === "in-progress") {
            this.abort_upload_ctrl.abort();
        } else if(this.upload_status === "done") {
            this.handle_delete_embedded_image(selected_container);
        }
    }

    handle_delete_embedded_image(selected_container) {        
        const msg = `האם למחוק את <span style="font-weight:bold">${selected_container.dataset.itemName}</span>?`; 
        this.confirm_modal.open(msg, () => {
            this.approve_delete_image(selected_container)
        });
    }

    approve_delete_image(selected_container) {
        
        let current_state_item = this.embedded_images.items.find(item => {
            return item.name === selected_container.dataset.itemName;
        });
        selected_container.querySelector(".fa-trash").style.display = "none";
        selected_container.querySelector(".delete-spinner").style.display = "block";

        this.files_upload_list.querySelectorAll('.file-upload-item.after-upload, .file-upload-item-mobile.after-upload').forEach(item => {
            item.querySelector(".fa-trash").style.opacity = 0.4;
            item.querySelector(".fa-trash").classList.add("disable-button");
        });

        const url = this.get_url(this.ENV, "/delete-embedded-image");
        if(this.DEBUG_EMULATE_SERVER === false) {
            axios.delete(url, {
                params: {
                    token: current_state_item.token,
                    journey_id: this.journey_id
                }   
            }).then( response => {                    
                if(response.data.success === true) {
                    this.handle_delete_success(selected_container);
                }
            }).catch( e => {
                this.handle_server_error();
            });
        } else {
            setTimeout(() => {
                this.handle_delete_success(selected_container);
            },3000);
        }
    }
   
    handle_delete_success(selected_container) {

        selected_container.querySelector(".fa-trash").style.display = "block";
        selected_container.querySelector(".delete-spinner").style.display = "none";
        selected_container.classList.remove("after-upload");
        selected_container.classList.add("before-upload");
        selected_container.querySelector(".progress").value = 0;

        this.files_upload_list.querySelectorAll('.file-upload-item.after-upload, .file-upload-item-mobile.after-upload').forEach(item => {
            item.querySelector(".fa-trash").style.opacity = 1;
            item.querySelector(".fa-trash").classList.remove("disable-button");
        });

        this.files_upload_list.querySelectorAll(".btn-before-mobile").forEach( item=> {
            item.classList.remove("disable-button");            
        });


        this.isMobile === false ? 
            selected_container.querySelector(".file-progress-container-desktop").style.visibility = "hidden" : 
            selected_container.querySelector(".file-progress-container-mobile").style.display = "none" ;

        if(this.isMobile === true) {
            selected_container.querySelector(".btn-before-mobile").style.display = "flex";            
        }

        this.update_step_state(selected_container, "", {name: "", size: ""}, null);
        const is_complete = this.check_completion();
        if(is_complete === true) {
            this.bottom_bar_left.style.display = "flex";        
            this.left_btn_action.style.display = "flex";
        } else {
            this.bottom_bar_left.style.display = "none";        
            this.left_btn_action.style.display = "none";
        }   
    }

    handle_show_choose_container(event) {
        this.choose_title.innerText = event.target.parentNode.dataset.itemName;
        this.choose_file_constainer.style.display = "flex";

        this.files_upload_list.querySelectorAll(".clickable-container").forEach( item=> {
            item.classList.add("disable-button");
            jQuery(item).animate({opacity: 0.3}, 500);    
        });
        
        jQuery(this.choose_file_constainer).animate({opacity: 1}, 500);
    }

    handle_hide_choose_container() { 
        
        this.drop_zone.querySelector(".bounce-text").classList.remove("bounce");
        jQuery(this.choose_file_constainer).animate({opacity: 0}, 500, () => {
            this.choose_file_constainer.style.display = "none";
            //this.choose_title.innerText = "";        
        });

        this.files_upload_list.querySelectorAll(".clickable-container").forEach( item=> {
            item.classList.remove("disable-button");
            jQuery(item).animate({opacity: 1}, 500);        
        });
    }

    update_step_state(selected_container, token, file) {
        const item_name = selected_container.dataset.itemName;
        let current_state_item = this.embedded_images.items.find(item => {
            return item.name === item_name;
        });
        
        current_state_item.token = token;
        current_state_item.dt = Date.now();
        current_state_item.file_name = file.name;
        current_state_item.file_size = file.size;
    }

    check_completion() {
        let counter = 0, result = false;

        this.embedded_images.items.forEach(item => {
            if(item.required === true && item.token !=="") {
                counter++;
            }
        });

        result = counter === this.num_required_images ? true : false;
        return result;
    }

    formatBytes(bytes)  {
        if (bytes === 0) return '0 Bytes';
        const k = 1024;
        const dm = 1;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    formatFilename(name) {
        let result = "";
        let split_arr = name.split('.');
        const fExtention = split_arr.pop();               
        let fName = split_arr.join("."); 
        
        //if file name without extension contains more than 15 characters   
        if(fName.length > 14){
            result = fName.substr(0,7) + "..." + fName.substr(-7) + "." + fExtention;
        } else {
            result = name;
        } 
        return result;                
    }

    validate_file(file) {
        let result = { ok: true, err_message: "" };
        if(this.allowed_file_types.indexOf(file.type) === -1) {
            result.err_message = "יש לצרף תמונה מסוג png או jpg בלבד";
            result.ok = false;
        } 
        return result;
    }

    get_title () {
        return "<span class='step-file-upload'>תודה על מילוי המסמך</span><i class='far fa-thumbs-up'></i>";             
    }

    get_subtitle() {
        let result = `<span class="step-file-upload">${this.embedded_images.title}:</span>`;
        
        return result;
    }

    handle_server_error() {
        this.bottom_bar_right.style.display = "none";  
        this.white_board_title.innerHTML = this.language === "heb" ? "לצערנו אירעה תקלה במערכת <i class='icon-warning-sign'></i>" : "Oooops, we have a system error <i class='icon-warning-sign'></i>";
        this.white_board_subtitle.innerHTML = this.language === "heb" ? "<span class='gray'>נסה לחתום שוב על המסמך מאוחר יותר</span>" : "<span class='gray'>Please try to sign the document later</span>";
        this.signatures_validator.innerHTML = this.language === "heb" ? "נסה לחתום שוב על המסמך מאוחר יותר" : "Please try to sign the document later";
        this.files_upload_list.style.display = "none";
        this.choose_file_constainer.style.display = "none";
    }

    show_validator_error(msg) {
        if(this.isMobile === false) {
            this.upload_validator.innerText = msg;
            this.upload_validator.style.visibility = "visible";
        } else {
            this.alert_modal.open(msg);
        }
    }

    hide_validator_error() {        
        this.upload_validator.style.visibility = "hidden";
    }

    render_before_upload_button() {
        const result = document.createElement("div");
        result.classList.add("btn-before", "disable-button");
        result.innerHTML = "<i class='fas fa-upload'></i>";
        return result;
    }

    render_before_upload_button_mobile() {
        const result = document.createElement("div");
        result.classList.add("btn-before-mobile");
        result.innerHTML = "<i class='fas fa-upload disable-button'></i>";
        return result;
    }

    render_after_upload_button() {
        const result = document.createElement("div");
        result.classList.add("btn-after", "disable-button");
        result.innerHTML = "<i class='fas fa-check'></i>";
        return result;
    } 
    
    render_additional_mobile_styles() {
        if(this.embedded_images.items.length >=3) {
            this.white_board.style.alignItems = "end";
            this.white_board.querySelector(".inner").style.marginBottom = "100px";
        }
    }

    remove_additional_mobile_styles() {
        if(this.embedded_images.items.length >=3) {
            this.white_board.style.alignItems = "center";
            this.white_board.querySelector(".inner").style.marginBottom = "inherit";
        }
    }

    get_bottom_text () {
        let result = "";
        if(this.isMobile === false) {
            result = this.language === "heb" ? "לחץ על  <b>הבא</b> להמשיך" : "Click <b>Next</b> to continue";        
        } else {
            result = this.language === "heb" ? "<span style='font-size:.85em'>לחץ על <b>הבא</b><br> להמשך</span>" : "Click <b>Next</b> to continue";        
        }

        return result; 
    }

    count_required_images() {
        let result = 0;

        this.embedded_images.items.forEach(item => {
            if(item.required === true) {
                result++;
            }
        });        
        return result;
    }

    count_images() {
        const all_images_count = this.embedded_images.items.length;
        const required_images_count = this.count_required_images();
        return { all_images_count, required_images_count };
    }
}

module.exports = StepFilesUpload;