import Logs from "./Logs";
import stringDirection from "string-direction";
import SignaturePositioningTool from "./SignaturePositioningTool";

class PayloadForServer {
    constructor () {
        if(!PayloadForServer.instance) {
            PayloadForServer.instance = this;
            this.paper_properties = SignaturePositioningTool.instance.get_paper_properties();
            
            this.logs = new Logs();
            this.payload = null;            
        }
        return PayloadForServer.instance;
    }    

    fill_payload_with_pages_and_placeholders() {
        let pages_collection = document.querySelectorAll('.image-wrapper');
        pages_collection.forEach( page => {
            
            const main_img = page.querySelector(".data-image");
            let page_payload = { 
                main_image_src: main_img.src, 
                checkboxes: [], 
                signatures: [], 
                text_rows: [],
                date_rows:[],
                text_areas: [],
                static_signatures: [],
                static_text_rows: [] 
            };

            this.collect_signatures_from_page(page, page_payload, this.payload.is_multiple_endusers); 

            if(this.payload.is_multiple_endusers === false) {
                this.collect_date_rows_from_page(page, page_payload, this.payload.user_language);   
                this.collect_text_rows_from_page(page, page_payload);             
                this.collect_text_areas_from_page(page, page_payload);
                this.collect_checkboxes_from_page(page, page_payload);             
                this.collect_static_signatures_from_page(page, page_payload);
                this.collect_static_text_row_from_page(page, page_payload);
            }
            this.payload.pages.push(page_payload);                      
        });
        this.payload.logs = this.logs.collect_logs_from_placeholders();    
    }
   

    fill_payload_with_embedded_images(embedded_images_items) {
        embedded_images_items.forEach(item => {
            if(item.token !== "") {
                const embedded_image_item = { name: item.name, token: item.token, dt: item.dt };
                this.payload.embedded_images.push(embedded_image_item);            
            }
        });
        let embedded_images_logs = this.logs.collect_logs_from_embedded_images(embedded_images_items);
        this.payload.logs = [...this.payload.logs, ...embedded_images_logs];
    }

    collect_checkboxes_from_page(page, page_payload) {
        const checkboxes = page.querySelectorAll(".checkbox-placeholder"); 
        checkboxes.forEach( item => {
            const innerCheckbox = item.querySelector(".unchecked");
            const font_size = Number(innerCheckbox.style.fontSize.replace("px", ""));
            const top_pt = Number(item.style.top.replace("pt", "")).toFixed(1);
            const font_size_pt = Number((font_size * 3/4).toFixed(1));            
            const checkbox_right = Number(item.style.right.replace("pt", "")); 
            const left_pt = (this.paper_properties.PT_WIDTH - (checkbox_right + font_size_pt)).toFixed(1);
            
            page_payload.checkboxes.push({
                checked: item.dataset.isDirty,
                dt: item.dataset.dt,                                        
                left_pt: left_pt, 
                top_pt: top_pt, 
                font_size_pt: font_size_pt              
            });                             
        });
    }

    collect_static_text_row_from_page(page, page_payload) {
        const static_text_rows = page.querySelectorAll(".text-in-advance-placeholder");             
        static_text_rows.forEach(item => {
            const { width, height } = item.getBoundingClientRect();
            const width_pt = Number((width * 3/4).toFixed(1));
            const height_pt = height * 3/4;
            const top_pt = (Number(item.style.top.replace("pt", "")) + height_pt/2).toFixed(1);
            const right_pt = Number(item.style.right.replace("pt", "")); 
            const left_pt = (this.paper_properties.PT_WIDTH - (right_pt + width_pt)).toFixed(1);
            const text = item.innerText;
            const direction = stringDirection.getDirection(text);
            const font_size_pt = Math.round(Number(item.style.fontSize.replace("px", "")) * 3/4);

            page_payload.static_text_rows.push({
                direction: direction,
                text: text, 
                left_pt: left_pt, 
                top_pt: top_pt, 
                font_size_pt: font_size_pt
            });                                   
        });
    }   

    collect_static_signatures_from_page(page, page_payload) {
        const static_signatures = page.querySelectorAll("[data-tx-name=in-advance-signature]");             
        static_signatures.forEach(item => {
            let { width, height } = item.getBoundingClientRect();
            const top_pt = Number(item.style.top.replace("pt", "")).toFixed(1);
            const width_pt = Number((width *3/4).toFixed(1));
            const height_pt = (height *3/4).toFixed(1);
            const sign_right = Number(item.style.right.replace("pt", "")); 
            const left_pt = (this.paper_properties.PT_WIDTH - (sign_right + width_pt)).toFixed(1);
            
            page_payload.static_signatures.push({
                src: item.src, 
                left_pt: left_pt, 
                top_pt: top_pt, 
                width_pt: width_pt, 
                height_pt: height_pt
            });              
        });  
    }

    collect_text_areas_from_page(page, page_payload) {
        const text_areas = page.querySelectorAll(".text-area-placeholder[data-is-dirty=true]");    
        text_areas.forEach(item => {
            const { width, height } = item.getBoundingClientRect();
            const width_pt = Number((width * 3/4).toFixed(1));
            const height_pt = height * 3/4;
            const top_pt = Number(item.style.top.replace("pt", ""));
            const right_pt = Number(item.style.right.replace("pt", "")); 
            const left_pt = Number((this.paper_properties.PT_WIDTH - (right_pt + width_pt)).toFixed(1));
            const text = item.querySelector(".inner").innerText;
            const direction = stringDirection.getDirection(text);
            const font_size_pt = Math.round(Number(item.style.fontSize.replace("px", "")) * 3/4);

            page_payload.text_areas.push({
                dt: item.dataset.dt,
                direction: direction,
                text: text, 
                container_width_pt: width_pt,                
                container_height_pt: height_pt,                
                container_left_pt: left_pt, 
                container_top_pt: top_pt.toFixed(1),
                text_left_pt: (direction === "rtl" || direction === "bidi") ? left_pt + width_pt - 6 : left_pt + 6,
                text_top_pt: (top_pt + 8).toFixed(1),//plus the padding
                text_width_pt: width_pt - 6,//minus the padding
                font_size_pt: font_size_pt
            });                                   
        });
    }

    collect_text_rows_from_page(page, page_payload) {
        const text_rows = page.querySelectorAll(".text-field-placeholder[data-is-dirty=true]");    
        text_rows.forEach(item => {
            const { width, height } = item.getBoundingClientRect();
            const width_pt = Number((width * 3/4).toFixed(1));
            const height_pt = height * 3/4;
            const top_pt = (Number(item.style.top.replace("pt", "")) + height_pt/2).toFixed(1);
            const right_pt = Number(item.style.right.replace("pt", "")); 
            
            const text = item.querySelector(".inner").innerText;
            const direction = stringDirection.getDirection(text);

            let left_pt = 0;
            if(direction == "ltr") {              
                left_pt = (this.paper_properties.PT_WIDTH - (right_pt + width_pt)).toFixed(1);
            } else {
                left_pt = (this.paper_properties.PT_WIDTH - right_pt).toFixed(1);
            }          
            
            const font_size_pt = Math.round(Number(item.style.fontSize.replace("px", "")) * 3/4);

            page_payload.text_rows.push({
                dt: item.dataset.dt,
                direction: direction,
                text: text, 
                font_weight: item.style.fontWeight,
                left_pt: left_pt, 
                top_pt: top_pt, 
                font_size_pt: font_size_pt
            });                                   
        });
    }

    collect_date_rows_from_page(page, page_payload, language) {
        const date_rows = page.querySelectorAll(".date-field-placeholder[data-is-dirty=true]");    
        date_rows.forEach(item => {
            const { width, height } = item.getBoundingClientRect();
            const width_pt = Number((width * 3/4).toFixed(1));
            const height_pt = height * 3/4;
            const top_pt = (Number(item.style.top.replace("pt", "")) + height_pt/2).toFixed(1);
            const right_pt = Number(item.style.right.replace("pt", "")); 
            
            const text = item.querySelector(".span-date-str").innerText;
            const direction = language === "eng" ? "ltr" : "rtl";

            let left_pt = 0;
            if(direction == "ltr") {              
                left_pt = (this.paper_properties.PT_WIDTH - (right_pt + width_pt)).toFixed(1);
            } else {
                left_pt = (this.paper_properties.PT_WIDTH - right_pt).toFixed(1);
            } 

            const font_size_pt = Math.round(Number(item.style.fontSize.replace("px", "")) * 3/4);

            page_payload.date_rows.push({
                dt: item.dataset.dt,
                direction: direction,
                text: text, 
                font_weight: item.style.fontWeight,
                left_pt: left_pt, 
                top_pt: top_pt, 
                font_size_pt: font_size_pt
            });                                   
        });
    }

    collect_signatures_from_page(container, page_payload, is_multiple_endusers) {
        const signatures = container.querySelectorAll(".sign-here-placeholder"); 
        signatures.forEach(item => {
            let { width, height } = item.getBoundingClientRect();
            const top_pt = Number(item.style.top.replace("pt", "")).toFixed(1);
            const width_pt = Number((width * 3/4).toFixed(1));
            const height_pt = (height * 3/4).toFixed(1);
            const sign_right = Number(item.style.right.replace("pt", "")); 
            const left_pt = (this.paper_properties.PT_WIDTH - (sign_right + width_pt)).toFixed(1);
            
            page_payload.signatures.push({
                    is_shadow: false,
                    signature_db_id: item.dataset.signatureId,
                    dt: item.dataset.dt,
                    src: item.src, 
                    left_pt: left_pt, 
                    top_pt: top_pt, 
                    width_pt: width_pt, 
                    height_pt: height_pt,
                    opacity: item.classList.contains("sign-opaque") === false ? "ALPHA_NORMAL" : "ALPHA_OPAQUE"
                }
            );                             
        });

        if(is_multiple_endusers === true) {
            const shadow_signatures = container.querySelectorAll(".sign-here-placeholder-shadow");
            shadow_signatures.forEach(item => {
                page_payload.signatures.push({
                    is_shadow: true,
                    signature_db_id: item.dataset.signatureId
                });
            }); 
        }
    }

    initialize_payload (
        journey_id, 
        server_token, 
        language, 
        business_name, 
        business_email_address,
        business_ip_address,
        doc_name, 
        customer_fullname, 
        enduser_email,
        enduser_contact_value,
        business_self_sign,
        review_doc_course,
        is_demo,
        is_multiple_endusers) {
       
            this.payload = { 
                journey_id: journey_id,
                token: server_token,
                user_language: language, 
                customer: { 
                    name: business_name, 
                    email: business_email_address, 
                    original_file_name: doc_name,
                    ip_address: business_ip_address
                }, 
                enduser_fullname: customer_fullname,
                enduser_email: enduser_email,
                enduser_contact_value: enduser_contact_value,
                paper_size: SignaturePositioningTool.instance.paper_name,
                pages: [],
                logs: null,
                self_sign: business_self_sign,
                review_doc_course: review_doc_course,
                is_demo: is_demo,
                is_multiple_endusers: is_multiple_endusers,
                embedded_images: []
           };  
    }
}
const instance = new PayloadForServer();
module.exports.instance = instance;