import GoodSignBase from '../GoodSignBase';

class ConfirmModal extends GoodSignBase {
    constructor () {
        super();        
        
        this.container_parent = this.get_overlay_element();     
        this.container = null;        
        this.language = this.get_language();
        this.isMobile = this.is_mobile();

        this.btn_ok = null;
        this.btn_cancel = null;
        this.msg = null;
        this.ok_callback = null;

        this.append_modal_markup_to_its_parent();
        this.init_members_from_appended_markup();
        this.bind_events();
    }

    append_modal_markup_to_its_parent() {
        const html =  
            `<div id="modal-confirm-dialog">
                <div class="confirm-header">
                    <span id="btn-close-confirm"> 
                        <i class="far fa-times-circle fa-3x"></i> 
                    </span>
                </div>
                <div class="confirm-msg">
                                    
                </div>
                <div id="btn-confirm-ok"></div>
                <div id="btn-confirm-cancel"></div>
            </div>`;
        
        const frag = document.createRange().createContextualFragment(html);
        this.container_parent.appendChild(frag);
    }

    init_members_from_appended_markup() {
        this.container = this.container_parent.querySelector("#modal-confirm-dialog");
        this.container.classList.add(this.language);
        this.isMobile && this.container.classList.add("mobile-images");

        this.btn_close_confirm = this.container.querySelector("#btn-close-confirm"); 
        this.btn_ok = this.container.querySelector("#btn-confirm-ok");
        this.btn_cancel = this.container.querySelector("#btn-confirm-cancel");
        this.msg = this.container.querySelector(".confirm-msg");
       
        this.btn_ok.innerText = this.language === "heb" ? "כן" : "OK";
        this.btn_cancel.innerText = this.language === "heb" ? "ביטול" : "Cancel";
    }

    bind_events () {
        this.btn_close_confirm.addEventListener("click", () => {
            this.close();            
        }, false);

        this.btn_cancel.addEventListener("click", () => {
            this.close();            
        }, false);

        this.btn_ok.addEventListener("click", () => {
            this.close();
            this.ok_callback();            
        }, false);
    }

    open (msg, callback) { 
        this.disable_body_scrolling();
        this.show_overlay();
        this.msg.innerHTML = msg;
        this.container.style.display = "grid";
        this.ok_callback = callback;
     }

    close () {
        this.hide_overlay();
        this.container.style.display = "none";
    }

    is_open () {
        return this.container.style.display === "grid";
    }
}

module.exports = ConfirmModal;