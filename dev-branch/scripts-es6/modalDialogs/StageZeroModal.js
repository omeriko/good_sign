import jQuery from "jquery";
import GoodSignBase from '../GoodSignBase';

class StageZeroModal extends GoodSignBase {
    constructor (stepZero, end_user_secret_code, reusable_doc) {
        super();
        this.stepZero = stepZero;
        this.container_parent = this.get_overlay_element();
        
        this.container = null;
        this.language = this.get_language();        
        this.lbl_enduser_name = null;
        this.lbl_secret = null; 
        this.lbl_email = null; 

        this.input_secret  = null;
        this.input_enduser_name  = null;
        this.input_email = null;

        this.btn_ok = null;        
        this.validator_name = null;
        this.validator_secret = null; 
        this.validator_email = null;      
        this.spinner = null;
        this.is_valid_user_details = false;
        this.is_valid_user_secret = false;
        this.email_dynamic_area$ = null;
        this.scenario = "none";
        this.with_email = false;
        this.email_regex_pattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        this.append_modal_markup_to_its_parent();
        this.init_members_from_appended_markup(); 
       
        this.end_user_secret_code = end_user_secret_code;
        this.reusable_doc = reusable_doc;
        this.determine_scenario();        
        this.set_scenario();      
               
        this.bind_events();
    }

    append_modal_markup_to_its_parent() {
        const html = 
            `<div id="modal-stage-zero">
                <div class="header"></div>
                <div class="name-label-container group-name">
                    <label id="lbl-eu-name" class="lbl" for="txt-stage0-enduser-name"></label>
                </div>
                <div class="name-input-container group-name">
                    <input class="txt" id="txt-stage0-enduser-name" type="text" autocomplete="off" maxlength="50" />
                </div>                            
                <div id="name-validator" class="name-validator validator group-name"></div>
                <div class="secret-label-container group-secret">
                    <label id="lbl-secret" class="lbl" for="txt-stage0-secret"></label>
                </div>
                <div class="secret-input-container group-secret">
                    <input class="txt" id="txt-stage0-secret" type="tel" autocomplete="off" maxlength="9"/>
                </div>                            
                <div id="secret-validator" class="secret-validator validator group-secret"></div> 
                
                <div class="email-checkbox-container group-name" data-is-dirty="false">
                    <i class="far fa-square cbx-inner unchecked"></i>  
                    <i class="fas fa-check-square cbx-inner checked btn-approve" style="display: none"></i>  
                    <span class="txt-email-me-copy"></span>
                </div>
                <div class="email-padding group-name"></div>
                <div class="email-dynamic-area">
                    <div class="email-label-container group-email">
                        <label id="lbl-email" class="lbl" for="txt-stage0-email"></label>
                    </div>
                    <div class="email-input-container group-email">
                        <input type="text" class="txt" id="txt-stage0-email" autocomplete="email" maxlength="50"/>
                    </div>   
                    <div id="email-validator" class="email-validator validator group-email"></div>
                </div>
                <div id="btn-stage0-ok" class="btn-container">
                    <span class="lbl"></span>
                    <i id="spinner" class="spinner fas fa-spinner fa-spin" aria-hidden="true"></i>
                </div>               
            </div>`;

        const frag = document.createRange().createContextualFragment(html);
        this.container_parent.appendChild(frag);
    }

    init_members_from_appended_markup() {
        this.container = this.container_parent.querySelector("#modal-stage-zero");
        this.container.classList.add(this.language);
        this.is_mobile() && this.container.classList.add("mobile-images");
        
        this.lbl_enduser_name = this.container.querySelector("#lbl-eu-name");
        this.lbl_secret = this.container.querySelector("#lbl-secret");
        this.lbl_email = this.container.querySelector("#lbl-email");
        
        this.input_secret  = this.container.querySelector("#txt-stage0-secret");
        this.input_enduser_name  = this.container.querySelector("#txt-stage0-enduser-name");        
        this.input_email = this.container.querySelector("#txt-stage0-email");

        this.validator_name = this.container.querySelector("#name-validator");        
        this.validator_secret = this.container.querySelector("#secret-validator");        
        this.validator_email = this.container.querySelector("#email-validator");
        this.email_dynamic_area$ = jQuery(".email-dynamic-area");
        this.btn_ok = this.container.querySelector("#btn-stage0-ok");
        this.spinner = this.container.querySelector("#spinner");    
    }

    bind_events () {
        this.btn_ok.addEventListener("click", () => {
            this.handle_click_ok();
        }, false);

        document.body.addEventListener('keyup', e => {
            if(e.keyCode ===13) { //'enter' key press
                if(this.is_open() === true) {
                    this.trigger_insert_text();                        
                }                      
            }          
        });
        
        this.container.querySelectorAll(".cbx-inner").forEach(item => {
            item.addEventListener("click", e => {
                this.handle_send_email_cbx_click(e.target);                
            });
        });
    }

    handle_send_email_cbx_click(ele_trigger) {
        if(ele_trigger.parentNode.dataset.isDirty === "false") {

            ele_trigger.parentNode.dataset.isDirty = "true"; 
            ele_trigger.parentNode.querySelector(".unchecked").style.display = "none";
            jQuery(ele_trigger.parentNode.querySelector(".checked")).fadeIn(250);           
            const height_expand = this.is_mobile() ? "270px" : "94px";

            this.email_dynamic_area$.animate({ height: height_expand }, 400, () => {
                this.container.querySelectorAll(".group-email").forEach(item => item.style.display = "flex");
                this.input_email.focus();
                this.with_email = true;    
            });
           
        } else if (ele_trigger.parentNode.dataset.isDirty === "true"){
            
            ele_trigger.parentNode.dataset.isDirty = "false";                
            jQuery(ele_trigger.parentNode.querySelector(".checked")).fadeOut(100, function() {
                this.parentNode.querySelector(".unchecked").style.display = "block"    
            });
            this.email_dynamic_area$.animate({height: 0}, 300);
            this.with_email = false;
            this.container.querySelectorAll(".group-email").forEach(item => item.style.display = "none");
        }
    } 

    handle_click_ok() {           
        //TODO: rewrite with promises
        if (this.scenario === "enduser-details") {
            this.validate_user_details();   
            if(this.is_valid_user_details === true) {
                this.close();
            }
            
        } else if(this.scenario === "enduser-secret") {
            this.validate_user_secret(false);            
        } else if(this.scenario === "both") {
            this.validate_user_details();            
            this.validate_user_secret(true); 
        }
    }

    validate_user_secret(check_both) {        
        let txt = this.input_secret.value.trim();
        this.validator_secret.innerText = "";       
        this.is_valid_user_secret = true;
        let what = "";
        if (txt === '') {
            if (this.end_user_secret_code.type === "tz") {
                what = "מס׳ תעודת-הזהות";
            } else if(this.end_user_secret_code.type === "random-number") {
                what = "קד סודי";
            }
            this.input_secret.focus();
            this.validator_secret.innerText = `${this.language === 'heb' ? 'יש להזין' : 'Enter'} ${what}`;
            this.validator_secret.style.display = "block";
            this.is_valid_user_secret = false;
        } else {
            this.check_secret_in_server(check_both);
        }       
    }

    check_secret_in_server (check_both) {
        
        this.spinner.style.display = "block";
        
        setTimeout(() => {                   
            let what = "";    
            let txt = this.input_secret.value.trim();
            if(txt ===  this.end_user_secret_code.value) {
                this.is_valid_user_secret = true;
                if(check_both === true) {
                    if(this.is_valid_user_details === true ) {                        
                        this.close();
                    }    
                } else {                    
                    this.close();
                }                
            } else {                    
                if (this.end_user_secret_code.type === "tz") {
                    what = "מס׳ תעודת-הזהות";
                } else if(this.end_user_secret_code.type === "random-number") {
                    what = "הקוד הסודי";
                }
                this.input_secret.focus();
                this.validator_secret.innerText = `${what} ${this.language === 'heb' ? 'שגוי' : 'is incorrect'}`;
                this.validator_secret.style.display = "block";
                this.is_valid_user_secret = false;
                
            }
            this.spinner.style.display = "none";
        },1500 );
    }

    validate_user_details() {
        let result_name = true, result_email = true;
        let txt_name = this.input_enduser_name.value.trim(), txt_email = null;
        this.validator_name.innerText = "";        
        this.is_valid_user_details = true;

        if (txt_name === '') {
            this.input_enduser_name.focus();
            this.validator_name.innerText = "יש להזין שם מלא (או שם חברה)";
            result_name = false;
        } else if(txt_name.length < 2) {
            this.input_enduser_name.focus();
            this.validator_name.innerText = "יש להזין לפחות 2 תווים";
            result_name = false;
        } else if(txt_name.length > 40) {
            this.input_enduser_name.focus();
            this.validator_name.innerText = "יש להזין לכל היותר 40 תווים";
            result_name = false;
        } else {           
            if(this.validate_name_characters(txt_name) === false) {
                this.validator_name.innerText = "תווים לא חוקיים";
                result_name = false;
            }           
        } 
        
        if(this.with_email === true) {
            txt_email = this.input_email.value.trim();
            this.validator_email.innerText = "";  
            if (txt_email === '') {
                this.input_email.focus();
                this.validator_email.innerText = "יש להזין דוא׳׳ל";
                result_email = false;
            } else if(txt_email.length > 50) {
                this.input_email.focus();
                this.validator_email.innerText = "יש להזין לכל היותר 50 תווים";
                result_email = false;
            } else {           
                result_email = this.email_regex_pattern.test(txt_email);
                if(result_email === false) {
                    this.validator_email.innerText = "כתובת דוא׳׳ל לא חוקית";
                }        
            }               
        }

        this.is_valid_user_details = result_name && result_email;
    }

    validate_name_characters (text) {
        let result = true;
        const disallowed_chars = ["\\", "/", ":", "*", "?", "<", ">", "|" ];
        for(let i = 0; i < disallowed_chars.length; i++) {
            if(text.includes(disallowed_chars[i]) === true) {
                result = false;
                break
            }
        }
        return result;
    }

    open () { 
        this.disable_body_scrolling();
        this.show_overlay();
        
        let what = "";
        if (this.end_user_secret_code.type === "tz") {
            what = "<span>מס׳ תעודת-זהות</span> <span style='font-size: 0.83em'>(בתשע ספרות)</span>"            
        } else if(this.end_user_secret_code.type === "random-number") {
            what = "קוד סודי";
        }   
        this.lbl_secret.innerHTML = `הזן ${what}`;
        this.lbl_enduser_name.innerHTML = "<span>הזן שם מלא</span> <span style='font-size: 0.83em'>(או שם חברה)</span>";        
        this.lbl_email.innerText = "הזן כתובת דוא׳׳ל";
        this.btn_ok.querySelector(".lbl").innerText = this.language === "heb" ? "אישור" : "OK";
        this.container.querySelector(".txt-email-me-copy").innerText = "ברצוני לקבל עותק של המסמך החתום בדוא׳׳ל";
                
        this.container.style.display = "block";  
        this.focus_text();
    }

    focus_text() {
        if(this.is_mobile() === false) {
            switch(this.scenario) {
                case "enduser-secret": 
                    this.input_secret.focus();
                    break;
                case "enduser-details": 
                    this.input_enduser_name.focus();
                    break;
                case "both": 
                    this.input_enduser_name.focus();
                break;
            }            
        }
    }

    is_open () {
        return this.container.style.display === "grid";
    }

    trigger_insert_text () {
        this.btn_ok.click();
    }

    close () {
        let step_zero_payload = null;       
        this.hide_overlay();
        this.container.style.display = "none";
        if(this.scenario === "enduser-details" || this.scenario === "both") {
            let txt_name = this.input_enduser_name.value.trim();
            let txt_email = "";
            txt_name = txt_name.replace('"', "''");
            txt_name = txt_name.replace('״', "''");
            
            if(this.with_email === true) {
                txt_email = this.input_email.value.trim();
            }
            step_zero_payload = { end_user_name_or_company: txt_name, end_user_email: txt_email };
            this.stepZero.handle_zero_modal_complete(step_zero_payload);
        }
    }    

    determine_scenario () {
        this.scenario = "none";
        
        if(this.end_user_secret_code.enabled === true) {
            this.scenario = "enduser-secret";
        } 
        
        if(this.reusable_doc.value === true && this.reusable_doc.require_enduser_details ===  true) {
            if(this.scenario === "enduser-secret") {
                this.scenario = "both";
            } else {
                this.scenario = "enduser-details";
            }
        }
    }

    set_scenario() {
        switch(this.scenario) {
            case "enduser-secret": 
            this.container.querySelectorAll(".group-secret").forEach(item => item.style.display = "flex");
            break;
            case "enduser-details": 
            this.container.querySelectorAll(".group-name").forEach(item => item.style.display = "flex");
            break;
            case "both": 
            this.container.querySelectorAll(".group-secret, .group-name").forEach(item => item.style.display = "flex");
            break;
        }
    }

    disable_ok_button() {
        this.btn_ok.classList.add("disable-button");
    }

    enable_ok_button() {
        this.btn_ok.classList.remove("disable-button");
    }

}

module.exports = StageZeroModal;