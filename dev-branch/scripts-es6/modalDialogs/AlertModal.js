import GoodSignBase from '../GoodSignBase';

class AlertModal extends GoodSignBase {
    constructor () {
        super();        
        
        this.container_parent = this.get_overlay_element();     
        this.container = null;        
        this.language = this.get_language();
        this.isMobile = this.is_mobile();

        this.btn_ok = null;
        this.msg = null;

        this.append_modal_markup_to_its_parent();
        this.init_members_from_appended_markup();
        this.bind_events();
    }

    append_modal_markup_to_its_parent() {
        const html =  
            `<div id="modal-alert-dialog">
                <div class="alert-header">
                    <span id="btn-close-alert">
                        <i class="fas fa-exclamation-triangle"></i> 
                    </span>
                </div>
                <div class="alert-msg">
                                    
                </div>
                <div id="btn-alert-ok"></div>                
            </div>`;
        
        const frag = document.createRange().createContextualFragment(html);
        this.container_parent.appendChild(frag);
    }

    init_members_from_appended_markup() {
        this.container = this.container_parent.querySelector("#modal-alert-dialog");
        this.container.classList.add(this.language);
        this.isMobile && this.container.classList.add("mobile-images");

        this.btn_ok = this.container.querySelector("#btn-alert-ok");
        this.msg = this.container.querySelector(".alert-msg");
       
        this.btn_ok.innerText = this.language === "heb" ? "אוקיי" : "OK";        
    }

    bind_events () {

        this.btn_ok.addEventListener("click", () => {
            this.close();            
        }, false);
    }

    open (msg) { 
        this.disable_body_scrolling();
        this.show_overlay();
        this.msg.innerHTML = msg;
        this.container.style.display = "grid";
     }

    close () {
        this.hide_overlay();
        this.container.style.display = "none";
    }
}

module.exports = AlertModal;