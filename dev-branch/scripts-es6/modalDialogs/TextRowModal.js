import GoodSignBase from '../GoodSignBase';
import Tooltips from "../Tooltips";

class TextRowModal extends GoodSignBase {
    constructor () {
        super();        
        
        this.container_parent = this.get_overlay_element();     
        this.container = null;        
        this.language = this.get_language();
        this.isMobile = this.is_mobile();

        
        this.tooltip_util = Tooltips.instance;
        this.current_placeholder = null;        
        this.btn_close = null;
        this.btn_ok = null;
        this.lbl = null;
        this.required_txt = null;
        this.required_txt_container = null;
        this.input = null; 
        
        this.append_modal_markup_to_its_parent();
        this.init_members_from_appended_markup();
        this.bind_events();
    }

    append_modal_markup_to_its_parent() {
        const html =  
            `<div id="modal-dialog-text-row">
                <div class="text-row-header">
                    <span id="btn-close-text-row-dialog"> 
                        <i class="far fa-times-circle fa-3x"></i> 
                    </span>
                </div>
                <div class="text-row-2nd-row">
                    <div class="inner">
                        <label class="lbl" for="text-field-row"></label>
                        <input class="txt" id="text-field-row" type="text" autocomplete="off" maxlength="40" />
                    </div>                    
                </div>                
                <div class="text-row-3rd-row">                    
                    <div class="inner required-txt-container">
                        <i class="fas fa-asterisk"></i>
                        <span class="required-txt"></span>
                    </div>
                </div>
                <div id="btn-text-row-ok-container"></div>                
            </div>`;
        
        const frag = document.createRange().createContextualFragment(html);
        this.container_parent.appendChild(frag);
    }

    init_members_from_appended_markup() {
        this.container = this.container_parent.querySelector("#modal-dialog-text-row");
        this.container.classList.add(this.language);
        this.isMobile && this.container.classList.add("mobile-images");

        this.btn_close = this.container.querySelector("#btn-close-text-row-dialog");
        this.lbl = this.container.querySelector(".lbl");
        this.input = this.container.querySelector(".txt");
        this.required_txt = this.container.querySelector(".required-txt");
        this.required_txt.innerText = this.language === "heb" ? "שדה חובה" : "Required field";
        this.required_txt_container = this.container.querySelector(".required-txt-container");
        this.btn_ok = this.container.querySelector("#btn-text-row-ok-container");
        this.btn_ok.innerText = this.language === "heb" ? "אישור" : "OK";
    }

    bind_events () {
        
        this.btn_close.addEventListener("click", () => {
            this.value = '';
            this.close();
        }, false);

        this.btn_ok.addEventListener("click", () => {

            var txt = this.input.value.trim();
            if (txt !== '') {
                this.handle_dirty_text_insertion(this.current_placeholder, txt);
                this.input.value = ''; 
                if(this.tooltip_util.has_tooltip(this.current_placeholder.parentNode) === true) {
                    this.tooltip_util.remove_tooltip(this.current_placeholder.parentNode);
                }
                this.tooltip_util.attach_thankyou_to_element(this.current_placeholder.parentNode);
                this.copy_text_to_same_text_placeholders(txt); 
            } else {
                this.current_placeholder.innerHTML = '';
                this.current_placeholder.parentNode.dataset.isDirty = 'false';
                this.current_placeholder.parentNode.style.minWidth = this.current_placeholder.parentNode.dataset.minWidth + "px";
                this.current_placeholder.parentNode.dataset.dt = '';
                if(this.current_placeholder.parentNode.dataset.isRequired === "true") {
                    this.append_asterisk_to_placeholder(this.current_placeholder.parentNode);
                }
            }                      

            this.close();
        }, false);
    }

    handle_dirty_text_insertion (placeholder, txt) {
        placeholder.innerHTML = txt.replace(/\s/g, '&nbsp;');
        placeholder.parentNode.style.width = "auto";
        placeholder.parentNode.style.minWidth = "auto";
        placeholder.parentNode.dataset.isDirty = "true";
        placeholder.parentNode.dataset.dt = new Date().valueOf();
        if(placeholder.parentNode.dataset.isRequired === "true") {
            this.remove_asterisk_from_placeholder(placeholder.parentNode);
        }
    } 

    copy_text_to_same_text_placeholders(txt) {
        const node_list = document.querySelectorAll(`[data-label-helper='${this.current_placeholder.parentNode.dataset.labelHelper}']`);
        node_list.forEach(element => {
            if(element !== this.current_placeholder) {
                this.handle_dirty_text_insertion(element.querySelector(".inner"), txt);
            }
        });
    }

    open (placeholder) { 
        this.current_placeholder = placeholder;
        this.disable_body_scrolling();
        this.show_overlay();
        const lbl_text = `${this.language === 'heb' ? 'הזן' : 'Enter'} ${this.current_placeholder.parentNode.dataset.labelHelper}:`
        this.lbl.innerText = lbl_text;
        this.isMobile === false && this.lbl.setAttribute("title", lbl_text);                
        this.required_txt_container.style.display = this.current_placeholder.parentNode.dataset.isRequired === "true" ? "block" : "none";
        this.input.value = this.current_placeholder.innerText;    
        this.container.style.display = "grid";
        this.isMobile === false && this.input.focus();      
     }

    close () {
        this.enable_body_scrolling();
        this.required_txt_container.style.display = "none";
        this.hide_overlay();
        this.container.style.display = "none";
    }

    is_open () {
        return this.container.style.display === "grid";
    }

    trigger_insert_text () {
        this.btn_ok.click();
    }
}

module.exports = TextRowModal;