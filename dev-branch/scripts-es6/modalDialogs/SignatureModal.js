import SignPad from '../SignPad';
import GoodSignBase from '../GoodSignBase';
import Tooltips from "../Tooltips";

class SignatureModal extends GoodSignBase {
    constructor () {
        super();   
        this.container_parent = this.get_overlay_element();     
        this.container = null;        
        this.language = this.get_language();
        this.isMobile = this.is_mobile();
        this.sign_pad = null;
        this.LINE_COLOR = "#001eaa";
        this.tooltip_util = Tooltips.instance;   
        this.current_placeholder = null;
        this.canvas_parent = null;
        this.btn_insert_signature = null;
        this.btn_clear_signature = null;
        this.btn_close_modal_signature = null;

        this.append_modal_markup_to_its_parent();
        this.init_members_from_appended_markup();
                
        this.bind_events();
    }

    append_modal_markup_to_its_parent() {
        const html =  
            `<div id="modal-dialog-signature">
                <div class="signature-header">
                    <span id="btn-close-signature-dialog"> 
                        <i class="far fa-times-circle fa-3x"></i> 
                    </span>  
                    <span class="title"></span>
                </div>
                <div id="canvas-wrapper">
                    <div class="canvas-wrapper-inner">
                        <canvas id="canvas-signature" class="canvas-signature"></canvas>
                    </div>
                </div>
                <div id="btn-signature-container-ok"></div>
                <div id="btn-signature-container-clear"></div>
            </div>`;
        
        const frag = document.createRange().createContextualFragment(html);
        this.container_parent.appendChild(frag);
    }

    init_members_from_appended_markup() {
        this.container = this.container_parent.querySelector("#modal-dialog-signature");
        this.container.classList.add(this.language);
        this.isMobile && this.container.classList.add("mobile-images");
        this.sign_pad = new SignPad(this.container.querySelector("#canvas-signature"), this.isMobile === false ? 9 : 14, this.LINE_COLOR);
        let modal_title = this.container.querySelector(".title");        
        modal_title.innerText = this.language === "heb" ? "צייר את חתימתך" : "Draw Your Signature";
        this.canvas_parent = this.container.querySelector("#canvas-wrapper");
        
        this.btn_insert_signature = this.container.querySelector("#btn-signature-container-ok");
        this.btn_clear_signature = this.container.querySelector("#btn-signature-container-clear");
        this.btn_close_modal_signature = this.container.querySelector("#btn-close-signature-dialog");        
        this.btn_clear_signature.innerText = this.language === "heb" ? "נקה" : "Clear";
        this.btn_insert_signature.innerText = this.language === "heb" ? "אישור" : "OK";
    }

    bind_events () {
        this.btn_close_modal_signature.addEventListener("click", () => {
            this.close();            
        }, false);

        this.btn_clear_signature.addEventListener("click", () => {
            this.sign_pad.reset();
        }, false); 

        this.btn_insert_signature.addEventListener("click", () => {

            if (this.sign_pad.isDirty() === true) {                 
                this.current_placeholder.src = this.sign_pad.get_data_url();
                this.current_placeholder.dataset.isDirty = "true";
                this.current_placeholder.dataset.dt = new Date().valueOf();                
               
                if(this.tooltip_util.has_tooltip(this.current_placeholder) === true) {
                    this.tooltip_util.remove_tooltip(this.current_placeholder);
                }                    
                this.tooltip_util.attach_thankyou_to_element(this.current_placeholder);
            } 
            
            this.close();
        }, false);     
    }

    open (placeholder) {        
        let ratio = 0, 
        canvas_height = 0, 
        canvas_width = this.isMobile === true ? 750 : 600; 

        this.current_placeholder = placeholder;

        ratio = Number((this.current_placeholder.clientHeight/this.current_placeholder.clientWidth).toFixed(2));
        if(ratio < 0.5) {
            canvas_height = 0.5 * canvas_width;
        } else {
            canvas_height = this.current_placeholder.clientHeight * canvas_width / this.current_placeholder.clientWidth;
        }
        this.container.style.gridTemplateRows = this.get_template_rows(canvas_height);
        this.disable_body_scrolling();
        this.show_overlay();

        this.container.style.display = "grid";
        const is_opaque = this.current_placeholder.classList.contains("sign-opaque");   
        this.sign_pad.set_canvas_dimentions(canvas_width, canvas_height, is_opaque);
        this.canvas_parent.style.width = this.canvas_parent.clientWidth;
        this.canvas_parent.style.height = this.canvas_parent.clientHeight;
    }

    get_template_rows(canvas_height) {
        var result = "";
        if(this.isMobile === false ){ 
            result = `60px ${canvas_height + 8}px 50px`;
        } else {
            result = `160px ${canvas_height + 20}px 140px`;
        }
        return result;
    }


    close () {
        this.sign_pad.reset();
        this.enable_body_scrolling();
        this.hide_overlay();
        this.container.style.display = "none";
    }
   
    is_open () {
        return this.container.style.display === "grid";
    }

    trigger_insert_signature () {
        this.btn_insert_signature.click();
    }
}

module.exports = SignatureModal;