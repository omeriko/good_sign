
import GoodSignBase from './GoodSignBase';

class Logs extends GoodSignBase {
    constructor () {
        super();        
    }
    
    collect_logs_from_placeholders() {
        let result = [], all_dirty_placeholders = this.get_dirty_placeholders_sorted_vertically();           
    
        all_dirty_placeholders.forEach(item => { 

            let log_item = { dt: item.dataset.dt, type: "" }; 

            if(item.classList.contains("sign-here-placeholder") === true ) {
                log_item.type = "signature";
            } else if(item.classList.contains("text-field-placeholder") === true) {
                log_item.type = "text_row";
            } else if (item.classList.contains("text-area-placeholder") === true) {
                log_item.type = "text_area";
            } else if (item.classList.contains("checkbox-placeholder") === true) {
                log_item.type = "checkbox";
            } else if (item.classList.contains("date-field-placeholder") === true) {
                log_item.type = "date";
            }
            result.push(log_item);
        });

        return result;
    } 
    
    collect_logs_from_embedded_images(embedded_images_items) {
        let result = [];           
    
        embedded_images_items.forEach(item => {
            if(item.token !== "") {
                result.push({ type: "embedded_image", name: item.name, dt: item.dt });
            }
        });

        return result;
    }

    get_dirty_placeholders_sorted_vertically() {
        let dirty_placeholders = document.querySelectorAll(".text-field-placeholder[data-is-dirty='true'], .text-area-placeholder[data-is-dirty='true'], .sign-here-placeholder, .checkbox-placeholder[data-is-dirty='true'], .date-field-placeholder[data-is-dirty='true']");
        let arr = Array.from(dirty_placeholders); 
        arr.sort(sort_by_top_position);               

        return arr;

        function sort_by_top_position (a, b) {
            var a_top =  Number(a.dataset.globalTopForSorting);
            var b_top =  Number(b.dataset.globalTopForSorting);
            return a_top - b_top;                    
        }                
    }
}

module.exports = Logs;