import mixpanel  from 'mixpanel-browser';

class Analytics {
    constructor () {
        if(!Analytics.instance) {
            Analytics.instance = this; 
            this.dt_session_start = 0;
            this.upload_start_time_for_end_user = 0;
            this.upload_complete_time_for_end_user = 0;
        }
        return Analytics.instance;
    }

    /*Setters*/
    set_upload_start_time (millis) {
        this.upload_start_time_for_end_user = millis;
    }

    set_upload_complete_time (millis) {
        this.upload_complete_time_for_end_user = millis;
    }    

    init(env) {
        const mixpanel_token = env === "dev" ? "0a73cb3e39c3e6c29591cdc51e71ca4f" : "22270100c0b93bd9e170cb5faba9eaf9";
        mixpanel.init(mixpanel_token);
        this.dt_session_start = new Date();
    }

    send(
        documentPlaceholders,
        step_files_upload,
        business_name, 
        business_self_sign,
        is_mobile,
        is_demo, 
        reusable_doc, 
        end_user_secret_code) {
        
        const count_all_signatures = documentPlaceholders.count_all_signatures();
        const { all_text_rows_count, reuired_text_rows_count }  = documentPlaceholders.count_text_rows();
        const { all_text_areas_count, reuired_text_areas_count }  = documentPlaceholders.count_text_areas();
        const { all_checkboxes_count, marked_checkboxes_count }  = documentPlaceholders.count_checkboxes();
        const { all_date_fields_count, required_date_fields_count }  = documentPlaceholders.count_date_fields();
        const { all_images_count, required_images_count } = step_files_upload.count_images();

        this.send_customer_analytics(business_name);
        this.send_document_completion_analytics(
            business_name,
            this.dt_session_start,
            new Date(), 
            is_mobile,
            business_self_sign,            
            is_demo,
            reusable_doc.value,
            reusable_doc.require_enduser_details,
            end_user_secret_code.enabled,  
            count_all_signatures,
            all_text_rows_count, 
            reuired_text_rows_count,
            all_text_areas_count, 
            reuired_text_areas_count,
            all_checkboxes_count, 
            marked_checkboxes_count,
            all_date_fields_count, 
            required_date_fields_count,
            all_images_count, 
            required_images_count
        );   
    }

    send_customer_analytics(name) {
                
        mixpanel.identify(name);

        mixpanel.people.set({
            "$name": name,
            "Last Document Completion": new Date().toISOString()
        });

        mixpanel.people.set_once({
            "First Document Completion": new Date().toISOString()
        });

        mixpanel.people.increment("Total Number Of Completions");
    }

    send_document_completion_analytics(
        customerName, 
        dt_session_start,        
        dt_session_complete,
        is_mobile,
        is_self_sign, 
        is_demo, 
        is_reusable,
        is_end_user_name_required,
        is_end_user_secret_required,
        
        count_all_signatures,
        all_text_rows_count, 
        reuired_text_rows_count,
        all_text_areas_count, 
        reuired_text_areas_count,
        all_checkboxes_count, 
        marked_checkboxes_count,
        all_date_fields_count, 
        required_date_fields_count,
        all_images_count, 
        required_images_count
    ) {
        const session_length_in_minutes = this.get_length_in_minutes(dt_session_start, dt_session_complete);
        const post_assets_length_in_seconds = this.get_length_in_seconds(this.upload_start_time_for_end_user, this.upload_complete_time_for_end_user);

        mixpanel.track("Document Completion", {
            "$name": customerName,
            "Session Start": dt_session_start.toISOString(),
            "Session Complete": dt_session_complete.toISOString(),
            "Length In Minutes": session_length_in_minutes,
            "Is Mobile": is_mobile,
            "Is Self Sign": is_self_sign,
            "Is Demo": is_demo,
            "Is Reusable": is_reusable,
            "Is End User Name Required": is_end_user_name_required,
            "Is End User Secret Required": is_end_user_secret_required,
            "Count All Signatures": count_all_signatures,
            "Count All Text Rows": all_text_rows_count,
            "Count Required Text Rows": reuired_text_rows_count,
            "Count All Text Areas": all_text_areas_count,
            "Count Required Text Areas": reuired_text_areas_count,
            "Count All Checkboxes": all_checkboxes_count,
            "Count Marked Checkboxes": marked_checkboxes_count,            
            "Count All Date Fields": all_date_fields_count,
            "Count Required Date Fields": required_date_fields_count,
            "Count All Images": all_images_count, 
            "Count Required Images": required_images_count,
            "Post Doc Assets Length In Seconds": post_assets_length_in_seconds
        });
    }
    
    get_length_in_minutes(dt_start, dt_end) {
        const ONE_MINUTE_IN_MILLISECOND = 60000;
        const diff = dt_end - dt_start;
        const diff_in_minutes = diff/ONE_MINUTE_IN_MILLISECOND;
        const diff_nicer =  Number(diff_in_minutes.toFixed(2));
        return diff_nicer;
    }

    get_length_in_seconds(dt_start, dt_end) {
        const ONE_SECOND_IN_MILLISECOND = 1000;
        const diff = dt_end - dt_start;
        const diff_in_minutes = diff/ONE_SECOND_IN_MILLISECOND;
        const diff_nicer =  Number(diff_in_minutes.toFixed(1));
        return diff_nicer;
    }

}

const instance = new Analytics();
module.exports.instance = instance;