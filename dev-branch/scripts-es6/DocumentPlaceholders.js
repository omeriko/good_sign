import SignaturePositioningTool from "./SignaturePositioningTool";
import GoodSignBase from './GoodSignBase';
import Tooltips from "./Tooltips";
import jQuery from "jquery";
import stringDirection from "string-direction";
import flatpickr from "flatpickr";
import { Hebrew } from "flatpickr/dist/l10n/he.js";
import { english } from "flatpickr/dist/l10n/default.js";

class DocumentPlaceholders extends GoodSignBase {
    constructor (
        modal_dialog_text_area, 
        modal_dialog_text_row, 
        modal_dialog_signature,
        end_user_contact_value,
        multiple_endusers) {
        super(); 
        
        this.tooltip_util = Tooltips.instance;  
        this.isMobile = this.is_mobile();
        this.language = this.get_language();        
        this.mobile_current_placeholder_pointer = 0;
        this.TEXT_AREA_PLACEHOLDER_STYLES = { fontSize: 16, lineHeight: 24 }; 
        this.last_scroll_top = 0;
        this.is_scrolling_helper;
        this.scroll_reason_helper = "";
        this.modal_dialog_text_area =  modal_dialog_text_area;
        this.modal_dialog_text_row = modal_dialog_text_row; 
        this.modal_dialog_signature = modal_dialog_signature;
        this.end_user_contact_value = end_user_contact_value;
        this.multiple_endusers = multiple_endusers;
    }
    
    position_all_placeholders() {
        this.position_checkboxes();
        if(this.multiple_endusers === false) {
            this.position_signatures_single_enduser();
        } else {
            this.position_signatures_multiple_endusers();
        }
        this.position_text_fields();
        this.position_date_fields();        
        this.position_text_areas();
        this.position_in_advance_fields();
        this.position_in_advance_signatures();       
        this.bind_events();
    }
    
    bind_events() {       

        document.querySelectorAll('.sign-here-placeholder').forEach(item => {
            item.addEventListener('click', e => {
                this.modal_dialog_signature.open(e.target);
            });
        });

        document.querySelectorAll('.text-field-placeholder').forEach(item => {
            item.addEventListener('click', e => {
                this.modal_dialog_text_row.open(e.target.parentNode.querySelector(".inner"));
            });
        });
        
        document.querySelectorAll('.text-area-placeholder').forEach( item => {
            item.addEventListener('click', e => {
                this.modal_dialog_text_area.open(e.target);
            });            
        });

        document.querySelectorAll('.clear-date').forEach(item => {
            item.addEventListener('click', e => {                
                this.handle_date_updated("", e.target);
            });
        });

        document.querySelectorAll(".checkbox-placeholder").forEach(item => {
            item.addEventListener('click', e => {                
                if(e.target.parentNode.dataset.isDirty === "false") {
                    
                    e.target.parentNode.dataset.isDirty = "true";
                    e.target.parentNode.dataset.dt = new Date().valueOf(); 
                    e.target.style.display = "none";
                    jQuery(e.target.nextElementSibling).fadeIn(200);

                } else if(e.target.parentNode.dataset.isDirty === "true") {

                    e.target.parentNode.dataset.isDirty = "false"; 
                    jQuery(e.target).fadeOut(100, function() {
                        this.previousElementSibling.style.display = "block"    
                    });
                }
            });
        });

        if(this.isMobile === false) {

            document.querySelectorAll('.sign-here-placeholder').forEach(item => {
                item.addEventListener('mouseenter', e => {
                    const isDirty = e.target.dataset.isDirty;
                    if(isDirty === "false" && this.tooltip_util.has_tooltip(e.target) === false) {
                        this.tooltip_util.attach_tooltip_to_element(e.target);
                    }
                });                
                item.addEventListener('mouseleave', e => {
                    const isDirty = e.target.dataset.isDirty;
                    if(isDirty === "false") {
                        this.tooltip_util.remove_tooltip(e.target);
                    }
                });
            });

            document.querySelectorAll(".checkbox-placeholder").forEach(item => {
                item.addEventListener('mouseenter', e => {
                    const isDirty = e.target.dataset.isDirty;
                    if(isDirty === "false" && this.tooltip_util.has_tooltip(e.target) === false) {
                        this.tooltip_util.attach_tooltip_to_element(e.target);
                    }
                });                
                item.addEventListener('mouseleave', e => {                    
                    if(this.tooltip_util.has_tooltip(e.target)) {
                        this.tooltip_util.remove_tooltip(e.target);
                    }
                });
            });
        
            jQuery('.textual').mouseenter({outerRef: this}, function (args) {
                const isDirty = this.dataset.isDirty;
                if(isDirty === "false") {
                    args.data.outerRef.tooltip_util.attach_tooltip_to_element(this);
                }
            }).mouseleave({outerRef: this}, function (args) {
                const isDirty = this.dataset.isDirty;
                if(isDirty === "false") {
                    args.data.outerRef.tooltip_util.remove_tooltip(this);
                }
            });
        }
    }

    position_checkboxes() {
        let target_container,
            pivot_ele,
            checkbox_placeholder,
            checked_ele, unchecked_ele,
            pivot_position = null,
            image_checkboxes = document.querySelectorAll("img[src$='check_box.jpg']");
            
        for (let i = 0; i < image_checkboxes.length; i++) {
            pivot_ele = image_checkboxes[i];
        
            pivot_position = SignaturePositioningTool.instance.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);
            checkbox_placeholder = document.createElement("div");
            checkbox_placeholder.classList.add('checkbox-placeholder');                
            checkbox_placeholder.dataset.isDirty = "false";
            checkbox_placeholder.dataset.dt = '';

            unchecked_ele = document.createElement("i");
            unchecked_ele.classList.add("far", "fa-square", "cbx-inner", "unchecked");               
            unchecked_ele.style.fontSize =  `${pivot_ele.height}px`;

            checked_ele = document.createElement("i");                
            checked_ele.classList.add("fas", "fa-check-square", "cbx-inner", "checked");
            checked_ele.style.display = "none";
            checked_ele.style.fontSize = `${pivot_ele.height}px`;

            checkbox_placeholder.appendChild(unchecked_ele); 
            checkbox_placeholder.appendChild(checked_ele);            

            checkbox_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;            
            
            checkbox_placeholder.style.top = `${pivot_position.top - 2}pt`;
            checkbox_placeholder.style.right = `${pivot_position.right}pt`;
                            
            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(checkbox_placeholder);            

            if(this.isMobile === true) {
                checkbox_placeholder.classList.add('mobile-images');
                checkbox_placeholder.dataset.initialFontSize = pivot_ele.height;
                
                checkbox_placeholder.dataset.initialTop = pivot_position.top ;//in points
                checkbox_placeholder.dataset.initialRight = pivot_position.right;//in points
            }

            pivot_ele.parentNode.removeChild(pivot_ele);
        }
    }
    
    position_signatures_multiple_endusers() {
        let target_container,
            pivot_ele,
            sign_here_placeholder,            
            pivot_position = null,
            image_signatures = document.querySelectorAll("img[src$='sign_field_1.jpg'], img[src$='sign_field_2.jpg'], img[src$='sign_field_3.jpg'], img[src$='sign_field_4.jpg'], img[src$='sign_field_5.jpg']");

        for (let i = 0; i < image_signatures.length; i++) {
            pivot_ele = image_signatures[i];
            pivot_position = SignaturePositioningTool.instance.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);
            if(pivot_ele.dataset.contactValue === this.end_user_contact_value) {                
                sign_here_placeholder = this.create_regular_signature_placeholder(pivot_ele, pivot_position);
            } else {
                sign_here_placeholder = this.create_shadow_signature(pivot_ele, pivot_position); 
            }

            sign_here_placeholder.dataset.name = pivot_ele.dataset.name
            sign_here_placeholder.dataset.contactType = pivot_ele.dataset.contactType
            sign_here_placeholder.dataset.contactValue = pivot_ele.dataset.contactValue 
            sign_here_placeholder.dataset.signatureId = pivot_ele.dataset.signatureId;   

            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(sign_here_placeholder); 
            pivot_ele.parentNode.removeChild(pivot_ele);
        } 
    }

    position_signatures_single_enduser() {
        let target_container,
            pivot_ele,
            sign_here_placeholder,
            pivot_position = null,           
            image_signatures = document.querySelectorAll("img[src$='sign_field.jpg'], img[src$='sign_field_opaque.jpg']");

        for (let i = 0; i < image_signatures.length; i++) {
            pivot_ele = image_signatures[i];
            
            pivot_position = SignaturePositioningTool.instance.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);
            sign_here_placeholder = this.create_regular_signature_placeholder(pivot_ele, pivot_position);                            
            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(sign_here_placeholder);              

            pivot_ele.parentNode.removeChild(pivot_ele);
        } 
    }

    create_shadow_signature(pivot_ele, pivot_position) {
        let result = document.createElement('img');
        result.classList.add('sign-here-placeholder-shadow');
                                        
        result.style.width = `${pivot_ele.clientWidth}px`;
        result.style.height = `${pivot_ele.clientHeight}px`;
        result.style.top = `${pivot_position.top}pt`;
        result.style.right = `${pivot_position.right}pt`;

        return result;
    }

    create_regular_signature_placeholder(pivot_ele, pivot_position) {
        let sign_here_placeholder = document.createElement('img');
        sign_here_placeholder.classList.add('sign-here-placeholder');
        
        if(pivot_ele.src.indexOf('_opaque') !== -1) {
            sign_here_placeholder.classList.add('sign-opaque');
        } 
                        
        sign_here_placeholder.dataset.isDirty = 'false';
        sign_here_placeholder.dataset.dt = '';
        sign_here_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;
        
        if(this.isMobile === true) {
            sign_here_placeholder.classList.add('mobile-images');
            sign_here_placeholder.dataset.initialWidth = pivot_ele.offsetWidth;
            sign_here_placeholder.dataset.initialHeight = pivot_ele.offsetHeight;
            sign_here_placeholder.dataset.initialTop = pivot_position.top ;//in points
            sign_here_placeholder.dataset.initialRight = pivot_position.right;//in points
        }

        sign_here_placeholder.style.width = `${pivot_ele.clientWidth}px`;
        sign_here_placeholder.style.height = `${pivot_ele.clientHeight}px`;
        sign_here_placeholder.style.top = `${pivot_position.top}pt`;
        sign_here_placeholder.style.right = `${pivot_position.right}pt`;

        return sign_here_placeholder;
    }

    position_text_fields() {
        let pivot_ele = null, 
            pivot_position = null, 
            text_field_placeholder = null,
            target_container = null,
            inner = null,
            is_required = false,
            is_font_bold = false,
            image_text_fields = document.querySelectorAll("[src$='sign_text.jpg']");
        
        for (let i = 0; i < image_text_fields.length; i++) {
            pivot_ele = image_text_fields[i];
        
            pivot_position = SignaturePositioningTool.instance.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);

            text_field_placeholder = document.createElement('div');
            text_field_placeholder.classList.add('text-field-placeholder', 'textual', this.language);                
            this.isMobile && text_field_placeholder.classList.add('mobile-images');               

            is_font_bold = this.is_text_placeholer_bold(pivot_ele);
            is_required = this.is_text_placeholer_required(pivot_ele);
            
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('^^^', '');
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('~~~', '');                           
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('ltr', '');
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('rtl', '');
            
            text_field_placeholder.dataset.labelHelper = pivot_ele.dataset.txName;
            text_field_placeholder.dataset.isRequired = is_required;
            text_field_placeholder.dataset.isDirty = "false";//TODO
            text_field_placeholder.dataset.dt = "";
            text_field_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;
            
            inner = document.createElement('div');
            inner.classList.add('inner');
            text_field_placeholder.appendChild(inner);

            if(this.isMobile === true) {
                text_field_placeholder.dataset.initialWidth = pivot_ele.clientWidth;
                text_field_placeholder.dataset.initialHeight = pivot_ele.clientHeight;
                text_field_placeholder.dataset.initialTop = pivot_position.top;
                text_field_placeholder.dataset.initialRight = pivot_position.right;                    
                text_field_placeholder.dataset.initialFontSize = pivot_ele.clientHeight*0.5;
                text_field_placeholder.dataset.initialLineHeight = pivot_ele.clientHeight-4;
            }

            text_field_placeholder.style.width      = `${pivot_ele.clientWidth}px`;
            text_field_placeholder.style.minWidth   = `${pivot_ele.clientWidth}px`;
            text_field_placeholder.dataset.minWidth = pivot_ele.clientWidth;
            text_field_placeholder.style.height     = `${pivot_ele.clientHeight}px`;
            text_field_placeholder.style.fontSize   = `${ pivot_ele.clientHeight * 0.5}px`;
            text_field_placeholder.style.lineHeight = `${pivot_ele.clientHeight - 4}px`;                
            text_field_placeholder.style.top   = `${pivot_position.top}pt`;
            text_field_placeholder.style.right = `${pivot_position.right}pt`;
            text_field_placeholder.style.fontWeight = is_font_bold ? "bold" : "normal";
            
            is_required === true && this.append_asterisk_to_placeholder(text_field_placeholder);

            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(text_field_placeholder);

            pivot_ele.parentNode.removeChild(pivot_ele);
        }
    }

    position_date_fields() {
        let pivot_ele,
            pivot_position,
            date_field_placeholder,
            is_font_bold,
            is_required, 
            txt_trigger_dtpicker,
            span_date_str,
            target_container,
            image_date_fields = document.querySelectorAll("[src$='date_field.jpg']");

        for (let i = 0; i < image_date_fields.length; i++) {
            pivot_ele = image_date_fields[i];
            pivot_position = SignaturePositioningTool.instance.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);

            date_field_placeholder = document.createElement('div');
            date_field_placeholder.setAttribute("type", "text");
            date_field_placeholder.classList.add('date-field-placeholder', 'textual', this.language);                
            this.isMobile && date_field_placeholder.classList.add('mobile-images');               

            is_font_bold = this.is_text_placeholer_bold(pivot_ele);
            is_required = this.is_text_placeholer_required(pivot_ele);
            
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('^^^', '');
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('~~~', '');
            
            date_field_placeholder.dataset.labelHelper = pivot_ele.dataset.txName;
            date_field_placeholder.dataset.isRequired = is_required;
            date_field_placeholder.dataset.isDirty = "false";//TODO
            date_field_placeholder.dataset.dt = "";
            date_field_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;                           
            
            if(this.isMobile === true) {
                date_field_placeholder.dataset.initialWidth = pivot_ele.clientWidth;
                date_field_placeholder.dataset.initialHeight = pivot_ele.clientHeight;
                date_field_placeholder.dataset.initialTop = pivot_position.top;
                date_field_placeholder.dataset.initialRight = pivot_position.right;                    
                date_field_placeholder.dataset.initialFontSize = pivot_ele.clientHeight*0.5;
                date_field_placeholder.dataset.initialLineHeight = pivot_ele.clientHeight-4;
            }

            date_field_placeholder.style.width      = `${pivot_ele.clientWidth}px`;
            date_field_placeholder.style.minWidth   = `${pivot_ele.clientWidth}px`;
            date_field_placeholder.dataset.minWidth = pivot_ele.clientWidth;
            date_field_placeholder.style.height     = `${pivot_ele.clientHeight}px`;
            
            date_field_placeholder.style.fontSize   = `${ pivot_ele.clientHeight * 0.46}px`;
            date_field_placeholder.style.lineHeight = `${pivot_ele.clientHeight-4}px`;                
            date_field_placeholder.style.top   = `${pivot_position.top}pt`;
            date_field_placeholder.style.right = `${pivot_position.right}pt`;
            date_field_placeholder.style.fontWeight = is_font_bold ? "bold" : "normal";
            
            is_required === true && this.append_asterisk_to_placeholder(date_field_placeholder);
            
            txt_trigger_dtpicker = document.createElement('input');
            txt_trigger_dtpicker.classList.add("txt-trigger-dtpicker");
            txt_trigger_dtpicker.setAttribute("type", "text");
            date_field_placeholder.appendChild(txt_trigger_dtpicker);

            span_date_str = document.createElement('span');
            span_date_str.classList.add("span-date-str");
            date_field_placeholder.appendChild(span_date_str);

            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(date_field_placeholder);

            this.attach_calendar_logic(txt_trigger_dtpicker);

            pivot_ele.parentNode.removeChild(pivot_ele);
        }
    }

    attach_calendar_logic (txt_ele) {
        const calendar = flatpickr(txt_ele, {
            allowInput: false,
            enableTime: false,
            dateFormat: "d/m/Y",
            locale: this.language === "eng" ? english : Hebrew
        });

        calendar.config.onChange.push((selectedDates, dateStr, instance) => {
            this.handle_date_updated( dateStr, instance.element);
        });

        calendar.config.onOpen.push(() => {
            !this.isMobile && this.show_overlay();
        });

        calendar.config.onClose.push(() => {
            if(this.isMobile === false) {
                this.hide_overlay();
            }
        });
        
        if(this.isMobile === false) {
            this.append_clear_btn_to_placeholder(txt_ele.parentNode);
        }
    }

    handle_date_updated(dateStr, instance) {
        const _date_field_placeholder = instance.parentNode;
        _date_field_placeholder.querySelector(".span-date-str").innerText = dateStr;
        _date_field_placeholder.dataset.isDirty = dateStr === "" ? "false" : "true";
        _date_field_placeholder.dataset.dt = dateStr === "" ? "" : new Date().valueOf();
        
        if(_date_field_placeholder.dataset.isRequired === "true") {
            if(dateStr !== "") {
                this.remove_asterisk_from_placeholder(_date_field_placeholder);
            } else {
                this.append_asterisk_to_placeholder(_date_field_placeholder);
            }           
        }

        if(this.tooltip_util.has_tooltip(_date_field_placeholder) === true) {
            this.tooltip_util.remove_tooltip(_date_field_placeholder);
        }

        if(this.isMobile === false) {
            _date_field_placeholder.querySelector(".fa-times").style.display = dateStr ==="" ? "none" : "block";
        }
    }

    append_clear_btn_to_placeholder(placeholder) {        
        let fa_ele = document.createElement('i');
        fa_ele.classList.add('fas', 'fa-times', 'clear-date');            
        fa_ele.setAttribute('title', this.language === 'heb' ? 'נקה' : 'Clear');
        placeholder.appendChild(fa_ele);
        
    }

    position_text_areas() {
        let pivot_ele = null,  
            pivot_position = null, 
            text_area_placeholder = null, 
            target_container = null,
            is_required = false,
            label = '', 
            max_chars_allowed = 0, 
            inner = null,
            image_text_areas = document.querySelectorAll("[src$='sign_text_area.jpg']");

        for (let i = 0; i < image_text_areas.length; i++) {
            pivot_ele = image_text_areas[i];
        
            pivot_position = SignaturePositioningTool.instance.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);
            text_area_placeholder = document.createElement('div');
            text_area_placeholder.classList.add('text-area-placeholder', 'textual', this.language);
            
            this.isMobile && text_area_placeholder.classList.add('mobile-images');               
            
            is_required = this.is_text_placeholer_required(pivot_ele);                
            
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('ltr', '');
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('rtl', '');
            label = is_required === true ? pivot_ele.dataset.txName.replace('~~~', '') : pivot_ele.dataset.txName;
            text_area_placeholder.dataset.labelHelper = label;
            text_area_placeholder.dataset.isRequired = is_required;
            text_area_placeholder.dataset.isDirty = 'false';
            text_area_placeholder.dataset.dt = "";
            text_area_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;
            
            inner = document.createElement('div');
            inner.classList.add('inner');
            text_area_placeholder.appendChild(inner);

            if(this.isMobile === true) {
                text_area_placeholder.dataset.initialWidth = pivot_ele.clientWidth;
                text_area_placeholder.dataset.initialHeight = pivot_ele.clientHeight;
                text_area_placeholder.dataset.initialTop = pivot_position.top;
                text_area_placeholder.dataset.initialRight = pivot_position.right;
                text_area_placeholder.dataset.initialFontSize = this.TEXT_AREA_PLACEHOLDER_STYLES.fontSize;
                text_area_placeholder.dataset.initialLineHeight = this.TEXT_AREA_PLACEHOLDER_STYLES.lineHeight;
            }
           
            text_area_placeholder.style.fontSize   = `${this.TEXT_AREA_PLACEHOLDER_STYLES.fontSize}px`;
            text_area_placeholder.style.lineHeight = `${this.TEXT_AREA_PLACEHOLDER_STYLES.lineHeight}px`;
            text_area_placeholder.style.width  = `${pivot_ele.clientWidth}px`;
            text_area_placeholder.style.height =  `${pivot_ele.clientHeight}px`;           
            text_area_placeholder.style.top = `${pivot_position.top}pt`;
            text_area_placeholder.style.right = `${pivot_position.right}pt`;                
            
            is_required === true &&  this.append_asterisk_to_placeholder(text_area_placeholder);
                        

            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(text_area_placeholder);
            
            max_chars_allowed = this.calc_max_chars_for_textarea(text_area_placeholder); 
            text_area_placeholder.dataset.maxLength = max_chars_allowed;                 

            pivot_ele.parentNode.removeChild(pivot_ele);
        }        
    }

    position_in_advance_fields() {
        var i, 
            p_ele = null,
            text_direction,
            pivot_ancestor = null,  
            pivot_position = null, 
            text_in_advance_placeholder = null,
            target_container = null,        
            in_advance_fields = this.get_in_advance_items_sorted_top_to_bottom();  
        
        for (i = 0; i < in_advance_fields.length; i++) {
            p_ele = in_advance_fields[i];
            
            pivot_ancestor = p_ele.parentNode; // grab div element
            pivot_position = SignaturePositioningTool.instance.calc_position(pivot_ancestor.style.top, pivot_ancestor.style.left, pivot_ancestor.clientWidth);

            text_in_advance_placeholder = document.createElement('div');
            text_in_advance_placeholder.classList.add('text-in-advance-placeholder');
            text_direction = stringDirection.getDirection(p_ele.textContent);            
            text_in_advance_placeholder.classList.add(text_direction  === "ltr" ? "ltr" : "rtl");
            
            text_in_advance_placeholder.innerHTML = p_ele.textContent;
            
            this.isMobile && text_in_advance_placeholder.classList.add('mobile-images');   
            text_in_advance_placeholder.dataset.isDirty = "true";
            
            text_in_advance_placeholder.style.height = `${pivot_ancestor.clientHeight}px`;
            text_in_advance_placeholder.style.fontSize = `${ pivot_ancestor.clientHeight*0.5}px`;
            text_in_advance_placeholder.style.lineHeight = `${pivot_ancestor.clientHeight}px`;
            text_in_advance_placeholder.style.top = `${pivot_position.top}pt`;
            text_in_advance_placeholder.style.right = `${pivot_position.right - 18}pt`;
                          
            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(text_in_advance_placeholder);

            pivot_ancestor.parentNode.removeChild(pivot_ancestor);

            if(this.isMobile === true) {
                text_in_advance_placeholder.dataset.initialWidth = text_in_advance_placeholder.clientWidth;
                text_in_advance_placeholder.dataset.initialHeight = text_in_advance_placeholder.clientHeight;
                text_in_advance_placeholder.dataset.initialTop = pivot_position.top;
                text_in_advance_placeholder.dataset.initialRight = pivot_position.right - 18;
                text_in_advance_placeholder.dataset.initialFontSize = text_in_advance_placeholder.clientHeight*0.5;
                text_in_advance_placeholder.dataset.initialLineHeight = text_in_advance_placeholder.clientHeight;
            }
        }   
    }

    position_in_advance_signatures() {
        let i,
            pivot_ele,
            pivot_position, 
            target_container,          
            in_advance_signatures = document.querySelectorAll('[data-tx-name=in-advance-signature]');

        for (i = 0; i < in_advance_signatures.length; i++) {
            pivot_ele = in_advance_signatures[i];                

            pivot_position = SignaturePositioningTool.instance.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);

            pivot_ele.dataset.isDirty = "true";

            if(this.isMobile === true) {
                pivot_ele.classList.add('mobile-images'); 
                pivot_ele.dataset.initialWidth = pivot_ele.offsetWidth;
                pivot_ele.dataset.initialHeight = pivot_ele.offsetHeight;
                pivot_ele.dataset.initialTop = pivot_position.top ;//in points
                pivot_ele.dataset.initialRight = pivot_position.right;//in points
            }

            pivot_ele.style.top = `${pivot_position.top}pt`;
            pivot_ele.style.right = `${pivot_position.right - 2}pt`;

            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(pivot_ele);               
        }
    } 
    
    is_text_placeholer_bold (placeholder) {
        let txt = placeholder.dataset.txName; 
        return txt.includes("^^^");
    }
    
    is_text_placeholer_required (placeholder) {
        let txt = placeholder.dataset.txName; 
        return txt.includes("~~~");
    }

    calc_max_chars_for_textarea (placeholder) { 
        var result = 0;
        var placeholeder_width = this.isMobile === true ?  Number(placeholder.dataset.initialWidth)  : placeholder.clientWidth; 
        var placeholeder_height = this.isMobile === true ? Number(placeholder.dataset.initialHeight) : placeholder.clientHeight; 

        var rows = Math.floor(placeholeder_height / this.TEXT_AREA_PLACEHOLDER_STYLES.lineHeight);
        var colls = Math.floor(placeholeder_width / this.TEXT_AREA_PLACEHOLDER_STYLES.fontSize); 
        
        var max_chars_allowed = Math.round(colls * rows * 2.1);
        result = max_chars_allowed - (max_chars_allowed % 10);             
        return result;
    }

    get_in_advance_items_sorted_top_to_bottom(){
        let node_list = document.querySelectorAll("[data-tx-name^='in-advance-label'] > p");
        let arr = Array.from(node_list); 
        arr.sort(sort_by_top_position);

        return arr;

        function sort_by_top_position (a, b) {
            var a_top =  Number(a.parentNode.parentNode.style.top.replace('pt', ''));
            var b_top =  Number(b.parentNode.parentNode.style.top.replace('pt', ''));
            return a_top - b_top;                    
        }
    } 
    
    fill_all_text_placeholders () {
        
        const date_values = ["13.03.2022" ,"20.06.2020", "29.10.1949", "18.12.2001", "17.05.1977"];      
        const all_values = ["תאריך לידה" ,"1101 שח", "Omer Sinai", "17-03-2007", "12345.52$", "ניסים" , "בניין 16"];      
        const all_lorem = [ "בוקר טוב עולם, מה שלום כולם, אני יורד מן החוטים, אני כבר ילד אמיתי", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever s"];
       
        document.querySelectorAll('.text-field-placeholder[data-is-required=true]').forEach(item => {  
            let index = Math.round(Math.random() * 100) % all_values.length;
            this.modal_dialog_text_row.handle_dirty_text_insertion(item.querySelector(".inner"), all_values[index]);           
        });    
        
        document.querySelectorAll('.text-area-placeholder[data-is-required=true]').forEach(item => { 
            let index = Math.round(Math.random() * 100) % all_lorem.length; 
            this.modal_dialog_text_area.handle_dirty_text_insertion(item.querySelector(".inner"), all_lorem[0]);           
        });

        document.querySelectorAll('.date-field-placeholder[data-is-required=true]').forEach(item => {  
            let index = Math.round(Math.random() * 100) % date_values.length;
            this.handle_date_updated(date_values[index], item.querySelector(".txt-trigger-dtpicker"));            
        });
        
        document.querySelectorAll('.checkbox-placeholder').forEach(item => { 
            if(Math.round((Math.random() * 100))%10 > 6) {
                item.dataset.isDirty = "true";
                item.dataset.dt = new Date().valueOf(); 
                item.querySelector(".checked").style.display = "block";
                item.querySelector(".unchecked").style.display = "none";
            } else {
                item.dataset.isDirty = "false";
                item.dataset.dt = ""; 
                item.querySelector(".checked").style.display = "none";
                item.querySelector(".unchecked").style.display = "block";
            }
            
        });
    }      

    get_required_placeholders_vertically_sorted() {
        var node_list = document.querySelectorAll(".sign-here-placeholder[data-is-dirty='false'], .textual[data-is-required='true'][data-is-dirty='false']");
        var arr = Array.from(node_list); 
        arr.sort(sort_by_top_position);               

        return arr;

        function sort_by_top_position (a, b) {
            var a_top =  Number(a.dataset.globalTopForSorting);
            var b_top =  Number(b.dataset.globalTopForSorting);
            return a_top - b_top;                    
        }               
    }   

    remove_all_tooltips () {
        var node_list = document.querySelectorAll(".sign-here-placeholder, .textual");
        node_list.forEach(ele => {
            if(this.tooltip_util.has_tooltip(ele)) {
                this.tooltip_util.remove_tooltip(ele);
            } 
        });       
    }

    count_all_signatures() {
        return document.querySelectorAll('.sign-here-placeholder').length;
    }

    count_text_rows() {
        const all_text_rows_count = document.querySelectorAll('.text-field-placeholder').length;
        const reuired_text_rows_count = document.querySelectorAll('.text-field-placeholder[data-is-required=true]').length;
        return { all_text_rows_count, reuired_text_rows_count };
    }

    count_text_areas() {
        const all_text_areas_count = document.querySelectorAll('.text-area-placeholder').length;
        const reuired_text_areas_count = document.querySelectorAll('.text-area-placeholder[data-is-required=true]').length;
        return { all_text_areas_count, reuired_text_areas_count };
    }

    count_checkboxes() {
        const all_checkboxes_count = document.querySelectorAll(".checkbox-placeholder").length;
        const marked_checkboxes_count = document.querySelectorAll('.checkbox-placeholder[data-is-dirty=true]').length;
        return { all_checkboxes_count, marked_checkboxes_count };
    }

    count_date_fields() {
        const all_date_fields_count = document.querySelectorAll(".date-field-placeholder").length;
        const required_date_fields_count = document.querySelectorAll('.date-field-placeholder[data-is-dirty=true]').length;
        return { all_date_fields_count, required_date_fields_count };
    }
}

module.exports = DocumentPlaceholders;