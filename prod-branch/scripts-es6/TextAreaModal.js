import GoodSignBase from './GoodSignBase';
import stringDirection from "string-direction";
import Tooltips from "./Tooltips";

class TextAreaModal extends GoodSignBase {
    constructor (parent_element, app) {
        super();        
        this.container = parent_element; 
        this.language = this.get_language();
        this.tooltip_util = Tooltips.instance;
        this.current_placeholder = null;
        this.parent = app;
        this.is_mobile() && this.container.classList.add("mobile-images");
        this.container.classList.add(this.language);

        this.lbl = this.container.querySelector(".lbl");
        this.input = this.container.querySelector(".txt-area");
        this.max_chars_allowed = this.container.querySelector("#max-chars-allowed");
        this.remaining_chars = this.container.querySelector("#chars-remaining");
        this.btn_ok = this.container.querySelector("#btn-ok-text-area");
        this.required_txt = this.container.querySelector(".required-txt");
        this.btn_close = this.container.querySelector(".close-modal");;       
        
        var TEXT_AREA_INPUT_STYLES = { fontSize: this.is_mobile()===false ? 18 : 50 , lineHeight: this.is_mobile()===false ? 27 : 75 };
        this.input.style.fontSize = TEXT_AREA_INPUT_STYLES.fontSize + "px";
        this.input.style.lineHeight = TEXT_AREA_INPUT_STYLES.lineHeight + "px";           

        this.btn_ok.innerText = this.language === "heb" ? "אישור" : "OK";
        this.required_txt.innerText = this.language === "heb" ? "שדה חובה" : "Required field";
        this.container.querySelector("#max-lbl").innerText= this.language === "heb" ? "מכסימום  " : "Max' ";
        this.container.querySelector("#chars-lbl").innerText = this.language === "heb" ? " תווים " : " characters ";
        this.container.querySelector("#chars-left-lbl").innerText = this.language === "heb" ? "נותרו " : "remaining ";
        
        this.btn_ok.innerText = this.language === "heb" ? "אישור" : "OK";
        this.required_txt.innerText = this.language === "heb" ? "שדה חובה" : "Required field";
        this.bind_events();
    }

    bind_events () {
        
        this.btn_close.addEventListener("click", () => {            
            this.close();
        }, false);

        this.btn_ok.addEventListener('click', () => {                
            var txt = this.input.value.trim();
            
            if (txt !== '') {
                this.handle_dirty_text_insertion(this.current_placeholder, txt);
                this.input.value = '';
                if(this.tooltip_util.has_tooltip(this.current_placeholder.parentNode) === true) {
                    this.tooltip_util.remove_tooltip(this.current_placeholder.parentNode); 
                }
                this.tooltip_util.attach_thankyou_to_element(this.current_placeholder.parentNode);
                
            } else {
                this.current_placeholder.innerHTML = '';
                this.current_placeholder.parentNode.dataset.isDirty = 'false';
                this.current_placeholder.parentNode.dataset.dt = '';
                if(this.current_placeholder.parentNode.dataset.isRequired === "true") {
                    this.append_asterisk_to_placeholder(this.current_placeholder.parentNode);                        
                } 
            }
            
            this.is_mobile() && this.parent.render_bottom_bar();            
            this.close();
        }, false);       

        this.input.addEventListener("input", e => {
            var remaining = Number(this.max_chars_allowed.innerText) - e.target.value.length;
            this.remaining_chars.innerText = remaining;
        });
    }

    handle_dirty_text_insertion (placeholder, txt) {
        let inner_html = txt.replace(/\s/g, '<span> </span>');
        inner_html = inner_html.replace(',', '<span>,</span>');
        inner_html = inner_html.replace('.', '<span>.</span>');

        placeholder.innerHTML = inner_html;                                 
        placeholder.parentNode.dataset.isDirty = 'true';
        placeholder.parentNode.dataset.dt = new Date().valueOf();
        const direction = stringDirection.getDirection(txt);
        placeholder.parentNode.style.textAlign = (direction === 'rtl' || direction === 'bidi') ? 'right' : 'left';
        if(placeholder.parentNode.dataset.isRequired === "true") {
            this.remove_asterisk_from_placeholder(placeholder.parentNode);                        
        } 
    }

    append_asterisk_to_placeholder (placeholder) {
        placeholder.setAttribute('title', this.language === 'heb' ? 'שדה חובה' : 'Required field');
        var fa_ele = document.createElement('i');
        fa_ele.classList.add('fas', 'fa-asterisk');            
        placeholder.appendChild(fa_ele);
    }

    remove_asterisk_from_placeholder (placeholder) {
        var fa_ele = placeholder.querySelector('.fa-asterisk');
        if(fa_ele !== null) {
            placeholder.removeChild(fa_ele);
        }
    }

    open (placeholder) { 
        this.current_placeholder = placeholder;
        this.disable_body_scrolling();
        this.show_overlay();
        this.lbl.innerText = `${this.language === 'heb' ? 'הזן' : 'Enter'} ${this.current_placeholder.parentNode.dataset.labelHelper}:`;  
        
        this.input.value = this.current_placeholder.innerText;
        this.max_chars_allowed.innerText = this.current_placeholder.parentNode.dataset.maxLength;
        this.remaining_chars.innerText = Number(this.current_placeholder.parentNode.dataset.maxLength) - this.current_placeholder.innerText.length;
        this.required_txt.parentNode.style.visibility = this.current_placeholder.parentNode.dataset.isRequired === "true" ? "visible" : "hidden";
        this.input.setAttribute("maxlength" , this.current_placeholder.parentNode.dataset.maxLength);
        this.container.style.display = "block";
        this.is_mobile() === false && this.input.focus(); 
     }

    close () {
        this.enable_body_scrolling();
        this.required_txt.parentNode.style.visibility = "hidden";
        this.hide_overlay();
        this.container.style.display = "none";
        this.input.value = "";
    }

    is_open () {
        return this.container.style.display === "block";
    }

    trigger_insert_text () {
        this.btn_ok.click();
    }
}

module.exports = TextAreaModal;