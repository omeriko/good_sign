import Mobile from './Mobile';
import jQuery from "jquery";

class Tooltips {

    constructor(language) {
        if(!Tooltips.instance){
            Tooltips.instance = this;            
            this.language = language;            
        }

        return Tooltips.instance;
    }

    has_tooltip (element) {
        return element._tippy !== undefined;
    }

    remove_tooltip (element) {
        element._tippy.destroy();
    }

    hide_tooltip (element) {
        element._tippy.hide();
    }

    attach_tooltip_to_element (element) {

        let ele_top = Number(element.dataset.globalTopForSorting);
        if(element.classList.contains('textual')) {

            this.attach_tooltip_to_textual (element, ele_top);    

        } else if(element.classList.contains('sign-here-placeholder')) {
            
            this.attach_tooltip_to_signature (element, ele_top);

        } else if(element.classList.contains('checkbox-placeholder')) {
            
            this.attach_tooltip_to_checkbox (element, ele_top);
        }        
    }

    attach_tooltip_to_textual (element, ele_top) {
        let content =  
                `<div class="tippy-content-before txt-input ${this.language}">                              
                    <div class="tt-msg">
                        ${this.language === 'heb' ? `לחץ כדי להזין ${element.dataset.labelHelper}` : `Click to enter ${element.dataset.labelHelper}`}
                    </div>
                    ${Mobile.instance.is_mobile() === true ? "<div class='tt-close'><i class='fas fa-times-circle tt-hide'></i></div>" :''}                              
                </div>`;

        tippy(element, { 
            content: content, 
            allowHTML: true, 
            hideOnClick: false, 
            trigger: 'manual', 
            placement: ele_top < 120 ? 'bottom' : 'top', 
            zIndex: 8, 
            touch: true, 
            showOnCreate: true, 
            interactive: true, 
            theme: 'default',            
            onShown: instance => {
                if(Mobile.instance.is_mobile() === true){   
                    instance.popper.querySelector(".tt-msg").addEventListener('click', function(){
                        jQuery(instance.reference.querySelector('.inner')).trigger('click');
                    }, false);

                    instance.popper.querySelector(".tt-hide").addEventListener('click', function(){
                        instance.destroy();
                    }, false);
                }
            }
        });
    }

    attach_tooltip_to_signature (element, ele_top) {
        let content = `<div class="tippy-content-before ${this.language}">                              
                    <div class="tt-msg">
                        ${this.language === 'heb' ? 'לחץ כדי לחתום' : 'Click to sign'}
                    </div>
                    ${ Mobile.instance.is_mobile() === true ? "<div class='tt-close'><i class='fas fa-times-circle tt-hide'></i></div>" : '' }                              
                </div>`; 

        tippy(element, { 
            content: content, 
            allowHTML: true, 
            hideOnClick: false, 
            trigger: 'manual', 
            placement: ele_top < 120 ? 'bottom' : 'top', 
            zIndex: 8, 
            touch: true, 
            showOnCreate: true, 
            interactive: true,
            theme: 'default',            
            onShown: instance => {
                if(Mobile.instance.is_mobile() === true){   
                    instance.popper.querySelector(".tt-msg").addEventListener('click', function(){
                        jQuery(instance.reference).trigger('click');
                    }, false);

                    instance.popper.querySelector(".tt-hide").addEventListener('click', function(){
                        instance.destroy();
                    }, false);
                }
            }
        });
    }
    
    attach_tooltip_to_checkbox (element, ele_top) {
        let content = `<div class="tippy-content-before ${this.language}">                              
                    <div class="tt-msg">
                        ${this.language === 'heb' ? 'לחץ אם ברצונך לסמן' : 'Click to check'}
                    </div>
                    ${ Mobile.instance.is_mobile() === true ? "<div class='tt-close'><i class='fas fa-times-circle tt-hide'></i></div>" : '' }                              
                </div>`; 

        tippy(element, { 
            content: content, 
            allowHTML: true, 
            hideOnClick: false, 
            trigger: 'manual', 
            placement: ele_top < 120 ? 'bottom' : 'top', 
            zIndex: 8, 
            touch: true, 
            showOnCreate: true, 
            interactive: true,
            theme: 'default',            
            onShown: instance => {
                if(Mobile.instance.is_mobile() === true){   
                    instance.popper.querySelector(".tt-msg").addEventListener('click', function(){
                        jQuery(instance.reference.querySelector(".unchecked")).trigger('click');
                        instance.destroy();
                    }, false);

                    instance.popper.querySelector(".tt-hide").addEventListener('click', function(){
                        instance.destroy();
                    }, false);
                }
            }
        });
    }

    attach_thankyou_to_element (element) {
        var content =  `<div class="tippy-content-after ${instance.language}"><i class="fas fa-check"></i></div>`;
        var tip = tippy(
                    element, { 
                    content: content,  
                    allowHTML: true, 
                    hideOnClick: false, 
                    trigger: 'manual', 
                    placement:'top', 
                    zIndex: 8, 
                    arrow: false, 
                    touch: true, 
                    interactive: true, 
                    showOnCreate: true,
                    theme: 'default', 
                    onCreate: instance => {
                        if(Mobile.instance.is_mobile() === true){   
                            instance.popper.querySelector(".tippy-box").style.borderRadius = "100px";
                        }
                    }
                });            
        
        setTimeout(function () {
            tip.setProps({
                duration: 500
            });
            tip.hide();
        }, 1000);
    }

    attach_approve_agreement_to_element (element) {
        let content =  
                `<div class="tippy-content-before txt-input ${this.language}">                              
                    <div class="tt-msg">
                        ${this.language === 'heb' ? "אנא אשר את תנאי השימוש" : "Please approve the agreement terms" }
                    </div>                                           
                </div>`;

        tippy(element, { 
            content: content, 
            allowHTML: true, 
            hideOnClick: false, 
            trigger: 'manual', 
            placement: 'top-end', 
            zIndex: 8, 
            touch: true, 
            showOnCreate: true, 
            interactive: true,
            theme: 'green', 
        });
    }
    remove_all_tooltips() {
        let node_list = document.querySelectorAll(".sign-here-placeholder, .textual");
        (node_list || []).forEach( item => {
            if(this.has_tooltip(item) === true) {
                this.remove_tooltip(item);
            }
        });
    }
}

function get_languae() {
    let result = 'eng';
    let hidden_doc_lang = document.querySelector("#doc_lang");
    if (hidden_doc_lang !== null) {
        result = Number(hidden_doc_lang.value) === 0 ? 'eng' : 'heb';
    }
    return result;
}


const instance = new Tooltips(get_languae());
Object.freeze(instance);

module.exports.instance = instance;