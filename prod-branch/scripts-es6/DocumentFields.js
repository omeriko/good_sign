import SignaturePositioningTool from "./SignaturePositioningTool";
import GoodSignBase from './GoodSignBase';
import Tooltips from "./Tooltips";
import jQuery from "jquery";

class DocumentFields extends GoodSignBase {
    constructor () {
        super();
        if(!DocumentFields.instance) {
            DocumentFields.instance = this;
            this.positioning_tool = new SignaturePositioningTool();
            this.tooltip_util = Tooltips.instance;  
            this.isMobile = this.is_mobile();
            this.language = this.get_language();
            this.TEXT_AREA_PLACEHOLDER_STYLES = { fontSize: this.isMobile ===false ? 16 : 16, lineHeight: this.isMobile ===false ? 24: 24 };
        }
        return DocumentFields.instance;       
    }

    position_all_placeholders(modal_dialog_text_area, modal_dialog_text_row, modal_dialog_signature) {
        this.position_checkboxes();
        this.position_signatures();
        this.position_text_fields(modal_dialog_text_row);        
        this.position_text_areas(modal_dialog_text_area);
        this.position_in_advance_fields();
        this.position_in_advance_signatures();
        this.bind_events(modal_dialog_text_area, modal_dialog_text_row, modal_dialog_signature);
    }

    bind_events(modal_dialog_text_area, modal_dialog_text_row, modal_dialog_signature) {
        
        document.querySelectorAll('.sign-here-placeholder').forEach(item => {
            item.addEventListener('click', e => {
                modal_dialog_signature.open(e.target);
            });
        });

        document.querySelectorAll('.text-field-placeholder').forEach(item => {
            item.addEventListener('click', e => {
                modal_dialog_text_row.open(e.target.parentNode.querySelector(".inner"));
            });
        });       
        
        document.querySelectorAll('.text-area-placeholder').forEach( item => {
            item.addEventListener('click', e => {
                modal_dialog_text_area.open(e.target);
            });            
        });

        document.querySelectorAll(".checkbox-placeholder").forEach(item => {
            item.addEventListener('click', e => {                
                if(e.target.parentNode.dataset.isDirty === "false") {
                    
                    e.target.parentNode.dataset.isDirty = "true";
                    e.target.parentNode.dataset.dt = new Date().valueOf(); 
                    e.target.style.display = "none";
                    jQuery(e.target.nextElementSibling).fadeIn(200);

                } else if(e.target.parentNode.dataset.isDirty === "true") {

                    e.target.parentNode.dataset.isDirty = "false"; 
                    jQuery(e.target).fadeOut(100, function() {
                        this.previousElementSibling.style.display = "block"    
                    });
                }
            });
        });

        if(this.isMobile === false) {            

            document.querySelectorAll('.sign-here-placeholder').forEach(item => {
                item.addEventListener('mouseenter', e => {
                    const isDirty = e.target.dataset.isDirty;
                    if(isDirty === "false" && this.tooltip_util.has_tooltip(e.target) === false) {
                        this.tooltip_util.attach_tooltip_to_element(e.target);
                    }
                });                
                item.addEventListener('mouseleave', e => {
                    const isDirty = e.target.dataset.isDirty;
                    if(isDirty === "false") {
                        this.tooltip_util.remove_tooltip(e.target);
                    }
                });
            });

            document.querySelectorAll(".checkbox-placeholder").forEach(item => {
                item.addEventListener('mouseenter', e => {
                    const isDirty = e.target.dataset.isDirty;
                    if(isDirty === "false" && this.tooltip_util.has_tooltip(e.target) === false) {
                        this.tooltip_util.attach_tooltip_to_element(e.target);
                    }
                });                
                item.addEventListener('mouseleave', e => {                    
                    if(this.tooltip_util.has_tooltip(e.target)) {
                        this.tooltip_util.remove_tooltip(e.target);
                    }
                });
            });

        
            jQuery('.textual').mouseenter({outerRef: this}, function (args) {
                const isDirty = this.dataset.isDirty;
                if(isDirty === "false") {
                    args.data.outerRef.tooltip_util.attach_tooltip_to_element(this);
                }
            }).mouseleave({outerRef: this}, function (args) {
                const isDirty = this.dataset.isDirty;
                if(isDirty === "false") {
                    args.data.outerRef.tooltip_util.remove_tooltip(this);
                }
            });
        }
    }

    position_checkboxes() {
        let target_container,
            pivot_ele,
            checkbox_placeholder,
            checked_ele, unchecked_ele,
            pivot_position = null,
            image_checkboxes = document.querySelectorAll("img[src='check_box.jpg']");
            
        for (let i = 0; i < image_checkboxes.length; i++) {
            pivot_ele = image_checkboxes[i];
        
            pivot_position = this.positioning_tool.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);
            checkbox_placeholder = document.createElement("div");
            checkbox_placeholder.classList.add('checkbox-placeholder');                
            checkbox_placeholder.dataset.isDirty = "false";
            checkbox_placeholder.dataset.dt = '';

            unchecked_ele = document.createElement("i");
            unchecked_ele.classList.add("far", "fa-square", "cbx-inner", "unchecked");               
            unchecked_ele.style.fontSize =  `${pivot_ele.height}px`;

            checked_ele = document.createElement("i");                
            checked_ele.classList.add("fas", "fa-check-square", "cbx-inner", "checked");
            checked_ele.style.display = "none";
            checked_ele.style.fontSize = `${pivot_ele.height}px`;

            checkbox_placeholder.appendChild(unchecked_ele); 
            checkbox_placeholder.appendChild(checked_ele);            

            checkbox_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;            
            
            checkbox_placeholder.style.top = `${pivot_position.top}pt`;
            checkbox_placeholder.style.right = `${pivot_position.right}pt`;
                            
            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(checkbox_placeholder);            

            if(this.isMobile === true) {
                checkbox_placeholder.classList.add('mobile-images');
                checkbox_placeholder.dataset.initialFontSize = pivot_ele.height;
                
                checkbox_placeholder.dataset.initialTop = pivot_position.top ;//in points
                checkbox_placeholder.dataset.initialRight = pivot_position.right;//in points
            }

            pivot_ele.parentNode.removeChild(pivot_ele);
        }
    }

    position_signatures() {
        let target_container,
            pivot_ele,
            sign_here_placeholder,
            pivot_position = null,
            image_signatures = document.querySelectorAll("img[src='sign_field.jpg'], img[src='sign_field_opaque.jpg']");

        for (let i = 0; i < image_signatures.length; i++) {
            pivot_ele = image_signatures[i];
            
            pivot_position = this.positioning_tool.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);

            sign_here_placeholder = document.createElement('img');
            sign_here_placeholder.classList.add('sign-here-placeholder');
            
            if(pivot_ele.src.indexOf('_opaque') !== -1) {
                sign_here_placeholder.classList.add('sign-opaque');
            } 
                           
            sign_here_placeholder.dataset.isDirty = 'false';
            sign_here_placeholder.dataset.dt = '';
            sign_here_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;
            
            if(this.isMobile === true) {
                sign_here_placeholder.classList.add('mobile-images');
                sign_here_placeholder.dataset.initialWidth = pivot_ele.offsetWidth;
                sign_here_placeholder.dataset.initialHeight = pivot_ele.offsetHeight;
                sign_here_placeholder.dataset.initialTop = pivot_position.top ;//in points
                sign_here_placeholder.dataset.initialRight = pivot_position.right;//in points
            }

            sign_here_placeholder.style.width = `${pivot_ele.clientWidth}px`;
            sign_here_placeholder.style.height = `${pivot_ele.clientHeight}px`;
            sign_here_placeholder.style.top = `${pivot_position.top}pt`;
            sign_here_placeholder.style.right = `${pivot_position.right}pt`;
                            
            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(sign_here_placeholder);              

            pivot_ele.parentNode.removeChild(pivot_ele);
        } 
    }

    position_text_fields(modal_dialog_text_row) {
        let pivot_ele = null, 
            pivot_position = null, 
            text_field_placeholder = null,
            target_container = null,
            inner = null,
            is_required = false,
            is_font_bold = false,
            image_text_fields = document.querySelectorAll("[src='sign_text.jpg']");
        
        for (let i = 0; i < image_text_fields.length; i++) {
            pivot_ele = image_text_fields[i];
        
            pivot_position = this.positioning_tool.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);

            text_field_placeholder = document.createElement('div');
            text_field_placeholder.classList.add('text-field-placeholder', 'textual', this.language);                
            this.isMobile && text_field_placeholder.classList.add('mobile-images');               

            is_font_bold = this.is_text_placeholer_bold(pivot_ele);
            is_required = this.is_text_placeholer_required(pivot_ele);
            
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('^^^', '');
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('~~~', '');                           
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('ltr', '');
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('rtl', '');
            
            text_field_placeholder.dataset.labelHelper = pivot_ele.dataset.txName;
            text_field_placeholder.dataset.isRequired = is_required;
            text_field_placeholder.dataset.isDirty = "false";//TODO
            text_field_placeholder.dataset.dt = "";
            text_field_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;
            
            inner = document.createElement('div');
            inner.classList.add('inner');
            text_field_placeholder.appendChild(inner);

            if(this.isMobile === true) {
                text_field_placeholder.dataset.initialWidth = pivot_ele.clientWidth;
                text_field_placeholder.dataset.initialHeight = pivot_ele.clientHeight;
                text_field_placeholder.dataset.initialTop = pivot_position.top;
                text_field_placeholder.dataset.initialRight = pivot_position.right;                    
                text_field_placeholder.dataset.initialFontSize = pivot_ele.clientHeight*0.5;
                text_field_placeholder.dataset.initialLineHeight = pivot_ele.clientHeight-4;
            }

            text_field_placeholder.style.width      = `${pivot_ele.clientWidth}px`;
            text_field_placeholder.style.minWidth   = `${pivot_ele.clientWidth}px`;
            text_field_placeholder.dataset.minWidth = pivot_ele.clientWidth;
            text_field_placeholder.style.height     = `${pivot_ele.clientHeight}px`;
            text_field_placeholder.style.fontSize   = `${ pivot_ele.clientHeight * 0.5}px`;
            text_field_placeholder.style.lineHeight = `${pivot_ele.clientHeight - 4}px`;                
            text_field_placeholder.style.top   = `${pivot_position.top}pt`;
            text_field_placeholder.style.right = `${pivot_position.right}pt`;
            text_field_placeholder.style.fontWeight = is_font_bold ? "bold" : "normal";
            
            if(is_required === true) {
                modal_dialog_text_row.append_asterisk_to_placeholder(text_field_placeholder);
            }                              
                           
            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(text_field_placeholder);

            pivot_ele.parentNode.removeChild(pivot_ele);
        }
    }

    position_text_areas(modal_dialog_text_area) {
        let pivot_ele = null,  
            pivot_position = null, 
            text_area_placeholder = null, 
            target_container = null,
            is_required = false,
            label = '', 
            max_chars_allowed = 0, 
            inner = null,
            image_text_areas = document.querySelectorAll("[src='sign_text_area.jpg']");

        for (let i = 0; i < image_text_areas.length; i++) {
            pivot_ele = image_text_areas[i];
        
            pivot_position = this.positioning_tool.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);
            text_area_placeholder = document.createElement('div');
            text_area_placeholder.classList.add('text-area-placeholder', 'textual', this.language);
            
            this.isMobile && text_area_placeholder.classList.add('mobile-images');               
            
            is_required = this.is_text_placeholer_required(pivot_ele);                
            
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('ltr', '');
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('rtl', '');
            label = is_required === true ? pivot_ele.dataset.txName.replace('~~~', '') : pivot_ele.dataset.txName;
            text_area_placeholder.dataset.labelHelper = label;
            text_area_placeholder.dataset.isRequired = is_required;
            text_area_placeholder.dataset.isDirty = 'false';
            text_area_placeholder.dataset.dt = "";
            text_area_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;
            
            inner = document.createElement('div');
            inner.classList.add('inner');
            text_area_placeholder.appendChild(inner);

            if(this.isMobile === true) {
                text_area_placeholder.dataset.initialWidth = pivot_ele.clientWidth;
                text_area_placeholder.dataset.initialHeight = pivot_ele.clientHeight;
                text_area_placeholder.dataset.initialTop = pivot_position.top;
                text_area_placeholder.dataset.initialRight = pivot_position.right;
                text_area_placeholder.dataset.initialFontSize = this.TEXT_AREA_PLACEHOLDER_STYLES.fontSize;
                text_area_placeholder.dataset.initialLineHeight = this.TEXT_AREA_PLACEHOLDER_STYLES.lineHeight;
            }
           
            text_area_placeholder.style.fontSize   = `${this.TEXT_AREA_PLACEHOLDER_STYLES.fontSize}px`;
            text_area_placeholder.style.lineHeight = `${this.TEXT_AREA_PLACEHOLDER_STYLES.lineHeight}px`;
            text_area_placeholder.style.width  = `${pivot_ele.clientWidth}px`;
            text_area_placeholder.style.height =  `${pivot_ele.clientHeight}px`;           
            text_area_placeholder.style.top = `${pivot_position.top}pt`;
            text_area_placeholder.style.right = `${pivot_position.right}pt`;                
            
            if(is_required === true) {
                modal_dialog_text_area.append_asterisk_to_placeholder(text_area_placeholder);
            }            

            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(text_area_placeholder);
            
            max_chars_allowed = this.calc_max_chars_for_textarea(text_area_placeholder); 
            text_area_placeholder.dataset.maxLength = max_chars_allowed;                 

            pivot_ele.parentNode.removeChild(pivot_ele);
        }        
    }

    position_in_advance_fields() {
        var i, 
            p_ele = null,
            pivot_ancestor = null,  
            pivot_position = null, 
            text_in_advance_placeholder = null,
            target_container = null,        
            in_advance_fields = this.get_items_sorted_top_to_bottom();  
        
        for (i = 0; i < in_advance_fields.length; i++) {
            p_ele = in_advance_fields[i];
            
            pivot_ancestor = p_ele.parentNode; // grab div element
            pivot_position = this.positioning_tool.calc_position(pivot_ancestor.style.top, pivot_ancestor.style.left, pivot_ancestor.clientWidth);

            text_in_advance_placeholder = document.createElement('div');
            text_in_advance_placeholder.classList.add('text-in-advance-placeholder');
            
            text_in_advance_placeholder.innerHTML = p_ele.textContent;
                        
            this.isMobile && text_in_advance_placeholder.classList.add('mobile-images');   
            text_in_advance_placeholder.dataset.isDirty = "true";

            if(this.isMobile === true) {
                text_in_advance_placeholder.dataset.initialWidth = pivot_ancestor.clientWidth;
                text_in_advance_placeholder.dataset.initialHeight = pivot_ancestor.clientHeight;
                text_in_advance_placeholder.dataset.initialTop = pivot_position.top;
                text_in_advance_placeholder.dataset.initialRight = pivot_position.right - 18;
                text_in_advance_placeholder.dataset.initialFontSize = pivot_ancestor.clientHeight*0.5;
                text_in_advance_placeholder.dataset.initialLineHeight = pivot_ancestor.clientHeight;
            }

            text_in_advance_placeholder.style.width  = `${pivot_ancestor.clientWidth}px`;                
            text_in_advance_placeholder.style.height = `${pivot_ancestor.clientHeight}px`;
            text_in_advance_placeholder.style.fontSize = `${ pivot_ancestor.clientHeight*0.5}px`;
            text_in_advance_placeholder.style.lineHeight = `${pivot_ancestor.clientHeight}px`;
            text_in_advance_placeholder.style.top = `${pivot_position.top}pt`;
            text_in_advance_placeholder.style.right = `${pivot_position.right - 18}pt`;
                          
            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(text_in_advance_placeholder);

            pivot_ancestor.parentNode.removeChild(pivot_ancestor);
        }   
    }

    position_in_advance_signatures() {
        let i,
            pivot_ele,
            pivot_position, 
            target_container,          
            in_advance_signatures = document.querySelectorAll('[data-tx-name=in-advance-signature]');

        for (i = 0; i < in_advance_signatures.length; i++) {
            pivot_ele = in_advance_signatures[i];                

            pivot_position = this.positioning_tool.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);

            pivot_ele.dataset.isDirty = "true";

            if(this.isMobile === true) {
                pivot_ele.classList.add('mobile-images'); 
                pivot_ele.dataset.initialWidth = pivot_ele.offsetWidth;
                pivot_ele.dataset.initialHeight = pivot_ele.offsetHeight;
                pivot_ele.dataset.initialTop = pivot_position.top ;//in points
                pivot_ele.dataset.initialRight = pivot_position.right;//in points
            }

            pivot_ele.style.top = `${pivot_position.top}pt`;
            pivot_ele.style.right = `${pivot_position.right - 18}pt`;

            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(pivot_ele);               
        }
    } 
    
    is_text_placeholer_bold  (placeholder) {
        let txt = placeholder.dataset.txName; 
        return txt.includes("^^^");
    }
    
    is_text_placeholer_required (placeholder) {
        let txt = placeholder.dataset.txName; 
        return txt.includes("~~~");
    }

    calc_max_chars_for_textarea (placeholder) { 
        var result = 0;
        var placeholeder_width = this.isMobile === true ?  Number(placeholder.dataset.initialWidth)  : placeholder.clientWidth; 
        var placeholeder_height = this.isMobile === true ? Number(placeholder.dataset.initialHeight) : placeholder.clientHeight; 

        var rows = Math.floor(placeholeder_height / this.TEXT_AREA_PLACEHOLDER_STYLES.lineHeight);
        var colls = Math.floor(placeholeder_width / this.TEXT_AREA_PLACEHOLDER_STYLES.fontSize); 
        
        var max_chars_allowed = Math.round(colls * rows * 2.1);
        result = max_chars_allowed - (max_chars_allowed % 10);             
        return result;
    }

    get_items_sorted_top_to_bottom(){
        let node_list = document.querySelectorAll("[data-tx-name^='in-advance-label'] > p");
        let arr = Array.from(node_list); 
        arr.sort(sort_by_top_position);

        return arr;

        function sort_by_top_position (a, b) {
            var a_top =  Number(a.parentNode.parentNode.style.top.replace('pt', ''));
            var b_top =  Number(b.parentNode.parentNode.style.top.replace('pt', ''));
            return a_top - b_top;                    
        }
    } 
    
    fill_all_text_placeholders (modal_dialog_text_row, modal_dialog_text_area) {
               
        const all_values = ["תאריך לידה" ,"1101 שח", "Omer Sinai", "17-03-2007", "12345.52$", "ניסים" , "בניין 16"];      
        const all_lorem = [ "בוקר טוב עולם, מה שלום כולם, אני יורד מן החוטים, אני כבר ילד אמיתי", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever s"];
       
        document.querySelectorAll('.text-field-placeholder[data-is-required=true]').forEach(item => {  
            let index = Math.round(Math.random() * 100) % all_values.length;
            modal_dialog_text_row.handle_dirty_text_insertion(item.querySelector(".inner"), all_values[index]);           
        });    
        
        document.querySelectorAll('.text-area-placeholder[data-is-required=true]').forEach(item => { 
            let index = Math.round(Math.random() * 100) % all_lorem.length; 
            modal_dialog_text_area.handle_dirty_text_insertion(item.querySelector(".inner"), all_lorem[0]);           
        });
        
        document.querySelectorAll('.checkbox-placeholder').forEach(item => { 
            if(Math.round((Math.random() * 100))%10 > 5) {
                item.dataset.isDirty = "true";
                item.dataset.dt = new Date().valueOf(); 
                item.querySelector(".checked").style.display = "block";
                item.querySelector(".unchecked").style.display = "none";
            } else {
                item.dataset.isDirty = "false";
                item.dataset.dt = ""; 
                item.querySelector(".checked").style.display = "none";
                item.querySelector(".unchecked").style.display = "block";
            }
            
        });
    }

    determine_fields_status() {
        let result = "";
        //get all placeholders that are REQUIRED and NOT DIRTY 
        let node_list = document.querySelectorAll(".sign-here-placeholder[data-is-dirty='false'], .textual[data-is-required='true'][data-is-dirty='false']");
        if(node_list.length > 0) {
            result = 'required-and-unrequired-fields';
        } else if(node_list.length === 0) {
            //get all placeholders that are NOT REQUIRED and NOT DIRTY 
            node_list = document.querySelectorAll(".textual[data-is-required='false'][data-is-dirty='false'], .checkbox-placeholder");
            if(node_list.length > 0) {
                result = 'only-unrequired-fields';
            } else if (node_list.length === 0) {
                result = 'no-fields-left';
            }
        }
        return result;
    }

    get_sorted_placeholders() {
        var node_list = document.querySelectorAll(".sign-here-placeholder[data-is-dirty='false'], .textual[data-is-dirty='false'], .checkbox-placeholder");
        var arr = Array.from(node_list); 
        arr.sort(sort_by_top_position);               

        return arr;

        function sort_by_top_position (a, b) {
            var a_top =  Number(a.dataset.globalTopForSorting);
            var b_top =  Number(b.dataset.globalTopForSorting);
            return a_top - b_top;                    
        }                
    }
}

const instance = new DocumentFields();
Object.freeze(instance);

module.exports.instance = instance;