class SignaturePositioningTool {
    constructor () {
        this.SIZES = {
            a4:     { name: 'a4',     PT_WIDTH: 595, PT_HEIGHT: 842,  CM_WIDTH: 21,    CM_HEIGHT: 29.7 /*orientation: 'portrait'*/ },
            legal:  { name: 'legal',  PT_WIDTH: 612, PT_HEIGHT: 1009, CM_WIDTH: 21.6,  CM_HEIGHT: 35.6  /*orientation: 'portrait'*/ },
            letter: { name: 'letter', PT_WIDTH: 612, PT_HEIGHT: 792,  CM_WIDTH: 21.6,  CM_HEIGHT: 27.9 /*orientation: 'portrait'*/ },
            a4_landscape: { name: 'a4_landscape', PT_WIDTH: 842, PT_HEIGHT: 595, CM_WIDTH: 29.7, CM_HEIGHT: 21 /*orientation: 'landscape'*/ }
        };

        this.paper_name = get_paper_name();
        this.PAPER_WIDTH = this.SIZES[this.paper_name].PT_WIDTH;
        this.PAPER_HEIGHT = this.SIZES[this.paper_name].PT_HEIGHT;

    
        function get_paper_name() {
        var result = "a4";
            var paper_name = document.querySelector("#paper-size");
            if (paper_name !== null) {
                result = paper_name.value.toLowerCase();
        }
        return result;
    }
    }    
    
    calc_position(global_top_str, global_left_str,signature_width_px) {
            
        var global_number_top_in_pt = Number(global_top_str.replace(/pt/gi, ''));
        var global_number_left_in_pt = Number(global_left_str.replace(/pt/gi, ''));

        var page_num = calculate_page_number.call(this, global_number_top_in_pt);
        var offset_top = calculate_offset_top.call(this, global_number_top_in_pt, page_num);
        var offset_right = calculate_offset_right.call(this, global_number_left_in_pt, signature_width_px);

        return {
            page_num: page_num,
            right: offset_right,
            top: offset_top,
            global_top_for_sorting: Math.floor(global_number_top_in_pt)
        }

        function calculate_page_number(global_top_number) {
            var result = 0, calculationN, calculationZ, calculationP;                
    
            var calculationN = Math.floor(global_top_number / this.PAPER_HEIGHT);
            if (calculationN > 0) {
                calculationZ = calculationN * 28 + 6;
                calculationP = Math.floor((global_top_number - calculationZ) / this.PAPER_HEIGHT);
                result = calculationP + 1;
            } else {
                result = 1;
            }
    
            return result;
        }

        function calculate_offset_top(global_top_number, page_num) {
            var result = 0,                    
                calculationH = 0,
                calculationR = 0;
               
            if (page_num === 1) {
                result = global_top_number + 6;
            } else {
                calculationH = (page_num - 1) * 28 + 6;
                calculationR = global_top_number - calculationH;
                result = calculationR % this.PAPER_HEIGHT;
                result += 11;
            } 
    
            return result;
        }

        function calculate_offset_right(global_left, signature_width_px) {
            var result;
            var img_width_pt = signature_width_px * 3 / 4;
            result = this.PAPER_WIDTH - global_left - img_width_pt - 3;
            return result;
        }
    }

    get_current_height() {
        return this.PAPER_HEIGHT;
    }

    get_current_width() {
        return this.PAPER_WIDTH;
    }

    get_paper_properties() {
        return this.SIZES[this.paper_name];
    }
} 

module.exports = SignaturePositioningTool;