import SignaturePositioningTool from "./SignaturePositioningTool";
import Tooltips from "./Tooltips";
import SignatureModal from "./SignatureModal";
import TextRowModal from "./TextRowModal";
import TextAreaModal from "./TextAreaModal";
import GoodSignBase from "./GoodSignBase";
import AgreementModal from "./AgreementModal";
import jQuery from "jquery";
import PayloadForServer from "./PayloadForServer";
import DocumentFields from "./DocumentFields";

class App extends GoodSignBase {
    constructor() {   
        super();     
        this.current_stage = 0;        
        this.DEBUG_EMULATE_SENDING_PDF = false;
        this.DEBUG_JUMP_DIRECTLY_TO_STAGE = 1;        
        this.DEBUG_CLEAR_CONSOLE_ON_START= false;
        this.DEBUG_AUTOFILL_TEXTS_AND_CBXS = false; 
        this.isMobile = this.is_mobile();        
        this.LINE_COLOR = "#001eaa";          
        this.ENV = "prod";
        this.stages = [];
        
        this.modal_dialog_signature = null;
        this.modal_dialog_text_row = null;
        this.modal_dialog_text_area = null;
        this.modal_dialog_agreement = null;
              
        this.back_to_doc = null;
        this.white_board = null;
        this.white_board_title = null;
        this.white_board_subtitle = null;
        this.bottom_bar = null;
        this.bottom_bar_left = null;
        this.bottom_bar_center = null;
        this.bottom_bar_right = null;
        this.button_open_terms = null;
        this.center_btn_up = null;
        this.center_btn_down = null;
        this.upload_to_server_spinner = null;
        this.left_btn_action = null;
        this.left_btn_action_txt = null; 
        this.selected_text_row = null; 
        this.signatures_validator = null;
        this.body = document.querySelector("body");        

        this.pdf_document = null;
        this.payload_for_server = null;
        this.business_name = "";
        this.business_email_address = "";
        this.customer_fullname = "";
        this.additional_message = "";
        this.business_self_sign = false;
        this.server_token = ""; 
        this.journey_id="";
        this.doc_name = ""; 
        
        this.route_upload_pdf_assets = "/upload-pdf-assets";

        this.btn_zoom_pan_restore = null;
        this.advertise_good_sign = null;
        this.approve_agreement_container = null;
        this.txt_i_accept = null;
        this.additional_legal_terms = null;
        this.has_additional_legal_terms = false;
        this.has_white_label_image = false;
        this.logo_image_name = "";
        this.logo_extra_class = "";
        this.container_p = {};
        this.data_images = null;
        this.mobile_scenario_all_placeholders = null;
        this.current_placeholder_pointer = 0; 
        this.IS_ONLINE = false; 
        this.positioning_tool = new SignaturePositioningTool();
        this.documentFields = DocumentFields.instance;
        this.language = this.get_language();
        this.set_data_from_hidden_fields();        
        this.tooltip_util = Tooltips.instance;  
        
        this.payload_for_server_pdf_generation = {};
        this.url_to_server_generated_pdf = "";
        this.override_debug_behavior();
        this.prepare_head();
        this.append_additional_markup_to_body();        
        this.remove_paragraph_margin_bottom();
        
        this.data_images = this.set_data_images_class_names_and_sizes();
        
        this.wrap_data_images_with_containers();
        
        this.set_class_members();       
        
        this.set_language_styles();
        this.init_content_assets();
        
        if (navigator.onLine === true) {            
            this.IS_ONLINE = true;
        } else {
            this.IS_ONLINE = false;
            this.handle_go_to_stage_seven(); 
            return;          
        }

        this.documentFields.position_all_placeholders(this.modal_dialog_text_area, this.modal_dialog_text_row, this.modal_dialog_signature);
        this.increase_height_to_last_page();
        if(this.DEBUG_AUTOFILL_TEXTS_AND_CBXS === true) {
            this.documentFields.fill_all_text_placeholders(this.modal_dialog_text_area, this.modal_dialog_text_row);            
        }
        
        if(this.isMobile === true) {
            this.init_pinched_elements_data_structure();
        }

        this.bind_events.call(this);
    }

    initialize () {

        switch(this.DEBUG_JUMP_DIRECTLY_TO_STAGE) {
            case 1: 
                this.handle_go_to_stage_one(); 
                break;
            case 2: 
                this.left_btn_action.classList.remove("partial-disabled");
                this.handle_go_to_stage_two({review_doc: false}); 
                break;
            case 2.5: 
                this.handle_stage_generate_pdf(); 
                break;
            case 3: 
                this.show_overlay();
                this.disable_body_scrolling();
                this.handle_go_to_stage_three(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;
            case 4: 
                this.handle_go_to_stage_four(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;    
            case 5: 
                this.handle_go_to_stage_five(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;            
            case 6: //not applicable for direct jump                    
                break;
            case 7: //show disconnected message
                this.show_overlay();
                this.handle_go_to_stage_seven(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;
            case 8: 
                this.handle_go_to_stage_eight(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;            
            default: 
                this.handle_go_to_stage_one();
        }
        this.remove_style_shel_omer3();
        this.DEBUG_CLEAR_CONSOLE_ON_START && setTimeout( () => console.clear(), 1000);                                
    }    

    handle_go_to_stage_one () {
        this.disable_body_scrolling();
        if(this.business_self_sign === false) {
            this.left_btn_action.classList.add("partial-disabled");
            this.current_stage = 1;            
            this.render_current_stage(); 
            this.remove_style_shel_omer3();
        } else if (this.business_self_sign === true) {
            this.handle_go_to_stage_eight();
            this.remove_style_shel_omer3();
        }
    }

    handle_go_to_stage_two(options) {
        this.modal_dialog_agreement.close();
              
        window.scroll({ top: 0, left: 0, behavior: "auto" });          
        this.enable_body_scrolling();            
        this.current_stage = 2;
        this.render_current_stage();            
        
        if(this.isMobile === true) { 
           this.render_bottom_bar();
        } else {           
            if(options.review_doc === false){
                this.handle_stage_two()
            }
        }          
    }

    handle_stage_two() {

        var i, top, ele, count_required_and_dirty = 0, 
        sorted_reuired_placeholders = get_sorted_reuired_placeholders();//returns only required textual fields, required text areas and signatures
             
        for (i = 0; i < sorted_reuired_placeholders.length; i++) {
            ele = sorted_reuired_placeholders[i];
            if (ele.dataset.isDirty === "false") {                   
                top = jQuery(ele).offset().top;// ..ele.offsetTop;
                window.scroll({ top: top - 400, left: 0, behavior: 'smooth' });
                break;
            } else {
                count_required_and_dirty = count_required_and_dirty + 1;
            }
        }
            
        if (count_required_and_dirty === sorted_reuired_placeholders.length) {            
            this.handle_stage_generate_pdf();
        }
        

        function get_sorted_reuired_placeholders() {
            var node_list = document.querySelectorAll(".sign-here-placeholder, .textual[data-is-required='true']");
            var arr = Array.from(node_list); 
            arr.sort(sort_by_top_position);               

            return arr;

            function sort_by_top_position (a, b) {
                var a_top =  Number(a.dataset.globalTopForSorting);
                var b_top =  Number(b.dataset.globalTopForSorting);
                return a_top - b_top;                    
            }               
        }    
    }
    
    prepare_doc_to_pdf_generation() {
        if(this.isMobile === true) {
            this.restore_container_to_initial_scale();
        }
        
        this.disable_body_scrolling();
        this.tooltip_util.remove_all_tooltips();
        
        window.scroll({ top: 0, left: 0, behavior: 'auto' });        
    } 

    async handle_stage_generate_pdf() {
                
        this.prepare_doc_to_pdf_generation();       
        const paper_properties = this.positioning_tool.get_paper_properties();
            
        this.payload_for_server_pdf_generation = this.payload_for_server.initialize_payload(this.journey_id, this.server_token, this.language, this.business_name, this.business_email_address, this.doc_name, this.customer_fullname, this.business_self_sign, paper_properties);
        this.payload_for_server.fill_payload_for_generating_server_pdf(this.payload_for_server_pdf_generation);
        this.handle_go_to_stage_three();
    }
    
    handle_go_to_stage_three() {
        this.hide_overlay();       
        this.bottom_bar.style.display = "block";

        this.current_stage = 3;
        this.render_current_stage();
        this.signatures_validator.style.display = "none";
    }

    handle_go_to_stage_four () {            
        this.signatures_validator.style.display = "block";
        this.left_btn_action.classList.remove("fully-disabled");     
        
        if(this.business_self_sign == false) {            
            this.current_stage = 4;
        } else {
            this.current_stage = 5;
        }
        this.render_current_stage();
    }

    handle_server_error() {
        this.upload_to_server_spinner.style.display = "none";
        this.left_btn_action.style.display = "none";
        this.white_board_title.innerHTML = this.language === "heb" ? "לצערנו אירעה תקלה במערכת <i class='icon-warning-sign'></i>" : "Oooops, we have a system error <i class='icon-warning-sign'></i>";
        this.white_board_subtitle.innerHTML = this.language === "heb" ? "<span class='gray'>נסה לחתום שוב על המסמך מאוחר יותר</span>" : "<span class='gray'>Please try to sign the document later</span>";
        this.signatures_validator.innerHTML = this.language === "heb" ? "נסה לחתום שוב על המסמך מאוחר יותר" : "Please try to sign the document later";
    }   

    handle_go_to_stage_five() {
        this.disable_body_scrolling();
        this.left_btn_action.classList.remove("none");
        this.advertise_good_sign.style.display = "block";            
        this.current_stage = 5;
        this.render_current_stage(); 
    }       

    handle_go_to_stage_six() {       
       window.open(this.url_to_server_generated_pdf, '_blank');       
    } 

    handle_go_to_stage_eight(){
        this.current_stage = 8;            
        this.render_current_stage();  
    }

    jump_to_placeholder(direction) {
        var top = 0, ele = this.mobile_scenario_all_placeholders[this.current_placeholder_pointer];

        do { 
            if(this.tooltip_util.has_tooltip(ele) === true) {
                this.tooltip_util.remove_tooltip(ele);
            }

            modify_next_element_pointer.call(this, direction);                
            ele = this.mobile_scenario_all_placeholders[this.current_placeholder_pointer];
        } while(ele.dataset.isDirty === 'true');

       
        top = jQuery(ele).offset().top; //ele.getBoundingClientRect().top;
        window.scroll({ top: top - 300, left: 0, behavior: 'smooth' });
        this.tooltip_util.attach_tooltip_to_element(ele);

        function modify_next_element_pointer(direction) {
            
            switch(direction) {
                case 'up': 

                    if(this.current_placeholder_pointer > 0 ) {
                        this.current_placeholder_pointer -= 1;                                                
                    } else if(this.current_placeholder_pointer === 0 ) {
                        this.current_placeholder_pointer = this.mobile_scenario_all_placeholders.length-1;                                                
                    }
                    
                    break;
                case 'down': 
                    if(this.current_placeholder_pointer < this.mobile_scenario_all_placeholders.length) {
                        this.current_placeholder_pointer += 1;
                        this.current_placeholder_pointer = this.current_placeholder_pointer % this.mobile_scenario_all_placeholders.length;
                    } 
                    break;
                default:
            }
        }
    } 
    
    bind_pinch_and_pan () {  
            
        var mc = new Hammer.Manager(this.container_p.ref);            
        var pinch = new Hammer.Pinch();
        var pan = new Hammer.Pan({ direction: Hammer.DIRECTION_ALL });
        
        pinch.recognizeWith(pan);            
        mc.add([pinch, pan]);            
        mc.add( new Hammer.Tap({ event: 'doubletap', taps: 2, interval: 500, posThreshold: 100 }) );
        
        mc.on("pinchstart panstart", () => {
            this.container_p.children.forEach( item => {
                item.children.sign_placeholders.forEach( _item => {
                    this.tooltip_util.has_tooltip(_item.ref) == true &&  this.tooltip_util.hide_tooltip(_item.ref);               
                });

                item.children.text_field_placeholders.forEach( _item => {
                    this.tooltip_util.has_tooltip(_item.ref) == true &&  this.tooltip_util.hide_tooltip(_item.ref);               
                });

                item.children.text_area_placeholders.forEach( _item => {
                    this.tooltip_util.has_tooltip(_item.ref) == true &&  this.tooltip_util.hide_tooltip(_item.ref);               
                });

                item.children.checkbox_placeholders.forEach( _item => {
                    this.tooltip_util.has_tooltip(_item.ref) == true &&  this.tooltip_util.hide_tooltip(_item.ref);               
                });
            });
            
        }).on("pinchmove panmove", ev => {                
            this.container_p.ref.style.left = `${this.container_p.current_left + ev.deltaX}px`;                
            this.container_p.ref.style.top =  `${this.container_p.current_top + ev.deltaY}px`;             

            if(this.container_p.current_width * ev.scale >= this.container_p.initial_width) {
                //set new width and height to p
                this.container_p.ref.style.width = (this.container_p.current_width * ev.scale) + "px";
                this.container_p.ref.style.height = (this.container_p.current_height * ev.scale) + "px";
                
                //calculate new width and height for all children: images_wrapper 
                this.container_p.children.forEach( item => {                        
                    item.ref.style.width = `${item.current_width * ev.scale}px`;
                    item.ref.style.height = `${item.current_height * ev.scale}px`;
                    
                    //calculate new width and height for data_image child 
                    item.children.data_image.ref.style.width = `${item.children.data_image.current_width * ev.scale}px`;
                    item.children.data_image.ref.style.height = `${item.children.data_image.current_height * ev.scale}px`;

                    //calculate new width and height for checkbox children 
                    item.children.checkbox_placeholders.forEach( item => {
                        item.ref.querySelector(".checked").style.fontSize = `${item.current_fontsize * ev.scale}px`;
                        item.ref.querySelector(".unchecked").style.fontSize = `${item.current_fontsize * ev.scale}px`;                        
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;                            
                    });

                    //calculate new width and height for signature children 
                    item.children.sign_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;                            
                    });

                    //calculate new sizes for text_field children 
                    item.children.text_field_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                        item.ref.style.fontSize = `${item.current_height * 0.5 * ev.scale}px`;                           
                        item.ref.style.lineHeight = `${(item.current_height * ev.scale) -(4*ev.scale)}px`;
                    });

                     //calculate new sizes for text_area children 
                     item.children.text_area_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                        item.ref.style.fontSize = `${item.current_font_size * ev.scale}px`;                           
                        item.ref.style.lineHeight = `${item.current_line_height * ev.scale}px`;
                    });

                    //calculate new sizes for in advance labels children 
                    item.children.in_advance_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                        item.ref.style.fontSize = `${item.current_height * 0.5 * ev.scale}px`;                           
                        item.ref.style.lineHeight = `${item.current_line_height * ev.scale}px`;
                    });
                     //calculate new sizes for in advance signatures children
                    item.children.in_advance_signature_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                    });
                    
                });
            }
        })
        .on("pinchend panend", ev => {
            //console.log(`pinchend panend:dir: ${ev.direction}, scale ${ev.scale}, dX: ${ev.deltaX}, ele: ${ev.target.className} `);
            
            this.container_p.current_width = Number(this.container_p.ref.style.width.replace('px', ''));
            this.container_p.current_height = Number(this.container_p.ref.style.height.replace('px', ''));
            this.container_p.current_left = Number(this.container_p.ref.style.left.replace('px',''));
            this.container_p.current_top = Number(this.container_p.ref.style.top.replace('px',''));
                            
            //calculate and update current_width for all children: images_wrapper 
            this.container_p.children.forEach( item => {
                item.current_width =  Number(item.ref.style.width.replace('px',''));
                item.current_height = Number(item.ref.style.height.replace('px',''));

                item.children.data_image.current_width =  Number(item.children.data_image.ref.style.width.replace('px',''));
                item.children.data_image.current_height = Number(item.children.data_image.ref.style.height.replace('px',''));

                //calculate new width and height for checkbox children 
                item.children.checkbox_placeholders.forEach( item => {
                    item.current_fontsize  =  Number(item.ref.querySelector(".checked").style.fontSize.replace('px',''));
                    item.current_top    =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right  =  Number(item.ref.style.right.replace('pt',''));                        
                });

                //calculate new width and height for signature children 
                item.children.sign_placeholders.forEach( item => {
                    item.current_width  =  item.ref.offsetWidth;
                    item.current_height =  item.ref.offsetHeight;
                    item.current_top    =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right  =  Number(item.ref.style.right.replace('pt',''));                        
                });

                //calculate new sizes for text_field children 
                item.children.text_field_placeholders.forEach( item => {
                    item.current_width       =  item.ref.offsetWidth;
                    item.current_height      =  item.ref.offsetHeight;
                    item.current_top         =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right       =  Number(item.ref.style.right.replace('pt',''));
                    item.current_font_siz    =  Number(item.ref.style.fontSize.replace('px',''));
                    item.current_line_height =  Number(item.ref.style.lineHeight.replace('px',''));                        
                });

                //calculate new sizes for text_area children 
                item.children.text_area_placeholders.forEach( item => {
                    item.current_width    =  item.ref.offsetWidth;
                    item.current_height   =  item.ref.offsetHeight;
                    item.current_top      =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right    =  Number(item.ref.style.right.replace('pt',''));
                    item.current_font_size = Number(item.ref.style.fontSize.replace('px',''));
                    item.current_line_height = Number(item.ref.style.lineHeight.replace('px',''));
                });

                //calculate new sizes for in_advance children 
                item.children.in_advance_placeholders.forEach( item => {
                    item.current_width    =  item.ref.offsetWidth;
                    item.current_height   =  item.ref.offsetHeight;
                    item.current_top      =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right    =  Number(item.ref.style.right.replace('pt',''));
                    item.current_font_size = Number(item.ref.style.fontSize.replace('px',''));
                    item.current_line_height = Number(item.ref.style.lineHeight.replace('px',''));
                });

                item.children.in_advance_signature_placeholders.forEach( item =>{
                    item.current_width  =  item.ref.offsetWidth;
                    item.current_height =  item.ref.offsetHeight;
                    item.current_top    =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right  =  Number(item.ref.style.right.replace('pt',''));
                });
            }); 
            jQuery(this.btn_zoom_pan_restore).show(500);
        })
        .on("doubletap", () => {
           this.restore_container_to_initial_scale();
        });
    }

    restore_container_to_initial_scale () {

        jQuery(this.btn_zoom_pan_restore).fadeOut(200);
        //return the size of it to its initial size            
        this.container_p.current_width    =  this.container_p.initial_width;
        this.container_p.current_height   =  this.container_p.initial_height;
        this.container_p.ref.style.width  = `${this.container_p.current_width}px`;
        this.container_p.ref.style.height = `${this.container_p.current_height}px`;
        this.container_p.current_left     =  this.container_p.initial_left;    
        this.container_p.ref.style.left   = `${this.container_p.current_left}px`;
        this.container_p.current_top      =  this.container_p.initial_top;   
        this.container_p.ref.style.top    = `${this.container_p.current_top}px`;          
        
        this.container_p.children.forEach( item => {
            item.current_width = item.initial_width;
            item.current_height = item.initial_height;
            item.ref.style.width  = `${item.current_width}px`;
            item.ref.style.height = `${item.current_height}px`;
            
            item.children.data_image.current_width = item.children.data_image.initial_width;
            item.children.data_image.current_height = item.children.data_image.initial_height;
            item.children.data_image.ref.style.width = `${item.children.data_image.current_width}px`;
            item.children.data_image.ref.style.height = `${item.children.data_image.current_height}px`;

            //set initial sizes for checkbox children 
            item.children.checkbox_placeholders.forEach( item => {
                item.current_fontsize = item.initial_fontsize; 
                item.current_top    = item.initial_top; 
                item.current_right  = item.initial_right;                
               
                item.ref.querySelector(".checked").style.fontSize = `${item.current_fontsize}px`; 
                item.ref.querySelector(".unchecked").style.fontSize = `${item.current_fontsize}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
            });

            //set initial sizes for signature children 
            item.children.sign_placeholders.forEach( item => {
                item.current_width  = item.initial_width; 
                item.current_height = item.initial_height; 
                item.current_top    = item.initial_top; 
                item.current_right  = item.initial_right;
                
                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
            });

            //set initial sizes for text_field children 
            item.children.text_field_placeholders.forEach( item => {
                item.current_width    = item.initial_width; 
                item.current_height   = item.initial_height; 
                item.current_top      = item.initial_top; 
                item.current_right    = item.initial_right;
                item.current_font_size = item.initial_font_size;
                item.current_line_height = item.initial_line_height;

                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
                item.ref.style.fontSize    = `${item.current_font_size}px`; 
                item.ref.style.lineHeight  = `${item.current_line_height}px`;
            });

            //set initial sizes for text_area children 
            item.children.text_area_placeholders.forEach( item => {
                item.current_width    = item.initial_width; 
                item.current_height   = item.initial_height; 
                item.current_top      = item.initial_top; 
                item.current_right    = item.initial_right;
                item.current_font_size = item.initial_font_size;
                item.current_line_height = item.initial_line_height;

                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
                item.ref.style.fontSize    = `${item.current_font_size}px`; 
                item.ref.style.lineHeight  = `${item.current_line_height}px`;
            });

            //calculate new sizes for in advance labels children 
            item.children.in_advance_placeholders.forEach( item => {
                item.current_width    = item.initial_width; 
                item.current_height   = item.initial_height; 
                item.current_top      = item.initial_top; 
                item.current_right    = item.initial_right;
                item.current_font_size = item.initial_font_size;
                item.current_line_height = item.initial_line_height;

                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
                item.ref.style.fontSize    = `${item.current_font_size}px`; 
                item.ref.style.lineHeight  = `${item.current_line_height}px`;
            });
            //calculate new sizes for in advance signatures children 
            item.children.in_advance_signature_placeholders.forEach( item => {
                item.current_width  = item.initial_width; 
                item.current_height = item.initial_height; 
                item.current_top    = item.initial_top; 
                item.current_right  = item.initial_right;
                
                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
            });
        });
    }
    
    handle_action_click() {
        switch (this.current_stage) {
            case 1:                
                if (this.left_btn_action.classList.contains("partial-disabled") === false) {
                    this.handle_go_to_stage_two({review_doc: false});
                }
                break;
            case 2:
                if(this.isMobile === true){
                    this.handle_stage_generate_pdf();
                } else {
                    this.handle_stage_two();
                }
                break;
            case 3:      
                
                if(this.DEBUG_EMULATE_SENDING_PDF === false ) {
                    this.left_btn_action.classList.add("fully-disabled");
                    this.back_to_doc.classList.add("none");
                    this.upload_to_server_spinner.style.display = "block";
                    this.upload_pdf_payload_to_server();                     
                                                      
                } else {
                    this.left_btn_action.classList.add("fully-disabled");
                    this.back_to_doc.classList.add("none");
                    this.upload_to_server_spinner.style.display = "block";
                    
                    setTimeout(() => {
                        this.upload_to_server_spinner.style.display = "none";
                        this.handle_go_to_stage_four();  
                    },2000);                        
                }

                break;
            case 4:                                 
                this.handle_go_to_stage_five();            
                break;
            case 5:
                this.handle_go_to_stage_six();                
                break;            
            case 8:
                this.handle_go_to_stage_two({review_doc: false});
                break; 
            case 9:
                window.open(this.pdf_document.output('bloburl', { filename: "fileName" }), '_blank');
                break; 
            default: break;
        }

        return false;
    } 

    upload_pdf_payload_to_server() {
        let url = this.get_url(this.ENV, this.route_upload_pdf_assets);
        
        jQuery.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: this.payload_for_server_pdf_generation
        }).done( data => {
            this.upload_to_server_spinner.style.display = "none";
            this.handle_go_to_stage_four();
            this.url_to_server_generated_pdf = data.long_url;    
        }).fail(()=>{
            this.handle_server_error();
        });
    }
        
    handle_approve_agreement_click(ele_trigger) {
        if(ele_trigger.parentNode.dataset.isDirty === "false") {

            ele_trigger.parentNode.dataset.isDirty = "true"; 
            ele_trigger.parentNode.querySelector(".unchecked").style.display = "none";
            jQuery(ele_trigger.parentNode.querySelector(".checked")).fadeIn(250);
            this.left_btn_action.classList.remove("partial-disabled");

        } else if (ele_trigger.parentNode.dataset.isDirty === "true"){
            
            ele_trigger.parentNode.dataset.isDirty = "false";                
            jQuery(ele_trigger.parentNode.querySelector(".checked")).fadeOut(100, function() {
                this.parentNode.querySelector(".unchecked").style.display = "block"    
            });
            this.left_btn_action.classList.add("partial-disabled");
        }
    }

    bind_events() {

        this.button_open_terms.addEventListener('click', function() {            
            this.modal_dialog_agreement.open();
        }.bind(this), false);
        
        this.approve_agreement_container.querySelectorAll(".cbx-inner").forEach(item => {
            item.addEventListener("click", e => {
                this.handle_approve_agreement_click(e.target);                
            });
        });

        this.txt_i_accept.addEventListener('click', e => {
            this.handle_approve_agreement_click(e.target);
        });             

        if(this.isMobile === true) {
            this.center_btn_up.addEventListener('click', function() {
                this.jump_to_placeholder('up');
                
            }.bind(this), false);

            this.center_btn_down.addEventListener('click', () => {
                this.jump_to_placeholder('down');                
            }, false);
           
            // Listen for pinch and pan 
            this.bind_pinch_and_pan();

            this.btn_zoom_pan_restore.addEventListener('click', () => {
                this.restore_container_to_initial_scale();
            }, false);           
        }

        // Listen for the key press.
        this.body.addEventListener('keyup', e => {
            switch(e.keyCode) {
                case 27: //esc key press
                    if(this.modal_dialog_signature.is_open() === true) {
                        this.modal_dialog_signature.close();
                    } else if(this.modal_dialog_text_row.is_open() === true) {
                        this.modal_dialog_text_row.close();                          
                    } else if(this.modal_dialog_text_area.is_open() === true) {
                        this.modal_dialog_text_area.close();
                    } else if(this.modal_dialog_agreement.is_open() === true) {
                        this.modal_dialog_agreement.close();
                    } 
                    break;
                case 13:  //'enter' key press
                    if(this.modal_dialog_text_row.is_open() === true) {
                        this.modal_dialog_text_row.trigger_insert_text();
                    } else if(this.modal_dialog_signature.is_open() === true) {
                        this.modal_dialog_signature.trigger_insert_signature();                        
                    }
                    break;    
            }          
        });           
        
        window.addEventListener('offline', () => { 
            this.handle_go_to_stage_seven(); 
        });
        
        window.addEventListener('online', () => { 
            this.initialize();
        });

        this.back_to_doc.addEventListener("click", () => {            
            this.handle_review_doc_click();                
        }, false);

        this.left_btn_action.addEventListener("click", this.handle_action_click.bind(this), false);
        
        this.left_btn_action.addEventListener("mouseenter", (e) => {            
            if (e.target.classList.contains("partial-disabled") && this.tooltip_util.has_tooltip(this.left_btn_action) === false) {
                this.tooltip_util.attach_approve_agreement_to_element(this.left_btn_action);
            }
        }, false);

        this.left_btn_action.addEventListener('mouseleave', e => {
             if(e.target.classList.contains("partial-disabled") && this.tooltip_util.has_tooltip(this.left_btn_action)) {
                 this.tooltip_util.remove_tooltip(e.target);
             }
        }, false);
    }
    
    init_pinched_elements_data_structure(){
        let container = document.querySelector("body > p:nth-child(1)"), 
        image_wrappers = container.querySelectorAll(".image-wrapper"),                       
        pivot_data_image = null, 
        sign_plh_coll = null,
        text_field_coll = null,  
        text_area_coll = null,
        checkbox_coll = null,
        in_advance_field_coll = null, 
        in_advance_signature_coll = null, 
        pivot_image_wrapper = null;

        container.style.left = `${Number((this.body.offsetWidth / 2) - (container.offsetWidth/2))}px`;
        container.style.top = "0"; 
        
        this.container_p = {
            ref: container,
            initial_width: container.offsetWidth,
            current_width: container.offsetWidth,
            initial_height: container.offsetHeight,
            current_height: container.offsetHeight,
            initial_left: container.offsetLeft,
            current_left: container.offsetLeft,
            initial_top: container.offsetTop,
            current_top: container.offsetTop,
            children: []                    
        };

        image_wrappers.forEach( item => {
            
            pivot_data_image = item.querySelector('.data-image');
            sign_plh_coll = item.querySelectorAll('.sign-here-placeholder');
            text_field_coll = item.querySelectorAll('.text-field-placeholder');
            text_area_coll = item.querySelectorAll(".text-area-placeholder");
            checkbox_coll = item.querySelectorAll(".checkbox-placeholder");
            in_advance_field_coll = item.querySelectorAll(".text-in-advance-placeholder");
            in_advance_signature_coll = item.querySelectorAll('[data-tx-name=in-advance-signature]');

            pivot_image_wrapper = {
                ref: item,
                current_width: item.offsetWidth,                       
                current_height: item.offsetHeight,        
                initial_width: Number(item.dataset.initialWidth),                               
                initial_height: Number(item.dataset.initialHeight),
                initial_left: item.offsetLeft,
                current_left: item.offsetLeft,                    
                children: {
                    data_image: { 
                        ref: pivot_data_image,
                        current_width: pivot_data_image.clientWidth,
                        initial_width: Number(pivot_data_image.dataset.initialWidth),
                        current_height: pivot_data_image.clientHeight,                            
                        initial_height: Number(pivot_data_image.dataset.initialHeight)                            
                    },
                    sign_placeholders:[],
                    text_field_placeholders:[],
                    text_area_placeholders:[],
                    checkbox_placeholders:[],
                    in_advance_placeholders:[],
                    in_advance_signature_placeholders: []
                }
            };

            sign_plh_coll.forEach(function(sign_plh){
                var new_sign_plh =  {
                    ref: sign_plh,
                    current_width: sign_plh.offsetWidth,
                    initial_width: Number(sign_plh.dataset.initialWidth),
                    current_height: sign_plh.offsetHeight,
                    initial_height: Number(sign_plh.dataset.initialHeight),
                    current_top: Number(sign_plh.dataset.initialTop),
                    initial_top: Number(sign_plh.dataset.initialTop),
                    current_right: Number(sign_plh.dataset.initialRight),                        
                    initial_right: Number(sign_plh.dataset.initialRight)       
                };
                pivot_image_wrapper.children.sign_placeholders.push(new_sign_plh);
            });

            checkbox_coll.forEach(function(cbx_plh){
                var new_cbx_plh =  {
                    ref: cbx_plh,
                    current_fontsize: Number(cbx_plh.dataset.initialFontSize),
                    initial_fontsize: Number(cbx_plh.dataset.initialFontSize),                    
                    current_top: Number(cbx_plh.dataset.initialTop),
                    initial_top: Number(cbx_plh.dataset.initialTop),
                    current_right: Number(cbx_plh.dataset.initialRight),                        
                    initial_right: Number(cbx_plh.dataset.initialRight)       
                };
                pivot_image_wrapper.children.checkbox_placeholders.push(new_cbx_plh);
            });

            text_field_coll.forEach(function(textf_plh){
                var new_text_field_plh =  {
                    ref: textf_plh,
                    current_width: textf_plh.offsetWidth,                        
                    initial_width: Number(textf_plh.dataset.initialWidth),
                    current_height: textf_plh.offsetHeight,
                    initial_height: Number(textf_plh.dataset.initialHeight),
                    current_top: Number(textf_plh.dataset.initialTop),
                    initial_top: Number(textf_plh.dataset.initialTop),
                    current_right: Number(textf_plh.dataset.initialRight),                        
                    initial_right: Number(textf_plh.dataset.initialRight),
                    initial_font_size: Number(textf_plh.dataset.initialFontSize),
                    current_font_size: Number(textf_plh.dataset.initialFontSize),
                    initial_line_height: Number(textf_plh.dataset.initialLineHeight),
                    current_line_height: Number(textf_plh.dataset.initialLineHeight)
                };
                pivot_image_wrapper.children.text_field_placeholders.push(new_text_field_plh);
            });

            text_area_coll.forEach(function(texta_plh){
                var new_textarea_plh =  {
                    ref: texta_plh,
                    current_width: texta_plh.offsetWidth,
                    initial_width: Number(texta_plh.dataset.initialWidth),
                    current_height: texta_plh.offsetHeight,                        
                    initial_height: Number(texta_plh.dataset.initialHeight),
                    current_top: Number(texta_plh.dataset.initialTop),
                    initial_top: Number(texta_plh.dataset.initialTop),
                    current_right: Number(texta_plh.dataset.initialRight),
                    initial_right: Number(texta_plh.dataset.initialRight),
                    initial_font_size: Number(texta_plh.dataset.initialFontSize),
                    current_font_size: Number(texta_plh.dataset.initialFontSize),
                    initial_line_height: Number(texta_plh.dataset.initialLineHeight),
                    current_line_height: Number(texta_plh.dataset.initialLineHeight)
                };
                pivot_image_wrapper.children.text_area_placeholders.push(new_textarea_plh);
            });

            in_advance_field_coll.forEach(function(in_adv_item){
                var new_in_adv_obj = {
                    ref: in_adv_item,
                    current_width: in_adv_item.offsetWidth,
                    initial_width: Number(in_adv_item.dataset.initialWidth),
                    current_height: in_adv_item.offsetHeight,                        
                    initial_height: Number(in_adv_item.dataset.initialHeight),
                    current_top: Number(in_adv_item.dataset.initialTop),
                    initial_top: Number(in_adv_item.dataset.initialTop),
                    current_right: Number(in_adv_item.dataset.initialRight),
                    initial_right: Number(in_adv_item.dataset.initialRight),
                    initial_font_size: Number(in_adv_item.dataset.initialFontSize),
                    current_font_size: Number(in_adv_item.dataset.initialFontSize),
                    initial_line_height: Number(in_adv_item.dataset.initialLineHeight),
                    current_line_height: Number(in_adv_item.dataset.initialLineHeight)
                };
                pivot_image_wrapper.children.in_advance_placeholders.push(new_in_adv_obj);
            });

            in_advance_signature_coll.forEach(function(in_adv_item) {
                var new_in_adv_obj = {
                    ref: in_adv_item,
                    current_width: in_adv_item.offsetWidth,
                    initial_width: Number(in_adv_item.dataset.initialWidth),
                    current_height: in_adv_item.offsetHeight,
                    initial_height: Number(in_adv_item.dataset.initialHeight),
                    current_top: Number(in_adv_item.dataset.initialTop),
                    initial_top: Number(in_adv_item.dataset.initialTop),
                    current_right: Number(in_adv_item.dataset.initialRight),                        
                    initial_right: Number(in_adv_item.dataset.initialRight) 
                };

                pivot_image_wrapper.children.in_advance_signature_placeholders.push(new_in_adv_obj);
            });
            
            this.container_p.children.push(pivot_image_wrapper);               
        });            
    }

    render_bottom_bar() {

        var status = this.documentFields.determine_fields_status();

        switch(status){
            case "required-and-unrequired-fields":
                this.bottom_bar_center.style.display = "flex";
                this.bottom_bar_left.style.display = "none"; 
                break;
            case "only-unrequired-fields":
                this.bottom_bar_center.style.display = "flex";
                this.bottom_bar_left.style.display = "flex"; 
                this.signatures_validator.style.display = "none"; 
                break;
            case "no-fields-left":
                this.bottom_bar_center.style.display = "none";
                this.bottom_bar_left.style.display = "flex"; 
                this.signatures_validator.style.display = "none"; 
                break;
        }           
        
        refresh_placeholders_corousel.call(this);
                
        function refresh_placeholders_corousel() {
            var top, ele = null;
            this.mobile_scenario_all_placeholders = this.documentFields.get_sorted_placeholders();
            
            this.mobile_scenario_all_placeholders.forEach( item => {
                if(this.tooltip_util.has_tooltip(item) === true ) {
                    this.tooltip_util.remove_tooltip(item);
                }
            });

            if(this.mobile_scenario_all_placeholders.length > 0) {
                this.current_placeholder_pointer = 0;
                ele = this.mobile_scenario_all_placeholders[this.current_placeholder_pointer];
                top = jQuery(ele).offset().top; //ele.getBoundingClientRect().top;                
                window.scroll({ top: top - 400, left: 0, behavior: 'smooth' });                
                if(this.tooltip_util.has_tooltip(ele) === false) {                
                    this.tooltip_util.attach_tooltip_to_element(ele);
                }
            }             
        }
    }   
    

    handle_go_to_stage_seven() {
        this.hide_overlay();   
        this.current_stage = 7;            
        this.render_current_stage();                     
        this.disable_body_scrolling(); 
    }   

    set_data_from_hidden_fields () {        
        
        var hidden_business_address = document.querySelector("#mail-to-address");
        if (hidden_business_address !== null) {
            this.business_email_address = hidden_business_address.value;
        }

        var hidden_business_name = document.querySelector("#mail-to-name");
        if (hidden_business_name !== null) {
            this.business_name = hidden_business_name.value;
            if(this.business_name.includes("הזנפרץ")) {
                this.has_additional_legal_terms = true;
                this.has_white_label_image = true;
                this.logo_image_name = "shift.jpeg";
                this.logo_extra_class = "";
            } else if(this.business_name === "goodsign") {
                this.has_white_label_image = true;
                this.logo_image_name = "example_logo.png";
                this.logo_extra_class = "example-logo";
            }
        }

        var hidden_customer_name = document.querySelector("#customer-fullname");
        if (hidden_customer_name !== null) {
            this.customer_fullname = hidden_customer_name.value;
        }

        var hidden_additional_message = document.querySelector("#additional-message");
        if (hidden_additional_message !== null) {
            this.additional_message = hidden_additional_message.value;
        }
        
        var hidden_business_self_sign = document.querySelector("#business-self-sign");
        if (hidden_business_self_sign !== null) {
            this.business_self_sign = hidden_business_self_sign.value === "true";
        }

        var hidden_token = document.querySelector("#token");	
        if (hidden_token !== null) {	
            this.server_token = hidden_token.value;	
        }

        var hidden_doc_name = document.querySelector("#doc_name");
        if (hidden_doc_name !== null) {
            this.doc_name = hidden_doc_name.value;
        }
        
        var hidden_journey_id = document.querySelector("#journey-id");
        if (hidden_journey_id !== null) {	
            this.journey_id = hidden_journey_id.value;	
        } 
    }

    override_debug_behavior () {
        var debug_env = document.querySelector("#debug-env");
        if(debug_env !== null) {
            this.ENV = debug_env.value.toLowerCase().trim();
            console.log(`working with env: ${this.ENV}`);
        }

        var debug_jump_to_ele = document.querySelector("#debug-jump-directly-to-page");
        if(debug_jump_to_ele !== null) {
            this.DEBUG_JUMP_DIRECTLY_TO_STAGE = Number(debug_jump_to_ele.value.trim());
        }
        
        var debug_emulate_sending_pdf = document.querySelector('#debug-emulate-sending-pdf');
        if(debug_emulate_sending_pdf !== null) {
            this.DEBUG_EMULATE_SENDING_PDF = debug_emulate_sending_pdf.value.toLowerCase().trim() === 'true';
        }
        var debug_autofill_texts_and_cbxs = document.querySelector('#debug-autofill-texts-and-cbxs');
        if(debug_autofill_texts_and_cbxs !== null) {
            this.DEBUG_AUTOFILL_TEXTS_AND_CBXS = debug_autofill_texts_and_cbxs.value.toLowerCase().trim() === 'true';            
        }
    }

    set_data_images_class_names_and_sizes(){        
        var image_collection = document.querySelectorAll("img[src^='data:image']:not([data-tx-name])");
        
        image_collection.forEach(function(item) {
            //add each data image a class name "data-image" for better selector
            item.classList.add("data-image");
            item.dataset.initialWidth = item.clientWidth;
            item.dataset.initialHeight = item.clientHeight;                
        });
        
        return image_collection;            
    }

    wrap_data_images_with_containers() {
            
        var images_parent = this.data_images[0].parentNode;

        remove_br_elements();           
        wrap_each_data_image_with_container.call(this);
        
        function wrap_each_data_image_with_container() {
            var i, pivot_wrapper_div = null, wrappers_collection = null;                    

            for (i = 0; i < this.data_images.length; i++) {
                pivot_wrapper_div = document.createElement('div');
                pivot_wrapper_div.setAttribute('id', `image-wrapper${i + 1}`);
                pivot_wrapper_div.classList.add('image-wrapper');
                if(this.isMobile===true){
                    pivot_wrapper_div.classList.add('mobile-images');
                }
                images_parent.appendChild(pivot_wrapper_div);
            } 
            
            wrappers_collection = document.querySelectorAll('.image-wrapper');
            for (i = 0; i < wrappers_collection.length; i++) {
                wrappers_collection[i].appendChild(this.data_images[i]);
            }                
            set_containers_of_data_image_sizes();
        }
        function set_containers_of_data_image_sizes () {
            var wrappers_coll = document.querySelectorAll('.image-wrapper');
            wrappers_coll.forEach(function(item){
                item.style.width = `${item.offsetWidth}px`;
                item.style.height = `${item.offsetHeight}px`;
                item.dataset.initialWidth = item.offsetWidth;
                item.dataset.initialHeight = item.offsetHeight;
            });    
        }

        function remove_br_elements() {
            var i, brNodeList = null;

            brNodeList = images_parent.querySelectorAll("br");

            for (i = 0; i < brNodeList.length; i++) {
                brNodeList[i].parentNode.removeChild(brNodeList[i]);
            }
        }           


    }

    set_class_members() {        
        
        this.white_board = document.querySelector("#white-board");
        this.white_board_title = this.white_board.querySelector(".title");
        this.white_board_subtitle = this.white_board.querySelector(".sub-title");
        
        this.bottom_bar = document.querySelector("#bottom-bar");            
        this.bottom_bar_left = this.bottom_bar.querySelector("#left-side");
        this.left_btn_action = this.bottom_bar_left.querySelector("#btn-action");
        this.left_btn_action_txt = this.bottom_bar_left.querySelector("#btn-action > .txt");
        
        this.bottom_bar_center = this.bottom_bar.querySelector("#center-side");
        this.center_btn_up = this.bottom_bar_center.querySelector("#btn-jump-up");
        this.center_btn_down = this.bottom_bar_center.querySelector("#btn-jump-down");

        this.bottom_bar_right = this.bottom_bar.querySelector("#right-side");
        this.back_to_doc = this.bottom_bar.querySelector("#back-to-doc");

        this.upload_to_server_spinner = this.bottom_bar.querySelector("#upload-to-server-spinner");
        this.signatures_validator = this.bottom_bar.querySelector("#signatures-validator");
        this.btn_zoom_pan_restore = document.querySelector("#zoom-pan-restore");
        this.advertise_good_sign = document.querySelector("#advertise-good-sign");    
        this.approve_agreement_container = document.querySelector("#approve-terms-container");        
        this.txt_i_accept = this.approve_agreement_container.querySelector("#txt-i-accept");        
        this.button_open_terms = this.approve_agreement_container.querySelector("#btn-open-terms");
        this.additional_legal_terms = document.querySelector("#additional-legal-terms");        
        
        this.modal_dialog_signature = new SignatureModal(
            document.querySelector("#modal-dialog-signature"), this.LINE_COLOR, this);
            
        this.modal_dialog_text_row = new TextRowModal(
            document.querySelector("#modal-dialog-text-row"), this);

        this.modal_dialog_text_area = new TextAreaModal(
            document.querySelector("#modal-dialog-text-area"), this);
        
        this.modal_dialog_agreement = new AgreementModal(
            document.querySelector("#modal-dialog-agreement"), this
        );

        this.payload_for_server = new PayloadForServer(this);

        if (this.isMobile === true) {
            this.bottom_bar.classList.add("mobile-images");
            this.btn_zoom_pan_restore.classList.add('mobile-images');                
            this.white_board.classList.add("mobile-images"); 
            this.advertise_good_sign.classList.add("mobile-images");            
        }        
    }
    
    set_language_styles () {          

        if (this.language === "heb") {
            this.white_board.classList.add("heb");
            this.bottom_bar.classList.add("heb");             
            this.btn_zoom_pan_restore.classList.add("heb");
            this.advertise_good_sign.classList.add("heb");           
        } else {
            this.white_board.classList.add("eng");
            this.bottom_bar.classList.add("eng");           
            this.btn_zoom_pan_restore.classList.add("eng");
            this.advertise_good_sign.classList.add("eng");            
        }
    }     

    prepare_head () {
        let head = document.querySelector("head");
        let old_viewport, title, base;
        
        base = head.querySelector("base");
        if (base !== null) {
            base.parentNode.removeChild(base);
        }

        old_viewport = head.querySelector("meta[name='viewport']");
        if (old_viewport !== null) {
            old_viewport.parentNode.removeChild(old_viewport);            
        }        

        title = head.querySelector("title");
        if (title !== null) {
            title.innerText = "good-sign";
        } else {
            title = document.createElement("title");
            title.innerText = "good-sign";
            head.appendChild(title);
        }

        var old_theme_color = head.querySelector("meta[name='theme-color']");
        if (old_theme_color !== null) {
            old_theme_color.parentNode.removeChild(old_theme_color);
        }

        var new_theme_color = document.createElement("meta");
        new_theme_color.setAttribute("name", "theme-color");
        new_theme_color.setAttribute("content", "#586AC3");
        head.appendChild(new_theme_color);
    }

    append_additional_markup_to_body() {
        let logo_image_url = this.get_url(this.ENV, `/public/images/${this.logo_image_name}`);
        let html = `<div id="overlay"><div id="modal-dialog-signature"><table class="tbl-modal"><tr><td colspan="2" class="first-row"> <span id="close-modal" class="close-modal"> <i class="far fa-times-circle fa-3x"></i> </span> <span class="title"></span></td></tr><tr><td colspan="2"><div class="sign-plate"> <canvas id="canvas-signature" class="canvas-signature"></canvas></div></td></tr><tr><td class="last-row-split clear"> <a href="javascript:void(0)" id="btn-clear-signature" class="btn-clear-signature"></a></td><td class="last-row-split"> <a href="javascript:void(0)" id="btn-insert-signature" class="btn-insert-signature"></a></td></tr></table></div><div id="modal-dialog-text-row"><table class="tbl-modal"><tr><td class="first-row"> <span id="close-modal" class="close-modal"> <i class="far fa-times-circle fa-3x"></i> </span><div class="middle-row"> <label class="lbl" for="text-field-row"></label><div class="txt-wrapper"> <input class="txt-input" id="text-field-row" type="text" autocomplete="off" maxlength="40" /><div class="reqiured-info"> <sup><i class="fas fa-asterisk"></i></sup> <span class="required-txt"></span></div></div></div></td></tr><tr><td class="last-row-wide"> <a href="javascript:void(0)" id="btn-ok-text-row" class="btn-ok-text"></a></td></tr></table></div><div id="modal-dialog-text-area"><table class="tbl-modal"><tr><td class="first-row"> <span id="close-modal" class="close-modal"> <i class="far fa-times-circle fa-3x"></i> </span></td></tr><tr><td class="second-row"> <label class="lbl" for="text-field-area"></label></td></tr><tr><td class="third-row"><div class="sign-plate"><textarea class="txt-area" id="text-field-area" type="text"></textarea><div class="sign-plate-bottom"><div class="chars-counter"> <span id="max-lbl"></span><span id="max-chars-allowed"></span><span id="chars-lbl"></span>(<span id="chars-left-lbl"></span><span id="chars-remaining"></span>)</div><div class="required-info"> <sup><i class="fas fa-asterisk"></i></sup> <span class="required-txt"></span></div></div></div></td></tr><tr><td class="last-row-wide"> <a href="javascript:void(0)" id="btn-ok-text-area" class="btn-ok-text"></a></td></tr></table></div><div id="modal-dialog-agreement"><div class="close-agreement"> <i class="far fa-times-circle fa-3x" aria-hidden="true"></i></div><div id="agreement-text-container"></div></div></div><div id="white-board" class="white-board"><div class="white-label-image" style="display: ${this.has_white_label_image ? 'block': 'none'}"> <img class="img ${this.logo_extra_class}" src="${logo_image_url}" /></div><div class="inner"><div class="title"></div><div class="sub-title"></div><p id="additional-legal-terms" class="additional-legal-terms"></p><div id="approve-terms-container" class="approve-terms-container" data-is-dirty="false"> <i class="far fa-square cbx-inner unchecked"></i> <i class="fas fa-check-square cbx-inner checked btn-approve" style="display: none"></i> <span id="txt-i-accept" class="btn-approve btn-approve"></span> <a id="btn-open-terms" href="javascript:void(0)"></a></div></div><div id="advertise-good-sign" class="advertise-good-sign"> <span class="advertise-prefix"></span> <a target="_blank" rel="noopener" class="link-to-good-sign" href="http://good-sign.co.il/">good-sign</a></div></div><div id="bottom-bar" class="bottom-bar"><div id="right-side" class="right-side"> <button id="back-to-doc" class="back-to-doc"> <i class="fas"></i> <span class="txt"></span> </button></div><div id="center-side" class="center-side"> <button id="btn-jump-down" class="btn-action"> <span class="fa-stack fa-1x"> <i class="fas fa-file fa-stack-2x"></i> <i class="fas fa-long-arrow-alt-down fa-stack-1x"></i> </span> <span class="txt"></span> </button> <button id="btn-jump-up" class="btn-action"> <span class="txt"></span> <span class="fa-stack fa-1x"> <i class="fas fa-file fa-stack-2x fa-flip-horizontal"></i> <i class="fas fa-long-arrow-alt-up fa-stack-1x"></i> </span> </button></div><div id="left-side" class="left-side"> <button id="btn-action" class="btn-action"> <span class="txt"></span> <i class="fas fa-2x "></i> </button> <i id="upload-to-server-spinner" class="fas fa-spinner fa-spin"></i> <span id="signatures-validator" class="signatures-validator"></span></div></div><div id="zoom-pan-restore" class="zoom-pan-restore"><div class="inner"></div></div>`;
        let frag = document.createRange().createContextualFragment(html);
        this.body.appendChild(frag);

        const paper_name = this.positioning_tool.paper_name;

        if(this.isMobile === false && paper_name !== "a4_landscape") {
            html = `<div class="white-label-image" style="display: ${this.has_white_label_image ? 'block': 'none'}">
                        <img class="img ${this.logo_extra_class}" src="${logo_image_url}"/>
                    </div>`;
            frag = document.createRange().createContextualFragment(html);
            this.body.appendChild(frag);
        }
    } 

    remove_paragraph_margin_bottom(){
        var p_ele = document.querySelector('body > p');
        if(Number(p_ele.style.marginBottom.replace('pt','')) > 0) {
            p_ele.style.marginBottom = 0;
        }
    } 
    
    get_hello_message () {
        let result = '';
        if(this.ENV === "pre-prod"){
            let h = new Date().getHours();
            if(h >= 0 && h < 12 ) {
                result = this.language === "heb" ? 'בוקר טוב' : 'Good Morning';
            } else if(h >= 12 && h < 18 ) {
                result = this.language === "heb" ? 'צהריים טובים' : 'Good Afternoon';
            } else if(h >= 18 && h < 23 ) {
                result = this.language === "heb" ? 'ערב טוב' : 'Good vening';
            }
            result = `${result} ${this.customer_fullname},`;
        } else {
            result = this.language === "heb" ? `שלום ${this.customer_fullname !== '' ? this.customer_fullname : ''  },` : `${this.customer_fullname !== '' ? this.customer_fullname + " " : ''  }Welcome Aboard!`;
        }
        return result;       
    }
    
    init_content_assets() {            

        this.stages = [{
            white_board: {//1
                visible: true,
                title: {
                    visible: true,
                    text: this.get_hello_message()
                },
                subtitle: {
                    visible: true,
                    text: this.language === "heb" ? `קיבלת מסמך לחתימה מאת <b>${this.business_name}</b>` : `<b>${this.business_name}</b> sent you a document that requires your signature`
                },
                checkbox: {
                    visible: true,
                    prefix_text: this.language === 'heb' ? "אני מאשר את" : "I accept the terms in the",
                    link_text: this.language === 'heb' ? "תנאי השימוש" : "agreement"
                }
            },
            bottom_bar: {//1
                visible: true,                    
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {
                    visible: true,
                    btn_action: {
                        text: this.language === "heb" ? "הבא" : "Next",
                        icon: this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right",
                        visible: true,
                        validator_text: this.language === "heb" ? "לחץ על  <b>הבא</b> כדי להתחיל" : "Click <b>Next</b> to begin"
                    }
                }
            }
        }, {
            white_board: {//2
                visible: false,
                title: {                    
                    visible: false,
                    text: ""                        
                },
                subtitle: {
                    visible: false,
                    text: ""
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//2
                visible: true,
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: this.isMobile,
                    up_text: this.language === "heb" ? 'הקודם' : 'Previous',
                    down_text: this.language === "heb" ? 'הבא' : 'Next'
                },
                left: {
                    visible: this.isMobile === false,
                    btn_action: {
                        text: this.isMobile === true ? this.language === "heb" ? "המשך" : "Continue" : this.language === "heb" ? " הבא" : "Next",
                        icon: this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right",
                        visible: true,
                        validator_text: this.language === "heb" ? "עבור על המסמך" : "Review the document"
                    }
                }
            }
        }, {
            white_board: {//3
                visible: true,
                title: {                    
                    visible: true,   
                    text: get_stage_3_title.call(this)
                },
                subtitle: {
                    visible: true,
                    text: get_stage_3_subtitle.call(this)
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//3
                visible: true,
                right: {
                    visible: true,
                    btn_back_to_doc: {
                        text: this.language === "heb" ? "חזרה למסמך" : "back to doc'",
                        icon: this.language === "heb" ? "fa-long-arrow-alt-right" : "fa-long-arrow-alt-left"
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {                        
                    visible: true,
                    btn_action: {
                        text: get_stage_3_btn_action_caption.call(this),
                        icon: this.business_self_sign === false ? "fa-paper-plane" : "fa-file-pdf",
                        visible: true,
                        validator_text: ""
                    }
                }
            }
        }, {
            white_board: {//4
                visible: true,
                title: {
                    visible: true,
                    text: this.language === "heb" ? `מעולה,` : `Great,`
                },
                subtitle: {
                    visible: true,
                    text: this.language === "heb" ? `<span class='title-lower'> עותק חתום נשלח בהצלחה אל <b>${this.business_name}</b> <i class='far fa-thumbs-up'></i></span>` : `<span class='title-lower'>A signed copy was succesfully sent to <b>${this.business_name}</b> <i class='far fa-thumbs-up'></i></span>`
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//4
                visible: true,
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {
                    visible: true,
                    btn_action: {
                        text: this.language === "heb" ? "הבא" : "Next",
                        icon: this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right",
                        visible: true,
                        validator_text: this.language === "heb" ? "לחץ על <b>הבא</b> להמשך..." : "Click <b>Next</b> to continue..."
                    }
                }
            }
        }, {
            white_board: {//5
                visible: true,
                title: {
                    visible: true,
                    text: get_stage_5_title.call(this)
                },
                subtitle: {
                    visible: true,
                    text: this.language === "heb" ? 
                        `${this.additional_message === '' ? '' : "<div class='additional-message'><i class='fas fa-info-circle'></i><p>" + this.additional_message + "</p></div>" }`                            
                        : 
                        `${this.additional_message === '' ? '' : "<div class='additional-message'><i class='fas fa-info-circle'></i><p>" + this.additional_message + "</p></div>" }`
                },
                checkbox: {
                    visible: false
                }                    
            },
            bottom_bar: {//5 bottom
                visible: true,
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {//5 bottom left
                    visible: true,
                    btn_action: {
                        text: this.language === "heb" ? "פתח מסמך" : "<span style='font-size: .85em'>View Document</span>",
                        icon: "fa-external-link-alt",
                        visible: true,
                        validator_text: this.language === 'heb' ? "לצפייה במסמך החתום לחץ <b>פתח מסמך</b>" : "Click here to review the document"
                    }
                }
            }
        }, 
        {
            dummy: true
        },
        {
            white_board: {//7 // disconnected stage
                visible: true,                    
                title: {
                    visible: true,
                    text: this.isMobile === true ? get_mobile_offline_message.call(this) : get_desktop_offline_message.call(this)
                },
                subtitle: {
                    visible: false,
                    text: ""
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//6
                visible: true,
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {
                    visible: false,
                    btn_action: {
                        text: "",
                        icon: "icon-envelope",
                        visible: false,
                        validator_text: ""
                    }
                }
            }
        }, {
            white_board: {//8 // business-self-sign Start
                visible: true,
                title: {
                    visible: true,
                    text: `<b class="gray">${this.business_name}</b>:<br>${this.language === "heb" ? "מסלול לחתימה עצמית" : "Self signature process"} <i class="fas fa-edit"></i>`
                },
                subtitle: {
                    visible: false,
                    text: ""
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//8
                visible: true,
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {
                    visible: true,
                    btn_action: {     
                        visible: true,                       
                        text: this.language === "heb" ? "הבא" : "Next",
                        icon: this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right",
                        validator_text: this.language === "heb" ? "לחץ על  <b>הבא</b> כדי להתחיל" : "Click <b>Next</b> to begin"
                    }
                }
            }
        }, {
            white_board: {//9 // business-self-sign end
                visible: true,
                title: {
                    visible: true,
                    text: this.language === "heb" ? "המסמך שחתמת עליו מוכן <i class='fas fa-check'></i>" : "She document you've signed on is ready <i class='far fa-smile'></i>" 
                },
                subtitle: {
                    visible: false,
                    text: ""
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//9
                visible: true,
                right: {
                    visible: true,
                    btn_back_to_doc: {
                        text: this.language === "heb" ? "חזרה למסמך" : "back to document",
                        icon: this.language === "heb" ? "fa-long-arrow-alt-right" : "fa-long-arrow-alt-left"
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {
                    visible: true,
                    btn_action: {     
                        visible: true,                       
                        text: this.language === "heb" ? "פתח מסמך" : "<span style='font-size: .85em'>View Document</span>",
                        icon: "fa-external-link-alt",
                        validator_text: ''
                    }
                }
            }
        }];

        function get_stage_5_title() {
            let result;
            if(this.business_self_sign === false) {
                result = this.language === "heb" ? 
                    "תודה רבה על שיתוף הפעולה, סיימנו <i class='fas fa-check'></i>" : 
                    "Thanks, we're done here <i class='fas fa-check'></i>";
            } else {
                result = this.language === "heb" ? 
                    "המסמך מוכן <i class='fas fa-check'></i>" : 
                    "The file is ready <i class='fas fa-check'></i>";
            }
            return result;
        }

        function get_stage_3_btn_action_caption() {
            let result;
            if(this.business_self_sign === false) {
                result = this.language === "heb" ? "שלח" : "Send";
            } else {
                result = this.language === "heb" ? "צור מסמך" : "Generate File";
            }
            return result;
        }

        function get_stage_3_subtitle() {
            let result;
            if(this.business_self_sign === false) {
                result = this.language === "heb" ? `<span class='title-lower'>כדי להחזיר קובץ חתום אל <b>${this.business_name}</b></span>` : `<span class='title-lower'>In order to return a signed document to <b>${this.business_name}</b></span>`
            } else {
                result = "";
            }
            return result;
        }

        function get_stage_3_title() {
            let result;
            if(this.business_self_sign === false) {
                result = this.language === "heb" ? `לחץ <b>שלח</b>,` : `Click <b>Send</b>,`;
            } else {
                result = this.language === "heb" ? `לחץ <b>צור מסמך</b>` : `Click <b>Generate File</b>`;
            }
            return result;
        }
        
        function get_mobile_offline_message() {
            var result = this.language === "heb" ? "צר לנו, מכשירך אינו מחובר לרשת" : "Sorry, your device isn't connected to a network";
                result += this.language === "heb" ? "<br><span class='title-lower gray'>נסה שנית כאשר תהיה מחובר </span>" : "<br><span class='title-lower gray'>Try again once you get reconnected</span>";
            return result;
        }

        function get_desktop_offline_message() {
            var result = this.language === "heb" ? "המחשב שלך אינו מחובר לרשת" : "Sorry, your computer is not connected to a network";
                result += this.language === "heb" ? "<br><span class='title-lower gray'> אנא בדוק את החיבור שלך לרשת</span>" : "<br><span class='title-lower gray'>Please check your computer's connection</span>";
            return result;
        }

        this.set_additional_texts();            
    }
    
    set_additional_texts () { 

        this.btn_zoom_pan_restore.querySelector(".inner").innerHTML =  this.language === 'heb' ?  
                        "<div class='txt'>חזרה</div><div class='txt'>לגודל</div><div class='txt'>התחלתי</span>" : 
                        "<div class='txt'>Back</div><div class='txt'>to default</div><div class='txt'>view</span>";
        this.advertise_good_sign.querySelector('.advertise-prefix').innerText = this.language === 'heb' ? "מופק באמצעות:" : "Powered by:";       
    }

    render_current_stage () {
            
        var current_stage = this.current_stage;
        var stages = this.stages;

        this.white_board.style.display = stages[current_stage - 1].white_board.visible === true ? "flex" : "none";
        this.white_board_title.style.display = stages[current_stage - 1].white_board.title.visible === true ? "block" : "none";
                
        this.white_board_title.innerHTML = stages[current_stage - 1].white_board.title.text;
        this.white_board_subtitle.style.display = stages[current_stage - 1].white_board.subtitle.visible === true ? "block" : "none";
        this.white_board_subtitle.innerHTML = stages[current_stage - 1].white_board.subtitle.text;

        if(stages[current_stage - 1].white_board.checkbox.visible === true) {
            this.approve_agreement_container.style.display = "flex";
            this.txt_i_accept.innerText = stages[current_stage-1].white_board.checkbox.prefix_text;
            this.button_open_terms.innerText = stages[current_stage-1].white_board.checkbox.link_text;
            
            if(this.has_additional_legal_terms) {
                this.additional_legal_terms.innerHTML = this.modal_dialog_agreement.get_additional_terms_html();
                this.additional_legal_terms.style.display = "block";                
            } else {
                this.additional_legal_terms.style.display = "none";                
            }
        } else {
            this.approve_agreement_container.style.display = "none";
            this.additional_legal_terms.style.display = "none";
        }
        
        this.bottom_bar.style.display = stages[current_stage - 1].bottom_bar.visible === true ? "block" : "none";//TODO
                    
        if(current_stage !== 5 ) {
            this.bottom_bar_left.style.display = stages[current_stage - 1].bottom_bar.left.visible === true ? "flex" : "none";
        } else {
            this.bottom_bar_left.style.display = "flex";
        }

        this.left_btn_action.style.display = stages[current_stage - 1].bottom_bar.left.btn_action.visible === true ? "flex" : "none";
        this.left_btn_action_txt.innerHTML = stages[current_stage - 1].bottom_bar.left.btn_action.text;
        
        this.left_btn_action.querySelector(".fas").classList.remove("fa-chevron-circle-left", "fa-chevron-circle-right", "fa-paper-plane", "fa-external-link-alt", "fa-file-pdf");
        this.left_btn_action.querySelector(".fas").classList.add(stages[current_stage - 1].bottom_bar.left.btn_action.icon);
                
        this.bottom_bar_center.style.display = stages[current_stage - 1].bottom_bar.center.visible === true ?  "flex" : "none";
        this.center_btn_up.querySelector('.txt').innerText = stages[current_stage - 1].bottom_bar.center.up_text;
        this.center_btn_down.querySelector('.txt').innerText = stages[current_stage - 1].bottom_bar.center.down_text;
        
        this.bottom_bar_right.style.display = stages[current_stage - 1].bottom_bar.right.visible === true ? 'block' : 'none';
        this.back_to_doc.querySelector(".txt").innerText = stages[current_stage - 1].bottom_bar.right.btn_back_to_doc.text;
        if(stages[current_stage - 1].bottom_bar.right.btn_back_to_doc.icon !== "" ) {
            this.back_to_doc.querySelector(".fas").classList.add(stages[current_stage - 1].bottom_bar.right.btn_back_to_doc.icon);              
        }
        
        this.signatures_validator.innerHTML = stages[current_stage - 1].bottom_bar.left.btn_action.validator_text;            
    }           
    
    increase_height_to_last_page() {
        var wrappers_coll = document.querySelectorAll('.image-wrapper');
        var bigger_height = this.positioning_tool.get_current_height() * 4/3 * 1.3;// * 4/3: convert from points to px, *1.3: increase height by a factor of 1.3
        wrappers_coll[wrappers_coll.length-1].style.height = `${bigger_height}px`;
        wrappers_coll[wrappers_coll.length-1].dataset.initialHeight = bigger_height;
    }
    
    handle_review_doc_click () {        
        this.handle_go_to_stage_two({review_doc: true});
    }    
    
    remove_style_shel_omer3 () {
        var styleEle = document.querySelector("#shel-omer2");
        if(styleEle !== null) {
            styleEle.parentNode.removeChild(styleEle);
        }  
    }
}

window.addEventListener("DOMContentLoaded", () => {
    var app = new App();
    if( app.IS_ONLINE === true) {
        app.initialize();
    }
});

module.exports = App;