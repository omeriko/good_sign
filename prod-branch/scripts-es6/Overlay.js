
class Overlay {
    constructor () {
        if(!Overlay.instance) {
            Overlay.instance = this;
            this._overlay = [];
        }
        return Overlay.instance;
    }
    
    show () {
        if(this._overlay.length === 0) {
            this.init_overlay();
        }
        this._overlay[0].style.display = "flex";
    }

    hide () {
        if(this._overlay.length === 0) {
            this.init_overlay();
        }
        this._overlay[0].style.display = "none";
    }

    init_overlay () {
        var overlay = document.querySelector("#overlay");
        this._overlay.push(overlay);
    } 
}

const instance = new Overlay();
Object.freeze(instance);

module.exports.instance = instance;