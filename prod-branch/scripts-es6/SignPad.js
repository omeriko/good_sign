import jQuery from "jquery";
class SignPad {
    constructor (canvasEle, lineWidth, lineColor) {        
        this.canvas = canvasEle;
        this.context = null;
        this.points = [];
        this.holdClick = false;    
        this.is_dirty = false;
        this.lineWidth = lineWidth;
        this.lineColor = lineColor;

        this.context = this.canvas.getContext('2d');
        this.context.lineJoin = this.context.lineCap = 'round';
        this.context.fillStyle = "#ffffff";
        

        jQuery(this.canvas).on('touchstart mousedown', e => {
            this.holdClick = true;
            var mousePosition = this.getMousePosition(this.canvas, e);                    
            this.points.push({x: mousePosition.x, y: mousePosition.y, break: false});
            return false;
        }).on('touchmove mousemove', e => {
            if (this.holdClick === true) {
                var mousePosition = this.getMousePosition(this.canvas, e);                    
                this.draw(this.context, mousePosition.x, mousePosition.y);
                
                if( this.is_dirty === false ){
                    this.is_dirty = true;                        
                }
            }
            return false;
        }).on('touchend mouseup', e => {
            e.preventDefault();
            this.holdClick = false;
            if(this.points.length > 0) {
                this.points[this.points.length - 1].break = true;
            }

            return false;
        });
    }

    getMousePosition (canvas, evt) {
        var result_left = 0, result_top = 0;
        var rect = canvas.getBoundingClientRect();
        var cursor = touch(evt);
        
        if(window.scrollX === 0) {
           result_left = cursor.x - rect.left;
        } else {
            result_left = (cursor.x - window.scrollX) - rect.left;
        }               
        
        if(window.scrollY === 0) {
           result_top = cursor.y - rect.top;
        } else {
            result_top = (cursor.y - window.scrollY) - rect.top;
        }  
        
        return {
            x: result_left, 
            y: result_top 
        };

        function touch(e) {
            var touch = null;
            if (e.type !== 'click' && e.type !== 'mousedown' && e.type !== 'mousemove') {
                touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            } else {
                touch = e;
            }

            return ({x: touch.pageX, y: touch.pageY});
        }
    }

    draw (ctx, x, y) {
        this.points.push({x: x, y: y, break: false});
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        
        var p1 = this.points[0];
        var p2 = this.points[1];

        ctx.beginPath();
        ctx.moveTo(p1.x, p1.y);  

        for (var i = 1; i < this.points.length; i++) {
            var midPoint = calculateMiddlePoint(p1, p2);
            if (p1.break) {
                ctx.moveTo(p2.x, p2.y); 
            } else {
                ctx.quadraticCurveTo(p1.x, p1.y, midPoint.x, midPoint.y);
            }
            p1 = this.points[i];
            p2 = this.points[i+1];
        }
        
        ctx.lineWidth = this.lineWidth;
        ctx.lineTo(p1.x, p1.y);
        ctx.strokeStyle = this.lineColor;
        ctx.stroke();

        function calculateMiddlePoint (pointStart, pointEnd) {
            return {
                x: pointStart.x + (pointEnd.x - pointStart.x) / 2 ,
                y: pointStart.y + (pointEnd.y - pointStart.y) / 2
            }
        }
    } 

    set_canvas_dimentions (width, height) {
        this.canvas.setAttribute("width",width);
        this.canvas.setAttribute("height",height);
        this.canvas.style.width = width + "px";
        this.canvas.style.height = height + "px";
        this.reset();
    }

    reset () {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.points.length = 0;
        this.context.fillStyle = "#ffffff";
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.is_dirty = false;
    }

    get_data_url () {
        return this.canvas.toDataURL('image/jpeg', 1.0);
    }   

    isDirty () {
        return this.is_dirty;
    }
}

module.exports = SignPad;
