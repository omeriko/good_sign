import GoodSignBase from './GoodSignBase';
import Logs from "./Logs";
import stringDirection from "string-direction";

class PayloadForServer extends GoodSignBase {
    constructor (parent) {
        super();
        this.parent = parent;
        this.pdf_document = null;
        this.paper_properties = this.parent.positioning_tool.get_paper_properties();
        this.logs = new Logs();
    }    

    fill_payload_for_generating_server_pdf (payload) {
        let pages_collection = document.querySelectorAll('.image-wrapper');
        
        pages_collection.forEach( page => {
            
            const main_img = page.querySelector(".data-image");
            let page_payload = { 
                main_image_src: main_img.src, 
                checkboxes: [], 
                signatures: [], 
                text_rows: [],
                text_areas: [],
                static_signatures: [],
                static_text_rows: [] 
            };

            this.collect_signatures_from_page(page, page_payload); 
            this.collect_text_rows_from_page(page, page_payload);             
            this.collect_text_areas_from_page(page, page_payload);
            this.collect_checkboxes_from_page(page, page_payload);             
            this.collect_static_signatures_from_page(page, page_payload);
            this.collect_static_text_row_from_page(page, page_payload);
            payload.pages.push(page_payload);                      
        });
        payload.logs = this.logs.collect_logs_data();       
    }

    collect_checkboxes_from_page(page, page_payload) {
        const checkboxes = page.querySelectorAll(".checkbox-placeholder"); 
        checkboxes.forEach( item => {
            const innerCheckbox = item.querySelector(".unchecked");
            const font_size = Number(innerCheckbox.style.fontSize.replace("px", ""));
            const top_pt = Number(item.style.top.replace("pt", "")).toFixed(1);
            const font_size_pt = Number((font_size * 3/4).toFixed(1));            
            const checkbox_right = Number(item.style.right.replace("pt", "")); 
            const left_pt = (this.paper_properties.PT_WIDTH - (checkbox_right + font_size_pt)).toFixed(1);
            
            page_payload.checkboxes.push({
                checked: item.dataset.isDirty,
                dt: item.dataset.dt,                                        
                left_pt: left_pt, 
                top_pt: top_pt, 
                font_size_pt: font_size_pt              
            });                             
        });
    }

    collect_static_text_row_from_page(page, page_payload) {
        const static_text_rows = page.querySelectorAll(".text-in-advance-placeholder");             
        static_text_rows.forEach(item => {
            const { width, height } = item.getBoundingClientRect();
            const width_pt = Number((width * 3/4).toFixed(1));
            const height_pt = height * 3/4;
            const top_pt = (Number(item.style.top.replace("pt", "")) + height_pt/2).toFixed(1);
            const right_pt = Number(item.style.right.replace("pt", "")); 
            const left_pt = (this.paper_properties.PT_WIDTH - (right_pt + width_pt)).toFixed(1);
            const text = item.innerText;
            const direction = stringDirection.getDirection(text);
            const font_size_pt = Math.round(Number(item.style.fontSize.replace("px", "")) * 3/4);

            page_payload.static_text_rows.push({
                direction: direction,
                text: text, 
                left_pt: left_pt, 
                top_pt: top_pt, 
                font_size_pt: font_size_pt
            });                                   
        });
    }

    collect_static_signatures_from_page(page, page_payload) {
        const static_signatures = page.querySelectorAll("[data-tx-name=in-advance-signature]");             
        static_signatures.forEach(item => {
            let { width, height } = item.getBoundingClientRect();
            const top_pt = Number(item.style.top.replace("pt", "")).toFixed(1);
            const width_pt = Number((width *3/4).toFixed(1));
            const height_pt = (height *3/4).toFixed(1);
            const sign_right = Number(item.style.right.replace("pt", "")); 
            const left_pt = (this.paper_properties.PT_WIDTH - (sign_right + width_pt)).toFixed(1);
            
            page_payload.static_signatures.push({
                src: item.src, 
                left_pt: left_pt, 
                top_pt: top_pt, 
                width_pt: width_pt, 
                height_pt: height_pt
            });              
        });  
    }

    collect_text_areas_from_page(page, page_payload) {
        const text_areas = page.querySelectorAll(".text-area-placeholder[data-is-dirty=true]");    
        text_areas.forEach(item => {
            const { width, height } = item.getBoundingClientRect();
            const width_pt = Number((width * 3/4).toFixed(1));
            const height_pt = height * 3/4;
            const top_pt = Number(item.style.top.replace("pt", ""));
            const right_pt = Number(item.style.right.replace("pt", "")); 
            const left_pt = Number((this.paper_properties.PT_WIDTH - (right_pt + width_pt)).toFixed(1));
            const text = item.querySelector(".inner").innerText;
            const direction = stringDirection.getDirection(text);
            const font_size_pt = Math.round(Number(item.style.fontSize.replace("px", "")) * 3/4);

            page_payload.text_areas.push({
                dt: item.dataset.dt,
                direction: direction,
                text: text, 
                container_width_pt: width_pt,                
                container_height_pt: height_pt,                
                container_left_pt: left_pt, 
                container_top_pt: top_pt.toFixed(1),
                text_left_pt: (direction === "rtl" || direction === "bidi") ? left_pt + width_pt - 6 : left_pt + 6,
                text_top_pt: (top_pt + 8).toFixed(1),//plus the padding
                text_width_pt: width_pt - 6,//minus the padding
                font_size_pt: font_size_pt
            });                                   
        });
    }

    collect_text_rows_from_page(page, page_payload) {
        const text_rows = page.querySelectorAll(".text-field-placeholder[data-is-dirty=true]");    
        text_rows.forEach(item => {
            const { width, height } = item.getBoundingClientRect();
            const width_pt = Number((width * 3/4).toFixed(1));
            const height_pt = height * 3/4;
            const top_pt = (Number(item.style.top.replace("pt", "")) + height_pt/2).toFixed(1);
            const right_pt = Number(item.style.right.replace("pt", "")); 
            const left_pt = (this.paper_properties.PT_WIDTH - (right_pt + width_pt)).toFixed(1);
            const text = item.querySelector(".inner").innerText;
            const direction = stringDirection.getDirection(text);
            const font_size_pt = Math.round(Number(item.style.fontSize.replace("px", "")) * 3/4);

            page_payload.text_rows.push({
                dt: item.dataset.dt,
                direction: direction,
                text: text, 
                font_weight: item.style.fontWeight,
                left_pt: left_pt, 
                top_pt: top_pt, 
                font_size_pt: font_size_pt
            });                                   
        });
    }

    collect_signatures_from_page(container, page_payload) {
        const signatures = container.querySelectorAll(".sign-here-placeholder"); 
        signatures.forEach(item => {
            let { width, height } = item.getBoundingClientRect();
            const top_pt = Number(item.style.top.replace("pt", "")).toFixed(1);
            const width_pt = Number((width * 3/4).toFixed(1));
            const height_pt = (height * 3/4).toFixed(1);
            const sign_right = Number(item.style.right.replace("pt", "")); 
            const left_pt = (this.paper_properties.PT_WIDTH - (sign_right + width_pt)).toFixed(1);
            
            page_payload.signatures.push({
                    dt: item.dataset.dt,
                    src: item.src, 
                    left_pt: left_pt, 
                    top_pt: top_pt, 
                    width_pt: width_pt, 
                    height_pt: height_pt,
                    opacity: item.classList.contains("sign-opaque") === false ? "ALPHA_NORMAL" : "ALPHA_OPAQUE"
                }
            );                             
        });
    }

    initialize_payload (journey_id, server_token, language, business_name, business_email_address, doc_name, customer_fullname, business_self_sign, paper_properties) {
       return { 
            journey_id: journey_id,
            token: server_token,
            user_language: language, 
            customer: { 
                name: business_name, 
                email: business_email_address, 
                original_file_name: doc_name 
            }, 
            enduser_fullname: customer_fullname,
            paper_size: paper_properties.name,
            pages: [],
            logs: null,
            self_sign: business_self_sign
        };
    }
}
module.exports = PayloadForServer;