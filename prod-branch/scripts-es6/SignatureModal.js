import SignPad from './SignPad';
import GoodSignBase from './GoodSignBase';
import Tooltips from "./Tooltips";

class SignatureModal extends GoodSignBase {
    constructor (parent_element, line_color, app) {
        super();        
        this.container = parent_element;        
        
        this.language = this.get_language();
        this.tooltip_util = Tooltips.instance;
        this.parent = app;
        this.sign_pad = new SignPad(parent_element.querySelector("#canvas-signature"), this.is_mobile() === false ? 9 : 14, line_color);
        this.current_placeholder = null;

        this.is_mobile() && this.container.classList.add("mobile-images");
        this.container.classList.add(this.language);

        var modal_title = this.container.querySelector(".first-row > .title");        
        modal_title.innerText = this.language === "heb" ? "צייר את חתימתך" : "Draw Your Signature";

        this.canvas_parent = this.container.querySelector(".sign-plate");
        this.btn_insert_signature = this.container.querySelector("#btn-insert-signature");
        this.btn_clear_signature = this.container.querySelector("#btn-clear-signature");
        this.btn_close_modal_signature = this.container.querySelector("#close-modal");
        
        this.btn_clear_signature.innerText = this.language === "heb" ? "נקה" : "Clear";
        this.btn_insert_signature.innerText = this.language === "heb" ? "אישור" : "OK";
        
        this.bind_events();
    }

    bind_events () {
        this.btn_close_modal_signature.addEventListener("click", () => {
            this.close();            
        }, false);

        this.btn_clear_signature.addEventListener("click", () => {
            this.sign_pad.reset();
        }, false); 

        this.btn_insert_signature.addEventListener("click", () => {

            if (this.sign_pad.isDirty() === true) {                    
                this.current_placeholder.src = this.sign_pad.get_data_url();
                this.current_placeholder.dataset.isDirty = "true";
                this.current_placeholder.dataset.dt = new Date().valueOf();                
               
                if(this.tooltip_util.has_tooltip(this.current_placeholder) === true) {
                    this.tooltip_util.remove_tooltip(this.current_placeholder);
                }                    
                this.tooltip_util.attach_thankyou_to_element(this.current_placeholder);
                this.is_mobile() && this.parent.render_bottom_bar();
            } 
            
            this.close();
        }, false);     
    }

    open (placeholder) {        
        let ratio = 0, 
        canvas_height = 0, 
        canvas_width = this.is_mobile() === true ? 750 : 600; 

        this.current_placeholder = placeholder;

        ratio = Number((this.current_placeholder.clientHeight/this.current_placeholder.clientWidth).toFixed(2));
        if(ratio < 0.5) {
            canvas_height = 0.5 * canvas_width;
        } else {
            canvas_height = this.current_placeholder.clientHeight * canvas_width / this.current_placeholder.clientWidth;
        }
        this.disable_body_scrolling();
        this.show_overlay();

        this.container.style.display = "block";
        this.sign_pad.set_canvas_dimentions(canvas_width, canvas_height);
        this.canvas_parent.style.width = this.canvas_parent.clientWidth;
        this.canvas_parent.style.height = this.canvas_parent.clientHeight;
    }

    close () {
        this.sign_pad.reset();
        this.enable_body_scrolling();
        this.hide_overlay();
        this.container.style.display = "none";
    }
   
    is_open () {
        return this.container.style.display === "block";
    }

    trigger_insert_signature () {
        this.btn_insert_signature.click();
    }
}

module.exports = SignatureModal;