import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';
import SignaturePositioningTool from './SignaturePositioningTool';
import Tooltips from './Tooltips';
import SignatureModal from './SignatureModal';
import TextRowModal from './TextRowModal';
import TextAreaModal from './TextAreaModal';
import ProgressBar from './ProgressBar';
import GoodSignBase from './GoodSignBase';
import AgreementModal from './AgreementModal';
import jQuery from "jquery";
import { detect } from 'detect-browser';

class App extends GoodSignBase {
    constructor() {   
        super();     
        this.current_stage = 0;        
        this.DEBUG_EMULATE_SENDING_PDF = false;
        this.DEBUG_JUMP_DIRECTLY_TO_STAGE = 1;        
        this.DEBUG_CLEAR_CONSOLE_ON_START= false;
        this.SCREEN_SHOT_IMAGE_FORMAT = "image/jpeg";
        this.SCREEN_SHOT_IMAGE_QUALITY = 0.92;
        this.SCREENSHOT_SCALE = 3;
        this.TEXT_AREA_PLACEHOLDER_STYLES = { fontSize: this.is_mobile()===false ? 16 : 16, lineHeight: this.is_mobile()===false ? 24: 24 };
        this.LINE_COLOR = "#001eaa";          
        this.ENV = "prod";
        this.stages = [];
        
        this.modal_dialog_signature = null;
        this.modal_dialog_text_row = null;
        this.modal_dialog_text_area = null;
        this.modal_dialog_agreement = null;
              
        this.back_to_doc = null;
        this.white_board = null;
        this.white_board_title = null;
        this.white_board_subtitle = null;
        this.bottom_bar = null;
        this.bottom_bar_left = null;
        this.bottom_bar_center = null;
        this.bottom_bar_right = null;
        this.button_open_terms = null;
        this.center_btn_up = null;
        this.center_btn_down = null;
        this.upload_to_server_spinner = null;
        this.left_btn_action = null;
        this.left_btn_action_txt = null; 
        this.selected_text_row = null; 
        this.signatures_validator = null;
        this.approve_agreement_checkbox = null;
        this.body = document.querySelector("body");        

        this.pdf_document = null;
        
        this.business_name = '';
        this.business_email_address = '';
        this.customer_fullname = '';
        this.additional_message = '';
        this.business_self_sign = '';
        this.server_token = ''; 
        this.journey_id='';
        
        this.doc_name = '';          
        this.EMAIL_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        this.URL_PRODUCTION ="https://good-sign.net/upload_pdf";
        this.URL_PRE_PRODUCTION = "http://goodsignstaging-env.eba-mfma2id2.eu-central-1.elasticbeanstalk.com/upload_pdf";            
        this.URL_DEV = "http://192.168.1.13:8000/upload_pdf"; 
        this.btn_zoom_pan_restore = null;
        this.advertise_good_sign = null;
        this.approve_agreement = null;
        this.container_p = {};
        this.data_images = null;
        this.mobile_scenario_all_placeholders = null;
        this.current_placeholder_pointer = 0; 
        this.IS_ONLINE = false; 
        this.positioning_tool = new SignaturePositioningTool();
        this.language = this.get_language();
        this.set_data_from_hidden_fields();
        this.DID_PINCH_ZOOM_PAN = false;
        
        this.tooltip_util = Tooltips.instance;        
        this.override_debug_behavior();
        this.prepare_head();
        this.append_additional_markup_to_body();
        this.remove_paragraph_margin_bottom();
        
        this.data_images = this.set_data_images_class_names_and_sizes();
        
        this.wrap_data_images_with_containers();
        this.set_containers_of_data_image_sizes();        
        
        this.set_class_members();       
        
        this.set_language_styles();
        this.init_content_assets();
        
        if (navigator.onLine === true) {            
            this.IS_ONLINE = true;
        } else {
            this.IS_ONLINE = false;
            this.handle_go_to_stage_seven(); 
            return;          
        }

        this.position_all_placeholders();
        this.increase_height_to_last_page();
        this.is_mobile() === true && this.init_pinched_elements_data_structure();
        this.bind_events.call(this);
    }

    initialize () {

        switch(this.DEBUG_JUMP_DIRECTLY_TO_STAGE) {
            case 1: 
                this.handle_go_to_stage_one(); 
                break;
            case 2: 
                this.left_btn_action.classList.remove("partial-disabled");
                this.handle_go_to_stage_two({review_doc: false}); 
                break;
            case 2.5: 
                this.handle_stage_generate_pdf(); 
                break;
            case 3: 
                this.show_overlay();
                this.disable_body_scrolling();
                this.handle_go_to_stage_three(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;
            case 4: 
                this.handle_go_to_stage_four(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;    
            case 5: 
                this.handle_go_to_stage_five(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;            
            case 6: //not applicable for direct jump                    
                break;
            case 7: //show disconnected message
                this.show_overlay();
                this.handle_go_to_stage_seven(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;
            case 8: 
                this.handle_go_to_stage_eight(); 
                this.left_btn_action.classList.remove("partial-disabled");
                break;
            case 9: 
                this.left_btn_action.classList.remove("partial-disabled");
                this.show_overlay();
                this.handle_go_to_stage_nine(); 
                break;
            default: 
                this.handle_go_to_stage_one();
        }
        this.remove_style_shel_omer3();
        this.DEBUG_CLEAR_CONSOLE_ON_START && setTimeout( () => console.clear(), 1000);                                
    }

    handle_go_to_stage_one () {
        this.disable_body_scrolling();
        if(this.business_self_sign === false) {
            this.left_btn_action.classList.add("partial-disabled");
            this.current_stage = 1;            
            this.render_current_stage(); 
            this.remove_style_shel_omer3();
        } else if (this.business_self_sign === true) {
            this.handle_go_to_stage_eight();
            this.remove_style_shel_omer3();
        }
    }

    handle_go_to_stage_two(options) {
        this.modal_dialog_agreement.close();
              
        window.scroll({ top: 0, left: 0, behavior: 'auto' });          
        this.enable_body_scrolling();            
        this.current_stage = 2;
        this.render_current_stage();            
        
        if(this.is_mobile() === true) { 
           this.render_bottom_bar();
        } else {           
            if(options.review_doc === false){
                this.handle_stage_two()
            }
        }          
    }

    handle_stage_two() {

        var i, top, ele, count_required_and_dirty = 0, 
        sorted_reuired_placeholders = get_sorted_reuired_placeholders();//returns only required textual fields, required text areas and signatures
             
        for (i = 0; i < sorted_reuired_placeholders.length; i++) {
            ele = sorted_reuired_placeholders[i];
            if (ele.dataset.isDirty === "false") {                   
                top = jQuery(ele).offset().top;// ..ele.offsetTop;
                window.scroll({ top: top - 400, left: 0, behavior: 'smooth' });
                break;
            } else {
                count_required_and_dirty = count_required_and_dirty + 1;
            }
        }
            
        if (count_required_and_dirty === sorted_reuired_placeholders.length) {            
            this.handle_stage_generate_pdf();
        }
        

        function get_sorted_reuired_placeholders() {
            var node_list = document.querySelectorAll(".sign-here-placeholder, .textual[data-is-required='true']");
            var arr = Array.from(node_list); 
            arr.sort(sort_by_top_position);               

            return arr;

            function sort_by_top_position (a, b) {
                var a_top =  Number(a.dataset.globalTopForSorting);
                var b_top =  Number(b.dataset.globalTopForSorting);
                return a_top - b_top;                    
            }               
        }    
    }
    
    prepare_doc_to_pdf_generation() {
        this.is_mobile() === true && this.restore_container_to_initial_scale();
        this.restore_height_to_last_page();
        this.append_logs();
        this.hide_all_empty_text_fields();
        this.hide_border_from_dirty_text_fields();
        this.disable_body_scrolling();
        remove_all_tooltips.call(this);
        
        window.scroll({ top: 0, left: 0, behavior: 'auto' });
        
        function remove_all_tooltips(){
            var node_list = document.querySelectorAll(".sign-here-placeholder, .textual");
            node_list.forEach( item => {
                if(this.tooltip_util.has_tooltip(item) === true) {
                    this.tooltip_util.remove_tooltip(item);
                }
            });
        }
    } 

    async handle_stage_generate_pdf() {
        var i, progress_value = 0, progress_increment = 0, section_collection = null;            
                
        this.darker_overlay();
        this.show_overlay();
        this.progress_bar.show();        
        this.prepare_doc_to_pdf_generation();
       
        section_collection = document.querySelectorAll('.image-wrapper');                      
        
        progress_increment = Math.round(100 / section_collection.length);
        
        this.progress_bar.set_value(progress_value);        

        for (i = 0; i < section_collection.length; i++) {              
            await this.save_html_element_to_pdf(section_collection[i], i);
            this.progress_bar.set_value(progress_value + progress_increment);
        }

        this.progress_bar.set_value(100);        
                   
        setTimeout(() => {
            this.lighter_overlay();
            if(this.business_self_sign === false) {
                this.handle_go_to_stage_three();
            } else if (this.business_self_sign === true) {
                this.handle_go_to_stage_nine();
            }               
        },500);
    }

    async save_html_element_to_pdf(element, i, scale) {
        var canvas = null, imgData = null, 
        scale = calculate_scale.call(this, element),        
        paper_properties = this.positioning_tool.get_paper_properties(),
        options = { orientation: paper_properties.orientation, unit: 'cm', format: paper_properties.name, compress: true }; 
                
        canvas = await html2canvas(element, { scale: scale });            
        imgData = canvas.toDataURL(this.SCREEN_SHOT_IMAGE_FORMAT, this.SCREEN_SHOT_IMAGE_QUALITY); 

        if (i === 0) {                
            this.pdf_document = new jsPDF(options);
        } else {
            this.pdf_document.addPage(paper_properties.name , paper_properties.orientation);
        }
        this.pdf_document.addImage(imgData, 'JPEG', 0, 0, paper_properties.CM_WIDTH, paper_properties.CM_HEIGHT, '', 'FAST', 0);           

        function calculate_scale(element) {
           let result, count_signatures = count_signatures_in_element(element);
            
           if (this.is_mobile() === true) {//On mobile
                if(count_signatures > 0) { //if there are few signatures scale=2
                   result = this.SCREENSHOT_SCALE - 1;
                } else {
                    result = 1;//if there are no signatures scale=1
                }
            } else { //On desktop 
                result = this.SCREENSHOT_SCALE + 1; //FOR EACH PAGE, scale=4 
            }
            //maximum ios safari scale is 4. above that we get the "Canvas area exceeds the maximum limit" error
            console.log(`current scale: ${result}`);
            return result;

            function count_signatures_in_element(ele){
                return ele.querySelectorAll(".sign-here-placeholder").length;
            }
        }
    }

    append_logs() {
        var i, pivot_ele = null, pivot_date = null,log_row_p = null, pivot_txt = null,
            all_dirty_placeholders = get_dirty_placeholders_sorted_vertically(),
            new_logs_section = null;

        var pre_existing_log = document.querySelector(".appended-log");
        if (pre_existing_log !== null) {
            pre_existing_log.parentNode.removeChild(pre_existing_log);
        }

        if (all_dirty_placeholders.length > 0) {
            
            new_logs_section = append_new_logs_container.call(this);                            

            for (i = 0; i < all_dirty_placeholders.length; i++) {
                
                pivot_ele = all_dirty_placeholders[i];
                pivot_date = new Date(Number(pivot_ele.dataset.dt));
                log_row_p = document.createElement("p");
                log_row_p.classList.add("log-row");
                if(pivot_ele.classList.contains("sign-here-placeholder") === true ) {
                    pivot_txt = document.createTextNode(`(${ Number(i + 1).toString()} חתימה הוזנה בתאריך ${pivot_date.toLocaleDateString("he-IL")}  ושעה  ${pivot_date.toLocaleTimeString("he-IL")}`);
                } else if(pivot_ele.classList.contains("text-field-placeholder") === true) {
                    pivot_txt = document.createTextNode(`(${ Number(i + 1).toString()} שורת טקסט הוזנה בתאריך ${pivot_date.toLocaleDateString("he-IL")}  ושעה  ${pivot_date.toLocaleTimeString("he-IL")}`);
                } else if (pivot_ele.classList.contains("text-area-placeholder") === true) {
                    pivot_txt = document.createTextNode(`(${ Number(i + 1).toString()} פיסקת טקסט הוזנה בתאריך ${pivot_date.toLocaleDateString("he-IL")}  ושעה  ${pivot_date.toLocaleTimeString("he-IL")}`);
                }
                log_row_p.appendChild(pivot_txt);
                new_logs_section.appendChild(log_row_p);
            }
        }

        function append_new_logs_container() {
            var section_collection = document.querySelectorAll('.image-wrapper');
            var parent_node = section_collection[0].parentNode;
            var new_logs_section = document.createElement('div');
            new_logs_section.classList.add('image-wrapper');
            this.is_mobile() ===true && new_logs_section.classList.add('mobile-images');
           
            new_logs_section.setAttribute('id', `image-wrapper${section_collection.length + 1}`);
            new_logs_section.style.width = `${this.positioning_tool.get_current_width() * 4/3}px`;
            new_logs_section.style.height = `${this.positioning_tool.get_current_height() * 4/3}px`;
            new_logs_section.style.boxShadow = "none";
            new_logs_section.classList.add("appended-log");                
            parent_node.appendChild(new_logs_section);
            return new_logs_section;
        }

        function get_dirty_placeholders_sorted_vertically() {
            var node_list = document.querySelectorAll(".text-field-placeholder[data-is-dirty='true'], .text-area-placeholder[data-is-dirty='true'], .sign-here-placeholder");
            var arr = Array.from(node_list); 
            arr.sort(sort_by_top_position);               

            return arr;

            function sort_by_top_position (a, b) {
                var a_top =  Number(a.dataset.globalTopForSorting);
                var b_top =  Number(b.dataset.globalTopForSorting);
                return a_top - b_top;                    
            }                
        }
    }

    handle_go_to_stage_three() {
        this.hide_overlay();
        this.progress_bar.hide();
        this.bottom_bar.style.display = "block";

        this.current_stage = 3;
        this.render_current_stage();
        this.signatures_validator.style.display = "none";
    }

    handle_go_to_stage_four () {            
        this.signatures_validator.style.display = "block";
        this.current_stage = 4;            
        this.render_current_stage();
        this.left_btn_action.classList.remove("fully-disabled");            
    }

    handle_server_error() {
        this.upload_to_server_spinner.style.display = "none";
        this.left_btn_action.style.display = "none";
        this.white_board_title.innerHTML = this.language === "heb" ? "לצערנו אירעה תקלה במערכת <i class='icon-warning-sign'></i>" : "Oooops, we have a system error <i class='icon-warning-sign'></i>";
        this.white_board_subtitle.innerHTML = this.language === "heb" ? "<span class='gray'>נסה לחתום שוב על המסמך מאוחר יותר</span>" : "<span class='gray'>Please try to sign the document later</span>";
        this.signatures_validator.innerHTML = this.language === "heb" ? "נסה לחתום שוב על המסמך מאוחר יותר" : "Please try to sign the document later";
    }   

    handle_go_to_stage_five() {
        this.disable_body_scrolling();
        this.left_btn_action.classList.remove("none");
        this.advertise_good_sign.style.display = "block";            
        this.current_stage = 5;
        this.render_current_stage(); 
    }       

    handle_go_to_stage_six() {
        window.open(this.pdf_document.output('bloburl', { filename: "fileName" }), '_blank');
    } 

    handle_go_to_stage_eight(){
        this.current_stage = 8;            
        this.render_current_stage();  
    }

    jump_to_placeholder(direction) {
        var top = 0, ele = this.mobile_scenario_all_placeholders[this.current_placeholder_pointer];

        do { 
            if(this.tooltip_util.has_tooltip(ele) === true) {
                this.tooltip_util.remove_tooltip(ele);
            }

            modify_next_element_pointer.call(this, direction);                
            ele = this.mobile_scenario_all_placeholders[this.current_placeholder_pointer];
        } while(ele.dataset.isDirty === 'true');

       
        top = jQuery(ele).offset().top; //ele.getBoundingClientRect().top;
        window.scroll({ top: top - 300, left: 0, behavior: 'smooth' });
        this.tooltip_util.attach_tooltip_to_element(ele);

        function modify_next_element_pointer(direction) {
            
            switch(direction) {
                case 'up': 

                    if(this.current_placeholder_pointer > 0 ) {
                        this.current_placeholder_pointer -= 1;                                                
                    } else if(this.current_placeholder_pointer === 0 ) {
                        this.current_placeholder_pointer = this.mobile_scenario_all_placeholders.length-1;                                                
                    }
                    
                    break;
                case 'down': 
                    if(this.current_placeholder_pointer < this.mobile_scenario_all_placeholders.length) {
                        this.current_placeholder_pointer += 1;
                        this.current_placeholder_pointer = this.current_placeholder_pointer % this.mobile_scenario_all_placeholders.length;
                    } 
                    break;
                default:
            }
        }
    } 
    
    bind_pinch_and_pan () {  
            
        var mc = new Hammer.Manager(this.container_p.ref);            
        var pinch = new Hammer.Pinch();
        var pan = new Hammer.Pan({ direction: Hammer.DIRECTION_ALL });
        
        pinch.recognizeWith(pan);            
        mc.add([pinch, pan]);            
        mc.add( new Hammer.Tap({ event: 'doubletap', taps: 2, interval: 500, posThreshold: 100 }) );
        
        mc.on("pinchstart panstart", () => {
            this.container_p.children.forEach( item => {
                item.children.sign_placeholders.forEach( _item => {
                    this.tooltip_util.has_tooltip(_item.ref) == true &&  this.tooltip_util.hide_tooltip(_item.ref);               
                });

                item.children.text_field_placeholders.forEach( _item => {
                    this.tooltip_util.has_tooltip(_item.ref) == true &&  this.tooltip_util.hide_tooltip(_item.ref);               
                });

                item.children.text_area_placeholders.forEach( _item => {
                    this.tooltip_util.has_tooltip(_item.ref) == true &&  this.tooltip_util.hide_tooltip(_item.ref);               
                });
            });
            this.DID_PINCH_ZOOM_PAN = true;
        }).on("pinchmove panmove", ev => {                
            this.container_p.ref.style.left = `${this.container_p.current_left + ev.deltaX}px`;                
            this.container_p.ref.style.top =  `${this.container_p.current_top + ev.deltaY}px`;             

            if(this.container_p.current_width * ev.scale >= this.container_p.initial_width) {
                //set new width and height to p
                this.container_p.ref.style.width = (this.container_p.current_width * ev.scale) + "px";
                this.container_p.ref.style.height = (this.container_p.current_height * ev.scale) + "px";
                
                //calculate new width and height for all children: images_wrapper 
                this.container_p.children.forEach( item => {                        
                    item.ref.style.width = `${item.current_width * ev.scale}px`;
                    item.ref.style.height = `${item.current_height * ev.scale}px`;
                    
                    //calculate new width and height for data_image child 
                    item.children.data_image.ref.style.width = `${item.children.data_image.current_width * ev.scale}px`;
                    item.children.data_image.ref.style.height = `${item.children.data_image.current_height * ev.scale}px`;

                    //calculate new width and height for signature children 
                    item.children.sign_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;                            
                    });

                    //calculate new sizes for text_field children 
                    item.children.text_field_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                        item.ref.style.fontSize = `${item.current_height * 0.5 * ev.scale}px`;                           
                        item.ref.style.lineHeight = `${(item.current_height * ev.scale) -(4*ev.scale)}px`;
                    });

                     //calculate new sizes for text_area children 
                     item.children.text_area_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                        item.ref.style.fontSize = `${item.current_font_size * ev.scale}px`;                           
                        item.ref.style.lineHeight = `${item.current_line_height * ev.scale}px`;
                    });

                    //calculate new sizes for in advance labels children 
                    item.children.in_advance_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                        item.ref.style.fontSize = `${item.current_height * 0.5 * ev.scale}px`;                           
                        item.ref.style.lineHeight = `${item.current_line_height * ev.scale}px`;
                    });
                     //calculate new sizes for in advance signatures children
                    item.children.in_advance_signature_placeholders.forEach( item => {
                        item.ref.style.width  = `${item.current_width * ev.scale}px`;
                        item.ref.style.height = `${item.current_height * ev.scale}px`;
                        item.ref.style.top    = `${item.current_top * ev.scale}pt`;
                        item.ref.style.right  = `${item.current_right * ev.scale}pt`;
                    });
                    
                });
            }
        })
        .on("pinchend panend", ev => {
            //console.log(`pinchend panend:dir: ${ev.direction}, scale ${ev.scale}, dX: ${ev.deltaX}, ele: ${ev.target.className} `);
            
            this.container_p.current_width = Number(this.container_p.ref.style.width.replace('px', ''));
            this.container_p.current_height = Number(this.container_p.ref.style.height.replace('px', ''));
            this.container_p.current_left = Number(this.container_p.ref.style.left.replace('px',''));
            this.container_p.current_top = Number(this.container_p.ref.style.top.replace('px',''));
                            
            //calculate and update current_width for all children: images_wrapper 
            this.container_p.children.forEach( item => {
                item.current_width =  Number(item.ref.style.width.replace('px',''));
                item.current_height = Number(item.ref.style.height.replace('px',''));

                item.children.data_image.current_width =  Number(item.children.data_image.ref.style.width.replace('px',''));
                item.children.data_image.current_height = Number(item.children.data_image.ref.style.height.replace('px',''));

                //calculate new width and height for signature children 
                item.children.sign_placeholders.forEach( item => {
                    item.current_width  =  item.ref.offsetWidth;
                    item.current_height =  item.ref.offsetHeight;
                    item.current_top    =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right  =  Number(item.ref.style.right.replace('pt',''));                        
                });

                //calculate new sizes for text_field children 
                item.children.text_field_placeholders.forEach( item => {
                    item.current_width       =  item.ref.offsetWidth;
                    item.current_height      =  item.ref.offsetHeight;
                    item.current_top         =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right       =  Number(item.ref.style.right.replace('pt',''));
                    item.current_font_siz    =  Number(item.ref.style.fontSize.replace('px',''));
                    item.current_line_height =  Number(item.ref.style.lineHeight.replace('px',''));                        
                });

                //calculate new sizes for text_area children 
                item.children.text_area_placeholders.forEach( item => {
                    item.current_width    =  item.ref.offsetWidth;
                    item.current_height   =  item.ref.offsetHeight;
                    item.current_top      =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right    =  Number(item.ref.style.right.replace('pt',''));
                    item.current_font_size = Number(item.ref.style.fontSize.replace('px',''));
                    item.current_line_height = Number(item.ref.style.lineHeight.replace('px',''));
                });

                //calculate new sizes for in_advance children 
                item.children.in_advance_placeholders.forEach( item => {
                    item.current_width    =  item.ref.offsetWidth;
                    item.current_height   =  item.ref.offsetHeight;
                    item.current_top      =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right    =  Number(item.ref.style.right.replace('pt',''));
                    item.current_font_size = Number(item.ref.style.fontSize.replace('px',''));
                    item.current_line_height = Number(item.ref.style.lineHeight.replace('px',''));
                });

                item.children.in_advance_signature_placeholders.forEach( item =>{
                    item.current_width  =  item.ref.offsetWidth;
                    item.current_height =  item.ref.offsetHeight;
                    item.current_top    =  Number(item.ref.style.top.replace('pt',''));
                    item.current_right  =  Number(item.ref.style.right.replace('pt',''));
                });
            }); 
            jQuery(this.btn_zoom_pan_restore).show(500);
        })
        .on("doubletap", () => {
           this.restore_container_to_initial_scale();
        });
    }

    restore_container_to_initial_scale () {

        jQuery(this.btn_zoom_pan_restore).fadeOut(200);
        //return the size of it to its initial size            
        this.container_p.current_width    =  this.container_p.initial_width;
        this.container_p.current_height   =  this.container_p.initial_height;
        this.container_p.ref.style.width  = `${this.container_p.current_width}px`;
        this.container_p.ref.style.height = `${this.container_p.current_height}px`;
        this.container_p.current_left     =  this.container_p.initial_left;    
        this.container_p.ref.style.left   = `${this.container_p.current_left}px`;
        this.container_p.current_top      =  this.container_p.initial_top;   
        this.container_p.ref.style.top    = `${this.container_p.current_top}px`;          
        
        this.container_p.children.forEach( item => {
            item.current_width = item.initial_width;
            item.current_height = item.initial_height;
            item.ref.style.width  = `${item.current_width}px`;
            item.ref.style.height = `${item.current_height}px`;
            
            item.children.data_image.current_width = item.children.data_image.initial_width;
            item.children.data_image.current_height = item.children.data_image.initial_height;
            item.children.data_image.ref.style.width = `${item.children.data_image.current_width}px`;
            item.children.data_image.ref.style.height = `${item.children.data_image.current_height}px`;

            item.children.sign_placeholders.forEach( item => {
                item.current_width  = item.initial_width; 
                item.current_height = item.initial_height; 
                item.current_top    = item.initial_top; 
                item.current_right  = item.initial_right;
                
                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
            });

            //calculate new sizes for text_field children 
            item.children.text_field_placeholders.forEach( item => {
                item.current_width    = item.initial_width; 
                item.current_height   = item.initial_height; 
                item.current_top      = item.initial_top; 
                item.current_right    = item.initial_right;
                item.current_font_size = item.initial_font_size;
                item.current_line_height = item.initial_line_height;

                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
                item.ref.style.fontSize    = `${item.current_font_size}px`; 
                item.ref.style.lineHeight  = `${item.current_line_height}px`;
            });

            //calculate new sizes for text_field children 
            item.children.text_area_placeholders.forEach( item => {
                item.current_width    = item.initial_width; 
                item.current_height   = item.initial_height; 
                item.current_top      = item.initial_top; 
                item.current_right    = item.initial_right;
                item.current_font_size = item.initial_font_size;
                item.current_line_height = item.initial_line_height;

                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
                item.ref.style.fontSize    = `${item.current_font_size}px`; 
                item.ref.style.lineHeight  = `${item.current_line_height}px`;
            });

            //calculate new sizes for in advance labels children 
            item.children.in_advance_placeholders.forEach( item => {
                item.current_width    = item.initial_width; 
                item.current_height   = item.initial_height; 
                item.current_top      = item.initial_top; 
                item.current_right    = item.initial_right;
                item.current_font_size = item.initial_font_size;
                item.current_line_height = item.initial_line_height;

                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
                item.ref.style.fontSize    = `${item.current_font_size}px`; 
                item.ref.style.lineHeight  = `${item.current_line_height}px`;
            });
            //calculate new sizes for in advance signatures children 
            item.children.in_advance_signature_placeholders.forEach( item => {
                item.current_width  = item.initial_width; 
                item.current_height = item.initial_height; 
                item.current_top    = item.initial_top; 
                item.current_right  = item.initial_right;
                
                item.ref.style.width  = `${item.current_width}px`; 
                item.ref.style.height = `${item.current_height}px`; 
                item.ref.style.top    = `${item.current_top}pt`; 
                item.ref.style.right  = `${item.current_right}pt`;
            });
        });
    }
    
    handle_action_click() {
        switch (this.current_stage) {
            case 1:                
                if (this.left_btn_action.classList.contains("partial-disabled") === false) {
                    this.handle_go_to_stage_two({review_doc: false});
                }
                break;
            case 2:
                if(this.is_mobile() === true){
                    this.handle_stage_generate_pdf();
                } else {
                    this.handle_stage_two();
                }
                break;
            case 3:      
                
                if(this.DEBUG_EMULATE_SENDING_PDF === false ) {
                    this.left_btn_action.classList.add("fully-disabled");
                    this.back_to_doc.classList.add("none");
                    this.upload_to_server_spinner.style.display = "block";
                    
                    this.send_pdf_to_server(this.business_email_address, "to_business")
                            .done((data, textStatus, jqXHR) => {
                                if(jqXHR.status === 200 && data.success === true) {
                                    this.upload_to_server_spinner.style.display = "none";
                                    this.handle_go_to_stage_four(); 
                                }                                
                            }).fail( () => {                               
                                this.handle_server_error();
                            });                                   
                } else {
                    this.left_btn_action.classList.add("fully-disabled");
                    this.back_to_doc.classList.add("none");
                    this.upload_to_server_spinner.style.display = "block";
                    
                    setTimeout(() => {
                        this.upload_to_server_spinner.style.display = "none";
                        this.handle_go_to_stage_four();  
                    },2000);                        
                }

                break;
            case 4:                                 
                this.handle_go_to_stage_five();            
                break;
            case 5:
                this.handle_go_to_stage_six();                
                break;            
            case 8:
                this.handle_go_to_stage_two({review_doc: false});
                break; 
            case 9:
                window.open(this.pdf_document.output('bloburl', { filename: "fileName" }), '_blank');
                break; 
            default: break;
        }

        return false;
    } 

    handle_insert_signature_complete(is_dirty, placeholder) {
        if(is_dirty === true) {
            if(this.tooltip_util.has_tooltip(placeholder) === true) {
                this.tooltip_util.remove_tooltip(placeholder);
            }                    
            this.tooltip_util.attach_thankyou_to_element(placeholder);  
        } 
        this.is_mobile() && this.render_bottom_bar();                   
    }

    handle_insert_textrow_complete (txt, placeholder) {
        let node_list = null;

        if(txt !== "") {
            if(this.tooltip_util.has_tooltip(placeholder) === true) {
                this.tooltip_util.remove_tooltip(placeholder);
            }
            
            this.tooltip_util.attach_thankyou_to_element(placeholder);
            
            node_list = document.querySelectorAll(`[data-label-helper='${placeholder.dataset.labelHelper}']`);
            for(var i = 0; i < node_list.length; i++) {
                
                if(node_list[i] !== placeholder) {
                    this.modal_dialog_text_row.handle_dirty_text_insertion(
                        node_list[i].querySelector(".inner"), 
                        txt
                    );
                }
            }       
        }

        this.is_mobile() && this.render_bottom_bar();        
    }

    handle_insert_textarea_complete (is_dirty, placeholder) {
        if(is_dirty === true) {
            if(this.tooltip_util.has_tooltip(placeholder.parentNode) === true) {
                this.tooltip_util.remove_tooltip(placeholder.parentNode); 
            }
            this.tooltip_util.attach_thankyou_to_element(placeholder.parentNode);
        } 

        this.is_mobile() && this.render_bottom_bar();
    } 

    bind_events() {

        this.button_open_terms.addEventListener('click', function() {            
            this.modal_dialog_agreement.open();
        }.bind(this), false);
        
        this.approve_agreement_checkbox.addEventListener('change', (e) => {
            if(e.target.checked) {               
               this.left_btn_action.classList.remove("partial-disabled");
            } else {
                this.left_btn_action.classList.add("partial-disabled");
            }
        });  

        if(this.is_mobile() === true) {
            this.center_btn_up.addEventListener('click', function() {
                this.jump_to_placeholder('up');
                
            }.bind(this), false);

            this.center_btn_down.addEventListener('click', () => {
                this.jump_to_placeholder('down');                
            }, false);

            // Listen for pinch and pan 
            this.bind_pinch_and_pan();

            this.btn_zoom_pan_restore.addEventListener('click', () => {
                this.restore_container_to_initial_scale();
            }, false);
        }

        // Listen for the key press.
        this.body.addEventListener('keyup', e => {
            switch(e.keyCode) {
                case 27: //esc key press
                    if(this.modal_dialog_signature.is_open() === true) {
                        this.modal_dialog_signature.close();
                    } else if(this.modal_dialog_text_row.is_open() === true) {
                        this.modal_dialog_text_row.close();                          
                    } else if(this.modal_dialog_text_area.is_open() === true) {
                        this.modal_dialog_text_area.close();
                    } else if(this.modal_dialog_agreement.is_open() === true) {
                        this.modal_dialog_agreement.close();
                    } 
                    break;
                case 13:  //'enter' key press
                    if(this.modal_dialog_text_row.is_open() === true) {
                        this.modal_dialog_text_row.trigger_insert_text();
                    } else if(this.modal_dialog_signature.is_open() === true) {
                        this.modal_dialog_signature.trigger_insert_signature();                        
                    }
                    break;    
            }          
        });           
        
        window.addEventListener('offline', () => { 
            this.handle_go_to_stage_seven(); 
        });
        
        window.addEventListener('online', () => { 
            this.initialize();
        });

        this.back_to_doc.addEventListener("click", () => {
            this.lighter_overlay();
            this.handle_review_doc_click();                
        }, false);

        this.left_btn_action.addEventListener("click", this.handle_action_click.bind(this), false);
        
        this.left_btn_action.addEventListener("mouseenter", (e) => {            
            if (e.target.classList.contains("partial-disabled") && this.tooltip_util.has_tooltip(this.left_btn_action) === false) {
                this.tooltip_util.attach_approve_agreement_to_element(this.left_btn_action);
            }
        }, false);

        this.left_btn_action.addEventListener('mouseleave', e => {
             if(e.target.classList.contains("partial-disabled") && this.tooltip_util.has_tooltip(this.left_btn_action)) {
                 this.tooltip_util.remove_tooltip(e.target);
             }
        }, false);

        document.querySelectorAll('.sign-here-placeholder').forEach(item => {
            item.addEventListener('click', e => {
                this.modal_dialog_signature.open(e.target);
            });
        });

        if(this.is_mobile() === false) {            

            document.querySelectorAll('.sign-here-placeholder').forEach(item => {
                item.addEventListener('mouseenter', e => {
                    if(e.target.dataset.isDirty === "false" && this.tooltip_util.has_tooltip(e.target) === false) {
                        this.tooltip_util.attach_tooltip_to_element(e.target);
                    }
                });                
                item.addEventListener('mouseleave', e => {
                    if(e.target.dataset.isDirty === "false") {
                        this.tooltip_util.remove_tooltip(e.target);
                    }
                });
            });

        
            jQuery('.textual').mouseenter({outerRef: this}, function (args) {
                if(this.dataset.isDirty === "false") {
                    args.data.outerRef.tooltip_util.attach_tooltip_to_element(this);
                }
            }).mouseleave({outerRef: this}, function (args) {
                if(this.dataset.isDirty === "false") {
                    args.data.outerRef.tooltip_util.remove_tooltip(this);
                }
            });
        }

        document.querySelectorAll('.text-field-placeholder').forEach(item => {
            item.addEventListener('click', e => {
                this.modal_dialog_text_row.open(e.target);
            });
        });       
        
        document.querySelectorAll('.text-area-placeholder').forEach( item => {
            item.addEventListener('click', e => {
                this.modal_dialog_text_area.open(e.target);
            });            
        });
    }      
    
    init_pinched_elements_data_structure(){
        var container = document.querySelector("body > p:nth-child(1)"), 
            image_wrappers = container.querySelectorAll(".image-wrapper"),                       
            pivot_data_image = null, sign_plh_coll = null,
            text_field_coll = null,  text_area_coll = null,
            in_advance_field_coll = null, in_advance_signature_coll = null, 
            pivot_image_wrapper = null;

        container.style.left = `${Number((this.body.offsetWidth / 2) - (container.offsetWidth/2))}px`;
        container.style.top = "0"; 
        
        this.container_p = {
            ref: container,
            initial_width: container.offsetWidth,
            current_width: container.offsetWidth,
            initial_height: container.offsetHeight,
            current_height: container.offsetHeight,
            initial_left: container.offsetLeft,
            current_left: container.offsetLeft,
            initial_top: container.offsetTop,
            current_top: container.offsetTop,
            children: []                    
        };

        image_wrappers.forEach( item => {
            
            pivot_data_image = item.querySelector('.data-image');
            sign_plh_coll = item.querySelectorAll('.sign-here-placeholder');
            text_field_coll = item.querySelectorAll('.text-field-placeholder');
            text_area_coll = item.querySelectorAll(".text-area-placeholder");
            in_advance_field_coll = item.querySelectorAll(".text-in-advance-placeholder");
            in_advance_signature_coll = item.querySelectorAll('[data-tx-name=in-advance-signature]');

            pivot_image_wrapper = {
                ref: item,
                current_width: item.offsetWidth,                       
                current_height: item.offsetHeight,        
                initial_width: Number(item.dataset.initialWidth),                               
                initial_height: Number(item.dataset.initialHeight),
                initial_left: item.offsetLeft,
                current_left: item.offsetLeft,                    
                children: {
                    data_image: { 
                        ref: pivot_data_image,
                        current_width: pivot_data_image.clientWidth,
                        initial_width: Number(pivot_data_image.dataset.initialWidth),
                        current_height: pivot_data_image.clientHeight,                            
                        initial_height: Number(pivot_data_image.dataset.initialHeight)                            
                    },
                    sign_placeholders:[],
                    text_field_placeholders:[],
                    text_area_placeholders:[],
                    in_advance_placeholders:[],
                    in_advance_signature_placeholders: []
                }
            };

            sign_plh_coll.forEach(function(sign_plh){
                var new_sign_plh =  {
                    ref: sign_plh,
                    current_width: sign_plh.offsetWidth,
                    initial_width: Number(sign_plh.dataset.initialWidth),
                    current_height: sign_plh.offsetHeight,
                    initial_height: Number(sign_plh.dataset.initialHeight),
                    current_top: Number(sign_plh.dataset.initialTop),
                    initial_top: Number(sign_plh.dataset.initialTop),
                    current_right: Number(sign_plh.dataset.initialRight),                        
                    initial_right: Number(sign_plh.dataset.initialRight)       
                };
                pivot_image_wrapper.children.sign_placeholders.push(new_sign_plh);
            });

            text_field_coll.forEach(function(textf_plh){
                var new_text_field_plh =  {
                    ref: textf_plh,
                    current_width: textf_plh.offsetWidth,                        
                    initial_width: Number(textf_plh.dataset.initialWidth),
                    current_height: textf_plh.offsetHeight,
                    initial_height: Number(textf_plh.dataset.initialHeight),
                    current_top: Number(textf_plh.dataset.initialTop),
                    initial_top: Number(textf_plh.dataset.initialTop),
                    current_right: Number(textf_plh.dataset.initialRight),                        
                    initial_right: Number(textf_plh.dataset.initialRight),
                    initial_font_size: Number(textf_plh.dataset.initialFontSize),
                    current_font_size: Number(textf_plh.dataset.initialFontSize),
                    initial_line_height: Number(textf_plh.dataset.initialLineHeight),
                    current_line_height: Number(textf_plh.dataset.initialLineHeight)
                };
                pivot_image_wrapper.children.text_field_placeholders.push(new_text_field_plh);
            });

            text_area_coll.forEach(function(texta_plh){
                var new_textarea_plh =  {
                    ref: texta_plh,
                    current_width: texta_plh.offsetWidth,
                    initial_width: Number(texta_plh.dataset.initialWidth),
                    current_height: texta_plh.offsetHeight,                        
                    initial_height: Number(texta_plh.dataset.initialHeight),
                    current_top: Number(texta_plh.dataset.initialTop),
                    initial_top: Number(texta_plh.dataset.initialTop),
                    current_right: Number(texta_plh.dataset.initialRight),
                    initial_right: Number(texta_plh.dataset.initialRight),
                    initial_font_size: Number(texta_plh.dataset.initialFontSize),
                    current_font_size: Number(texta_plh.dataset.initialFontSize),
                    initial_line_height: Number(texta_plh.dataset.initialLineHeight),
                    current_line_height: Number(texta_plh.dataset.initialLineHeight)
                };
                pivot_image_wrapper.children.text_area_placeholders.push(new_textarea_plh);
            });

            in_advance_field_coll.forEach(function(in_adv_item){
                var new_in_adv_obj = {
                    ref: in_adv_item,
                    current_width: in_adv_item.offsetWidth,
                    initial_width: Number(in_adv_item.dataset.initialWidth),
                    current_height: in_adv_item.offsetHeight,                        
                    initial_height: Number(in_adv_item.dataset.initialHeight),
                    current_top: Number(in_adv_item.dataset.initialTop),
                    initial_top: Number(in_adv_item.dataset.initialTop),
                    current_right: Number(in_adv_item.dataset.initialRight),
                    initial_right: Number(in_adv_item.dataset.initialRight),
                    initial_font_size: Number(in_adv_item.dataset.initialFontSize),
                    current_font_size: Number(in_adv_item.dataset.initialFontSize),
                    initial_line_height: Number(in_adv_item.dataset.initialLineHeight),
                    current_line_height: Number(in_adv_item.dataset.initialLineHeight)
                };
                pivot_image_wrapper.children.in_advance_placeholders.push(new_in_adv_obj);
            });

            in_advance_signature_coll.forEach(function(in_adv_item) {
                var new_in_adv_obj = {
                    ref: in_adv_item,
                    current_width: in_adv_item.offsetWidth,
                    initial_width: Number(in_adv_item.dataset.initialWidth),
                    current_height: in_adv_item.offsetHeight,
                    initial_height: Number(in_adv_item.dataset.initialHeight),
                    current_top: Number(in_adv_item.dataset.initialTop),
                    initial_top: Number(in_adv_item.dataset.initialTop),
                    current_right: Number(in_adv_item.dataset.initialRight),                        
                    initial_right: Number(in_adv_item.dataset.initialRight) 
                };

                pivot_image_wrapper.children.in_advance_signature_placeholders.push(new_in_adv_obj);
            });
            
            this.container_p.children.push(pivot_image_wrapper);               
        });            
    }

    position_all_placeholders() {
        this.position_signatures();
        this.position_text_fields();        
        this.position_text_areas();
        this.position_in_advance_fields();
        this.position_in_advance_signatures(); 
    }

    position_signatures() {
        var i,
            target_container,
            pivot_ele,
            sign_here_placeholder,
            pivot_position = null;

        var image_signatures = document.querySelectorAll("img[src='sign_field.jpg'], img[src='sign_field_opaque.jpg']");

        for (i = 0; i < image_signatures.length; i++) {
            pivot_ele = image_signatures[i];
            
            pivot_position = this.positioning_tool.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);

            sign_here_placeholder = document.createElement('img');
            sign_here_placeholder.classList.add('sign-here-placeholder');
            
            if(pivot_ele.src.indexOf('_opaque') !== -1) {
                sign_here_placeholder.classList.add('sign-opaque');
            } 
                           
            sign_here_placeholder.dataset.isDirty = 'false';
            sign_here_placeholder.dataset.dt = '';
            sign_here_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;
            
            if(this.is_mobile() === true) {
                sign_here_placeholder.classList.add('mobile-images');
                sign_here_placeholder.dataset.initialWidth = pivot_ele.offsetWidth;
                sign_here_placeholder.dataset.initialHeight = pivot_ele.offsetHeight;
                sign_here_placeholder.dataset.initialTop = pivot_position.top ;//in points
                sign_here_placeholder.dataset.initialRight = pivot_position.right;//in points
            }

            sign_here_placeholder.style.width = `${pivot_ele.clientWidth}px`;
            sign_here_placeholder.style.height = `${pivot_ele.clientHeight}px`;
            sign_here_placeholder.style.top = `${pivot_position.top}pt`;
            sign_here_placeholder.style.right = `${pivot_position.right}pt`;
                            
            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(sign_here_placeholder);              

            pivot_ele.parentNode.removeChild(pivot_ele);
        }
           
    }

    position_text_fields() {
        var i, 
        pivot_ele = null, 
        pivot_position = null, 
        text_field_placeholder = null,
        target_container = null,
        inner = null,
        is_required = false,
        is_font_bold = false,
        image_text_fields = document.querySelectorAll("[src='sign_text.jpg']");
        
        for (i = 0; i < image_text_fields.length; i++) {
            pivot_ele = image_text_fields[i];
        
            pivot_position = this.positioning_tool.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);

            text_field_placeholder = document.createElement('div');
            text_field_placeholder.classList.add('text-field-placeholder', 'textual', this.language);                
            this.is_mobile() && text_field_placeholder.classList.add('mobile-images');               

            is_font_bold = this.is_text_placeholer_bold(pivot_ele);
            is_required = this.is_text_placeholer_required(pivot_ele);                
            
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('^^^', '');
            pivot_ele.dataset.txName = pivot_ele.dataset.txName.replace('~~~', '');                           
            
            text_field_placeholder.dataset.labelHelper = pivot_ele.dataset.txName;
            text_field_placeholder.dataset.isRequired = is_required;
            text_field_placeholder.dataset.isDirty = "false";//TODO
            text_field_placeholder.dataset.dt = "";
            text_field_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;
            
            inner = document.createElement('div');
            inner.classList.add('inner');
            text_field_placeholder.appendChild(inner);

            if(this.is_mobile() === true) {
                text_field_placeholder.dataset.initialWidth = pivot_ele.clientWidth;
                text_field_placeholder.dataset.initialHeight = pivot_ele.clientHeight;
                text_field_placeholder.dataset.initialTop = pivot_position.top;
                text_field_placeholder.dataset.initialRight = pivot_position.right;                    
                text_field_placeholder.dataset.initialFontSize = pivot_ele.clientHeight*0.5;
                text_field_placeholder.dataset.initialLineHeight = pivot_ele.clientHeight-4;
            }

            text_field_placeholder.style.width      = `${pivot_ele.clientWidth}px`;
            text_field_placeholder.style.minWidth   = `${pivot_ele.clientWidth}px`;
            text_field_placeholder.style.height     = `${pivot_ele.clientHeight}px`;
            text_field_placeholder.style.fontSize   = `${ pivot_ele.clientHeight * 0.5}px`;
            text_field_placeholder.style.lineHeight = `${pivot_ele.clientHeight - 4}px`;                
            text_field_placeholder.style.top   = `${pivot_position.top}pt`;
            text_field_placeholder.style.right = `${pivot_position.right}pt`;
            text_field_placeholder.style.fontWeight = is_font_bold ? "bold" : "normal";
            
            if(is_required === true) {
                this.modal_dialog_text_row.append_asterisk_to_placeholder(text_field_placeholder);
            }                              
                           
            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(text_field_placeholder);

            pivot_ele.parentNode.removeChild(pivot_ele);
        }
    } 

    position_text_areas() {
        var i,  pivot_ele = null,  pivot_position = null, 
        text_area_placeholder = null, target_container = null,
        is_required = false,  label = '', max_chars_allowed = 0, inner = null,
        image_text_areas = document.querySelectorAll("[src='sign_text_area.jpg']");

        for (i = 0; i < image_text_areas.length; i++) {
            pivot_ele = image_text_areas[i];
        
            pivot_position = this.positioning_tool.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);
            text_area_placeholder = document.createElement('div');
            text_area_placeholder.classList.add('text-area-placeholder', 'textual', this.language);
            
            this.is_mobile() && text_area_placeholder.classList.add('mobile-images');               

            is_required = this.is_text_placeholer_required(pivot_ele);                
            label = is_required === true ? pivot_ele.dataset.txName.replace('~~~', '') : pivot_ele.dataset.txName; 

            text_area_placeholder.dataset.labelHelper = label;
            text_area_placeholder.dataset.isRequired = is_required;
            text_area_placeholder.dataset.isDirty = 'false';
            text_area_placeholder.dataset.dt = "";
            text_area_placeholder.dataset.globalTopForSorting = pivot_position.global_top_for_sorting;
            
            inner = document.createElement('div');
            inner.classList.add('inner');
            text_area_placeholder.appendChild(inner);

            if(this.is_mobile() === true) {
                text_area_placeholder.dataset.initialWidth = pivot_ele.clientWidth;
                text_area_placeholder.dataset.initialHeight = pivot_ele.clientHeight;
                text_area_placeholder.dataset.initialTop = pivot_position.top;
                text_area_placeholder.dataset.initialRight = pivot_position.right;
                text_area_placeholder.dataset.initialFontSize = this.TEXT_AREA_PLACEHOLDER_STYLES.fontSize;
                text_area_placeholder.dataset.initialLineHeight = this.TEXT_AREA_PLACEHOLDER_STYLES.lineHeight;
            }
           
            text_area_placeholder.style.fontSize   = `${this.TEXT_AREA_PLACEHOLDER_STYLES.fontSize}px`;
            text_area_placeholder.style.lineHeight = `${this.TEXT_AREA_PLACEHOLDER_STYLES.lineHeight}px`;
            text_area_placeholder.style.width  = `${pivot_ele.clientWidth}px`;
            text_area_placeholder.style.height =  `${pivot_ele.clientHeight}px`;
            text_area_placeholder.style.textAlign = this.language === 'heb' ? 'right' : 'left';
            text_area_placeholder.style.top = `${pivot_position.top}pt`;
            text_area_placeholder.style.right = `${pivot_position.right}pt`;                
            
            if(is_required === true) {
                this.modal_dialog_text_area.append_asterisk_to_placeholder(text_area_placeholder);
            }            

            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(text_area_placeholder);
            
            max_chars_allowed = calc_max_chars_for_textarea.call(this, text_area_placeholder); 
            text_area_placeholder.dataset.maxLength = max_chars_allowed;                 

            pivot_ele.parentNode.removeChild(pivot_ele);
        }

        function calc_max_chars_for_textarea (placeholder) { 
            var result = 0;
            var placeholeder_width = this.is_mobile() === true ?  Number(placeholder.dataset.initialWidth)  : placeholder.clientWidth; 
            var placeholeder_height = this.is_mobile() === true ? Number(placeholder.dataset.initialHeight) : placeholder.clientHeight; 
    
            var rows = Math.floor(placeholeder_height / this.TEXT_AREA_PLACEHOLDER_STYLES.lineHeight);
            var colls = Math.floor(placeholeder_width / this.TEXT_AREA_PLACEHOLDER_STYLES.fontSize); 
            
            var max_chars_allowed = Math.round(colls * rows * 2.1);
            result = max_chars_allowed - (max_chars_allowed % 10);             
            return result;
        }
    }

    position_in_advance_fields() {
        var i, 
        p_ele = null,
        inner_html = '',
        span_list = null,
        pivot_ancestor = null,  
        pivot_position = null, 
        text_in_advance_placeholder = null,
        target_container = null,
        in_advance_fields = get_items_sorted_top_to_bottom();  
        
        for (i = 0; i < in_advance_fields.length; i++) {
            p_ele = in_advance_fields[i];
            
            pivot_ancestor = p_ele.parentNode; // grab div element
            pivot_position = this.positioning_tool.calc_position(pivot_ancestor.style.top, pivot_ancestor.style.left, pivot_ancestor.clientWidth);

            text_in_advance_placeholder = document.createElement('div');
            text_in_advance_placeholder.classList.add('text-in-advance-placeholder');
            
            text_in_advance_placeholder.innerHTML = p_ele.textContent;
            /*span_list = p_ele.querySelectorAll("span");

            if(span_list.length > 0) {
                inner_html = "";
                span_list.forEach(item => {
                    inner_html += item.innerHTML.replace(/\s/g, '<span> </span>');
                    inner_html = inner_html.replace(',', '<span>,</span>');
                    inner_html = inner_html.replace('.', '<span>.</span>');
                });
            } else {
                inner_html = p_ele.innerHTML.replace(/\s/g, '<span> </span>');
                inner_html = inner_html.replace(',', '<span>,</span>');
                inner_html = inner_html.replace('.', '<span>.</span>');
            }          

            text_in_advance_placeholder.innerHTML = inner_html;*/
            
            this.is_mobile() && text_in_advance_placeholder.classList.add('mobile-images');   
            text_in_advance_placeholder.dataset.isDirty = "true";

            if(this.is_mobile() === true) {
                text_in_advance_placeholder.dataset.initialWidth = pivot_ancestor.clientWidth;
                text_in_advance_placeholder.dataset.initialHeight = pivot_ancestor.clientHeight;
                text_in_advance_placeholder.dataset.initialTop = pivot_position.top;
                text_in_advance_placeholder.dataset.initialRight = pivot_position.right - 18;
                text_in_advance_placeholder.dataset.initialFontSize = pivot_ancestor.clientHeight*0.5;
                text_in_advance_placeholder.dataset.initialLineHeight = pivot_ancestor.clientHeight;
            }

            text_in_advance_placeholder.style.width  = `${pivot_ancestor.clientWidth}px`;                
            text_in_advance_placeholder.style.height = `${pivot_ancestor.clientHeight}px`;
            text_in_advance_placeholder.style.fontSize = `${ pivot_ancestor.clientHeight*0.5}px`;
            text_in_advance_placeholder.style.lineHeight = `${pivot_ancestor.clientHeight}px`;
            text_in_advance_placeholder.style.top = `${pivot_position.top}pt`;
            text_in_advance_placeholder.style.right = `${pivot_position.right - 18}pt`;
                          
            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(text_in_advance_placeholder);

            pivot_ancestor.parentNode.removeChild(pivot_ancestor);
        } 
        
        function get_items_sorted_top_to_bottom(){
            let node_list = document.querySelectorAll("[data-tx-name='in-advance-label'] > p");
            let arr = Array.from(node_list); 
            arr.sort(sort_by_top_position);

            return arr;

            function sort_by_top_position (a, b) {
                var a_top =  Number(a.parentNode.parentNode.style.top.replace('pt', ''));
                var b_top =  Number(b.parentNode.parentNode.style.top.replace('pt', ''));
                return a_top - b_top;                    
            }
        }
    }

    position_in_advance_signatures() {
        var i,
        pivot_ele,
        pivot_position, 
        target_container,          
        in_advance_signatures = document.querySelectorAll('[data-tx-name=in-advance-signature]');

        for (i = 0; i < in_advance_signatures.length; i++) {
            pivot_ele = in_advance_signatures[i];                

            pivot_position = this.positioning_tool.calc_position(pivot_ele.style.top, pivot_ele.style.left, pivot_ele.clientWidth);

            pivot_ele.dataset.isDirty = "true";

            if(this.is_mobile() === true) {
                pivot_ele.classList.add('mobile-images'); 
                pivot_ele.dataset.initialWidth = pivot_ele.offsetWidth;
                pivot_ele.dataset.initialHeight = pivot_ele.offsetHeight;
                pivot_ele.dataset.initialTop = pivot_position.top ;//in points
                pivot_ele.dataset.initialRight = pivot_position.right;//in points
            }

            pivot_ele.style.top = `${pivot_position.top}pt`;
            pivot_ele.style.right = `${pivot_position.right - 18}pt`;

            target_container = document.querySelector(`#image-wrapper${pivot_position.page_num}`);
            target_container.appendChild(pivot_ele);               
        }
    }    

    render_bottom_bar() {

        var status = determine_fields_status();

        switch(status){
            case "required-and-unrequired-fields":
                this.bottom_bar_center.style.display = "flex";
                this.bottom_bar_left.style.display = "none"; 
                break;
            case "only-unrequired-fields":
                this.bottom_bar_center.style.display = "flex";
                this.bottom_bar_left.style.display = "flex"; 
                this.signatures_validator.style.display = "none"; 
                break;
            case "no-fields-left":
                this.bottom_bar_center.style.display = "none";
                this.bottom_bar_left.style.display = "flex"; 
                this.signatures_validator.style.display = "none"; 
                break;
        }           
        
        refresh_placeholders_corousel.call(this);
        
        function determine_fields_status() {
            var result = "";
            //get all signatures placeholders that are NOT 'dirty' and all required textual placeholders that are NOT 'dirty'
            var node_list = document.querySelectorAll(".sign-here-placeholder[data-is-dirty='false'], .textual[data-is-required='true'][data-is-dirty='false']");
            if( node_list.length > 0) {
                result = 'required-and-unrequired-fields';
            } else if(node_list.length === 0) {
                node_list = document.querySelectorAll(".textual[data-is-required='false'][data-is-dirty='false']");
                if(node_list.length > 0) {
                    result = 'only-unrequired-fields';
                } else if (node_list.length === 0) {
                    result = 'no-fields-left';
                }
            }
            return result;
        }

        function refresh_placeholders_corousel() {
            var top, ele = null;
            this.mobile_scenario_all_placeholders = this.get_sorted_placeholders();
            
            this.mobile_scenario_all_placeholders.forEach( item => {
                if(this.tooltip_util.has_tooltip(item) === true ) {
                    this.tooltip_util.remove_tooltip(item);
                }
            });

            if(this.mobile_scenario_all_placeholders.length > 0) {
                this.current_placeholder_pointer = 0;
                ele = this.mobile_scenario_all_placeholders[this.current_placeholder_pointer];
                top = jQuery(ele).offset().top; //ele.getBoundingClientRect().top;                
                window.scroll({ top: top - 400, left: 0, behavior: 'smooth' });                
                if(this.tooltip_util.has_tooltip(ele) === false) {                
                    this.tooltip_util.attach_tooltip_to_element(ele);
                }
            }             
        }
    } 
    
    get_sorted_placeholders() {
        var node_list = document.querySelectorAll(".sign-here-placeholder[data-is-dirty='false'], .textual[data-is-dirty='false']");
        var arr = Array.from(node_list); 
        arr.sort(sort_by_top_position);               

        return arr;

        function sort_by_top_position (a, b) {
            var a_top =  Number(a.dataset.globalTopForSorting);
            var b_top =  Number(b.dataset.globalTopForSorting);
            return a_top - b_top;                    
        }                
    }

    handle_go_to_stage_seven() {
        this.hide_overlay();   
        this.current_stage = 7;            
        this.render_current_stage();                     
        this.disable_body_scrolling(); 
    }

    handle_go_to_stage_nine() {
        this.hide_overlay();   
        this.progress_bar.hide();
        this.bottom_bar.style.display = "block";
        this.signatures_validator.style.display = "none";
        this.current_stage = 9;            
        this.render_current_stage(); 
    }

    set_data_from_hidden_fields () {        
        
        var hidden_business_address = document.querySelector("#mail-to-address");
        if (hidden_business_address !== null) {
            this.business_email_address = hidden_business_address.value;
        }

        var hidden_business_name = document.querySelector("#mail-to-name");
        if (hidden_business_name !== null) {
            this.business_name = hidden_business_name.value;
        }

        var hidden_customer_name = document.querySelector("#customer-fullname");
        if (hidden_customer_name !== null) {
            this.customer_fullname = hidden_customer_name.value;
        }

        var hidden_additional_message = document.querySelector("#additional-message");
        if (hidden_additional_message !== null) {
            this.additional_message = hidden_additional_message.value;
        }
        
        var hidden_business_self_sign = document.querySelector("#business-self-sign");
        if (hidden_business_self_sign !== null) {
            this.business_self_sign = hidden_business_self_sign.value === "true";
        }

        var hidden_token = document.querySelector("#token");	
        if (hidden_token !== null) {	
            this.server_token = hidden_token.value;	
        }

        var hidden_doc_name = document.querySelector("#doc_name");
        if (hidden_doc_name !== null) {
            this.doc_name = hidden_doc_name.value;
        }
        
        var hidden_journey_id = document.querySelector("#journey-id");
        if (hidden_journey_id !== null) {	
            this.journey_id = hidden_journey_id.value;	
        } 
    }

    override_debug_behavior () {
        var debug_env = document.querySelector("#debug-env");
        if(debug_env !== null) {
            this.ENV = debug_env.value.toLowerCase().trim();
            console.log(`working with env: ${this.ENV}`);
        }

        var debug_jump_to_ele = document.querySelector("#debug-jump-directly-to-page");
        if(debug_jump_to_ele !== null) {
            this.DEBUG_JUMP_DIRECTLY_TO_STAGE = Number(debug_jump_to_ele.value.trim());
        }
        
        var debug_emulate_sending_pdf = document.querySelector('#debug-emulate-sending-pdf');
        if(debug_emulate_sending_pdf !== null) {
            this.DEBUG_EMULATE_SENDING_PDF = debug_emulate_sending_pdf.value.toLowerCase().trim() === 'true';
        }
    }

    set_data_images_class_names_and_sizes(){        
        var image_collection = document.querySelectorAll("img[src^='data:image']:not([data-tx-name])");
        
        image_collection.forEach(function(item) {
            //add each data image a class name "data-image" for better selector
            item.classList.add("data-image");
            item.dataset.initialWidth = item.clientWidth;
            item.dataset.initialHeight = item.clientHeight;                
        });
        
        return image_collection;            
    }

    wrap_data_images_with_containers() {
            
        var images_parent = this.data_images[0].parentNode;

        remove_br_elements();           
        wrap_each_data_image_with_container.call(this);
        
        function wrap_each_data_image_with_container() {
            var i, pivot_wrapper_div = null, wrappers_collection = null;                    

            for (i = 0; i < this.data_images.length; i++) {
                pivot_wrapper_div = document.createElement('div');
                pivot_wrapper_div.setAttribute('id', `image-wrapper${i + 1}`);
                pivot_wrapper_div.classList.add('image-wrapper');
                if(this.is_mobile()===true){
                    pivot_wrapper_div.classList.add('mobile-images');
                }
                images_parent.appendChild(pivot_wrapper_div);
            } 
            
            wrappers_collection = document.querySelectorAll('.image-wrapper');
            for (i = 0; i < wrappers_collection.length; i++) {
                wrappers_collection[i].appendChild(this.data_images[i]);
            }                
        }

        function remove_br_elements() {
            var i, brNodeList = null;

            brNodeList = images_parent.querySelectorAll("br");

            for (i = 0; i < brNodeList.length; i++) {
                brNodeList[i].parentNode.removeChild(brNodeList[i]);
            }
        }           
    }

    set_containers_of_data_image_sizes () {

        var wrappers_coll = document.querySelectorAll('.image-wrapper');
        wrappers_coll.forEach(function(item){
            item.style.width = `${item.offsetWidth}px`;
            item.style.height = `${item.offsetHeight}px`;
            item.dataset.initialWidth = item.offsetWidth;
            item.dataset.initialHeight = item.offsetHeight;
        });    
    }

    set_class_members() {        
        
        this.white_board = document.querySelector("#white-board");
        this.white_board_title = this.white_board.querySelector(".title");
        this.white_board_subtitle = this.white_board.querySelector(".sub-title");
        
        this.bottom_bar = document.querySelector("#bottom-bar");            
        this.bottom_bar_left = this.bottom_bar.querySelector("#left-side");
        this.left_btn_action = this.bottom_bar_left.querySelector("#btn-action");
        this.left_btn_action_txt = this.bottom_bar_left.querySelector("#btn-action > .txt");
        
        this.bottom_bar_center = this.bottom_bar.querySelector("#center-side");
        this.center_btn_up = this.bottom_bar_center.querySelector("#btn-jump-up");
        this.center_btn_down = this.bottom_bar_center.querySelector("#btn-jump-down");

        this.bottom_bar_right = this.bottom_bar.querySelector("#right-side");
        this.back_to_doc = this.bottom_bar.querySelector("#back-to-doc");

        this.upload_to_server_spinner = this.bottom_bar.querySelector("#upload-to-server-spinner");
        this.signatures_validator = this.bottom_bar.querySelector("#signatures-validator");
        this.btn_zoom_pan_restore = document.querySelector("#zoom-pan-restore");
        this.advertise_good_sign = document.querySelector("#advertise-good-sign");    
        this.approve_agreement = document.querySelector("#approve-agreement");        
        this.approve_agreement_checkbox = this.approve_agreement.querySelector("#real-checkbox");
        this.button_open_terms = this.approve_agreement.querySelector("#btn-open-terms");

        this.modal_dialog_signature = new SignatureModal(
            document.querySelector("#modal-dialog-signature"), this.LINE_COLOR, this);
            
        this.modal_dialog_text_row = new TextRowModal(
            document.querySelector("#modal-dialog-text-row"), this);

        this.modal_dialog_text_area = new TextAreaModal(
            document.querySelector("#modal-dialog-text-area"), this);
        
        this.modal_dialog_agreement = new AgreementModal(
            document.querySelector("#modal-dialog-agreement"), this
        );

        this.progress_bar = new ProgressBar();

        if (this.is_mobile() === true) {
            this.bottom_bar.classList.add("mobile-images");
            this.btn_zoom_pan_restore.classList.add('mobile-images');                
            this.white_board.classList.add("mobile-images"); 
            this.advertise_good_sign.classList.add("mobile-images");            
        } 
    }
    
    set_language_styles () {          

        if (this.language === "heb") {
            this.white_board.classList.add("heb");
            this.bottom_bar.classList.add("heb");             
            this.btn_zoom_pan_restore.classList.add("heb");
            this.advertise_good_sign.classList.add("heb");           
        } else {
            this.white_board.classList.add("eng");
            this.bottom_bar.classList.add("eng");           
            this.btn_zoom_pan_restore.classList.add("eng");
            this.advertise_good_sign.classList.add("eng");            
        }
    }     

    prepare_head () {
        var head = document.querySelector("head");

        var base = head.querySelector("base");
        if (base !== null) {
            base.parentNode.removeChild(base);
        }

        var old_viewport = head.querySelector("meta[name='viewport']");
        if (old_viewport !== null) {
            old_viewport.parentNode.removeChild(old_viewport);
        }            

        var title = head.querySelector("title");
        if (title !== null) {
            title.innerText = "Good-Sign";
        } else {
            title = document.createElement("title");
            title.innerText = "Good-Sign";
            head.appendChild(title);
        }

        var old_theme_color = head.querySelector("meta[name='theme-color']");
        if (old_theme_color !== null) {
            old_theme_color.parentNode.removeChild(old_theme_color);
        }

        var new_theme_color = document.createElement("meta");
        new_theme_color.setAttribute("name", "theme-color");
        new_theme_color.setAttribute("content", "#586AC3");
        head.appendChild(new_theme_color);
    }

    append_additional_markup_to_body() {
        var html = `<div id="overlay"><div id="progress-wrapper" class="none"><div class="progress-text-top"> <strong class="direction"></strong> <i class="fas fa-check none"></i> <span class="three-dots-container"> <span class="direction three-dots">.</span> <span class="direction three-dots">.</span> <span class="direction three-dots">.</span> </span></div><div class="progress-bar"><div class="progress-bar-inner"></div><div class="inner-text"></div></div></div><div id="modal-dialog-signature"><table class="tbl-modal"><tr><td colspan="2" class="first-row"> <span id="close-modal" class="close-modal"> <i class="far fa-times-circle fa-3x"></i> </span> <span class="title"></span></td></tr><tr><td colspan="2"><div class="sign-plate"> <canvas id="canvas-signature" class="canvas-signature"></canvas></div></td></tr><tr><td class="last-row-split clear"> <a href="javascript:void(0)" id="btn-clear-signature" class="btn-clear-signature"></a></td><td class="last-row-split"> <a href="javascript:void(0)" id="btn-insert-signature" class="btn-insert-signature"></a></td></tr></table></div><div id="modal-dialog-text-row"><table class="tbl-modal"><tr><td class="first-row"> <span id="close-modal" class="close-modal"> <i class="far fa-times-circle fa-3x"></i> </span><div class="middle-row"> <label class="lbl" for="text-field-row"></label><div class="txt-wrapper"> <input class="txt-input" id="text-field-row" type="text" autocomplete="off" maxlength="40" /><div class="reqiured-info"> <sup><i class="fas fa-asterisk"></i></sup> <span class="required-txt"></span></div></div></div></td></tr><tr><td class="last-row-wide"> <a href="javascript:void(0)" id="btn-ok-text-row" class="btn-ok-text"></a></td></tr></table></div><div id="modal-dialog-text-area"><table class="tbl-modal"><tr><td class="first-row"> <span id="close-modal" class="close-modal"> <i class="far fa-times-circle fa-3x"></i> </span></td></tr><tr><td class="second-row"> <label class="lbl" for="text-field-area"></label></td></tr><tr><td class="third-row"><div class="sign-plate"><textarea class="txt-area" id="text-field-area" type="text"></textarea><div class="sign-plate-bottom"><div class="chars-counter"> <span id="max-lbl"></span><span id="max-chars-allowed"></span><span id="chars-lbl"></span>(<span id="chars-left-lbl"></span><span id="chars-remaining"></span>)</div><div class="required-info"> <sup><i class="fas fa-asterisk"></i></sup> <span class="required-txt"></span></div></div></div></td></tr><tr><td class="last-row-wide"> <a href="javascript:void(0)" id="btn-ok-text-area" class="btn-ok-text"></a></td></tr></table></div><div id="modal-dialog-agreement"><div class="close-agreement"> <i class="far fa-times-circle fa-3x" aria-hidden="true"></i></div><div id="agreement-text-container"></div></div></div><div id="white-board" class="white-board"><div class="inner"><div class="title"></div><div class="sub-title"></div> <label class="checkbox-container" id="approve-agreement"> <input id="real-checkbox" type="checkbox"><div class="checkbox-custom"></div> <span id="txt-i-accept"></span> <a id="btn-open-terms" href="javascript:void(0)"></a> </label></div><div id="advertise-good-sign" class="advertise-good-sign"> <span class="advertise-prefix"></span> <a target="_blank" rel="noopener" class="link-to-good-sign" href="http://good-sign.co.il/">Good-Sign</a></div></div><div id="bottom-bar" class="bottom-bar"><div id="right-side" class="right-side"> <button id="back-to-doc" class="back-to-doc"> <i class="fas"></i> <span class="txt"></span> </button></div><div id="center-side" class="center-side"> <button id="btn-jump-down" class="btn-action"> <span class="fa-stack fa-1x"> <i class="fas fa-file fa-stack-2x"></i> <i class="fas fa-long-arrow-alt-down fa-stack-1x"></i> </span> <span class="txt"></span> </button> <button id="btn-jump-up" class="btn-action"> <span class="txt"></span> <span class="fa-stack fa-1x"> <i class="fas fa-file fa-stack-2x fa-flip-horizontal"></i> <i class="fas fa-long-arrow-alt-up fa-stack-1x"></i> </span> </button></div><div id="left-side" class="left-side"> <button id="btn-action" class="btn-action"> <span class="txt"></span> <i class="fas fa-2x "></i> </button> <i id="upload-to-server-spinner" class="fas fa-spinner fa-spin"></i> <span id="signatures-validator" class="signatures-validator"></span></div></div><div id="zoom-pan-restore" class="zoom-pan-restore"><div class="inner"></div></div>`;
        var frag = document.createRange().createContextualFragment(html);
        this.body.appendChild(frag);
    } 

    remove_paragraph_margin_bottom(){
        var p_ele = document.querySelector('body > p');
        if(Number(p_ele.style.marginBottom.replace('pt','')) > 0) {
            p_ele.style.marginBottom = 0;
        }
    } 
    
    get_hello_message () {
        let result = '';
        if(this.ENV === "pre-prod"){
            let h = new Date().getHours();
            if(h >= 0 && h < 12 ) {
                result = this.language === "heb" ? 'בוקר טוב' : 'Good Morning';
            } else if(h >= 12 && h < 18 ) {
                result = this.language === "heb" ? 'צהריים טובים' : 'Good Afternoon';
            } else if(h >= 18 && h < 23 ) {
                result = this.language === "heb" ? 'ערב טוב' : 'Good vening';
            }
            result = `${result} ${this.customer_fullname},`;
        } else {
            result = this.language === "heb" ? `שלום ${this.customer_fullname !== '' ? this.customer_fullname : ''  },` : `${this.customer_fullname !== '' ? this.customer_fullname + " " : ''  }Welcome Aboard!`;
        }
        return result;       
    }
    
    init_content_assets() {            

        this.stages = [{
            white_board: {//1
                visible: true,
                title: {
                    visible: true,
                    text: this.get_hello_message()
                },
                subtitle: {
                    visible: true,
                    text: this.language === "heb" ? `קיבלת מסמך לחתימה מאת <b>${this.business_name}</b>` : `<b>${this.business_name}</b> sent you a document that requires your signature`
                },
                checkbox: {
                    visible: true,
                    prefix_text: this.language === 'heb' ? "אני מאשר את" : "I accept the terms in the",
                    link_text: this.language === 'heb' ? "תנאי השימוש" : "agreement"
                }
            },
            bottom_bar: {//1
                visible: true,                    
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {
                    visible: true,
                    btn_action: {
                        text: this.language === "heb" ? "הבא" : "Next",
                        icon: this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right",
                        visible: true,
                        validator_text: this.language === "heb" ? "לחץ על  <b>הבא</b> כדי להתחיל" : "Click <b>Next</b> to begin"
                    }
                }
            }
        }, {
            white_board: {//2
                visible: false,
                title: {                    
                    visible: false,
                    text: ""                        
                },
                subtitle: {
                    visible: false,
                    text: ""
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//2
                visible: true,
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: this.isMobile,
                    up_text: this.language === "heb" ? 'הקודם' : 'Previous',
                    down_text: this.language === "heb" ? 'הבא' : 'Next'
                },
                left: {
                    visible: this.is_mobile() === false,
                    btn_action: {
                        text: this.is_mobile() === true ? this.language === "heb" ? "המשך" : "Continue" : this.language === "heb" ? " הבא" : "Next",
                        icon: this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right",
                        visible: true,
                        validator_text: this.language === "heb" ? "עבור על המסמך" : "Review the document"
                    }
                }
            }
        }, {
            white_board: {//3
                visible: true,
                title: {                    
                    visible: true,   
                    text: this.language === "heb" ? `לחץ <b>שלח</b>,` : `Click <b>Send</b>,`
                },
                subtitle: {
                    visible: true,
                    text: this.language === "heb" ? `<span class='title-lower'>כדי להחזיר קובץ חתום אל <b>${this.business_name}</b></span>` : `<span class='title-lower'>In order to return a signed document to <b>${this.business_name}</b></span>`
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//3
                visible: true,
                right: {
                    visible: true,
                    btn_back_to_doc: {
                        text: this.language === "heb" ? "חזרה למסמך" : "back to doc'",
                        icon: this.language === "heb" ? "fa-long-arrow-alt-right" : "fa-long-arrow-alt-left"
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {                        
                    visible: true,
                    btn_action: {
                        text: this.language === "heb" ? "שלח" : "Send",
                        icon: "fa-paper-plane",
                        visible: true,
                        validator_text: ""
                    }
                }
            }
        }, {
            white_board: {//4
                visible: true,
                title: {
                    visible: true,
                    text: this.language === "heb" ? `מעולה,` : `Great,`
                },
                subtitle: {
                    visible: true,
                    text: this.language === "heb" ? `<span class='title-lower'> עותק חתום נשלח בהצלחה אל <b>${this.business_name}</b> <i class='far fa-thumbs-up'></i></span>` : `<span class='title-lower'>A signed copy was succesfully sent to <b>${this.business_name}</b> <i class='far fa-thumbs-up'></i></span>`
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//4
                visible: true,
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {
                    visible: true,
                    btn_action: {
                        text: this.language === "heb" ? "הבא" : "Next",
                        icon: this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right",
                        visible: true,
                        validator_text: this.language === "heb" ? "לחץ על <b>הבא</b> להמשך..." : "Click <b>Next</b> to continue..."
                    }
                }
            }
        }, {
            white_board: {//5
                visible: true,
                title: {
                    visible: true,
                    text: this.language === "heb" ? 
                    "תודה רבה על שיתוף הפעולה, סיימנו <i class='fas fa-check'></i>" : 
                    "Thanks, we're done here <i class='fas fa-check'></i>"
                },
                subtitle: {
                    visible: true,
                    text: this.language === "heb" ? 
                        `${this.additional_message === '' ? '' : "<div class='additional-message'><i class='fas fa-info-circle'></i><p>" + this.additional_message + "</p></div>" }`                            
                        : 
                        `${this.additional_message === '' ? '' : "<div class='additional-message'><i class='fas fa-info-circle'></i><p>" + this.additional_message + "</p></div>" }`
                },
                checkbox: {
                    visible: false
                }                    
            },
            bottom_bar: {
                visible: true,
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {
                    visible: true,
                    btn_action: {
                        text: this.language === "heb" ? "פתח מסמך" : "<span style='font-size: .85em'>View Document</span>",
                        icon: "fa-external-link-alt",
                        visible: true,
                        validator_text: this.language === 'heb' ? "לצפייה במסמך החתום לחץ <b>פתח מסמך</b>" : "Click here to review the document"
                    }
                }
            }
        }, 
        {
            dummy: true
        },
        {
            white_board: {//7 // disconnected stage
                visible: true,                    
                title: {
                    visible: true,
                    text: this.is_mobile() === true ? get_mobile_offline_message.call(this) : get_desktop_offline_message.call(this)
                },
                subtitle: {
                    visible: false,
                    text: ""
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//6
                visible: true,
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {
                    visible: false,
                    btn_action: {
                        text: "",
                        icon: "icon-envelope",
                        visible: false,
                        validator_text: ""
                    }
                }
            }
        }, {
            white_board: {//8 // business-self-sign Start
                visible: true,
                title: {
                    visible: true,
                    text: `<b class="gray">${this.business_name}</b>:<br>${this.language === "heb" ? "מסלול לחתימה עצמית" : "Self signature process"} <i class="fas fa-edit"></i>`
                },
                subtitle: {
                    visible: false,
                    text: ""
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//8
                visible: true,
                right: {
                    visible: false,
                    btn_back_to_doc: {
                        text: "",
                        icon: ""
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {
                    visible: true,
                    btn_action: {     
                        visible: true,                       
                        text: this.language === "heb" ? "הבא" : "Next",
                        icon: this.language === "heb" ? "fa-chevron-circle-left" : "fa-chevron-circle-right",
                        validator_text: this.language === "heb" ? "לחץ על  <b>הבא</b> כדי להתחיל" : "Click <b>Next</b> to begin"
                    }
                }
            }
        }, {
            white_board: {//9 // business-self-sign end
                visible: true,
                title: {
                    visible: true,
                    text: this.language === "heb" ? "המסמך שחתמת עליו מוכן <i class='fas fa-check'></i>" : "She document you've signed on is ready <i class='far fa-smile'></i>" 
                },
                subtitle: {
                    visible: false,
                    text: ""
                },
                checkbox: {
                    visible: false
                }
            },
            bottom_bar: {//8
                visible: true,
                right: {
                    visible: true,
                    btn_back_to_doc: {
                        text: this.language === "heb" ? "חזרה למסמך" : "back to document",
                        icon: this.language === "heb" ? "fa-long-arrow-alt-right" : "fa-long-arrow-alt-left"
                    }
                },
                center: {
                    visible: false,
                    up_text: '',
                    down_text: ''
                },
                left: {
                    visible: true,
                    btn_action: {     
                        visible: true,                       
                        text: this.language === "heb" ? "פתח מסמך" : "<span style='font-size: .85em'>View Document</span>",
                        icon: "fa-external-link-alt",
                        validator_text: ''
                    }
                }
            }
        }];
        
        function get_mobile_offline_message() {
            var result = this.language === "heb" ? "צר לנו, מכשירך אינו מחובר לרשת" : "Sorry, your device isn't connected to a network";
                result += this.language === "heb" ? "<br><span class='title-lower gray'>נסה שנית כאשר תהיה מחובר </span>" : "<br><span class='title-lower gray'>Try again once you get reconnected</span>";
            return result;
        }

        function get_desktop_offline_message() {
            var result = this.language === "heb" ? "המחשב שלך אינו מחובר לרשת" : "Sorry, your computer is not connected to a network";
                result += this.language === "heb" ? "<br><span class='title-lower gray'> אנא בדוק את החיבור שלך לרשת</span>" : "<br><span class='title-lower gray'>Please check your computer's connection</span>";
            return result;
        }

        this.set_additional_texts();            
    }
    
    set_additional_texts () { 

        this.btn_zoom_pan_restore.querySelector(".inner").innerHTML =  this.language === 'heb' ?  
                        "<div class='txt'>חזרה</div><div class='txt'>לגודל</div><div class='txt'>התחלתי</span>" : 
                        "<div class='txt'>Back</div><div class='txt'>to default</div><div class='txt'>view</span>";
        this.advertise_good_sign.querySelector('.advertise-prefix').innerText = this.language === 'heb' ? "מופק באמצעות שירות:" : "Powered by:";       
    }       

    render_current_stage () {
            
        var current_stage = this.current_stage;
        var stages = this.stages;

        this.white_board.style.display = stages[current_stage - 1].white_board.visible === true ? "flex" : "none";
        this.white_board_title.style.display = stages[current_stage - 1].white_board.title.visible === true ? "block" : "none";
                
        this.white_board_title.innerHTML = stages[current_stage - 1].white_board.title.text;
        this.white_board_subtitle.style.display = stages[current_stage - 1].white_board.subtitle.visible === true ? "block" : "none";
        this.white_board_subtitle.innerHTML = stages[current_stage - 1].white_board.subtitle.text;

        if(stages[current_stage - 1].white_board.checkbox.visible === true) {
            this.approve_agreement.style.display = "flex";
            this.approve_agreement.querySelector("#txt-i-accept").innerText = stages[current_stage-1].white_board.checkbox.prefix_text;
            this.button_open_terms.innerText = stages[current_stage-1].white_board.checkbox.link_text;
        } else {
            this.approve_agreement.style.display = "none";
        }
        
        this.bottom_bar.style.display = stages[current_stage - 1].bottom_bar.visible === true ? "block" : "none";//TODO
                    
        this.bottom_bar_left.style.display = stages[current_stage - 1].bottom_bar.left.visible === true ? "flex" : "none";
        this.left_btn_action.style.display = stages[current_stage - 1].bottom_bar.left.btn_action.visible === true ? "flex" : "none";
        this.left_btn_action_txt.innerHTML = stages[current_stage - 1].bottom_bar.left.btn_action.text;
        
        this.left_btn_action.querySelector(".fas").classList.remove("fa-chevron-circle-left", "fa-chevron-circle-right", "fa-paper-plane", "fa-external-link-alt");
        this.left_btn_action.querySelector(".fas").classList.add(stages[current_stage - 1].bottom_bar.left.btn_action.icon);
                
        this.bottom_bar_center.style.display = stages[current_stage - 1].bottom_bar.center.visible === true ?  "flex" : "none";
        this.center_btn_up.querySelector('.txt').innerText = stages[current_stage - 1].bottom_bar.center.up_text;
        this.center_btn_down.querySelector('.txt').innerText = stages[current_stage - 1].bottom_bar.center.down_text;
        
        this.bottom_bar_right.style.display = stages[current_stage - 1].bottom_bar.right.visible === true ? 'block' : 'none';
        this.back_to_doc.querySelector(".txt").innerText = stages[current_stage - 1].bottom_bar.right.btn_back_to_doc.text;
        if(stages[current_stage - 1].bottom_bar.right.btn_back_to_doc.icon !== "" ) {
            this.back_to_doc.querySelector(".fas").classList.add(stages[current_stage - 1].bottom_bar.right.btn_back_to_doc.icon);              
        }
        
        this.signatures_validator.innerHTML = stages[current_stage - 1].bottom_bar.left.btn_action.validator_text;            
    }
   
    is_text_placeholer_required (placeholder) {
        var txt = placeholder.dataset.txName; 
        return txt.includes("~~~");
    }

    is_text_placeholer_bold (placeholder) {
        var txt = placeholder.dataset.txName; 
        return txt.includes("^^^");
    }  

    increase_height_to_last_page() {
        var wrappers_coll = document.querySelectorAll('.image-wrapper');
        var bigger_height = this.positioning_tool.get_current_height() * 4/3 * 1.3;// * 4/3: convert from points to px, *1.3: increase height by a factor of 1.3
        wrappers_coll[wrappers_coll.length-1].style.height = `${bigger_height}px`;
        wrappers_coll[wrappers_coll.length-1].dataset.initialHeight = bigger_height;
    }

    restore_border_to_dirty_text_fields () {
        let node_list = document.querySelectorAll(".text-field-placeholder[data-is-dirty='true']");
        node_list.forEach( item => {
            item.classList.remove('screenshot-adjustment');
        });
    }

    handle_review_doc_click () { 

        this.progress_bar.hide();
        this.enable_body_scrolling();
        this.restore_border_to_dirty_text_fields();
        this.show_all_empty_text_fields();
        this.handle_go_to_stage_two({review_doc: true});
    }

    show_all_empty_text_fields() {
        let node_list = document.querySelectorAll("[data-is-dirty='false']");
        
        node_list.forEach( item => {
            item.style.display = "block";
        });
    }
    
    remove_style_shel_omer3 () {
        var styleEle = document.querySelector("#shel-omer2");
        if(styleEle !== null) {
            styleEle.parentNode.removeChild(styleEle);
        }  
    } 

    restore_height_to_last_page () {        
        var wrappers_coll = document.querySelectorAll('.image-wrapper');
        wrappers_coll[wrappers_coll.length-1].style.height = `${this.positioning_tool.get_current_height() * 4/3}px`;
    }

    hide_all_empty_text_fields() {
        let node_list = document.querySelectorAll("[data-is-dirty='false']");
        
        node_list.forEach( item => {
            item.style.display = "none";
        });
    }

    hide_border_from_dirty_text_fields () {
        let node_list = document.querySelectorAll(".text-field-placeholder[data-is-dirty='true']");
        node_list.forEach( item => {
            item.classList.add('screenshot-adjustment');
        });
    }

    send_pdf_to_server(email_address, email_destination_type) { //email_type: "to_business" | "to_user"
        var  url, 
        save_as_name = decodeURIComponent(this.doc_name), 
        formData = new FormData();

        formData.append("pdfFile", this.pdf_document.output('blob'), save_as_name);
        formData.append('email_address', email_address);            
        formData.append('email_destination_type', email_destination_type);
        formData.append('customer_fullname', this.customer_fullname);
        formData.append('user_language', this.language);
        formData.append('business_name', this.business_name);
        formData.append('token', this.server_token);
        formData.append('journey_id', this.journey_id);
        formData.append('ios_safari_pinch_pan', JSON.stringify(this.get_ios_safari_pinch_pan()));

        switch(this.ENV) {
            case "dev": 
                url = this.URL_DEV;
                break;
            case "pre-prod": 
                url = this.URL_PRE_PRODUCTION;
                break;
            case "prod": 
                url = this.URL_PRODUCTION;
                break;
            default: 
        }

        return jQuery.ajax({
            url: url, 
            method: 'POST',
            data: formData,
            timeout: 120000,
            processData: false,
            contentType: false,
            xhrFields: {
                withCredentials: false
            }
        });
    }
    
    get_ios_safari_pinch_pan() {
        let result = { is_ios_safari_zoom : this.is_ios() && this.is_safari() && this.DID_PINCH_ZOOM_PAN };
        if (result.is_ios_safari_zoom === true) {
            const browser = detect();
            result.safari_ver = browser.version;
            result.os = browser.os;
            result.ios_ver = this.get_ios_version();
        }
        return result;
    }

    get_ios_version() {
        
        var agent = window.navigator.userAgent,
        start = agent.indexOf( "OS " );
        if( ( agent.indexOf( "iPhone" ) > -1 || agent.indexOf( "iPad" ) > -1 ) && start > -1 ){
            return window.Number( agent.substr( start + 3, 3 ).replace( "_", "." ) );
        }
        return 0;
    }
}

window.addEventListener('DOMContentLoaded', () => {
    var app = new App();
    if( app.IS_ONLINE === true) {
        app.initialize();
    }
});

module.exports = App;

