import GoodSignBase from './GoodSignBase';

class TextRowModal extends GoodSignBase {
    constructor (parent_element, app) {
        super();        
        this.container = parent_element; 
        this.language = this.get_language();
        this.current_placeholder = null;
        this.parent = app;
        this.is_mobile() && this.container.classList.add("mobile-images");
        this.container.classList.add(this.language);

        this.btn_close = this.container.querySelector(".close-modal");
        this.lbl    = this.container.querySelector(".lbl");
        this.input  = this.container.querySelector(".txt-input");
        this.btn_ok = this.container.querySelector("#btn-ok-text-row");
        this.required_txt = this.container.querySelector(".required-txt");

        this.btn_ok.innerText = this.language === "heb" ? "אישור" : "OK";
        this.required_txt.innerText = this.language === "heb" ? "שדה חובה" : "Required field";
        this.bind_events();
    }

    bind_events () {
        
        this.btn_close.addEventListener("click", () => {
            this.value = '';
            this.close();
        }, false);

        this.btn_ok.addEventListener("click", () => {

            var txt = this.input.value.trim();
            if (txt !== '') {
                this.handle_dirty_text_insertion(this.current_placeholder, txt);
                this.input.value = ''; 
            } else {
                this.current_placeholder.innerHTML = '';
                this.current_placeholder.parentNode.dataset.isDirty = 'false';
                this.current_placeholder.parentNode.dataset.dt = '';
                if(this.current_placeholder.parentNode.dataset.isRequired === "true") {
                    this.append_asterisk_to_placeholder(this.current_placeholder.parentNode);
                }
            }

            this.parent.handle_insert_textrow_complete(txt, this.current_placeholder.parentNode);

            this.close();
        }, false);
    }

    remove_asterisk_from_placeholder (placeholder){
        let fa_ele = placeholder.querySelector('.fa-asterisk');
            if(fa_ele !== null) {
                placeholder.removeChild(fa_ele);
            }
        
    }
    
    append_asterisk_to_placeholder (placeholder){        
        placeholder.setAttribute('title', this.language === 'heb' ? 'שדה חובה' : 'Required field');
        let fa_ele = document.createElement('i');
        fa_ele.classList.add('fas', 'fa-asterisk');            
        placeholder.appendChild(fa_ele);
    }

    handle_dirty_text_insertion (placeholder, txt) {
        placeholder.innerHTML = txt.replace(/\s/g, '&nbsp;');
        placeholder.parentNode.style.width = "auto";
        placeholder.parentNode.dataset.isDirty = "true";
        placeholder.parentNode.dataset.dt = new Date().valueOf();
        if(placeholder.parentNode.dataset.isRequired === "true") {
            this.remove_asterisk_from_placeholder(placeholder.parentNode);
        }
    }

    open (placeholder) { 
        this.current_placeholder = placeholder;
        this.disable_body_scrolling();
        this.show_overlay();
        this.lbl.innerHTML = `${this.language === 'heb' ? 'הזן' : 'Enter'} ${this.current_placeholder.parentNode.dataset.labelHelper}:`;
        this.container.style.display = "block";
        this.required_txt.parentNode.style.visibility = this.current_placeholder.parentNode.dataset.isRequired === "true" ? "visible" : "hidden";
        this.input.value = this.current_placeholder.innerText;    
        this.is_mobile() === false && this.input.focus();      
     }

    close () {
        this.enable_body_scrolling();
        this.required_txt.parentNode.style.visibility = "hidden";
        this.hide_overlay();
        this.container.style.display = "none";
    }

    is_open () {
        return this.container.style.display === "block";
    }

    trigger_insert_text () {
        this.btn_ok.click();
    }
}

module.exports = TextRowModal;