import Overlay from './Overlay';
import Mobile from './Mobile';
import Language from "./Language";

class GoodSignBase {
    
    disable_body_scrolling () {
        document.body.classList.add('overflow-y-hidden');
        document.body.classList.add('overflow-x-hidden');
    }

    enable_body_scrolling () {
        document.body.classList.remove('overflow-y-hidden');
        document.body.classList.remove('overflow-x-hidden');
    }

    show_overlay () {
        Overlay.instance.show();
    }
    
    hide_overlay () {
        Overlay.instance.hide();
    }

    darker_overlay () {
        Overlay.instance.make_darker();
    }

    lighter_overlay () {
        Overlay.instance.remove_darker();
    }
    
    is_mobile () {
        return Mobile.instance.is_mobile();
    }

    get_language () {
        return Language.instance.get_language();
    } 

    is_safari () {
        return !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);        
    }

    is_ios () {
        return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;  
    }
}

module.exports = GoodSignBase;