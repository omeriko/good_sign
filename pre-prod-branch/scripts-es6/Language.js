
class Language {
    constructor () {
        if(!Language.instance) {
            Language.instance = this;
            var hidden_doc_lang = document.querySelector("#doc_lang");
            
            this.language = 'eng';
            if (hidden_doc_lang !== null) {
                this.language = Number(hidden_doc_lang.value) === 0 ? 'eng' : 'heb';
            }
            
        }
        return Language.instance;
    }
    
    get_language () {
        return this.language;
    } 
}

const instance = new Language();
Object.freeze(instance);

module.exports.instance = instance;