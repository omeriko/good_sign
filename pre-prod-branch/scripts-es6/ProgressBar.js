import GoodSignBase from './GoodSignBase';

class ProgressBar extends GoodSignBase {
    constructor () {
        super();
        this.wrapper = document.querySelector("#progress-wrapper");
        this.bar = document.querySelector(".progress-bar-inner");
        this.bar_msg_top = document.querySelector(".progress-text-top");
        this.bar_inner_text = this.wrapper.querySelector(".inner-text"); 
        
        this.language = this.get_language();
        
        this.is_mobile() && this.wrapper.classList.add("mobile-images");
        this.wrapper.classList.add(this.language);
    }

    set_value(val) {
        this.bar.style.width = val + '%';
        this.bar_inner_text.innerText = val + '%';
        if (val > 50) {
            this.bar_inner_text.classList.add("above-50-per");
        } 

        if(val === 100) {
            this.bar_msg_top.querySelector(".fa-check").classList.remove('none');
            this.bar_msg_top.querySelector(".three-dots-container").classList.add("none");
            this.bar_msg_top.querySelector('strong').innerText = this.language === "heb" ? "הקובץ מוכן" : "Document is ready";
        }

    }

    show () {

        this.wrapper.classList.remove("none");

        this.bar_msg_top.querySelector('strong').innerText = this.language === "heb" ? "מכין קובץ" : "Preparing your document";
        this.bar_inner_text.classList.remove("above-50-per");
        this.bar_inner_text.classList.add("below-50-per");
    }

    hide () {
        this.wrapper.classList.add("none");
        this.bar_msg_top.querySelector(".fa-check").classList.add('none');
        this.bar_msg_top.querySelector(".three-dots-container").classList.remove("none");
    }
}

module.exports = ProgressBar;
