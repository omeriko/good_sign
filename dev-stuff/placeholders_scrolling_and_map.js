class DocumentPlaceholders{
    constructor () {
        this.placehoders_map = new Map();
        this.mobile_current_placeholder_pointer = 0;
        this.create_placeholders_map();
    }

    bind_events() {
        window.addEventListener("scroll", e => {             

            let scroll_top = window.pageYOffset || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"
            
            this.last_scroll_top = scroll_top <= 0 ? 0 : scroll_top; // For Mobile or negative scrolling

            // Clear our timeout throughout the scroll
            window.clearTimeout( this.is_scrolling_helper );
            // Set a timeout to run after scrolling ends
            this.is_scrolling_helper = setTimeout(() => {
                if(this.scroll_reason_helper !== "next_prev_click") {
                    this.handle_scroll_end(scroll_top);
                } else {
                    this.scroll_reason_helper = "finger_scroll";
                }
            }, 250);

        }, false);
       
    }

    create_placeholders_map() {
        let node_list = document.querySelectorAll(".sign-here-placeholder, .textual, .checkbox-placeholder");
        let arr = Array.from(node_list); 
        arr.sort(sort_by_top_position); 
        
        let pivot_page_num = 0;
        for(let i = 0; i < arr.length; i++ ) {
            pivot_page_num = this.positioning_tool.calculate_page_number(Number(arr[i].dataset.globalTopForSorting));
            this.placehoders_map.set(i, { ele: arr[i], page_num: pivot_page_num })
        }        
        
        function sort_by_top_position (a, b) {
            var a_top =  Number(a.dataset.globalTopForSorting);
            var b_top =  Number(b.dataset.globalTopForSorting);
            return a_top - b_top;                    
        }                
    }

    scroll_to_placeholder () {
        const ele = this.placehoders_map.get(this.mobile_current_placeholder_pointer).ele;
        const top = jQuery(ele).offset().top; //ele.getBoundingClientRect().top;                
        window.scroll({ top: top - 100, left: 0, behavior: 'smooth' });                
        if(this.tooltip_util.has_tooltip(ele) === false) {                
            this.tooltip_util.attach_tooltip_to_element(ele);
        }
    }

    remove_tooltip_from_current_placeholder() {
        let ele = this.placehoders_map.get(this.mobile_current_placeholder_pointer).ele;
        if(this.tooltip_util.has_tooltip(ele) === true) {
            this.tooltip_util.remove_tooltip(ele);
        }
    }

    is_in_viewport(ele) {
        const rect = ele.getBoundingClientRect();
        return (
            rect.top >= 0 &&           
            rect.bottom <= (document.documentElement.clientHeight - 194)
        );
    }

    handle_scroll_end(scroll_top) {
                
        for(let i = 0; i < this.placehoders_map.size; i++) {
            let value = this.placehoders_map.get(i);
            
            if(value.ele.dataset.isDirty === "false" && this.is_in_viewport(value.ele)===true) {
                this.handle_found_placeholder_in_viewport(value.ele, i);
                break;
            } 
        }
    }

    handle_found_placeholder_in_viewport(ele, ele_index) {
        this.remove_tooltip_from_current_placeholder();
        this.tooltip_util.attach_tooltip_to_element(ele);
        this.mobile_current_placeholder_pointer = ele_index;
    }

    jump_to_placeholder(direction) {
        this.remove_tooltip_from_current_placeholder();
        let next_ele =this.jump_to_next_ele(direction);        
        
        const top = jQuery(next_ele).offset().top; //ele.getBoundingClientRect().top;
        this.scroll_reason_helper = "next_prev_click";
        window.scroll({ top: top - 300, left: 0, behavior: 'smooth' });
        this.tooltip_util.attach_tooltip_to_element(next_ele);
    }

    jump_to_next_ele(direction) {
        let ele;        

        do {             
            switch(direction) {
                case 'up':
                    if(this.mobile_current_placeholder_pointer > 0 ) {
                        this.mobile_current_placeholder_pointer -= 1;
                    } else {
                        this.mobile_current_placeholder_pointer = this.placehoders_map.size -1;
                    }
                    break;
                case 'down': 
                    if(this.mobile_current_placeholder_pointer + 1 < this.placehoders_map.size) {
                        this.mobile_current_placeholder_pointer += 1;                           
                    } else {
                        this.mobile_current_placeholder_pointer = 0;
                    }                   
                    break;
            } 
            
            ele = this.placehoders_map.get(this.mobile_current_placeholder_pointer).ele;
        } while(ele.dataset.isDirty === 'true');
        
        return ele;
    }

    remove_all_tooltips () {
        for(let i = 0; i < this.placehoders_map.size; i++) {
            let value = this.placehoders_map.get(i);
            
            if(this.tooltip_util.has_tooltip(value.ele)) {
                this.tooltip_util.remove_tooltip(value.ele);
            } 
        }
    }
}