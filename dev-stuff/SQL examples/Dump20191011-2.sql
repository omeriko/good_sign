-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: neighborhood
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'neighborhood'
--
/*!50003 DROP FUNCTION IF EXISTS `fo_validate_proximity_alert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fo_validate_proximity_alert`(
p_from_owner_id INT, 
p_area_id INT,
p_type_id tinyint

) RETURNS tinyint(4)
BEGIN
	DECLARE var_result_code TINYINT;
    DECLARE var_count INT;
    DECLARE var_prev TINYINT;    
    
    select count(*) INTO var_count
    from tbl_notifications
    where related_owner_id = p_from_owner_id 
          AND area_id = p_area_id 
          AND type_id = p_type_id 
          AND date_time > SUBTIME(NOW(), '00:30:00.000000');
    
    IF var_count > 0 THEN
    
		SET var_result_code = 1;
        
	ELSEIF var_count = 0 THEN
		
        SET var_result_code = 0;
        
        IF p_type_id = 4 THEN
			
            SELECT type_id INTO var_prev
			FROM  tbl_notifications  
			WHERE related_owner_id = p_from_owner_id 
			      AND  area_id = p_area_id 
			      AND DATE(date_time) = DATE(NOW())
			ORDER BY date_time DESC
			LIMIT 0,1;
            
            IF var_prev IS NULL THEN
				SET var_result_code = 3;
			ELSEIF var_prev = 4 THEN
				SET var_result_code = 4;		
			END IF; /*end of: p_type_id = 4 */
            
        END IF;/* end of: var_count = 0*/
        
    END IF;/*end of: var_count > 0*/
    
	RETURN var_result_code;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `BOusersLogin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `BOusersLogin`(
	IN pUsername varchar(20),
	IN pPwd varchar(10)
)
BEGIN
	select * from tbl_bousers where username= pUsername && password=pPwd;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_delete_all_mockup_users` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_delete_all_mockup_users`()
BEGIN
	SET SQL_SAFE_UPDATES=0;
    /*images*/
	DELETE im.* FROM tbl_images im
    inner join tbl_canines ca on im.canine_id = ca.id
	WHERE ca.is_mockup = 1;
	
    /*areas*/
	DELETE oa.* FROM tbl_owners_areas oa
    inner join tbl_canine_owners co on co.id = oa.owner_id
    inner join tbl_canines ca on ca.id = co.canine_id
    where ca.is_mockup = 1;
    
    /*friendships*/
    DELETE fr.* FROM tbl_friendships fr
    inner join tbl_canines ca on ca.id = fr.canine_idA
    where ca.is_mockup = 1;
    
    DELETE fr.* FROM tbl_friendships fr
    inner join tbl_canines ca on ca.id = fr.canine_idB
    where ca.is_mockup = 1;
    
    /*friendships_log*/        
    DELETE frl.* FROM tbl_friendship_log frl
    inner join tbl_canines ca on ca.id = frl.from_canine_id
    where ca.is_mockup = 1;    
    
    DELETE frl.* FROM tbl_friendship_log frl
    inner join tbl_canines ca on ca.id = frl.to_canine_id
    where ca.is_mockup = 1;
    
    
    /*notifications*/        
    DELETE nt.* FROM tbl_notifications nt
    inner join tbl_canine_owners co on co.id = nt.to_owner_id
    inner join tbl_canines ca on ca.id = co.canine_id
    where ca.is_mockup = 1;    
    
    DELETE nt.* FROM tbl_notifications nt
    inner join tbl_canine_owners co on co.id = nt.related_owner_id
    inner join tbl_canines ca on ca.id = co.canine_id
    where ca.is_mockup = 1;
    
    /*friendship_offers*/        
    DELETE fro.* FROM tbl_friendship_offers fro
    inner join tbl_canines ca on ca.id = fro.from_canine_id
    where ca.is_mockup = 1;    
    
    DELETE fro.* FROM tbl_friendship_offers fro
    inner join tbl_canines ca on ca.id = fro.to_canine_id
    where ca.is_mockup = 1;
    
    /*tbl_inspector_reports*/
     DELETE ins.* FROM tbl_inspector_reports ins
    inner join tbl_canine_owners co on co.id = ins.reporting_owner_id
    inner join tbl_canines ca on ca.id = co.canine_id
    where ca.is_mockup = 1;    
    
    /*tbl_users_operations*/
    DELETE uo.* FROM tbl_users_operations uo
    inner join tbl_canines ca on ca.id = uo.canine_id
    where ca.is_mockup = 1;
    
    /*tbl_canine_owners*/
    DELETE co.* FROM tbl_canine_owners co
    inner join tbl_canines ca on ca.id = co.canine_id
    where ca.is_mockup = 1;
	
    /*tbl_canines*/
    DELETE ca.* FROM tbl_canines ca    
    where ca.is_mockup = 1;
    
    SET SQL_SAFE_UPDATES=1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_delete_breed` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_delete_breed`(IN pid INT)
BEGIN
	Delete from tbl_breeds where id = pid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_delete_canine` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_delete_canine`(
	IN p_id int
)
BEGIN
	DECLARE VAR_owner_id INT;
    
    SET SQL_SAFE_UPDATES=0;    
    
	SELECT id INTO VAR_owner_id FROM tbl_canine_owners co WHERE co.canine_id = p_id;
    
    delete from `neighborhood`.`tbl_images`
	where canine_id = p_id;
    
    delete from `neighborhood`.`tbl_friendship_log`
    where from_canine_id = p_id OR to_canine_id = p_id;
	
    DELETE FROM `neighborhood`.`tbl_friendship_offers`
	where from_canine_id = p_id OR to_canine_id = p_id;

    DELETE FROM `neighborhood`.`tbl_friendships`
	WHERE canine_idA = p_id OR canine_idB = p_id;
	
	DELETE  ntf.*
	FROM `neighborhood`.`tbl_notifications` ntf 
	where to_owner_id = VAR_owner_id;    

	DELETE  ntf.*
	FROM `neighborhood`.`tbl_notifications` ntf 
	where related_owner_id = VAR_owner_id;
    
    DELETE FROM `neighborhood`.`tbl_owners_areas`
    WHERE owner_id = VAR_owner_id;
   
    delete from `neighborhood`.`tbl_inspector_reports`
	where reporting_owner_id = VAR_owner_id;
    
    delete from `neighborhood`.`tbl_canine_owners`
	where canine_id = p_id;
	
    delete from `neighborhood`.`tbl_users_operations`
	where canine_id = p_id;
    
	SET SQL_SAFE_UPDATES=1;
	
	delete from `neighborhood`.`tbl_canines`
	where id = p_id;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_all_breeds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_all_breeds`()
BEGIN
	select id, en_name, he_name_male, he_name_female, enabled
	from tbl_breeds;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_areas_by_owner_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_areas_by_owner_id`(
	IN p_owner_id INT
)
BEGIN
	SELECT id, `name`, center_lat, center_lng, radius,
		   from_time1 as from_time, to_time1 as to_time
    FROM tbl_owners_areas
	WHERE owner_id = p_owner_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_breed` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_breed`(
	IN p_id INT
)
BEGIN
	select * from tbl_breeds where id = p_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_canines_by_params` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_canines_by_params`(
	IN p_from datetime,
	IN p_to datetime,
	IN p_is_mock_up BIT
)
BEGIN
	IF p_is_mock_up = 0 THEN
		SELECT ca.id, ca.date_created, ca.`name`, ca.gender_id, ca.breed_id, 
			   ca.is_mockup, ca.initial_lat, ca.initial_lng, ca.language_id,
			   ca.color1, ca.color2,  ca.birth_year, ca.birth_month, ca.birth_day, ca.is_castrated 
		FROM tbl_canines ca    	
		WHERE ca.is_mockup = 0 and ca.date_created >= p_from AND ca.date_created < p_to 
		ORDER BY ca.date_created DESC;
	ELSE 
		SELECT ca.id, ca.date_created, ca.`name`, ca.gender_id, ca.breed_id, 
			   ca.is_mockup, ca.initial_lat, ca.initial_lng, ca.language_id,
			   ca.color1, ca.color2,  ca.birth_year, ca.birth_month, ca.birth_day, ca.is_castrated 
		FROM tbl_canines ca    	
		WHERE ca.date_created >= p_from AND ca.date_created < p_to 
		ORDER BY ca.date_created DESC;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_daily_proximity_alerts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_daily_proximity_alerts`(
	IN p_dt DATETIME
)
BEGIN
	TRUNCATE tbl_proximity_log_validated;
    
	SELECT * 
    FROM tbl_proximity_log
    WHERE DATE(p_dt) = DATE(date_time);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_images_by_owner_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_images_by_owner_id`(
 IN p_owner_id INT
)
BEGIN
	select id, image_path, owner_id, date_time  
	from tbl_images
	where owner_id = p_owner_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_member_by_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_member_by_id`(
IN p_canine_id INT
)
BEGIN
	SELECT id, date_created, `name`, gender_id, breed_id, initial_lat, initial_lng, language_id,
           color1, color2,  birth_year, birth_month, birth_day, is_castrated
	FROM tbl_canines 	
	where id = p_canine_id;

	SELECT id, first_name, email, registration_status, 
		   is_native, platform, platform_ver, phonegap_ver, version_name, android_code_version
	FROM tbl_canine_owners
	WHERE  canine_id = p_canine_id;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_member_operations_by_params` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_member_operations_by_params`(
IN p_owner_id INT,
IN p_from datetime,
IN p_to datetime

)
BEGIN
	SELECT  date_time, operation_type_id
	FROM `neighborhood`.`tbl_users_operations`
	WHERE owner_id = p_owner_id and date_time >= p_from AND date_time < p_to 
	order by date_time asc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_notification_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_notification_details`(
IN p_to_owner_id INT
)
BEGIN	
	
	SELECT ow.platform, ow.push_service_reg_id as `reg_id` 
	FROM tbl_canine_owners ow
	WHERE ow.id = p_to_owner_id;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_owner_notifications_by_params` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_owner_notifications_by_params`(
IN p_owner_id INT,
IN p_type_id TINYINT(3),
IN p_from datetime,
IN p_to datetime,
IN p_page_size TINYINT(3),
IN p_skip_records INT
)
BEGIN

SET @p_owner_id     = p_owner_id;
SET @p_type_id      = p_type_id;
SET @p_from         = p_from;
SET @p_to           = p_to;
SET @p_page_size    = p_page_size;
SET @p_skip_records = p_skip_records;

PREPARE STMT FROM 'SELECT SQL_CALC_FOUND_ROWS tbn.id, tbn.type_id, tbn.related_owner_id as `from_owner`, tbn.to_owner_id as `to_owner`,  
           tbn.is_pending, tbn.date_time, toa.`name` as \'area_name\', toa.from_time1 as from_time, toa.to_time1 as to_time, 
           toa.center_lat as lat, toa.center_lng as lng, toa.radius,
	CASE ? 
		WHEN tbn.related_owner_id THEN \'outgoing\' 
		WHEN tbn.to_owner_id THEN \'incoming\' 
	END \'direction\'
	FROM tbl_notifications tbn
    LEFT JOIN tbl_owners_areas toa ON toa.id = tbn.area_id
	WHERE (tbn.related_owner_id = ? OR tbn.to_owner_id = ?) AND 
          (? = 0 OR tbn.type_id = ?) AND 
          (tbn.date_time >= ? AND tbn.date_time < ?)
    ORDER BY tbn.date_time DESC	
    LIMIT ?
	OFFSET ?';    
    
 EXECUTE STMT USING @p_owner_id, @p_owner_id, @p_owner_id, @p_type_id, @p_type_id, @p_from, @p_to, @p_page_size, @p_skip_records;   

SELECT FOUND_ROWS() as `total`;	
		
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_owner_relationship_log_report` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_owner_relationship_log_report`(
IN p_first_owner_id INT,
IN p_other_owner_id INT
)
BEGIN
	SELECT tlg.id, tlg.friendship_state_id as `phase_id`, 
		   tlg.from_owner_id as `from_owner`, tlg.to_owner_id as `to_owner`, tlg.`datetime` as `date_time`
	FROM  tbl_friendship_log tlg
	WHERE (tlg.from_owner_id = p_first_owner_id and tlg.to_owner_id = p_other_owner_id) OR
		  (tlg.from_owner_id = p_other_owner_id and tlg.to_owner_id = p_first_owner_id)
	ORDER BY tlg.`datetime` DESC;	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_get_poc_transitions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_get_poc_transitions`(
IN p_from datetime,
IN p_to datetime
)
BEGIN

SELECT id, region_name, radius, entering_or_exiting, api_type, dt, 
		   center_lat, center_lng, actual_lat, actual_lng, actual_accuracy 
	FROM neighborhood.tbl_poc_geofence_transition
	where (dt BETWEEN p_from AND p_to);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_insert_breed` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_insert_breed`(
	IN pEn_name varchar(100),
	IN pHe_name_male varchar(100),
	IN pHe_name_female varchar(100),
	IN pEnabled bit)
BEGIN
	INSERT INTO tbl_breeds (en_name, he_name_male, he_name_female, enabled)
	 VALUES (pEn_name, pHe_name_male, pHe_name_female, pEnabled);

	SELECT LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_insert_proximity_log` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_insert_proximity_log`(

IN p_type_id TINYINT,
IN p_from_owner_id INT,
IN p_to_owner_id INT,
IN p_area_id INT,
IN p_date_time DATETIME,
IN p_conclusion VARCHAR(45)
)
BEGIN
	INSERT INTO tbl_proximity_log
    ( type_id, from_owner_id, to_owner_id, area_id, date_time, conclusion)
	VALUES (p_type_id, p_from_owner_id, p_to_owner_id, p_area_id, p_date_time, p_conclusion);



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_insert_validated_daily_proximity_alert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_insert_validated_daily_proximity_alert`(
IN p_id INT,
IN p_type_id INT,
IN p_from_owner_id INT,
IN p_to_owner_id INT,
IN p_area_id INT,
IN p_date_time DATETIME,
IN p_conclusion VARCHAR(45)
)
BEGIN
	
    DECLARE var_prev TINYINT;    
    DECLARE var_new_conclusion VARCHAR(45);
    
    SET var_new_conclusion = p_conclusion;
    
    IF p_type_id = 4 THEN				
		
         SELECT type_id INTO var_prev
		 FROM  tbl_proximity_log_validated  
		 WHERE from_owner_id = p_from_owner_id 
			   AND  area_id = p_area_id 
			   AND DATE(date_time) = DATE(p_date_time) 
			   AND old_conclusion = 'OK'
		 ORDER BY date_time DESC
		 LIMIT 0,1;
        
        IF var_prev IS NULL THEN
			SET var_new_conclusion = 'EXIT_AFTER_NONE';
		ELSEIF var_prev = 4 THEN
			SET var_new_conclusion = 'EXIT_AFTER_EXIT';		
        END IF;
	END IF;
    
    INSERT INTO `neighborhood`.`tbl_proximity_log_validated`
    (`id`,`type_id`,`from_owner_id`,`to_owner_id`,`area_id`,`date_time`,`old_conclusion`,`new_conclusion`)
    VALUES
    (p_id, p_type_id, p_from_owner_id, p_to_owner_id, p_area_id, p_date_time, p_conclusion ,var_new_conclusion);

    
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_owner_relationships_report` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_owner_relationships_report`(
IN p_owner_id INT
)
BEGIN
	/*pending requests sent*/
	SELECT tfo.id, tfo.to_owner_id as `owner_id`, tco.first_name, tca.`name`, tfo.`datetime` 
	FROM tbl_friendship_offers tfo
	INNER JOIN tbl_canine_owners tco on tfo.to_owner_id = tco.id
	INNER JOIN tbl_canines tca on tco.canine_id = tca.id
	WHERE tfo.from_owner_id = p_owner_id 
	ORDER BY tfo.`datetime` DESC;

	/*pending requests recieved*/
	SELECT tfo.id, tfo.from_owner_id as `owner_id`, tco.first_name, tca.`name`, tfo.`datetime` 
	FROM tbl_friendship_offers tfo
	INNER JOIN tbl_canine_owners tco on tfo.from_owner_id = tco.id
	INNER JOIN tbl_canines tca on tco.canine_id = tca.id
	WHERE tfo.to_owner_id = p_owner_id
	ORDER BY tfo.`datetime` DESC;

	/*friends*/
	SELECT * 
	FROM (SELECT /*  get all owner friends where the p_owner_id is A */
				tbf.id, tbf.owner_idB as `owner_id`, tco.first_name, tbc.`name`,'A' as `type`, tbf.date_time
		  FROM tbl_friendships tbf
		  INNER JOIN tbl_canine_owners tco ON tbf.owner_idB = tco.id
		  INNER JOIN tbl_canines tbc ON tbf.canine_idB = tbc.id
		  WHERE tbf.owner_idA = p_owner_id and tbf.is_friend = 1) a
	UNION ALL		  
	SELECT * 
	FROM (SELECT /*  get all owner friends where the p_owner_id is B */
				tbf.id, tbf.owner_idA as `owner_id`, tco.first_name, tbc.`name`,'B' as `type`, tbf.date_time
		  FROM tbl_friendships tbf
		  INNER JOIN tbl_canine_owners tco ON tbf.owner_idA = tco.id
		  INNER JOIN tbl_canines tbc ON tbf.canine_idA = tbc.id
		  WHERE tbf.owner_idB = p_owner_id and tbf.is_friend = 1) b 
    ORDER BY date_time DESC;

	
	/*enemies*/
	SELECT * 
	FROM (SELECT /*  get all owner enemies where the p_owner_id is A */
				tbf.id, tbf.owner_idB as `owner_id`, tco.first_name, tbc.`name`,'A' as `type`, tbf.date_time
		  FROM tbl_friendships tbf
		  INNER JOIN tbl_canine_owners tco ON tbf.owner_idB = tco.id
		  INNER JOIN tbl_canines tbc ON tbf.canine_idB = tbc.id
		  WHERE tbf.owner_idA = p_owner_id and tbf.is_friend = 0) a
	UNION ALL		  
	SELECT * 
	FROM (SELECT /*  get all owner enemies where the p_owner_id is B */
				tbf.id, tbf.owner_idA as `owner_id`, tco.first_name, tbc.`name`,'B' as `type`, tbf.date_time
		  FROM tbl_friendships tbf
		  INNER JOIN tbl_canine_owners tco ON tbf.owner_idA = tco.id
		  INNER JOIN tbl_canines tbc ON tbf.canine_idA = tbc.id
		  WHERE tbf.owner_idB = p_owner_id and tbf.is_friend = 0) b
	ORDER BY date_time DESC;	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_set_bouser_session` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_set_bouser_session`(
IN pUserId INT, 
IN pHash varchar(45)
)
BEGIN
	UPDATE tbl_bousers
    SET `hash` = pHash,
		expires = DATE_ADD(NOW(), INTERVAL 30 MINUTE)
    WHERE id = pUserId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_update_app_settings` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_update_app_settings`(
IN p_generate_mock_ngbrs BIT,                      
IN p_images_storage_limit INT,
IN p_viewport_mockup_seed DECIMAL(1,1),
IN p_min_area_radius INT,
IN p_max_area_radius INT,         
IN p_default_area_radius INT,            
IN p_radius_step INT,            
IN p_areas_count_limit INT,            
IN p_uploaded_image_max_size INT,            
IN p_inspector_alert_areas_search_radius INT,
IN p_hide_regions_feature BIT,
IN p_force_area_radius BIT,
IN p_forced_area_radius INT,
IN p_new_menu BIT
)
BEGIN

UPDATE `neighborhood`.`tbl_app_settings`
SET
`generate_mock_ngbrs` = p_generate_mock_ngbrs,
`hide_regions_feature` = p_hide_regions_feature,
`images_storage_limit` = p_images_storage_limit,
`uploaded_image_max_size` = p_uploaded_image_max_size,
`viewport_mockup_key` = p_viewport_mockup_seed,
`min_area_radius` = p_min_area_radius,
`max_area_radius` = p_max_area_radius,
`default_area_radius` = p_default_area_radius,
`radius_step` = p_radius_step,
`inspector_alert_areas_search_radius` = p_inspector_alert_areas_search_radius,
`areas_count_limit` = p_areas_count_limit,
`force_area_radius` = p_force_area_radius,
`forced_area_radius` = p_forced_area_radius,
`new_menu` = p_new_menu
WHERE `id` = 0;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_update_breed` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_update_breed`(
    IN pId int,
	IN pEn_name varchar(100),
	IN pHe_name_male varchar(100),
    IN pHe_name_female varchar(100),
	IN pEnabled bit	)
BEGIN
	UPDATE tbl_breeds
		SET en_name=pEn_name, 
	        he_name_male = pHe_name_male,
            he_name_female = pHe_name_female,
			enabled=pEnabled
	WHERE id = pId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_update_canine` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_update_canine`(
	IN p_id INT,
	IN pInitial_position_lng DECIMAL(18,15),
	IN pInitial_position_lat DECIMAL(17,15),
	IN p_name varchar(50),
    IN p_gender_id TINYINT(4), 
	IN p_breed_id INT(11),
    IN p_birth_year SMALLINT(5),
	IN p_birth_month TINYINT(3),
	IN p_birth_day TINYINT(3),
	IN p_color1 TINYINT,
	IN p_color2 TINYINT,
	IN p_is_castrated BIT
)
BEGIN
	UPDATE tbl_canines
	SET 
		initial_lat = pInitial_position_lat, 
        initial_lng = pInitial_position_lng,
        `name` = p_name,
        gender_id = p_gender_id,
		breed_id = p_breed_id,
        birth_year = p_birth_year,
		birth_month = p_birth_month,
		birth_day = p_birth_day,
		color1 = p_color1,
		color2 = p_color2,		
		is_castrated = p_is_castrated
    WHERE id = p_id;	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_verify_session` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_verify_session`(
IN pHash VARCHAR(45)
)
BEGIN
	DECLARE var_dt DATETIME;
    
    SELECT expires into var_dt
	FROM tbl_bousers 	
	WHERE `hash` = pHash; 
    
	IF NOW() < var_dt THEN
		SELECT 1 as `isValid`;
	 ELSEIF var_validation_code = 1 THEN  
		SELECT 0 as `isValid`;
	 END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bo_view_daily_proximity_validations` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bo_view_daily_proximity_validations`()
BEGIN
	SELECT tval.id, 
	tval.from_owner_id, 
	ca1.`name` as 'from', 
	tval.to_owner_id, 
	ca2.`name` as 'to',
	tval.area_id,
	tval.date_time,
	tnt.`name` as 'type',
	tval.old_conclusion,
	tval.new_conclusion
	FROM tbl_proximity_log_validated tval
	INNER JOIN tbl_canine_owners co1 ON co1.id = tval.from_owner_id
	INNER JOIN tbl_canines ca1 ON co1.canine_id = ca1.id
	INNER JOIN tbl_canine_owners co2 ON co2.id = tval.to_owner_id
	INNER JOIN tbl_canines ca2 ON co2.canine_id = ca2.id
	INNER JOIN tbl_notification_types tnt ON tnt.id = tval.type_id
	where tval.type_id = 4;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_approve_friendship_offer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_approve_friendship_offer`(
IN p_offer_id BIGINT)
BEGIN
    DECLARE relationship_id BIGINT;	
    DECLARE txt VARCHAR(100);
	DECLARE var_canine_idA, var_owner_idA, var_canine_idB, var_owner_idB INT;		
	
    /*Extract the friendship offer*/
	SELECT from_canine_id, from_owner_id, to_canine_id,   to_owner_id INTO 
           var_canine_idA, var_owner_idA, var_canine_idB, var_owner_idB
	FROM `neighborhood`.`tbl_friendship_offers`
    WHERE id = p_offer_id;
	
	/*Assuming we got it, we log the approval operation*/
	INSERT INTO `neighborhood`.`tbl_friendship_log`
    (`from_canine_id`,`from_owner_id`,`to_canine_id`,`to_owner_id`,`friendship_state_id`, `datetime` )
    VALUES ( var_canine_idA, var_owner_idA, var_canine_idB, var_owner_idB, 3, NOW()); 
	
	/*Delete the offer*/
	DELETE FROM  `neighborhood`.`tbl_friendship_offers`
	WHERE id = p_offer_id;
	
	/*insert a friendship record to tbl_friendship*/
	INSERT INTO `neighborhood`.`tbl_friendships`(`canine_idA`,`owner_idA`,`canine_idB`,`owner_idB`, `is_friend`, `date_time`)
	VALUES(var_canine_idA, var_owner_idA, var_canine_idB, var_owner_idB, 1, NOW());
	
	/*insert a friendship approval notification*/
    SET relationship_id = (SELECT LAST_INSERT_ID());
	SET txt = CONCAT('{ "type": "f_app", "state": "F", "f_id": ',relationship_id,'}');   
	call fo_insert_notification(var_owner_idA, var_owner_idB, txt, 1, 2/*type_id*/ ,NOW(), null); 

	/*return the newly friendship record */
	SELECT 
		relationship_id, 
		ca.id as `canine_id`, 
		co.id as `owner_id`,
		ca.gender_id,
		ca.breed_id, 
		ca.`name` as `name`, 
        'F' as state,
        MIN(im.image_path) as `representative_image_path`,
        co.location_datetime 
	FROM tbl_canine_owners co
    INNER JOIN tbl_canines ca ON co.canine_id = ca.id
	LEFT JOIN tbl_images im ON co.id = im.owner_id
	WHERE co.id = var_owner_idA
	GROUP BY ca.id, co.id, ca.gender_id, ca.breed_id, ca.`name`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_bo_get_app_settings` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_bo_get_app_settings`()
BEGIN

SELECT `tbl_app_settings`.`generate_mock_ngbrs`,
    `tbl_app_settings`.`generate_mock_ngbrs_for_owner_id`,
    `tbl_app_settings`.`hide_regions_feature`,
    `tbl_app_settings`.`images_storage_limit`,
    `tbl_app_settings`.`uploaded_image_max_size`,
    `tbl_app_settings`.`viewport_mockup_key`,
    `tbl_app_settings`.`min_area_radius`,
    `tbl_app_settings`.`max_area_radius`,
    `tbl_app_settings`.`default_area_radius`,
    `tbl_app_settings`.`radius_step`,
    `tbl_app_settings`.`inspector_alert_areas_search_radius`,
    `tbl_app_settings`.`areas_count_limit`,
    `tbl_app_settings`.`android_location_service_scheduler_interval`,
    `tbl_app_settings`.`force_area_radius`,
    `tbl_app_settings`.`forced_area_radius`
FROM `neighborhood`.`tbl_app_settings`
Limit 1;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_bo_get_image` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_bo_get_image`(
	IN p_id INT
)
BEGIN
	select `data` 
	from tbl_images
	where id = p_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_bo_get_owner_by_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_bo_get_owner_by_id`(
	IN p_id INT
)
BEGIN
	SELECT tco.first_name, tco.email, tc.`name` 
	FROM tbl_canine_owners tco
	LEFT JOIN tbl_canines tc ON tco.canine_id = tc.id
	WHERE tco.id = p_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_bo_update_canine` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_bo_update_canine`(
	IN p_id INT,
	IN p_owner_id INT,
	IN p_name varchar(50),
    IN p_gender_id TINYINT(4), 
	IN p_breed_id INT(11),
	IN p_color1 TINYINT,
	IN p_color2 TINYINT,	
	IN p_birth_year SMALLINT(5),
	IN p_birth_month TINYINT(3),
	IN p_birth_day TINYINT(3),
	IN p_is_castrated BIT,
	IN p_owner_fn varchar(50)
)
BEGIN
	UPDATE tbl_canines
	SET `name` = p_name,
        gender_id = p_gender_id,
		breed_id = p_breed_id,	
		color1 = p_color1,
		color2 = p_color2,
		birth_year = p_birth_year,
		birth_month = p_birth_month,
		birth_day = p_birth_day,
		is_castrated = p_is_castrated
    WHERE id = p_id;	

	UPDATE tbl_canine_owners
	SET first_name = p_owner_fn
	WHERE id = p_owner_id;

	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_cancel_friendship_offer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_cancel_friendship_offer`(
IN p_offer_id BIGINT)
BEGIN    
    
    INSERT INTO tbl_friendship_log
    (`from_canine_id`,`from_owner_id`,`to_canine_id`,`to_owner_id`,`friendship_state_id`, `datetime` )
    SELECT from_canine_id, from_owner_id, to_canine_id, to_owner_id, 2, NOW() 
    FROM tbl_friendship_offers
    WHERE id = p_offer_id;

	
	DELETE FROM  tbl_friendship_offers
	WHERE id = p_offer_id;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_delete_album_image` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_delete_album_image`(
	IN p_image_path VARCHAR(100)
)
BEGIN
	DELETE FROM tbl_images
	WHERE image_path = p_image_path;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_delete_area` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_delete_area`(
 IN	p_area_id INT
)
BEGIN
	Delete from tbl_owners_areas where id = p_area_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_end_friendship` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_end_friendship`(
IN p_friendship_id BIGINT,
IN p_ender_canine_id INT,
IN p_ender_owner_id INT)
BEGIN
	
	DECLARE var_canine_idA, var_owner_idA, var_canine_idB, var_owner_idB INT;	
	DECLARE var_friendship_state_id tinyint;

    SELECT canine_idA, owner_idA, canine_idB, owner_idB INTO var_canine_idA, var_owner_idA, var_canine_idB, var_owner_idB
	FROM `neighborhood`.`tbl_friendships`
    WHERE id = p_friendship_id
    LIMIT 1;

	IF p_ender_canine_id = var_canine_idA THEN 
		SELECT id INTO var_friendship_state_id
		FROM `neighborhood`.`tbl_friendship_state`
		WHERE `name` = 'a revokes';
	 ELSEIF p_ender_canine_id = var_canine_idB THEN 
		
		SELECT id INTO var_friendship_state_id
		FROM `neighborhood`.`tbl_friendship_state`
		WHERE `name` = 'b revokes';
	 END IF;
	
	INSERT INTO `neighborhood`.`tbl_friendship_log`
	(`from_canine_id`,`from_owner_id`,`to_canine_id`,`to_owner_id`,`friendship_state_id`, `datetime` )
	VALUES (var_canine_idA, var_owner_idA, var_canine_idB, var_owner_idB, var_friendship_state_id, NOW());

	DELETE FROM  `neighborhood`.`tbl_friendships`
	WHERE id = p_friendship_id;

	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_all_breeds_by_language` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_all_breeds_by_language`(
IN p_lang_id INT
)
BEGIN
	IF p_lang_id = 2 THEN
		select * from (select id, CONCAT(he_name_male, '|', he_name_female) as `he_name`, enabled 
		from tbl_breeds
		where id = 1 ) a
		union all
		select * from (select id, CONCAT(he_name_male, '|', he_name_female) as `he_name`, enabled 
		from tbl_breeds 
		where id != 1 
		order by he_name_male ASC) b;
	END IF;
	
	IF p_lang_id = 1 THEN
		select * from (select id, en_name, enabled 
		from tbl_breeds
		where id = 1 ) a
		union all
		select * from (select id, en_name, enabled 
		from tbl_breeds 
		where id != 1 
		order by en_name ASC) b;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_all_canine_colors_en` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_all_canine_colors_en`()
BEGIN
	select id, en_name
	from tbl_canine_colors
	order by en_name ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_all_dog_parks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_all_dog_parks`()
BEGIN
	SELECT * FROM tbl_dog_parks;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_areas_by_owner_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_areas_by_owner_id`(
	IN p_owner_id INT
)
BEGIN
	SELECT id, `name`, owner_id, center_lat, center_lng, radius, from_time1,to_time1, last_update 
	FROM tbl_owners_areas 
	WHERE owner_id = p_owner_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_areas_by_owner_ids` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_areas_by_owner_ids`(
	IN p_ownrs_ids VARCHAR(2500)
)
BEGIN
	SET @s=CONCAT('SELECT id, owner_id, center_lat, center_lng, radius, from_time1,to_time1, last_update FROM tbl_owners_areas WHERE owner_id IN (',p_ownrs_ids,')');
    
    PREPARE stmt FROM @s;
    EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_current_locations_by_ids` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_current_locations_by_ids`(
IN p_nghbrs_ownrs_ids VARCHAR(2500)
)
BEGIN
	SET @s=CONCAT('SELECT id, lat, lng FROM tbl_canine_owners WHERE id IN (',p_nghbrs_ownrs_ids,')');
    
    PREPARE stmt FROM @s;
    EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_disconnected_friends_per_bounds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_disconnected_friends_per_bounds`(
IN p_owner_id INT,
IN p_ne_lat DECIMAL(17,15),
IN p_ne_lng DECIMAL(18,15),
IN p_sw_lat DECIMAL(17,15),
IN p_sw_lng DECIMAL(18,15)
)
BEGIN

SELECT * /*where the disconnected neightbor is a friend of type B*/
FROM ( SELECT ca.id, ca.breed_id, ca.gender_id, ca.`name`, co.lat, co.lng, co.id AS 'current_owner_id',  
	   co.location_datetime
       FROM tbl_canine_owners co
       INNER JOIN tbl_friendships fr ON fr.owner_idB = co.id    
       INNER JOIN tbl_canines ca ON ca.id = co.canine_id
       WHERE  (co.lng < p_ne_lng AND co.lng > p_sw_lng) AND (co.lat < p_ne_lat AND co.lat > p_sw_lat) AND 
              fr.owner_idA = p_owner_id) a
UNION ALL
SELECT *  /*where the disconnected neighbor is a friend of type A*/
FROM ( SELECT ca.id, ca.breed_id, ca.gender_id, ca.`name`, co.lat, co.lng, co.id AS 'current_owner_id',  
	   co.location_datetime
       FROM tbl_canine_owners co
       INNER JOIN tbl_friendships fr ON fr.owner_idA = co.id    
       INNER JOIN tbl_canines ca ON ca.id = co.canine_id
       WHERE (co.lng < p_ne_lng AND co.lng > p_sw_lng) AND (co.lat < p_ne_lat AND co.lat > p_sw_lat) AND 
			  fr.owner_idB = p_owner_id) b
UNION ALL
SELECT * /*where the disconnected neighbor sent the user a friendship offer*/
FROM ( SELECT ca.id, ca.breed_id, ca.gender_id, ca.`name`, co.lat, co.lng, co.id AS 'current_owner_id',  
	   co.location_datetime
       FROM tbl_canine_owners co
       INNER JOIN tbl_friendship_offers fo ON fo.from_owner_id = co.id    
       INNER JOIN tbl_canines ca ON ca.id = co.canine_id
       WHERE (co.lng < p_ne_lng AND co.lng > p_sw_lng) AND (co.lat < p_ne_lat AND co.lat > p_sw_lat) AND 
			 fo.to_owner_id = p_owner_id) c
limit 2;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_friendship_state` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_friendship_state`(
IN p_from_canine_id INT,
IN p_from_owner_id INT,
IN p_to_canine_id INT,
IN p_to_owner_id INT)
BEGIN
	SELECT CASE 
				WHEN is_friend = 1 THEN 3 
				WHEN is_friend = 0 THEN 7 
			END 
	FROM  `neighborhood`.`tbl_friendships`
	WHERE canine_idA = p_to_canine_id AND owner_idA = p_to_owner_id AND
          canine_idB = p_from_canine_id AND owner_idB = p_from_owner_id;	
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_frnd_requests_as_notifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_frnd_requests_as_notifications`(
IN p_owner_id INT)
BEGIN
	SELECT 
		   fo.`datetime` as date_time,
           ca.`name` AS from_canine_name, 
		   fo.from_owner_id as related_owner_id, 
           co.first_name AS from_owner_name,
           CONCAT('{ "type": "f_req", "state": "B", "offer_id":', fo.id, '}') as `data`,		              
           MIN(tbl_im.image_path) as `representative_image_path`		   
	FROM tbl_friendship_offers fo
	INNER JOIN tbl_canine_owners co ON fo.from_owner_id = co.id
	INNER JOIN tbl_canines ca ON co.canine_id = ca.id	
    LEFT JOIN  tbl_images tbl_im ON fo.from_owner_id = tbl_im.owner_id	
	WHERE fo.to_owner_id = p_owner_id
	GROUP BY fo.`datetime`, ca.`name`, fo.from_owner_id, co.first_name, fo.id;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_inspectors_per_bounds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_inspectors_per_bounds`(
IN p_from_dt DATETIME,
IN p_ne_lat DECIMAL(17,15),
IN p_ne_lng DECIMAL(18,15),
IN p_sw_lat DECIMAL(17,15),
IN p_sw_lng DECIMAL(18,15)
)
BEGIN
	
    SELECT ir.id, ir.lat, ir.lng, ir.date_time, co.first_name, ca.`name`, ir.reporting_owner_id, ir.notified_owners_ids  
	FROM tbl_inspector_reports ir
	INNER JOIN tbl_canine_owners co on co.id = ir.reporting_owner_id
    INNER JOIN tbl_canines ca on ca.id = co.canine_id
	WHERE ir.date_time > p_from_dt and 
          (ir.lng < p_ne_lng AND ir.lng > p_sw_lng) AND 
		  (ir.lat < p_ne_lat AND ir.lat > p_sw_lat);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_is_authenticated` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_is_authenticated`(
IN p_owner_id INT,
IN p_auth_id VARCHAR(100)
)
BEGIN
	SELECT auth_id = p_auth_id
    FROM tbl_canine_owners
    WHERE id = p_owner_id; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_members_per_bounds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_members_per_bounds`(
IN p_ne_lat DECIMAL(17,15),
IN p_ne_lng DECIMAL(18,15),
IN p_sw_lat DECIMAL(17,15),
IN p_sw_lng DECIMAL(18,15),
IN p_is_mockup BIT,
IN p_limit INT
)
BEGIN   

SET @p_ne_lat    = p_ne_lat;
SET @p_ne_lng    = p_ne_lng;
SET @p_sw_lat    = p_sw_lat;
SET @p_sw_lng    = p_sw_lng;
SET @p_is_mockup = p_is_mockup;
SET @p_limit     = p_limit;

PREPARE STMT FROM 'SELECT ca.id, ca.breed_id, ca.gender_id, ca.`name`, co.lat, co.lng, co.id AS \'current_owner_id\'	
FROM tbl_canine_owners co 
INNER JOIN tbl_canines ca ON ca.id = co.canine_id 
WHERE (co.lng < ? AND co.lng > ?) AND (co.lat < ? AND co.lat > ?)  AND ca.is_mockup = ?';

/*LIMIT ?';*/
EXECUTE STMT USING @p_ne_lng, @p_sw_lng, @p_ne_lat, @p_sw_lat, @p_is_mockup/*, @p_limit*/;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_member_by_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_member_by_id`(
IN p_canine_id INT,
IN p_owner_id INT,
IN p_version_name VARCHAR(20),
IN p_android_code_version INT)
BEGIN
	
        UPDATE tbl_canine_owners
    SET version_name = p_version_name,
	    android_code_version = p_android_code_version
	WHERE id = p_owner_id;
    
    SELECT ca.id, ca.gender_id, ca.`name`, ca.breed_id, co.lat, co.lng, 
           ca.initial_lat, ca.initial_lng, ca.language_id, ca.date_created, ca.color1, ca.color2, ca.birth_year, ca.birth_month, 
           ca.birth_day, ca.is_castrated
	FROM tbl_canines ca
    INNER JOIN tbl_canine_owners co ON ca.id = co.canine_id
	where ca.id = p_canine_id AND co.id = p_owner_id;

	SELECT id, owner_id, `name`, center_lat, center_lng, radius, from_time1, to_time1, last_update
    FROM tbl_owners_areas
    WHERE owner_id = p_owner_id;

	SELECT image_path
    FROM tbl_images
    WHERE canine_id = p_canine_id and owner_id = p_owner_id;

	SELECT id, first_name, email, registration_status, is_native, auth_id 
	FROM tbl_canine_owners
	WHERE  canine_id = p_canine_id;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_neighbor_by_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_neighbor_by_id`(
IN p_owner_id INT
)
BEGIN
	DECLARE v_canine_id INT;
	SET v_canine_id = (SELECT canine_id FROM tbl_canine_owners WHERE id = p_owner_id);

    SELECT id, gender_id, `name`, breed_id, color1, color2, birth_year, birth_month, birth_day, is_castrated
	FROM tbl_canines 	
	where id = v_canine_id;
	
	SELECT image_path
	FROM tbl_images
	WHERE owner_id = p_owner_id;

	SELECT id, first_name, email, registration_status 
	FROM tbl_canine_owners
	WHERE  id = p_owner_id;    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_notification_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_notification_details`(
IN p_from_owner_id INT,
IN p_to_owner_id INT
)
BEGIN
	DECLARE v_language_id TINYINT;
	DECLARE v_push_service_reg_id varchar(2048);
    DECLARE v_platform varchar(20);
	DECLARE v_badge_count SMALLINT;
    DECLARE v_to_canine_name varchar(20); 

	SELECT ow.platform, ow.push_service_reg_id, ca.language_id, ca.`name` 
           INTO v_platform, v_push_service_reg_id, v_language_id, v_to_canine_name 
	FROM tbl_canine_owners ow
	INNER JOIN tbl_canines ca ON ow.canine_id = ca.id
    WHERE ow.id = p_to_owner_id;
	
    SELECT count(*) INTO v_badge_count
    FROM tbl_notifications
    WHERE to_owner_id = p_to_owner_id AND is_pending = 1 AND date_time > SUBTIME(NOW(), '72:00:00.000000');
    

    SELECT ow.first_name as owner_name, 
           ca.`name` as from_canine_name,            
           MIN(im.image_path) as image_path,
           v_push_service_reg_id as reg_id,
           v_platform as platform,
           v_badge_count as badge_count,
           v_language_id as language_id,
		   v_to_canine_name as to_canine_name	
    FROM tbl_canine_owners ow
	INNER JOIN tbl_canines ca ON ow.canine_id = ca.id
	LEFT JOIN tbl_images im ON ow.id = im.owner_id
    WHERE ow.id = p_from_owner_id
    GROUP BY ow.first_name, ca.`name`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_proximity_alert_data` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_proximity_alert_data`(
IN p_region_id INT, 
IN p_from_owner_id INT,
IN p_region_dt_of_from_owner DATETIME,
IN p_is_entering BIT
)
BEGIN
	DECLARE var_txt VARCHAR(200);
	DECLARE var_sub_type VARCHAR(10);
	DECLARE var_ntf_type_id TINYINT(3);
	DECLARE var_to_owner_id INT;
    DECLARE var_from_gender_id TINYINT(4);
	DECLARE var_count INT;
    DECLARE var_validation_code TINYINT;
	DECLARE var_region_name VARCHAR(20);
	DECLARE var_region_dt DATETIME;
	DECLARE var_result VARCHAR(20);/*OK | NOT_UPTO_DATE | NO_RELATIONSHIP | REGION_NOT_EXIST | HAPPEND_LATELY | EXIT_AFTER_NONE | EXIT_AFTER_EXIT*/
	DECLARE var_area_lat DECIMAL(17,15);
    DECLARE var_area_lng DECIMAL(18,15);

	SELECT count(*), owner_id, last_update, `name`, center_lat, center_lng into 
           var_count, var_to_owner_id, var_region_dt, var_region_name, var_area_lat, var_area_lng
	FROM tbl_owners_areas 	
	WHERE id = p_region_id;    
    
	IF var_count = 1 THEN /*region exists*/
		CALL fo_update_user_location_to(p_from_owner_id, var_area_lat, var_area_lng);
        SELECT count(*) INTO var_count
		FROM tbl_friendships 	
		WHERE (owner_idA = p_from_owner_id AND owner_idB = var_to_owner_id) OR 
			  (owner_idA = var_to_owner_id AND owner_idB = p_from_owner_id);
		
        IF var_count = 1 THEN /*relationship exists between the two owners*/
			IF p_region_dt_of_from_owner >= var_region_dt THEN /*region is upto date*/
				
                IF p_is_entering = 1 THEN 
					SET var_sub_type = "entering";
					SET var_ntf_type_id = 3;/*type_id: area_entering*/
				ELSE
					SET var_sub_type = "exiting";
					SET var_ntf_type_id = 4;/*type_id: area_exiting*/
				END IF;
                
                SET var_validation_code = fo_validate_proximity_alert(p_from_owner_id, p_region_id, var_ntf_type_id);
				
                IF var_validation_code = 0 THEN 
					
                    SET var_result = 'OK';
					select gender_id INTO var_from_gender_id
					from tbl_canines ca
					inner join tbl_canine_owners co on co.canine_id = ca.id
					where co.id = p_from_owner_id;

					SET var_txt = CONCAT('{ "type": "r_proximity", "sub_type": "',var_sub_type,'", "owner_id": ',p_from_owner_id,', "gender_id": ',var_from_gender_id, ', "region_id": ',p_region_id,',"region_name": "',var_region_name,'"}');   
					CALL fo_insert_notification(var_to_owner_id, p_from_owner_id, var_txt, 1, var_ntf_type_id, NOW(), p_region_id);                     
				
                ELSEIF var_validation_code = 1 THEN  
					SET var_result = 'HAPPEND_LATELY';
				ELSEIF var_validation_code = 3 THEN
					SET var_result = 'EXIT_AFTER_NONE';
				ELSEIF var_validation_code = 4 THEN
					SET var_result = 'EXIT_AFTER_EXIT';
				END IF;
                
			ELSE /*region is NOT upto date*/
				SET var_result = 'NOT_UPTO_DATE';  
			END IF;
		ELSE  /*relationship NOT exists between the two owners*/
		   SET var_result = 'NO_RELATIONSHIP';
		END IF;
	ELSE /*region not exist*/
		SET var_result = 'REGION_NOT_EXIST';
	END IF;
    
	CALL bo_insert_proximity_log(var_ntf_type_id, p_from_owner_id, var_to_owner_id, p_region_id, NOW(), var_result);
    SELECT var_result as `result`, var_to_owner_id as `to_owner_id`, var_region_name as `region_name`, var_from_gender_id as `from_gender_id`;
		
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_relationships_notifications_for_owner` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_relationships_notifications_for_owner`(
IN p_owner_id INT,
IN p_include_proximity BIT)
BEGIN

DROP TABLE IF EXISTS tbl_result	;

	IF p_include_proximity = 1 THEN /* including proximity notifications */
		CREATE TEMPORARY TABLE tbl_result AS (
			SELECT tn.id, 
				   tn.related_owner_id, 
				   co.first_name AS from_owner_name, 
				   ca.`name` AS from_canine_name, 
				   tn.`data`,				   
				   MIN(tbl_im.image_path) as `representative_image_path`,
                   tn.date_time,
                   tn.is_pending
			FROM tbl_notifications tn
			INNER JOIN tbl_canine_owners co ON tn.related_owner_id = co.id
			INNER JOIN tbl_canines ca ON co.canine_id = ca.id
			LEFT JOIN  tbl_images tbl_im ON tn.related_owner_id = tbl_im.owner_id	
			WHERE tn.to_owner_id = p_owner_id 
				  /*AND tn.date_time > SUBTIME(NOW(), '24:00:00.000000') AND tn.is_pending = 1*/
			GROUP BY tn.id, tn.related_owner_id, co.first_name, ca.`name`, tn.`data`, tn.date_time
            ORDER BY tn.date_time desc
            LIMIT 20
		);
	ELSE /*not including proximity notifications */
		CREATE TEMPORARY TABLE tbl_result AS (
			SELECT tn.id, 
				   tn.related_owner_id, 
				   co.first_name AS from_owner_name, 
				   ca.`name` AS from_canine_name, 
				   tn.`data`,
                   MIN(tbl_im.image_path) as `representative_image_path`,
				   tn.date_time,
                   tn.is_pending
			FROM tbl_notifications tn
			INNER JOIN tbl_canine_owners co ON tn.related_owner_id = co.id
			INNER JOIN tbl_canines ca ON co.canine_id = ca.id
			LEFT JOIN  tbl_images tbl_im ON tn.related_owner_id = tbl_im.owner_id	
			WHERE tn.to_owner_id = p_owner_id/* AND  tn.is_pending = 1 */
				  AND tn.type_id NOT IN (3,4) /*not including proximity notifications */
			GROUP BY tn.id, tn.related_owner_id, co.first_name, ca.`name`, tn.`data`, tn.date_time
            ORDER BY tn.date_time desc
            LIMIT 20
		);
	END IF;

UPDATE tbl_notifications 
SET is_pending = 0
WHERE to_owner_id = p_owner_id AND is_pending = 1 ;

SELECT * FROM tbl_result;
call fo_get_user_relationships(p_owner_id);
     
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_get_user_relationships` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_get_user_relationships`(
IN p_owner_id INT
)
BEGIN
	SELECT * 
	FROM (SELECT /*  get all user's friends where the user is A */
			tbl_f.id as `id`,
			tbl_f.canine_idB as `canine_id`, 
			tbl_f.owner_idB as `owner_id`,
            tbl_c.gender_id as `gender_id`,  
			tbl_c.breed_id as `breed_id`,
			tbl_c.`name` as `name`,			
            MIN(tbl_im.image_path) as `representative_image_path`,
			'F' as `state`, 
             tbl_co.location_datetime, /*for connection date time lately*/             
             tbl_co.first_name as `owner_name` /*for inspectors alerts owners names*/             
		  FROM `neighborhood`.`tbl_friendships` tbl_f
		  INNER JOIN `neighborhood`.`tbl_canines` tbl_c	ON tbl_f.canine_idB = tbl_c.id
          INNER JOIN `neighborhood`.`tbl_canine_owners` tbl_co ON tbl_f.owner_idB = tbl_co.id
		  LEFT JOIN `neighborhood`.`tbl_images` tbl_im ON tbl_f.owner_idB = tbl_im.owner_id
		  WHERE tbl_f.owner_idA = p_owner_id and tbl_f.is_friend = 1
		  GROUP BY tbl_f.id, tbl_f.canine_idB, tbl_f.owner_idB, tbl_c.gender_id, tbl_c.breed_id, tbl_c.`name`, `state`) a	
	
	UNION ALL		  
	SELECT * 
	FROM (SELECT /*  get all users's friends where the user is B */
			 tbl_f.id as `id`, 			  
			 tbl_f.canine_idA as `canine_id`, 
			 tbl_f.owner_idA as `owner_id`,
			 tbl_c.gender_id as `gender_id`,		
             tbl_c.breed_id as `breed_id`,
			 tbl_c.`name` as `name`,
			 MIN(tbl_im.image_path) as `representative_image_path`,
             'F' as `state`,
             tbl_co.location_datetime, /*for connection date time lately*/
             tbl_co.first_name as `owner_name` /*for inspectors alerts owners names*/             
	      FROM `neighborhood`.`tbl_friendships` tbl_f
          INNER JOIN `neighborhood`.`tbl_canines` tbl_c	ON tbl_f.canine_idA = tbl_c.id
		  INNER JOIN `neighborhood`.`tbl_canine_owners` tbl_co ON tbl_f.owner_idA = tbl_co.id
          LEFT JOIN `neighborhood`.`tbl_images` tbl_im ON tbl_f.owner_idA = tbl_im.owner_id
		  WHERE tbl_f.owner_idB = p_owner_id AND tbl_f.is_friend = 1
		  GROUP BY tbl_f.id, tbl_f.canine_idA, tbl_f.owner_idA, tbl_c.gender_id, tbl_c.breed_id, tbl_c.`name`,`state`) b
	UNION ALL
	SELECT * FROM ( SELECT /*  get all users that this users has defined as enemies */
			tbl_f.id as `id`,
			tbl_f.canine_idB as `canine_id`, 
			tbl_f.owner_idB as `owner_id`,
            tbl_c.gender_id as `gender_id`,  
			tbl_c.breed_id as `breed_id`,
			tbl_c.`name` as `name`,
			MIN(tbl_im.image_path) as `representative_image_path`,
            'EA' as `state`,
             tbl_co.location_datetime, /*for connection date time lately*/
             tbl_co.first_name as `owner_name` /*for inspectors alerts owners names*/             
		  FROM `neighborhood`.`tbl_friendships` tbl_f
		  INNER JOIN `neighborhood`.`tbl_canines` tbl_c	ON tbl_f.canine_idB = tbl_c.id
		  INNER JOIN `neighborhood`.`tbl_canine_owners` tbl_co ON tbl_f.owner_idB = tbl_co.id
          LEFT JOIN `neighborhood`.`tbl_images` tbl_im ON tbl_f.owner_idB = tbl_im.owner_id
		  WHERE tbl_f.owner_idA = p_owner_id and tbl_f.is_friend = 0
		  GROUP BY tbl_f.id, tbl_f.canine_idB, tbl_f.owner_idB, tbl_c.gender_id, tbl_c.breed_id, tbl_c.`name`,`state`) c
	UNION ALL
	SELECT * FROM (SELECT/*  get all the users that defined this user as enemy */
			tbl_f.id as `id`,
			tbl_f.canine_idA as `canine_id`, 
			tbl_f.owner_idA as `owner_id`,
            tbl_c.gender_id as `gender_id`,  
			tbl_c.breed_id as `breed_id`,
			tbl_c.`name` as `name`,
            MIN(tbl_im.image_path) as `representative_image_path`,
			'EB' as `state`,
             tbl_co.location_datetime,  /*for connection date time lately*/
             tbl_co.first_name as `owner_name` /*for inspectors alerts owners names*/             
		  FROM `neighborhood`.`tbl_friendships` tbl_f
		  INNER JOIN `neighborhood`.`tbl_canines` tbl_c	ON tbl_f.canine_idA = tbl_c.id
          INNER JOIN `neighborhood`.`tbl_canine_owners` tbl_co ON tbl_f.owner_idA = tbl_co.id
		  LEFT JOIN `neighborhood`.`tbl_images` tbl_im ON tbl_f.owner_idA = tbl_im.owner_id
		  WHERE tbl_f.owner_idB = p_owner_id and tbl_f.is_friend = 0
		  GROUP BY tbl_f.id, tbl_f.canine_idA, tbl_f.owner_idA, tbl_c.gender_id, tbl_c.breed_id, tbl_c.`name`, `state`) d
	UNION ALL
	SELECT * 
	FROM (SELECT /*  get all the users that this user has sent friendship offers */
			  tbl_fo.id as `id`, 			  
			  tbl_fo.to_canine_id as `canine_id`, 
			  tbl_fo.to_owner_id as `owner_id`, 
			  tbl_c.gender_id as `gender_id`,
			  tbl_c.breed_id as `breed_id`,
			  tbl_c.`name` as `name`,
              MIN(tbl_im.image_path) as `representative_image_path`,
			  'A' as `state`, 
              tbl_co.location_datetime,  /*for connection date time lately*/
              tbl_co.first_name as `owner_name` /*for inspectors alerts owners names*/             
	      FROM `neighborhood`.`tbl_friendship_offers` tbl_fo
		  INNER JOIN `neighborhood`.`tbl_canines` tbl_c	ON tbl_fo.to_canine_id = tbl_c.id
		  INNER JOIN `neighborhood`.`tbl_canine_owners` tbl_co ON tbl_fo.to_owner_id = tbl_co.id
		  LEFT JOIN `neighborhood`.`tbl_images` tbl_im ON tbl_fo.to_owner_id = tbl_im.owner_id
		  WHERE from_owner_id = p_owner_id
		  GROUP BY tbl_fo.id, tbl_fo.to_canine_id, tbl_fo.to_owner_id, tbl_c.gender_id, tbl_c.breed_id, tbl_c.`name`, `state`) e		  
	UNION ALL
	SELECT * 
	FROM (SELECT /*  get all the members that sent this members friendship offers */
              tbl_fo.id as `id`, 
              tbl_fo.from_canine_id as `canine_id`, 
              tbl_fo.from_owner_id as `owner_id`,
			  tbl_c.gender_id as `gender_id`,
			  tbl_c.breed_id as `breed_id`, 
			  tbl_c.`name` as `name`,
              MIN(tbl_im.image_path) as `representative_image_path`,
              'B' as `state`,
              tbl_co.location_datetime, /*for connection date time lately*/
              tbl_co.first_name as `owner_name` /*for inspectors alerts owners names*/             
	      FROM `neighborhood`.`tbl_friendship_offers` tbl_fo
		  INNER JOIN `neighborhood`.`tbl_canines` tbl_c	ON tbl_fo.from_canine_id = tbl_c.id
		  INNER JOIN `neighborhood`.`tbl_canine_owners` tbl_co ON tbl_fo.from_owner_id = tbl_co.id
          LEFT JOIN `neighborhood`.`tbl_images` tbl_im ON tbl_fo.from_owner_id = tbl_im.owner_id
		  WHERE to_owner_id = p_owner_id 
          GROUP BY tbl_fo.id, tbl_fo.from_canine_id, tbl_fo.from_owner_id, tbl_c.gender_id, tbl_c.breed_id, tbl_c.`name`, `state`) f;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_insert_area` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_insert_area`(
	IN p_owner_id INT,
	IN p_name VARCHAR(20),
	IN p_radius TINYINT UNSIGNED,
	IN p_center_lat DECIMAL(17,15),    
	IN p_center_lng DECIMAL(18,15),
	IN p_from_time1 VARCHAR(5),
	IN p_to_time1 VARCHAR(5)
)
BEGIN
	DECLARE area_id INT;
	INSERT INTO `neighborhood`.`tbl_owners_areas`
	(`owner_id`, `name`,`radius`,`center_lat`,`center_lng`,`from_time1`, `to_time1`, `last_update`)
	VALUES(p_owner_id, p_name, p_radius,p_center_lat,p_center_lng, p_from_time1, p_to_time1, NOW());

    SET area_id = (SELECT LAST_INSERT_ID());

	SELECT id, owner_id, `name`, radius, center_lat, center_lng, from_time1, to_time1, last_update
	FROM `neighborhood`.`tbl_owners_areas` 
    WHERE id = area_id;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_insert_canine` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_insert_canine`(    
	IN p_name varchar(50),
    IN pGender_id TINYINT(4), 
	IN pBreed_id INT(11),	
	IN pIs_mockup BIT,
	IN pInitial_position_lng DECIMAL(18,15),
	IN pInitial_position_lat DECIMAL(17,15),
	IN p_language_id TINYINT	
)
BEGIN
		
	INSERT INTO tbl_canines 
    (date_created, `name`, gender_id, breed_id, is_mockup, 
     initial_lat, initial_lng, language_id) 
	VALUES
	(NOW(), p_name, pGender_id, pBreed_id, pIs_mockup, 
     pInitial_position_lat, pInitial_position_lng, p_language_id);
		
	SELECT LAST_INSERT_ID();
	
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_insert_enemy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_insert_enemy`(
IN p_from_canine_id INT,
IN p_from_owner_id INT,
IN p_to_canine_id INT,
IN p_to_owner_id INT)
BEGIN

	DECLARE relationship_id BIGINT;

    INSERT INTO `neighborhood`.`tbl_friendship_log`
    (`from_canine_id`,`from_owner_id`,`to_canine_id`,`to_owner_id`,`friendship_state_id`, `datetime` )
    VALUES ( p_from_canine_id, p_from_owner_id, p_to_canine_id, p_to_owner_id, 7, NOW()); 

	INSERT INTO `neighborhood`.`tbl_friendships`
	(`canine_idA`, `owner_idA`, `canine_idB`,`owner_idB`, `is_friend`, `date_time`)
	VALUES(  p_from_canine_id, p_from_owner_id, p_to_canine_id, p_to_owner_id, 0, NOW());
	
	SET relationship_id = (SELECT LAST_INSERT_ID());

	SELECT relationship_id, 
		   ca.id as `canine_id`, 
		   co.id as `owner_id`,
		   ca.gender_id,
           ca.breed_id, 
           ca.`name` as `name`, 
           'EA' as state, 
           MIN(im.image_path) as `representative_image_path`,           
           co.location_datetime,      /*for connection date time lately*/
           co.first_name as `owner_name` /*for inspectors alerts owners names*/             
	FROM tbl_canine_owners co
    INNER JOIN tbl_canines ca ON co.canine_id = ca.id
	LEFT JOIN tbl_images im ON co.id = im.owner_id
	WHERE co.id = p_to_owner_id
	GROUP BY ca.id, co.id, ca.gender_id, ca.breed_id, ca.`name`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_insert_friendship_offer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_insert_friendship_offer`(
IN p_from_canine_id INT,
IN p_from_owner_id INT,
IN p_to_canine_id INT,
IN p_to_owner_id INT)
BEGIN
	DECLARE relationship_id BIGINT;
    DECLARE txt VARCHAR(100);

    INSERT INTO `neighborhood`.`tbl_friendship_log`
    (`from_canine_id`,`from_owner_id`,`to_canine_id`,`to_owner_id`,`friendship_state_id`, `datetime` )
    VALUES ( p_from_canine_id, p_from_owner_id, p_to_canine_id, p_to_owner_id, 1, NOW()); 

	INSERT INTO `neighborhood`.`tbl_friendship_offers`
	(`from_canine_id`, `from_owner_id`, `to_canine_id`,`to_owner_id`, `datetime`)
	VALUES(  p_from_canine_id, p_from_owner_id, p_to_canine_id, p_to_owner_id,NOW());
	
    SET relationship_id = (SELECT LAST_INSERT_ID());
    SET txt = CONCAT('{ "type": "f_req", "state": "B", "offer_id": ',relationship_id,'}');   
	call fo_insert_notification(p_to_owner_id, p_from_owner_id, txt, 1, 1/*type_id*/ , NOW(), null); 
    
    SELECT relationship_id,
		   ca.id as `canine_id`,
		   co.id as `owner_id`,
		   ca.gender_id,
		   ca.breed_id, 
		   ca.`name` as `name`,
		   'A' as state,
           MIN(im.image_path) as `representative_image_path`,            
           co.location_datetime,    /*for connection date time lately*/             
           co.first_name as `owner_name`  /*for inspectors alerts owners names*/             
	FROM tbl_canine_owners co
    INNER JOIN tbl_canines ca ON co.canine_id = ca.id
	LEFT JOIN tbl_images im ON co.id = im.owner_id
	WHERE co.id = p_to_owner_id
	GROUP BY ca.id, co.id, ca.gender_id, ca.breed_id, ca.`name`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_insert_image` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_insert_image`(
	IN p_canine_id INT,
	IN p_owner_id INT,
	IN p_img_path varchar(100)
)
BEGIN
	INSERT INTO tbl_images(canine_id, owner_id, date_time, image_path) 
    VALUES (p_canine_id, p_owner_id, NOW(), p_img_path);
	
	SELECT LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_insert_inspector_notification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_insert_inspector_notification`(
IN p_from_owner_id INT,
IN p_to_owner_id INT,
IN p_region_names VARCHAR(200)
)
BEGIN
	DECLARE var_txt VARCHAR(250);
    
	SET var_txt = CONCAT('{ "type": "inspector", "region_names": "',p_region_names,'"}');    
	call fo_insert_notification(p_to_owner_id, p_from_owner_id, var_txt, 1, 5/*type_id*/ , NOW(), null); 
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_insert_inspector_report` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_insert_inspector_report`(
	IN p_lat DECIMAL(17,15),	
	IN p_lng DECIMAL(18,15),	
	IN p_reporting_owner_id INT,
	IN p_dt DATETIME,
    IN p_owners_ids VARCHAR(200)
)
BEGIN
		
	INSERT INTO tbl_inspector_reports (lat, lng, reporting_owner_id, date_time, notified_owners_ids)
	VALUES (p_lat, p_lng, p_reporting_owner_id, p_dt, p_owners_ids);

	SELECT ir.id, ir.lat, ir.lng, ir.date_time, ir.reporting_owner_id, co.first_name, ca.`name`, ir.notified_owners_ids 
    FROM tbl_inspector_reports ir
    INNER JOIN tbl_canine_owners co on co.id = ir.reporting_owner_id
    INNER JOIN tbl_canines ca on ca.id = co.canine_id
    WHERE ir.id = LAST_INSERT_ID();

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_insert_notification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_insert_notification`(
IN p_to_owner_id INT,
IN p_related_owner_id INT,
IN p_data VARCHAR(200),
IN p_is_pending BIT,
IN p_type_id TINYINT(3),
IN p_date_time DATETIME,
IN p_area_id INT(10)
)
BEGIN
	INSERT INTO `neighborhood`.`tbl_notifications`
(`to_owner_id`, `related_owner_id`, `data`,`is_pending`, `type_id`, `date_time`, `area_id`)
VALUES(p_to_owner_id, p_related_owner_id, p_data, p_is_pending, p_type_id, p_date_time, p_area_id);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_insert_owner` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_insert_owner`(
    IN p_canine_id INT,	
    IN p_first_name VARCHAR(25),	
	IN p_email VARCHAR(100),
	IN p_registration_status TINYINT,
	IN p_is_native BIT,	    
    IN p_platform VARCHAR(20),
	IN p_platform_ver VARCHAR(20),  
    IN p_browser VARCHAR(20),
    IN p_browser_ver VARCHAR(20),
	IN p_lng DECIMAL(18,15),
	IN p_lat DECIMAL(17,15),	
	IN p_dom_load_length MEDIUMINT,
    IN p_phonegap_ver VARCHAR(40),
    IN p_version_name VARCHAR(20),
    IN p_android_code_version INT
)
BEGIN
		
	INSERT INTO tbl_canine_owners 
    (canine_id, first_name, email, registration_status, is_native, platform, platform_ver, 
     browser, browser_ver, lng, lat, location_datetime, dom_load_length, phonegap_ver, version_name, android_code_version) 
	VALUES (p_canine_id, p_first_name, p_email, p_registration_status, 
            p_is_native, p_platform, p_platform_ver, p_browser, p_browser_ver, 
            p_lng, p_lat, NOW(), p_dom_load_length, p_phonegap_ver, p_version_name, p_android_code_version);
		
	SELECT LAST_INSERT_ID();
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_insert_user_operation` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_insert_user_operation`(
IN p_canine_id INT,
IN p_owner_id INT,
IN p_action_id INT)
BEGIN

INSERT INTO `neighborhood`.`tbl_users_operations` (`canine_id`,`owner_id`,`date_time`,`operation_type_id`)
VALUES(p_canine_id, p_owner_id, NOW(), p_action_id);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_log_error` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_log_error`(	
	IN p_origin TINYINT,
	IN p_message VARCHAR(500),
	IN p_stack_trace VARCHAR(3000),
	IN p_aux VARCHAR(500)
)
BEGIN
	INSERT INTO `neighborhood`.`tbl_error_log`(`date_time_utc`, `origin`, `message`, `stack_trace`, `auxiliary_data`)
    VALUES(NOW(), p_origin, p_message, p_stack_trace, p_aux);	
	
	SELECT LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_refuse_friendship_offer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_refuse_friendship_offer`(
IN p_offer_id BIGINT,
IN p_from_canine_id INT,
IN p_from_owner_id INT,
IN p_to_canine_id INT,
IN p_to_owner_id INT)
BEGIN
	INSERT INTO `neighborhood`.`tbl_friendship_log`
    (`from_canine_id`,`from_owner_id`,`to_canine_id`,`to_owner_id`,`friendship_state_id`, `datetime` )
    VALUES ( p_from_canine_id, p_from_owner_id, p_to_canine_id, p_to_owner_id, 4, NOW()); 
	
	DELETE FROM  `neighborhood`.`tbl_friendship_offers`
	WHERE id = p_offer_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_search_users_per_bounds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_search_users_per_bounds`(
	IN p_ne_lat DECIMAL(17,15),
	IN p_ne_long DECIMAL(18,15),
	IN p_sw_lat DECIMAL(17,15),
	IN p_sw_long DECIMAL(18,15),
    IN p_include_mockups BIT
)
BEGIN
  
  IF p_include_mockups = 0 THEN
	  SELECT ca.id as canine_id, co.id as owner_id, ca.breed_id, ca.gender_id, ca.`name`, 
             MIN(im.image_path) as representative_image_path, co.location_datetime 
	  FROM tbl_canines ca
	  INNER JOIN tbl_canine_owners co ON co.canine_id = ca.id
	  LEFT JOIN tbl_images im ON im.owner_id = co.id
	  WHERE	 (co.lng < p_ne_long AND co.lng > p_sw_long AND co.lat < p_ne_lat AND co.lat > p_sw_lat)
		   AND ca.is_mockup = 0	   
	  GROUP BY ca.id, co.id, ca.breed_id, ca.gender_id, ca.`name`
	  ORDER BY 4;
   ELSE 
	  SELECT ca.id as canine_id, co.id as owner_id, ca.breed_id, ca.gender_id, ca.`name`, 
			 MIN(im.image_path) as representative_image_path, co.location_datetime 
	  FROM tbl_canines ca
	  INNER JOIN tbl_canine_owners co ON co.canine_id = ca.id
	  LEFT JOIN tbl_images im ON im.owner_id = co.id
	  WHERE (co.lng < p_ne_long AND co.lng > p_sw_long AND co.lat < p_ne_lat AND co.lat > p_sw_lat)
	  GROUP BY ca.id, co.id, ca.breed_id, ca.gender_id, ca.`name`
	  ORDER BY 4;
   END IF;	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_unmark_enemy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_unmark_enemy`(IN p_relationship_id BIGINT)
BEGIN
	INSERT INTO `neighborhood`.`tbl_friendship_log`
    (`from_canine_id`,`from_owner_id`,`to_canine_id`,`to_owner_id`,`friendship_state_id`, `datetime` )
    SELECT canine_idA, owner_idA, canine_idB, owner_idB, 8, NOW()
	FROM `neighborhood`.`tbl_friendships`
	WHERE id = p_relationship_id;
	
	DELETE FROM  `neighborhood`.`tbl_friendships`
	WHERE id = p_relationship_id;	
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_update_area` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_update_area`(
	IN p_area_id INT,
	IN p_name VARCHAR(20),
	IN p_radius TINYINT UNSIGNED,
	IN p_center_lat DECIMAL(17,15),    
	IN p_center_lng DECIMAL(18,15),
	IN p_from_time1 VARCHAR(5),
	IN p_to_time1 VARCHAR(5)	
)
BEGIN
	UPDATE `neighborhood`.`tbl_owners_areas`
	SET
	`name` = p_name,
	radius = p_radius,	
	center_lat = p_center_lat,
	center_lng = p_center_lng,
	from_time1 = p_from_time1,	
	to_time1 = p_to_time1,
	last_update = NOW()
	WHERE id = p_area_id;
	
	SELECT id, owner_id, `name`, radius, center_lat, center_lng, from_time1, to_time1, last_update
	FROM `neighborhood`.`tbl_owners_areas`
	WHERE id = p_area_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_update_inspector_report` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_update_inspector_report`(
IN p_inspector_report_id INT,
IN p_owners_ids VARCHAR(200) 
)
BEGIN
	UPDATE tbl_inspector_reports
    SET notified_owners_ids = p_owners_ids
    WHERE id = p_inspector_report_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_update_owner_auth_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_update_owner_auth_id`(
IN p_owner_id INT,	
IN p_auth_id VARCHAR(100)	
)
BEGIN
	UPDATE tbl_canine_owners
	SET auth_id = p_auth_id
	WHERE id = p_owner_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_update_owner_name` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_update_owner_name`(
IN p_owner_id INT(10),
 IN p_owner_name varchar(25))
BEGIN
	UPDATE tbl_canine_owners
	SET first_name = p_owner_name
	WHERE id = p_owner_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_update_push_service_reg_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_update_push_service_reg_id`(
IN p_owner_id INT,
IN p_reg_id VARCHAR(2048)
)
BEGIN
	
	UPDATE tbl_canine_owners
	SET push_service_reg_id = p_reg_id
    WHERE id = p_owner_id;
	 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_update_user_location_by` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_update_user_location_by`(
  IN p_owner_id INT,
  IN p_delta_lat DECIMAL(17,15),
  IN p_delta_lng DECIMAL(18,15)
)
BEGIN
	UPDATE tbl_canine_owners
	SET lat = lat + p_delta_lat,
        lng = lng + p_delta_lng, 
		location_datetime = NOW()
    WHERE id = p_owner_id;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fo_update_user_location_to` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fo_update_user_location_to`(
IN p_owner_id INT,
IN p_lat DECIMAL(17,15),
IN p_lng DECIMAL(18,15)
)
BEGIN
	UPDATE tbl_canine_owners
	SET lat = p_lat,
        lng = p_lng, 
		location_datetime = NOW()
    WHERE id = p_owner_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetAllBreeds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetAllBreeds`()
BEGIN
	select * from tbl_breeds;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_mockup_image` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_mockup_image`(
IN p_canine_id INT,
IN p_owner_id INT,
IN p_image_path varchar(100)
)
BEGIN
	INSERT INTO tbl_images(canine_id, owner_id, date_time, image_path) 
		VALUES(p_canine_id, p_owner_id, NOW(), p_image_path);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_proximity_transition` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_proximity_transition`(
IN p_region_id INT, 
IN p_from_owner_id INT,
IN p_is_entering BIT
)
BEGIN
	DECLARE var_sub_type VARCHAR(10);
	DECLARE var_ntf_type_id TINYINT(3);
	DECLARE var_to_owner_id INT;
    DECLARE var_from_gender_id TINYINT(4);
	DECLARE var_count INT;
	DECLARE var_region_name VARCHAR(20);
	DECLARE var_region_dt DATETIME;
	DECLARE var_result VARCHAR(20);/*OK | NOT_UPTO_DATE | NO_RELATIONSHIP | REGION_NOT_EXIST | HAPPEND_LATELY*/
	DECLARE var_area_lat DECIMAL(17,15);
    DECLARE var_area_lng DECIMAL(18,15);

	/*get the region */
	SELECT count(*), owner_id, last_update, `name`, center_lat, center_lng into var_count, var_to_owner_id, var_region_dt, var_region_name, var_area_lat, var_area_lng
	FROM tbl_owners_areas 	
	WHERE id = p_region_id;
	
	SET var_result = 'REGION_EXISTS';
	
	/*get the relationship */
	SELECT count(*) INTO var_count
	FROM tbl_friendships 	
	WHERE (owner_idA = p_from_owner_id AND owner_idB = var_to_owner_id) OR  (owner_idA = var_to_owner_id AND owner_idB = p_from_owner_id);		
	
	SET var_result = 'YES_RELATIONSHIP';

	IF p_is_entering = 1 THEN
		SET var_ntf_type_id = 3;/*type_id: area_entering*/
		SET var_count = fo_validate_proximity_alert_dev(p_from_owner_id,p_region_id, var_ntf_type_id);
	ELSE				
		SET var_ntf_type_id = 4;/*type_id: area_exiting*/
	END IF;

			
	 
	SELECT var_result as `result`, var_count as `count`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `poc_log_geofence_transition` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `poc_log_geofence_transition`(
IN p_region_name VARCHAR(50),
IN p_region_lat DECIMAL(17,15),	
IN p_region_lng DECIMAL(18,15),
IN p_current_lat DECIMAL(17,15),	
IN p_current_lng DECIMAL(18,15),
IN p_current_accuracy DECIMAL(17,15),	
IN p_radius INT,
IN p_entering_or_exiting VARCHAR(20),
IN p_api_type VARCHAR(20)
)
BEGIN

	INSERT INTO `neighborhood`.`tbl_poc_geofence_transition`
	(`region_name`, `radius`,`entering_or_exiting`,`api_type`, `dt`, `center_lat`, `actual_lat`, `center_lng`, `actual_lng`, `actual_accuracy`)
	VALUES(p_region_name, p_radius, p_entering_or_exiting, p_api_type, NOW(), p_region_lat, p_current_lat, p_region_lng, p_current_lng, p_current_accuracy);
    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-11 19:48:46
